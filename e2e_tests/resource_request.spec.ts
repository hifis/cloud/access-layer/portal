import AxeBuilder from "@axe-core/playwright";
import { expect, test } from "@playwright/test";
import config from "../playwright.config";
import { TEST_PROFILE } from "./login";

const CHECKBOX_LEGAL_TEXT =
    "I have read and agree to the terms of use. I confirm to use the Helmholtz Cloud resources for neither private nor economic purposes as defined by EU state aid law (EU-Beihilfenrecht).";

test("resource request default screen shows welcome message", async ({ page }) => {
    // Go to landing page
    await page.goto("/");

    // Find the login button
    await page.locator("nav").getByText("Sign in").click();

    // Sign into HIDaH
    await page.getByTestId("generated-identity-claims").fill(JSON.stringify(TEST_PROFILE, undefined, 2));
    await page.getByTestId("generated-identity-sign-in-button").click();

    // Check for welcome message
    await page.waitForURL(`${config.use?.baseURL}/services/`);
    await page.locator("nav").getByText(TEST_PROFILE.name).click();
    await page.locator("nav").getByText("Resource Requests").click();
    await page.waitForURL(`${config.use?.baseURL}/resources/`);
    await expect(page.locator("main")).toContainText("Right now you do not have any active resource requests on Helmholtz Cloud.");
});

test("when user does not have permission to request resource it shows the default message", async ({ page }) => {
    // Go to landing page
    await page.goto("/");

    // Find the login button
    await page.locator("nav").getByText("Sign in").click();

    // Sign into HIDaH
    await page.getByTestId("generated-identity-claims").fill(JSON.stringify(TEST_PROFILE, undefined, 2));
    await page.getByTestId("generated-identity-sign-in-button").click();

    // Try to request resource
    await page.goto("/resources/request/1be91786-b7e7-4fa3-81d9-1b95dd03cd52/db290569-fc71-446a-a131-d73d113ed76a/");
    await expect(page.locator("main")).toContainText("The service provider has not granted you permission to request");
});

test("the basic request flow works", async ({ page }) => {
    // Update claims to a profile which can make resource requests
    const permittedProfile = {
        sub: "784bad18-ea17-44b7-a64b-146554b4036f",
        name: "Resource Requester",

        // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
        given_name: "Resource",

        // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
        family_name: "Requester",

        // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
        preferred_username: "resource.requester",
        email: "resource.requester@localhost",

        // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
        email_verified: "resource.requester@localhost",

        // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
        eduperson_entitlement: ["urn:geant:helmholtz.de:group:Helmholtz-member#hidah"],

        // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
        eduperson_assurance: ["https://refeds.org/assurance/ATP/ePA-1m"],

        // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
        eduperson_scoped_affiliation: ["staff@hidah"],
    };

    const requesterProfile = { ...TEST_PROFILE, ...permittedProfile };

    // Go to landing page
    await page.goto("/");

    // Find the login button
    await page.locator("nav").getByText("Sign in").click();

    // Sign into HIDaH
    await page.getByTestId("generated-identity-claims").fill(JSON.stringify(requesterProfile, undefined, 2));
    await page.getByTestId("generated-identity-sign-in-button").click();

    // Try to request resource
    await page.goto("/resources/request/1be91786-b7e7-4fa3-81d9-1b95dd03cd52/db290569-fc71-446a-a131-d73d113ed76a/");

    // Accessibility of group selection
    let accessibilityResults = await new AxeBuilder({ page }).analyze();

    expect(accessibilityResults.violations).toEqual([]);

    // Group selection is unavailable
    const form = page.getByTestId("request-form");
    await expect(form).not.toContainText("There are no groups you're allowed to request this resource for.");
    await expect(form.locator("hifis-button", { hasText: "Continue" })).toHaveAttribute("variant", "disabled");

    await form.getByText("A group").click();
    await expect(form).toContainText("There are no groups you're allowed to request this resource for.");
    await expect(form.locator("hifis-button", { hasText: "Continue" })).toHaveAttribute("variant", "disabled");

    // Continue with personal resource
    await form.getByText("Myself").click();
    await expect(form).not.toContainText("There are no groups you're allowed to request this resource for.");
    await expect(form.locator("hifis-button", { hasText: "Continue" })).toHaveAttribute("variant", "default");
    await form.locator("hifis-button", { hasText: "Continue" }).click();

    // Basic validation of request form
    await expect(form).toContainText("Please fill in your resource's details");

    // Accessibility of form
    accessibilityResults = await new AxeBuilder({ page }).analyze();

    expect(accessibilityResults.violations).toEqual([]);

    // Back-button restores values
    await page.getByLabel("Team Name *").fill("test-name");
    await form.getByText("Go Back").click();
    await expect(form).toContainText("Who do you want to request Mattermost Team for?");
    await form.locator("hifis-button", { hasText: "Continue" }).click();

    await expect(form).toContainText("Please fill in your resource's details");
    await expect(page.getByLabel("Team Name *")).toHaveValue("test-name");

    // Live validation works

    // Test resource parameter entry
    await expect(page.locator(".error")).toHaveCount(1);
    await expect(form).toContainText("Please fill all fields marked with '*'.");

    await page.getByLabel("Team Name *").fill("e");
    await page.getByLabel("Team Name *").blur();
    await expect(form).toContainText("String is too short (1 < 2).");
    await expect(page.locator(".error")).toHaveCount(2);
    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "disabled");

    await page.getByLabel("Team Name *").fill("test-team");
    await page.getByLabel("Team Name *").blur();
    await expect(form).not.toContainText("String is too short (1 < 2).");
    await expect(page.locator(".error")).toHaveCount(1);
    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "disabled");

    await page.getByLabel("Team Slug *").fill("!!!");
    await page.getByLabel("Team Slug *").blur();
    await expect(page.locator(".error")).toHaveCount(2);
    await expect(form).toContainText("Your input does not match the required format.");

    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "disabled");

    await page.getByLabel("Team Slug *").fill("test-slug");
    await page.getByLabel("Team Slug *").blur();
    await expect(page.locator(".error")).toHaveCount(1);
    await expect(form).not.toContainText("Your input does not match the required format.");
    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "disabled");

    // Test Helmholtz Cloud parameter entry

    // Generate random name so we do not get duplicates for each run
    const randomPartOfName = crypto.randomUUID().slice(24);
    const testResourceName = `Test team ${randomPartOfName}`;

    await page.getByLabel("Display name on Helmholtz Cloud *").fill("!!!");
    await page.getByLabel("Display name on Helmholtz Cloud *").blur();
    await expect(page.locator(".error")).toHaveCount(2);
    await expect(form).toContainText("The name you entered is not valid.");
    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "disabled");

    await page.getByLabel("Display name on Helmholtz Cloud *").fill(testResourceName);
    await page.getByLabel("Display name on Helmholtz Cloud *").blur();
    await expect(form).not.toContainText("The name you entered is not valid.");

    // All required fields are filled, but form stays invalid because checkbox is missing
    await expect(page.locator(".error")).toHaveCount(0);
    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "disabled");

    // Legal text is present and unchecked at first
    await expect(form).toContainText(CHECKBOX_LEGAL_TEXT);
    await expect(form.getByLabel(CHECKBOX_LEGAL_TEXT)).toBeVisible();
    await expect(form.getByLabel(CHECKBOX_LEGAL_TEXT)).not.toBeChecked();

    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "disabled");

    // Checking the legal text box makes the form valid
    await form.getByLabel(CHECKBOX_LEGAL_TEXT).check();
    await expect(form.getByLabel(CHECKBOX_LEGAL_TEXT)).toBeChecked();

    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "default");

    // Form becomes invalid when legal text checkbox is unchecked
    await form.getByLabel(CHECKBOX_LEGAL_TEXT).uncheck();
    await expect(form.getByLabel(CHECKBOX_LEGAL_TEXT)).not.toBeChecked();

    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "disabled");

    // Clicking continue leads to resource list with the expected resource listed
    await form.getByLabel(CHECKBOX_LEGAL_TEXT).check();
    await expect(form.getByLabel(CHECKBOX_LEGAL_TEXT)).toBeChecked();

    await expect(form.locator("hifis-button", { hasText: "Send Request" })).toHaveAttribute("variant", "default");
    await form.locator("hifis-button", { hasText: "Send Request" }).click();

    // Check that the request has been submitted
    await page.waitForURL(`${config.use?.baseURL}/resources/`);
    await expect(page.locator("#resource-page > section > section > section > table > tbody")).toContainText(testResourceName);
    await expect(page.locator("#resource-page > section > section > section > table > tbody > tr", { hasText: testResourceName })).toHaveCount(1);

    await expect(page.locator("#resource-page > section > section > section > table > tbody > tr", { hasText: testResourceName })).toContainText(
        "Request Submitted",
    );

    await expect(page.locator("#resource-page > section > section > section > table > tbody > tr", { hasText: testResourceName })).toContainText(
        "Request Denied",
    );

    // Try to request resource (successful case)
    await page.goto("/resources/request/1be91786-b7e7-4fa3-81d9-1b95dd03cd52/db290569-fc71-446a-a131-d73d113ed76a/");

    // Continue with personal resource
    await form.getByText("Myself").click();
    await form.locator("hifis-button", { hasText: "Continue" }).click();

    // Generate random name so we do not get duplicates for each run
    const randomPartOfNameSuccessful = crypto.randomUUID().slice(24);
    const testResourceNameSuccessful = `Test team ${randomPartOfNameSuccessful}`;

    await page.getByLabel("Team Name *").fill("test-name");
    await page.getByLabel("Team Slug *").fill("test-slug");

    // Check the invite-only checkbox to get a successful response from the HCA
    await form.getByLabel("Invite-only *").check();
    await page.getByLabel("Display name on Helmholtz Cloud *").fill(testResourceNameSuccessful);
    await form.getByLabel(CHECKBOX_LEGAL_TEXT).check();
    await form.locator("hifis-button", { hasText: "Send Request" }).click();

    // Check that the request has been submitted
    await page.waitForURL(`${config.use?.baseURL}/resources/`);

    await expect(page.locator("#resource-page > section > section > section > table > tbody")).toContainText(testResourceNameSuccessful);
    await expect(
        page.locator("#resource-page > section > section > section > table > tbody > tr", {
            hasText: testResourceNameSuccessful,
        }),
    ).toHaveCount(1);

    await expect(
        page.locator("#resource-page > section > section > section > table > tbody > tr", {
            hasText: testResourceNameSuccessful,
        }),
    ).toContainText("Request Submitted");

    await expect(
        page.locator("#resource-page > section > section > section > table > tbody > tr", {
            hasText: testResourceNameSuccessful,
        }),
    ).toContainText("Ready");
});
