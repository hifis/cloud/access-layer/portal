import { expect, test } from "@playwright/test";
import testConfig from "../playwright.config";

// Run this file in parallel as it should only access the ingress
test.describe.configure({ mode: "parallel" });

test("metrics page is inaccessible", async ({ page }) => {
    const response = await page.goto("/internal/metrics");
    expect(response?.request()?.redirectedFrom()?.url()).toEqual(`${testConfig.use?.baseURL}/internal/metrics`);
    expect(response?.request()?.redirectedFrom()?.redirectedTo()?.url()).toEqual(`${testConfig.use?.baseURL}/`);
    expect(response?.url()).toBe(`${testConfig.use?.baseURL}/`);
});

test("healthz page is inaccessible", async ({ page }) => {
    const response = await page.goto("/internal/healthz/");
    expect(response?.request()?.redirectedFrom()?.url()).toEqual(`${testConfig.use?.baseURL}/internal/healthz/`);
    expect(response?.request()?.redirectedFrom()?.redirectedTo()?.url()).toEqual(`${testConfig.use?.baseURL}/`);
    expect(response?.url()).toBe(`${testConfig.use?.baseURL}/`);
});

test("readyz page is inaccessible", async ({ page }) => {
    const response = await page.goto("/internal/readyz/");
    expect(response?.request()?.redirectedFrom()?.url()).toEqual(`${testConfig.use?.baseURL}/internal/readyz/`);
    expect(response?.request()?.redirectedFrom()?.redirectedTo()?.url()).toEqual(`${testConfig.use?.baseURL}/`);
    expect(response?.url()).toBe(`${testConfig.use?.baseURL}/`);
});
