"""
add_provider_url

This migration works with existing data present, URLs are set to a blank value.
To avoid pydantic errors at runtime, a plony import must be run immediately afterwards so the table contains valid data.
"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "738f38a6dd8a"
down_revision = "443b140e7762"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "catalog_provider",
        sa.Column(
            "url",
            sa.Text,
            unique=False,
            index=False,
            nullable=True,
            comment="Website URL",
        ),
    )
    op.execute("UPDATE catalog_provider SET url = '';")
    op.alter_column("catalog_provider", "url", nullable=False)


def downgrade() -> None:
    op.drop_column("catalog_provider", "url")
