import core.app

from .base import *  # noqa: F403 | Importing all base settings is the intended effect here.

ALLOWED_HOSTS = core.app.require_envvar("ALLOWED_HOSTS").split(",")

# Keep Django-initiated connections around for a while (core DB connections are managed by the core pool still!)
CONN_MAX_AGE = 60 * 10

# Run regular health checks on the database connections in case the server goes down
CONN_HEALTH_CHECKS = True

# Explicitly set Debug to False
DEBUG = False

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "structured": {
            "format": '{"level": "${levelname}", "asctime": "${asctime}", "module": "${module}", "funcName": "${funcName}", "message": "${message}"}',
            "style": "$",
        },
    },
    "handlers": {
        "console_stream": {
            "level": core.app.require_envvar("LOG_LEVEL"),
            "class": "logging.handlers.RotatingFileHandler",
            # 1 MB
            "maxBytes": 1024 * 1024,
            "backupCount": 1,
            "filename": "/var/log/hifis_cloud_portal/django.log",
            "formatter": "structured",
        },
    },
    "loggers": {
        "": {
            "handlers": ["console_stream"],
            "level": core.app.require_envvar("LOG_LEVEL"),
            "propagate": False,
        },
        "django": {
            "handlers": ["console_stream"],
            "level": core.app.require_envvar("LOG_LEVEL"),
            "propagate": False,
        },
    },
}
