import asyncio
from collections.abc import Callable, Coroutine
from typing import Any

from asgiref.sync import async_to_sync
from django.core.cache import caches
from django.http import HttpRequest, HttpResponse
from django.utils.decorators import sync_and_async_middleware

import core.maintenance

NO_MESSAGE_CACHE_PLACEHOLDER = "NO MAINTENANCE MESSAGE"


# Decorator from Django which does not have type annotations
@sync_and_async_middleware  # type: ignore[misc]
def load_maintenance_message(
    get_response: Callable[[HttpRequest], HttpResponse],
) -> (
    Callable[[Coroutine[Any, Any, HttpResponse]], HttpResponse]
    | Callable[[HttpRequest], HttpResponse]
):
    """
    This middleware sets the maintenance message on the request if there's one.
    """

    async def put_maintenance_message(request: HttpRequest) -> HttpRequest:
        # Retrieve message from in-memory cache as getting it from DB can be rather slow
        cache = caches["maintenance-message"]

        cached_message = await cache.aget("message")
        if cached_message is None:
            # Empty cache, check in DB
            message = await core.maintenance.get_current_maintenance_message()

            if message is None:
                # There was no message, store that in
                await cache.aset("message", NO_MESSAGE_CACHE_PLACEHOLDER, 60)
            else:
                # Found message, go cache it
                # This results in maintenance messages stay visible for up to 60 seconds after they expired
                await cache.aset("message", message.message, 60)
                request.maintenance_message = message.message
        elif cached_message == NO_MESSAGE_CACHE_PLACEHOLDER:
            # Cached result: no message
            request.maintenance_message = None
        else:
            # Cached result: message
            request.maintenance_message = cached_message

        return request

    if asyncio.iscoroutinefunction(get_response):

        async def run_middleware(request: HttpRequest) -> HttpResponse:
            await put_maintenance_message(request)

            response: HttpResponse = await get_response(request)

            return response

    else:
        # Mypy does not like conditional function definitions
        def run_middleware(request: HttpRequest) -> HttpResponse:  # type: ignore[misc]
            # This is unfortunately untyped but we're checking it above in the async implementation
            sync_put_maintenance_message = async_to_sync(put_maintenance_message)
            sync_put_maintenance_message(request)

            response: HttpResponse = get_response(request)

            return response

    return run_middleware
