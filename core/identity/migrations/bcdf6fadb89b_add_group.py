"""
Add group
"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "bcdf6fadb89b"
down_revision = None
branch_labels = ("identity",)
depends_on = None


def upgrade() -> None:
    op.create_table(
        "identity_group",
        sa.Column(
            "urn",
            sa.Text,
            primary_key=True,
            nullable=False,
            comment="The group's URN",
        ),
    )


def downgrade() -> None:
    op.drop_table("identity_group")
