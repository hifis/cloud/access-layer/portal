# Comparing to magic values is part of testing examples
# ruff: noqa: PLR2004

from http import HTTPStatus
from typing import Any
from uuid import uuid4

import django.contrib.messages
import django.http.response
import pytest
from django.contrib.auth.models import AnonymousUser
from django.core.handlers.asgi import ASGIRequest

import core.catalog.testing
from web.resources import views

TEST_CLAIMS: dict[str, Any] = {
    "sub": str(uuid4()),
    "email": "test.user@localhost",
    "name": "Test User",
    "eduperson_entitlement": [
        "urn:geant:localhost:group:pytest#login.localhost",
        "urn:geant:localhost:group:python#login.localhost",
    ],
    "eduperson_assurance": [
        "https://localhost/verified",
    ],
}


@pytest.mark.usefixtures("_core_database")
@pytest.mark.django_db
class TestRequestForm:
    # Test base case
    @pytest.mark.asyncio
    async def test_base_case(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        provider = core.catalog.testing.make_provider()
        await core.catalog.testing.create_or_update_provider(provider=provider)

        service = core.catalog.testing.make_service(provider_id=provider.id)
        await core.catalog.testing.create_or_update_service(service=service)

        resource_type = core.catalog.testing.make_resource_type()
        await core.catalog.testing.create_or_update_resource_type(
            resource_type=resource_type
        )

        quota = core.catalog.testing.make_quota(
            service_id=service.id, resource_type_id=resource_type.id
        )
        await core.catalog.testing.create_or_update_quota(quota=quota)

        policy = core.catalog.testing.make_policy(quota_id=quota.id)
        policy.actor_requirements = core.catalog.PolicyActorRequirements(
            edu_person_entitlement=request_with_user.aai_profile.eduperson_entitlements,
            edu_person_assurance=request_with_user.aai_profile.eduperson_assurances,
            edu_person_scoped_affiliation=request_with_user.aai_profile.eduperson_scoped_affiliation,
        )
        await core.catalog.testing.create_or_update_policy(policy=policy)

        response = await views.request_form(
            request_with_user, resource_type_id=resource_type.id, service_id=service.id
        )

        response_text = str(response.content)

        assert response.status_code == HTTPStatus.OK
        assert str(service.title) in response_text
        assert str(resource_type.id) in response_text
        assert str(quota.id) in response_text
        assert str(policy.id) in response_text
        assert '<div id="request-form"' in response_text

    # Test user must be authenticated to access the page
    @pytest.mark.asyncio
    async def test_user_must_be_authenticated(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        anonymous_user = AnonymousUser()
        request_with_user.user = anonymous_user

        async def auser() -> AnonymousUser:
            return anonymous_user

        request_with_user.auser = auser

        response = await views.request_form(
            request_with_user, resource_type_id=uuid4(), service_id=uuid4()
        )

        assert response.status_code == HTTPStatus.FOUND
        assert response.url == "/oidc/authenticate/?next=/"

    # Test Service must exist
    @pytest.mark.asyncio
    async def test_service_must_exist(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        with pytest.raises(django.http.response.Http404, match="Service not found."):
            await views.request_form(
                request_with_user, resource_type_id=uuid4(), service_id=uuid4()
            )

    # Test ResourceType must exist
    @pytest.mark.asyncio
    async def test_resource_type_must_exist(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        provider = core.catalog.testing.make_provider()
        await core.catalog.testing.create_or_update_provider(provider=provider)

        service = core.catalog.testing.make_service(provider_id=provider.id)
        await core.catalog.testing.create_or_update_service(service=service)

        with pytest.raises(
            django.http.response.Http404, match="Resource type not found."
        ):
            await views.request_form(
                request_with_user, resource_type_id=uuid4(), service_id=service.id
            )

    # Test fallback in case there is no quota available
    @pytest.mark.asyncio
    async def test_no_access_page_if_no_policy_applies(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        provider = core.catalog.testing.make_provider()
        await core.catalog.testing.create_or_update_provider(provider=provider)

        service = core.catalog.testing.make_service(provider_id=provider.id)
        await core.catalog.testing.create_or_update_service(service=service)

        resource_type = core.catalog.testing.make_resource_type()
        await core.catalog.testing.create_or_update_resource_type(
            resource_type=resource_type
        )

        quota = core.catalog.testing.make_quota(
            service_id=service.id, resource_type_id=resource_type.id
        )
        await core.catalog.testing.create_or_update_quota(quota=quota)

        policy = core.catalog.testing.make_policy(quota_id=quota.id)
        policy.actor_requirements = core.catalog.PolicyActorRequirements(
            edu_person_entitlement=[
                request_with_user.aai_profile.eduperson_entitlements[0] + "changed"
            ],
            edu_person_assurance=[],
            edu_person_scoped_affiliation=[],
        )
        await core.catalog.testing.create_or_update_policy(policy=policy)

        response = await views.request_form(
            request_with_user, resource_type_id=resource_type.id, service_id=service.id
        )

        assert response.status_code == HTTPStatus.OK
        assert "<h1>Request Access</h1>" in str(response.content)
        assert "The service provider has not granted you permission" in str(
            response.content
        )
