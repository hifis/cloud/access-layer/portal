import AxeBuilder from "@axe-core/playwright";
import { expect, test } from "@playwright/test";
import { loggedInTest } from "./login.ts";

// NOTE: This may break in local development due to a very hard to track down bug. CI should be fine.

// Run this file in parallel as loading the page takes a while
test.describe.configure({ mode: "parallel" });

test("landing page should pass basic accessiblity checks", async ({ page }) => {
    await page.goto("/");
    const results = await new AxeBuilder({ page }).analyze();
    expect(results.violations).toEqual([]);
});

test("support page should pass basic accessiblity checks", async ({ page }) => {
    await page.goto("/helpdesk/");
    const results = await new AxeBuilder({ page }).analyze();
    expect(results.violations).toEqual([]);
});

test("privacy policy page should pass basic accessiblity checks", async ({ page }) => {
    await page.goto("/privacy");
    const results = await new AxeBuilder({ page }).analyze();
    expect(results.violations).toEqual([]);
});

test("terms of use page should pass basic accessiblity checks", async ({ page }) => {
    await page.goto("/terms-of-use");
    const results = await new AxeBuilder({ page }).analyze();
    expect(results.violations).toEqual([]);
});

test("service page should pass basic accessiblity checks", async ({ page }) => {
    await page.goto("/services/");
    const results = await new AxeBuilder({ page })
        .disableRules([
            // This rule does not work well for this page (there is no useful headline)
            "page-has-heading-one",
        ])
        .analyze();

    expect(results.violations).toEqual([]);
});

loggedInTest("deprovision confirm page should pass basic accessiblity checks", async ({ page }) => {
    await page.goto("/helpdesk/deprovisioning/e7e66b7a-01bb-4405-b498-70f81591fe38/confirm");
    const results = await new AxeBuilder({ page }).analyze();

    expect(results.violations).toEqual([]);
});

loggedInTest("resource requests page should pass basic accessiblity checks", async ({ page }) => {
    await page.goto("/resources/");
    const results = await new AxeBuilder({ page }).analyze();

    expect(results.violations).toEqual([]);
});
