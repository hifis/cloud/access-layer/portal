"""
create maintenance message
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = "2f6711ae4e5a"
down_revision = None
branch_labels = ("maintenance",)
depends_on = None


def upgrade() -> None:
    op.create_table(
        "maintenance_message",
        sa.Column("id", UUID, primary_key=True, nullable=False, comment="ID"),
        sa.Column(
            "show_until",
            sa.DateTime,
            unique=False,
            index=False,
            nullable=False,
            comment="Until when the maintenance message is shown",
        ),
        sa.Column(
            "message",
            sa.Text,
            unique=False,
            index=False,
            nullable=False,
            comment="Maintenance message text",
        ),
    )


def downgrade() -> None:
    op.drop_table("maintenance_message")
