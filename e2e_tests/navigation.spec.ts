import { expect, test } from "@playwright/test";

test("navigation links", async ({ page }) => {
    const response = await page.goto("/");
    expect(response?.status()).toBe(200);

    const nav = await page.locator("nav");

    const logo = await page.getByAltText("Logo Helmholtz Cloud").locator("..");
    await expect(logo).toHaveAttribute("href", "/");

    await expect(nav.getByText("Services")).toHaveAttribute("href", "/services/");
    await expect(nav.getByText("Support")).toHaveAttribute("href", "/helpdesk/");
    await expect(nav.getByText("Documentation")).toHaveAttribute("href", "https://hifis.net/doc/");
    // biome-ignore lint/performance/useTopLevelRegex: Run only once per test run
    await expect(nav.getByText("Sign in")).toHaveAttribute("href", /.*\/oidc\/authenticate\//);
});

test("hamburger menu", async ({ page }) => {
    const response = await page.goto("/");
    expect(response?.status()).toBe(200);

    const menuButton = page.locator("#hamburger-menu");
    const menuItems = page.locator("#menuitems");

    // By default the button is not visible but the menu is
    await expect(menuButton).not.toBeVisible();
    await expect(menuItems).toBeVisible();

    // Switch to phone
    await page.setViewportSize({ width: 320, height: 600 });

    // Check the button is visible but the menu is not
    await expect(menuButton).toBeVisible();
    await expect(menuItems).not.toBeVisible();

    // Check it's the hamburger icon
    // biome-ignore lint/performance/useTopLevelRegex: Run only once per test run
    await expect(menuButton.locator('[aria-label="Open Navigation Menu"]')).toHaveClass(/fa-bars/);
    // biome-ignore lint/performance/useTopLevelRegex: Run only once per test run
    await expect(menuButton.locator('[aria-label="Open Navigation Menu"]')).not.toHaveClass(/fa-xmark/);

    // Open the menu
    await menuButton.click();

    // Check it's the x mark icon now
    // biome-ignore lint/performance/useTopLevelRegex: Run only once per test run
    await expect(menuButton.locator('[aria-label="Close Navigation Menu"]')).not.toHaveClass(/fa-bars/);
    // biome-ignore lint/performance/useTopLevelRegex: Run only once per test run
    await expect(menuButton.locator('[aria-label="Close Navigation Menu"]')).toHaveClass(/fa-xmark/);

    // Check menu contents
    await expect(menuItems).toBeVisible();
    await expect(menuItems.getByText("Services")).toHaveAttribute("href", "/services/");
    await expect(menuItems.getByText("Support")).toHaveAttribute("href", "/helpdesk/");
    // biome-ignore lint/performance/useTopLevelRegex: Run only once per test run
    await expect(menuItems.getByText("Sign in")).toHaveAttribute("href", /.*\/oidc\/authenticate\//);

    // Close the menu
    await menuButton.click();
    await expect(menuItems).not.toBeVisible();

    // Check it's the hamburger icon again
    // biome-ignore lint/performance/useTopLevelRegex: Run only once per test run
    await expect(menuButton.locator('[aria-label="Open Navigation Menu"]')).toHaveClass(/fa-bars/);
    // biome-ignore lint/performance/useTopLevelRegex: Run only once per test run
    await expect(menuButton.locator('[aria-label="Open Navigation Menu"]')).not.toHaveClass(/fa-xmark/);

    // Switch back to desktop
    await page.setViewportSize({ width: 1920, height: 1080 });

    await expect(menuButton).not.toBeVisible();
    await expect(menuItems).toBeVisible();
});
