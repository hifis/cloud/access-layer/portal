import datetime
from typing import Any
from uuid import UUID, uuid4

import pytest
from cryptography.fernet import Fernet, InvalidToken
from django.contrib.sessions.backends.db import SessionStore
from django.utils import timezone
from pydantic import ValidationError

from web.base.models import AAIProfile, User

TEST_CLAIMS: dict[str, Any] = {
    "sub": str(uuid4()),
    "email": "test.user@localhost",
    "name": "Test User",
    "eduperson_entitlement": [
        "urn:geant:localhost:group:pytest#login.localhost",
        "urn:geant:localhost:group:python#login.localhost",
    ],
    "eduperson_assurance": [
        "https://localhost/verified",
    ],
    "eduperson_scoped_affiliation": ["staff"],
}


class TestUser:
    def test_user_to_string(self: "TestUser") -> None:
        user_id = uuid4()
        user = User(
            id=user_id,
            fallback_expire_at=timezone.now() + datetime.timedelta(hours=24),
        )

        assert str(user_id) == user.__str__()


class TestAAIProfile:
    def test_to_actor_identity(self: "TestAAIProfile") -> None:
        input_aai_profile = AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        conversion_result = input_aai_profile.to_actor_identity()

        assert conversion_result.model_dump() == {
            "vo_person_id": input_aai_profile.vo_person_id,
            "edu_person_entitlement": input_aai_profile.eduperson_entitlements,
            "edu_person_assurance": input_aai_profile.eduperson_assurances,
            "edu_person_scoped_affiliation": input_aai_profile.eduperson_scoped_affiliation,
        }


class TestUserDecryptAAIProfile:
    # it allows to decrypt a user end to end
    @pytest.mark.django_db
    def test_encrypt_and_decrypt(self: "TestUserDecryptAAIProfile") -> None:
        input_aai_profile = AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        session = SessionStore()
        session.create()

        user = User(
            id=uuid4(),
            fallback_expire_at=timezone.now() + datetime.timedelta(hours=24),
            session_id=session.session_key,
        )
        secret = user.set_encrypted_aai_profile(input_aai_profile)
        user.save()

        reloaded_user = User.objects.get(id=user.id)
        decrypted_profile = reloaded_user.decrypt_aai_profile(secret)

        assert decrypted_profile == input_aai_profile

    # it does not allow decrypting a user whose session ID is None
    @pytest.mark.django_db
    def test_raises_when_decrypting_uninitialized_user(
        self: "TestUserDecryptAAIProfile",
    ) -> None:
        input_aai_profile = AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        user = User(
            id=uuid4(),
            fallback_expire_at=timezone.now() + datetime.timedelta(hours=24),
            session_id=None,
        )
        secret = user.set_encrypted_aai_profile(input_aai_profile)
        user.save()

        reloaded_user = User.objects.get(id=user.id)

        with pytest.raises(
            RuntimeError,
            match="Can't decrypt profile of an unitialized user who is not linked to a session!",
        ):
            reloaded_user.decrypt_aai_profile(secret)

    # raises if secret is malformatted
    @pytest.mark.django_db
    def test_raises_with_broken_secret(self: "TestUserDecryptAAIProfile") -> None:
        input_aai_profile = AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        session = SessionStore()
        session.create()

        user = User(
            id=uuid4(),
            fallback_expire_at=timezone.now() + datetime.timedelta(hours=24),
            session_id=session.session_key,
        )
        user.set_encrypted_aai_profile(input_aai_profile)
        user.save()

        reloaded_user = User.objects.get(id=user.id)

        with pytest.raises(
            ValueError,
            match="Fernet key must be 32 url-safe base64-encoded bytes.",
        ):
            reloaded_user.decrypt_aai_profile("broken")

    # raises if portal token is valid but the password is wrong
    @pytest.mark.django_db
    def test_raises_with_wrong_password(self: "TestUserDecryptAAIProfile") -> None:
        input_aai_profile = AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        session = SessionStore()
        session.create()

        user = User(
            id=uuid4(),
            fallback_expire_at=timezone.now() + datetime.timedelta(hours=24),
            session_id=session.session_key,
        )
        user.set_encrypted_aai_profile(input_aai_profile)
        user.save()

        reloaded_user = User.objects.get(id=user.id)

        with pytest.raises(InvalidToken):
            reloaded_user.decrypt_aai_profile(Fernet.generate_key().decode("utf-8"))

    # raises if the data structure in the profile does not match the struct (struct updated in code but data stored in DB is old)
    @pytest.mark.django_db
    def test_raises_with_broken_data(self: "TestUserDecryptAAIProfile") -> None:
        session = SessionStore()
        session.create()

        secret = Fernet.generate_key()
        broken_profile = Fernet(secret).encrypt(b'{"test": true}').decode("utf-8")

        user = User(
            id=uuid4(),
            fallback_expire_at=timezone.now() + datetime.timedelta(hours=24),
            session_id=session.session_key,
            encrypted_aai_profile=broken_profile,
        )
        user.save()

        reloaded_user = User.objects.get(id=user.id)

        with pytest.raises(ValidationError, match="6 validation errors for AAIProfile"):
            reloaded_user.decrypt_aai_profile(secret.decode("utf-8"))
