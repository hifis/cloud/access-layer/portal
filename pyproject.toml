[project]
name = "portal"
version = "0.1.0"
description = "Helmholtz Cloud Portal"
maintainers = [
  {name = "Thomas Beermann", email = "thomas.beermann@desy.de"},
  {name = "Sebastian Wagner", email = "s.wagner@desy.de"}
]
readme = "README.md"
requires-python = ">=3.13,<4.0"

dependencies = [
    # Main web framework
    "Django >=5.0",
    # ASGI server for production
    "uvicorn[standard] >=0.20.0",
    # Recommended ASGI server wrapper for uvicorn in produciton
    "gunicorn >=20.1.0",
    # OIDC client/RP library for Helmholtz ID login
    "mozilla-django-oidc >=3.0.0",
    # Use binary distribution for now to save complexity in container image (no need for build stages/compilers)
    "psycopg[binary, pool] >=3.1.18",
    # Database URL parsing for Django
    "dj-database-url >=1.2.0",
    # Type checking interfaces
    "pydantic >=1.10.1",
    # portalctl CLI library
    "typer[all] >=0.7.0",
    # HTTP client for external services (e.g. plony)
    "httpx >=0.23.1",
    # Database migrations
    "alembic >=1.9.0",
    # Markdown rendering for service profiles
    "markdown-it-py >=2.1.0",
    # Prometheus instrumentation for django endpoints
    "django-prometheus >=2.2.0",
    # Exception tracking
    "sentry-sdk[django, httpx] >=1.43.0",
    # JSON schema validation for resources
    "jsonschema >=4.20.0",
    # RabbitMQ client for HCA
    "aio-pika >=9.4.0",
    # Settings management using Pydantic
    "pydantic-settings >=2.1.0",
    # Helmholtz Cloud Agent messages
    "helmholtz-cloud-agent >=0.7.0"
]

[dependency-groups]
dev = [
    # Fast all-in-one Python linting and formatting
    "ruff >=0.2.1",
    # Security checks
    "bandit[toml] >=1.7.4",
    # Type checking
    "mypy >=0.991",
    # Testing library
    "pytest >=7.1.3",
    # Async support for tests
    "pytest-asyncio >=0.23.6",
    # Django support for tests
    "pytest-django >=4.5.2",
    # Live reload
    "django-browser-reload >=1.6.0",
    # Code coverage reports for GitLab integration
    "pytest-cov >=4.0.0",
    # HTTP mocks for availability checking
    "pytest-httpx >=0.21.2"
]

[tool.uv.sources]
helmholtz-cloud-agent = { index = "hca-codebase" }

[[tool.uv.index]]
name = "hca-codebase"
url = "https://codebase.helmholtz.cloud/api/v4/projects/2482/packages/pypi/simple"
explicit = true

[tool.pytest.ini_options]
asyncio_mode = "strict"
asyncio_default_fixture_loop_scope = "function"
addopts = "--create-db --ds=web.portal.settings.test"
markers = [
    "load_extra_schemas: marks tests which migrate additional context database schemas in addition to their own schema",
    "False",
    "user_claims: marks tests which use the request_with_user fixture with custom claims",
    "None",
]

[tool.ruff.format]
preview = true

[tool.ruff.lint]
# https://docs.astral.sh/ruff/rules/
select = [
    "F",
    "E",
    "W",
    "I",
    "N",
    "UP",
    "ANN",
    "ASYNC",
    # Enable when all rules have been implemented
    # https://github.com/astral-sh/ruff/issues/1646
    # "S",
    "BLE",
    "FBT",
    "B",
    "A",
    "C4",
    "DTZ",
    "T10",
    "DJ",
    "EM",
    "EXE",
    "ISC",
    "ICN",
    "G",
    "INP",
    "PIE",
    "T20",
    "PYI",
    "PT",
    "Q",
    "RSE",
    "RET",
    "SLF",
    "SLOT",
    "SIM",
    "TID",
    "TCH",
    "INT",
    "ARG",
    "PTH",
    "TD",
    "FIX",
    "ERA",
    "PD",
    "PGH",
    "PL",
    "TRY",
    "FLY",
    "NPY",
    "PERF",
    "FURB",
    "LOG",
    "RUF",
]
ignore = ["E501", "RET504", "RET505", "PLC1901", "ISC001"]

[tool.ruff.lint.isort]
known-first-party = ["web", "core"]

[tool.ruff.lint.extend-per-file-ignores]
"*/test_*.py" = ["S101"]

[tool.bandit]
exclude_dirs = ["/.venv"]

# https://bandit.readthedocs.io/en/latest/plugins/b113_request_without_timeout.html
# Well-intentioned, however: false positives and httpx comes with a default timeout of 5 seconds unlike e.g. Go's client
skips = ["B113"]

[tool.bandit.assert_used]
skips = ["*/test_*.py"]
