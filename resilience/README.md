# Resilience setup

This folder provides a simple podman-compose setup that can be used to deploy the Helmholtz Cloud Portal on any VM in case of emergency. It consists of the Cloud Portal django app, an nginx to serve the static files, a dedicated PostgreSQL instance and a Caddy reverse proxy.

## Database

A backup can be used to restore the database. Store the SQL file that has been created with `pg_dumpall` in the `postgres` folder before the database container is started.

## Environment variables

Before the containers can start credentials have to be set in the environment variables. Copy the `portal_env.example` to `portal_env` and fill in passwords. 

## Build the portal container

Before the first run build the portal container:

```bash
podman-compose -f resilience/podman-compose.yaml build
```

## Starting the containers

(Podman only)
The `service_completed_successfully` condition [is not supported by podman-compose](https://github.com/containers/podman-compose/issues/575) so the `collectstatic` and `postgres` containers has to be run manually before the rest can be started:

```bash
podman-compose -f resilience/podman-compose.yaml up collectstatic postgres
```

Start all the containers:

```bash
podman-compose -f resilience/podman-compose.yaml up -d
```

## Cronjobs

The containers for the periodic jobs `availability-checker`, `clearusers`, `group-importer` and `plony-importer` only run once when the podman-compose setup is first started. The containers can be restarted anytime:

```bash
podman start resilience_availability-checker_1
```

This can be added to the crontab on the host system.
