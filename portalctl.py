import asyncio
import datetime

import httpx
import typer
from sentry_sdk import capture_exception

import core.app
import core.catalog
import core.identity
import core.maintenance
import core.monitoring
from core.cloud_agent.cloud_agent import (
    send_resource_requests_to_queue,
    start_cloud_agent_receivers,
)
from core.kpi import Client as KPIClient
from core.monitoring import AvailabilityClient
from core.plony import Client as PlonyClient
from testing.mm_hca import main as mm_hca
from testing.mock_hca import main as mock_hca

core.app.boot(core.app.require_envvar("PORTAL_CORE_DATABASE_URL"))

# Hide locals in logs for security
app = typer.Typer(pretty_exceptions_show_locals=False)

include_in_review_services_option = typer.Option(
    False,  # noqa: FBT003 external library
    help="Include services with 'service_card_review' or 'integration_completed' status in the import. This is used for previewing service listings outside the production environment.",
)


@app.command()
def import_from_plony(
    include_in_review_services: bool = include_in_review_services_option,  # noqa: FBT001 external library
) -> None:
    """Run the plony import."""

    base_url = core.app.require_envvar("PLONY_BASE_URL")
    user = core.app.require_envvar("PLONY_USER")
    password = core.app.require_envvar("PLONY_PASSWORD")

    plony_client = PlonyClient(user, password, base_url)

    loop = asyncio.new_event_loop()
    loop.run_until_complete(
        __run_plony_import(plony_client, include_in_review_services)
    )


@app.command()
def import_kpis() -> None:
    """Import KPIs from Helmholtz Codebase"""

    loop = asyncio.new_event_loop()
    loop.run_until_complete(__run_import_kpis())


@app.command()
def import_groups() -> None:
    """Import groups from hifis.net/doc"""

    # Download group list
    group_response = httpx.get(
        "https://hifis.net/doc/helmholtz-aai/vo-data/vo_groups.json",
        headers={"user-agent": "helmholtz.cloud group import"},
    )
    group_response.raise_for_status()

    # Extract and de-duplicate URNs
    urns = list({group["group_claim"] for group in group_response.json()})

    loop = asyncio.new_event_loop()
    loop.run_until_complete(core.identity.import_group_urns(urns))


async def __run_import_kpis() -> None:
    # Async function for running the import.
    # Needed since client needs to be closed.
    kpi_client = KPIClient()

    kpi_map = await kpi_client.get_kpis()

    await kpi_client.aclose()

    await core.catalog.import_kpis(kpi_map=kpi_map)


async def __run_plony_import(
    plony_client: PlonyClient,
    include_in_review_services: bool,  # noqa: FBT001 external library
) -> None:
    # Async function for running the import.
    # Needed since client needs to be closed.
    await core.catalog.run_plony_import(
        plony_client=plony_client, include_in_review_services=include_in_review_services
    )
    await plony_client.aclose()

    # Close the connection pool cleanly
    await (await core.db.DB.get_pool()).close(timeout=0)


@app.command()
def check_availability(retries: int = 3, delay: int = 10, timeout: int = 5) -> None:
    """Run the availability checker."""
    availability_client = AvailabilityClient(
        retries=retries, delay=delay, timeout=timeout
    )
    loop = asyncio.new_event_loop()
    loop.run_until_complete(__run_availability_checker(availability_client))


async def __run_availability_checker(availability_client: AvailabilityClient) -> None:
    await core.monitoring.run_availability_checker(availability_client)

    # Close the connection pool cleanly
    await (await core.db.DB.get_pool()).close(timeout=0)


async def _set_maintenance_message(message: str, show_until: datetime.date) -> None:
    await core.maintenance.set_maintenance_message(message, show_until)

    # Close the connection pool cleanly
    await (await core.db.DB.get_pool()).close(timeout=0)


@app.command()
def set_maintenance_message(show_until: datetime.datetime, message: str) -> None:
    """Set the maintenance message to show on the portal."""
    loop = asyncio.new_event_loop()
    loop.run_until_complete(_set_maintenance_message(message, show_until))


@app.command()
def send_cloud_agent_messages() -> None:
    """Check form submitted resources and send to cloud agent."""
    loop = asyncio.new_event_loop()
    loop.run_until_complete(send_resource_requests_to_queue())


@app.command()
def receive_cloud_agent_messages() -> None:
    """Start receivers to get message sent from cloud agent."""
    loop = asyncio.new_event_loop()
    loop.run_until_complete(start_cloud_agent_receivers())


@app.command()
def start_mock_hca() -> None:
    """start the mock cloud agent"""
    mock_hca.start_mock_hca()


@app.command()
def start_mm_hca() -> None:
    """start the mattermost use case testing cloud agent"""
    mm_hca.start_mm_hca()


if __name__ == "__main__":
    try:
        app()
    except SystemExit as e:
        # If the exit code is 0 it should not be reported
        if e.code != 0:
            # Capture and re-raise
            capture_exception(e)
            raise
    except BaseException as e:
        # Capture and re-raise
        capture_exception(e)
        raise
