"""
add_timezone_support
"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "8371dd766839"
down_revision = "d8c6f4d4fc96"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column(
        "monitoring_availability",
        "date",
        existing_type=sa.DateTime(timezone=False),
        type_=sa.DateTime(timezone=True),
    )
    op.alter_column(
        "monitoring_availability_history",
        "date",
        existing_type=sa.DateTime(timezone=False),
        type_=sa.DateTime(timezone=True),
    )


def downgrade() -> None:
    op.alter_column(
        "monitoring_availability",
        "date",
        existing_type=sa.DateTime(timezone=True),
        type_=sa.DateTime(timezone=False),
    )
    op.alter_column(
        "monitoring_availability_history",
        "date",
        existing_type=sa.DateTime(timezone=True),
        type_=sa.DateTime(timezone=False),
    )
