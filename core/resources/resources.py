import datetime
import json
from enum import Enum
from typing import Any
from uuid import uuid4

import jsonschema
import psycopg.rows
import pydantic
from pydantic import BaseModel, model_validator

import core.catalog
import core.db
import core.identity


class ResourceRequestError(Exception):
    pass


class QuotaExceededError(Exception):
    pass


class ResourceStateEnum(str, Enum):
    form_submitted = "form_submitted"
    request_submitted = "request_submitted"
    provisioned = "provisioned"
    deprovisioning_form_submitted = "deprovisioning_form_submitted"
    deprovisioning_request_submitted = "deprovisioning_request_submitted"
    deprovisioned = "deprovisioned"
    request_denied = "request_denied"


class Resource(BaseModel):
    id: pydantic.UUID4
    catalog_quota_id: pydantic.UUID4
    catalog_service_id: pydantic.UUID4
    identity_user_id_requested_by: pydantic.UUID4
    identity_group_urn_target: str | None
    identity_user_id_target: pydantic.UUID4 | None
    external_id: str | None
    name: str
    state: ResourceStateEnum
    state_text: str | None
    specification: dict[str, Any]
    created_at: datetime.datetime
    updated_at: datetime.datetime

    @model_validator(mode="after")
    def must_be_either_personal_or_group_resource(self: "Resource") -> "Resource":
        if (
            (self.identity_group_urn_target is None)
            and (self.identity_user_id_target is None)
        ) or (
            (self.identity_group_urn_target is not None)
            and (self.identity_user_id_target is not None)
        ):
            msg = "Either identity_group_urn_target or identity_user_id_target must be set"
            raise ValueError(msg)

        return self


async def get_resources_by_state(state: ResourceStateEnum) -> list[Resource]:
    """Return all resources in a given state."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        resource_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(Resource)
        ).execute(
            "SELECT * FROM resources_resources where state = %(state)s",
            {"state": state},
        )

        return await resource_rows.fetchall()


async def get_user_resources(
    *, actor_identity: core.identity.ActorIdentity, authoritative_directory: str
) -> list[Resource]:
    """Return all resources a user can access (personal resources and group resources)."""

    user_groups = actor_identity.get_group_memberships(
        authoritative_directory=authoritative_directory
    )

    async with core.db.DB.get_connection_with_transaction() as connection:
        # Just ints are inserted: false positive.
        resource_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(Resource)
        ).execute(
            "SELECT * FROM resources_resources where (identity_group_urn_target = ANY(%(user_groups)s) OR identity_user_id_target = %(user_id)s)",
            {
                "user_groups": user_groups,
                "user_id": actor_identity.vo_person_id,
            },
        )

        return await resource_rows.fetchall()


async def get_resource_by_id(resource_id: pydantic.UUID4) -> Resource | None:
    """Return a resource for a given id"""
    async with core.db.DB.get_connection_with_transaction() as connection:
        resource_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(Resource)
        ).execute(
            "SELECT * FROM resources_resources where id = %(resource_id)s",
            {"resource_id": resource_id},
        )

        return await resource_rows.fetchone()


async def request_resource(  # noqa: PLR0912, PLR0915 splitting this into multiple functions would not make this less complex
    *,
    actor_identity: core.identity.ActorIdentity,
    catalog_policy_id: pydantic.UUID4,
    target_entity: str,
    name: str,
    specification: dict[str, Any],
) -> Resource:
    """Request a resource. Raises ResourceRequestError when there's a general issue with the request and QuotaExceededError if the quota would be exceeded."""

    # Load policy
    policy = await core.catalog.get_policy(catalog_policy_id)

    if policy is None:
        msg = "The policy does not exist."
        raise ResourceRequestError(msg)

    # Load quota
    quota = await core.catalog.get_quota(policy.catalog_quota_id)

    if quota is None:
        msg = "The quota does not exist - this should never happen. The referential integrity between policy and quota is broken."
        raise ResourceRequestError(msg)

    # Load ResourceType
    resource_type = await core.catalog.get_resource_type(quota.catalog_resource_type_id)

    if resource_type is None:
        msg = "The resource type does not exist - this should never happen. The referential integrity between quota and resource type is broken."
        raise ResourceRequestError(msg)

    # Check actor is authorized to use policy
    policies_available_to_actor = await core.catalog.get_policies_available_to_actor(
        actor_identity=actor_identity,
        service_id=quota.catalog_service_id,
        resource_type_id=resource_type.id,
    )

    policy_ids_available_to_actor = [
        policy.id for policy in policies_available_to_actor
    ]

    if policy.id not in policy_ids_available_to_actor:
        msg = "The policy is not available to this actor."
        raise ResourceRequestError(msg)

    # Validate request data using resource type
    # ResourceType must cover all attributes, so unevaluatedProperties is False
    specification_passed_resource_type_schema = jsonschema.Draft202012Validator(
        resource_type.json_schema | {"unevaluatedProperties": False}
    ).is_valid(specification)

    if not specification_passed_resource_type_schema:
        msg = "The resource specification did not pass resource type schema validation."
        raise ResourceRequestError(msg)

    # Validate request data using policy
    # Policy may partially (or not at all) cover properties, so unevaluatedProperties are allowed
    specification_passed_policy_schema = jsonschema.Draft202012Validator(
        policy.json_schema
    ).is_valid(specification)

    if not specification_passed_policy_schema:
        msg = "The resource specification did not pass policy schema validation."
        raise ResourceRequestError(msg)

    # Validate target entity

    # Validate when actor requests personal resource that policy is valid for personal resources
    if target_entity == "self" and policy.target_entity != "self":
        msg = "'self' is not a valid target_entity for this policy."
        raise ResourceRequestError(msg)

    # Validate when actor requests group resource that policy is valid for group resources
    if target_entity != "self":
        # Wildcard in policy's target_entity: check that the requested URN starts with the wildcard.
        if policy.target_entity.endswith(":") and not target_entity.startswith(
            policy.target_entity
        ):
            msg = "The requested target entity does not match the policy."
            raise ResourceRequestError(msg)

        # Non-wildcard in policy's target_entity: check that the requested URN matches the policy's target_entity exactly.
        if not policy.target_entity.endswith(":") and (
            target_entity != policy.target_entity
        ):
            msg = (
                "The requested target entity does not match the policy's target_entity."
            )
            raise ResourceRequestError(msg)

    # In case of a group resource check that target_entity is a known group
    groups = await core.identity.get_group_urns()
    if target_entity != "self" and target_entity not in groups:
        msg = "The group selected as target_entity does not exist."
        raise ResourceRequestError(msg)

    # The resource request depends on the presence of this attribute at this time
    # This may change in the future
    if actor_identity.vo_person_id is None:
        msg = "Actor does not have vo_person_id!"
        raise ResourceRequestError(msg)

    async with core.db.DB.get_connection_with_transaction() as connection:
        # Create lock handle if it does not exist yet
        await connection.execute(
            """
            INSERT INTO resources_catalog_quota_lock_handles (catalog_quota_id) VALUES (%(quota_id)s) ON CONFLICT DO NOTHING;
            """,
            {"quota_id": quota.id},
        )

    async with core.db.DB.get_connection_with_transaction() as connection:
        # Acquire lock for the duration of this transaction so there's only one resource request working with the quota at a given time
        await connection.execute(
            """
            SELECT * FROM resources_catalog_quota_lock_handles WHERE catalog_quota_id = %(quota_id)s FOR UPDATE;
            """,
            {"quota_id": quota.id},
        )

        # Check each limit one by one
        for limit in quota.quota:
            limit_cursor = await connection.cursor(
                row_factory=psycopg.rows.dict_row
            ).execute(
                """
                SELECT SUM((specification->>%(property)s)::numeric) AS total FROM resources_resources WHERE catalog_quota_id = %(quota_id)s;
                """,
                {
                    "property": limit.property,
                    "quota_id": quota.id,
                },
            )

            # Technically fetchone() may return None, however the query would always return at least {"total": 0}
            # Add `or {}` for mypy, then the `total` key would be missing and we'd get a KeyError if
            # something unexpected happened and fetchone() returned None.
            total = ((await limit_cursor.fetchone()) or {})["total"]

            specification_value = specification.get(limit.property, 0)

            if not isinstance(specification_value, int):
                # This should be caught by the JSON schema but we only allow integers for now
                # Not covered by tests because this would be caught by the schema which only allows integers at this time
                # This check is an additional safety measure in case of changes in the schema check code e.g. expansion of available types
                msg = "The value passed in the specification is not an integer but is also limited by a quota! This should never happen."
                raise ResourceRequestError(msg)

            if specification_value < 0:
                # If this exception is raised then something is seriously broken in the policy / quota import
                msg = "The value passed in the specification is less than zero but is also limited by a quota! This should never happen."
                raise ResourceRequestError(msg)

            if ((total or 0) + specification_value) > limit.total:
                msg = "Request would exceed quota."
                raise QuotaExceededError(msg)

        # Create resource
        now = datetime.datetime.now(tz=datetime.UTC)
        resource = Resource(
            id=uuid4(),
            catalog_quota_id=quota.id,
            catalog_service_id=quota.catalog_service_id,
            identity_user_id_requested_by=actor_identity.vo_person_id,
            identity_group_urn_target=target_entity
            if target_entity != "self"
            else None,
            identity_user_id_target=(
                actor_identity.vo_person_id if target_entity == "self" else None
            ),
            external_id=None,
            name=name,
            state=ResourceStateEnum.form_submitted,
            state_text=None,
            specification=specification,
            created_at=now,
            updated_at=now,
        )

        await connection.execute(
            """
            INSERT INTO resources_resources
            (
                id,
                catalog_quota_id,
                catalog_service_id,
                identity_user_id_requested_by,
                identity_group_urn_target,
                identity_user_id_target,
                external_id,
                name,
                state,
                state_text,
                specification,
                created_at,
                updated_at
            ) VALUES (
                %(id)s,
                %(catalog_quota_id)s,
                %(catalog_service_id)s,
                %(identity_user_id_requested_by)s,
                %(identity_group_urn_target)s,
                %(identity_user_id_target)s,
                %(external_id)s,
                %(name)s,
                %(state)s,
                %(state_text)s,
                %(specification)s,
                %(created_at)s,
                %(updated_at)s
            )""",
            {
                "id": resource.id,
                "catalog_quota_id": resource.catalog_quota_id,
                "catalog_service_id": resource.catalog_service_id,
                "identity_user_id_requested_by": resource.identity_user_id_requested_by,
                "identity_group_urn_target": resource.identity_group_urn_target,
                "identity_user_id_target": resource.identity_user_id_target,
                "external_id": resource.external_id,
                "name": resource.name,
                "state": resource.state,
                "state_text": resource.state_text,
                "specification": json.dumps(resource.specification),
                "created_at": resource.created_at,
                "updated_at": resource.updated_at,
            },
        )

    return resource


async def mark_request_submitted(resource_id: pydantic.UUID4) -> None:
    """Mark a resource as submitted to service"""
    async with core.db.DB.get_connection_with_transaction() as connection:
        result_cursor = await connection.execute(
            """UPDATE resources_resources
               SET
                    state='request_submitted',
                    updated_at = %(updated_at)s
               WHERE
                    id = %(resource_id)s AND
                    state = 'form_submitted'
            """,
            {
                "updated_at": datetime.datetime.now(tz=datetime.UTC),
                "resource_id": resource_id,
            },
        )

    if result_cursor.statusmessage == "UPDATE 0":
        msg = "Resource not updated. Either id does not exist or is not in 'form_submitted' state"
        raise RuntimeError(msg)


async def mark_provisioned(
    resource_id: pydantic.UUID4, external_id: str | None
) -> None:
    """Mark a resource as provisioned by service and record external id"""
    async with core.db.DB.get_connection_with_transaction() as connection:
        result_cursor = await connection.execute(
            """UPDATE resources_resources
               SET
                    state='provisioned',
                    external_id = %(external_id)s,
                    updated_at = %(updated_at)s
               WHERE
                    id = %(resource_id)s AND
                    state = 'request_submitted'
            """,
            {
                "external_id": external_id,
                "updated_at": datetime.datetime.now(tz=datetime.UTC),
                "resource_id": resource_id,
            },
        )

    if result_cursor.statusmessage == "UPDATE 0":
        msg = "Resource not updated. Either id does not exist or is not in 'request_submitted' state"
        raise RuntimeError(msg)


async def mark_deprovisioning_form_submitted(resource_id: pydantic.UUID4) -> None:
    """Mark a resource as deprovisioning form submitted by the user"""
    async with core.db.DB.get_connection_with_transaction() as connection:
        result_cursor = await connection.execute(
            """UPDATE resources_resources
               SET
                    state='deprovisioning_form_submitted',
                    updated_at = %(updated_at)s
               WHERE
                    id = %(resource_id)s AND
                    state = 'provisioned'
            """,
            {
                "updated_at": datetime.datetime.now(tz=datetime.UTC),
                "resource_id": resource_id,
            },
        )

    if result_cursor.statusmessage == "UPDATE 0":
        msg = "Resource not updated. Either id does not exist or is not in 'provisioned' state"
        raise RuntimeError(msg)


async def mark_deprovisioning_request_submitted(resource_id: pydantic.UUID4) -> None:
    """Mark a resource as deprovisioning request submitted to service"""
    async with core.db.DB.get_connection_with_transaction() as connection:
        result_cursor = await connection.execute(
            """UPDATE resources_resources
               SET
                    state='deprovisioning_request_submitted',
                    updated_at = %(updated_at)s
               WHERE
                    id = %(resource_id)s AND
                    state = 'deprovisioning_form_submitted'
            """,
            {
                "updated_at": datetime.datetime.now(tz=datetime.UTC),
                "resource_id": resource_id,
            },
        )

    if result_cursor.statusmessage == "UPDATE 0":
        msg = "Resource not updated. Either id does not exist or is not in 'deprovisioning_form_submitted' state"
        raise RuntimeError(msg)


async def mark_deprovisioned(resource_id: pydantic.UUID4) -> None:
    """Mark a resource as deprovisioned by service"""
    async with core.db.DB.get_connection_with_transaction() as connection:
        result_cursor = await connection.execute(
            """UPDATE resources_resources
               SET
                    state='deprovisioned',
                    updated_at = %(updated_at)s
               WHERE
                    id = %(resource_id)s AND
                    state = 'deprovisioning_request_submitted'
            """,
            {
                "updated_at": datetime.datetime.now(tz=datetime.UTC),
                "resource_id": resource_id,
            },
        )

    if result_cursor.statusmessage == "UPDATE 0":
        msg = "Resource not updated. Either id does not exist or is not in 'deprovisioning_request_submitted' state"
        raise RuntimeError(msg)


async def mark_request_denied(resource_id: pydantic.UUID4, state_text: str) -> None:
    """Mark a resource as request denied by service"""
    async with core.db.DB.get_connection_with_transaction() as connection:
        result_cursor = await connection.execute(
            """UPDATE resources_resources
               SET
                    state='request_denied',
                    state_text = %(state_text)s,
                    updated_at = %(updated_at)s
               WHERE
                    id = %(resource_id)s AND
                    state = 'request_submitted'
            """,
            {
                "state_text": state_text,
                "updated_at": datetime.datetime.now(tz=datetime.UTC),
                "resource_id": resource_id,
            },
        )

    if result_cursor.statusmessage == "UPDATE 0":
        msg = "Resource not updated. Either id does not exist or is not in 'request_submitted' state"
        raise RuntimeError(msg)
