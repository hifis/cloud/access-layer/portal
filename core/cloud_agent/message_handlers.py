import datetime
import json
from typing import Any

import psycopg.rows
import pydantic
from helmholtz_cloud_agent.messages import messages as hca_messages

import core.db
from core.catalog.catalog import get_quota, get_resource_type
from core.resources.resources import (
    Resource,
    ResourceStateEnum,
    mark_deprovisioned,
    mark_deprovisioning_request_submitted,
    mark_provisioned,
    mark_request_denied,
    mark_request_submitted,
)


class Message(pydantic.BaseModel):
    type: str
    resource_id: pydantic.UUID4
    catalog_service_id: pydantic.UUID4
    queue: str
    payload: dict[str, Any]


async def add_message_to_history(
    *,
    message: Message,
) -> None:
    """Add a message to the history table"""
    async with core.db.DB.get_connection_with_transaction() as connection:
        await connection.execute(
            """INSERT INTO cloud_agent_message_history
                 (type, resource_id, catalog_service_id, queue, payload, created_at)
               VALUES
                 (%(type)s, %(resource_id)s, %(catalog_service_id)s, %(queue)s, %(payload)s, %(created_at)s)""",
            {
                "type": message.type,
                "resource_id": message.resource_id,
                "catalog_service_id": message.catalog_service_id,
                "queue": message.queue,
                "payload": json.dumps(message.model_dump(mode="json")["payload"]),
                "created_at": datetime.datetime.now(tz=datetime.UTC),
            },
        )


async def get_messages_from_history(
    *,
    message_type: str,
    resource_id: pydantic.UUID4,
    catalog_service_id: pydantic.UUID4,
    queue: str,
) -> list[Message]:
    """Return all messages from history for given parameters."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        # Load using dict_rows as otherwise the JSONB field would be returned as dict
        # Check what is actually stored in the database! Is it just a JSON string?
        dict_rows = await connection.cursor(row_factory=psycopg.rows.dict_row).execute(
            """SELECT * FROM cloud_agent_message_history
            WHERE type = %(type)s AND resource_id = %(resource_id)s AND catalog_service_id = %(catalog_service_id)s AND queue = %(queue)s
            """,
            {
                "type": message_type,
                "resource_id": resource_id,
                "catalog_service_id": catalog_service_id,
                "queue": queue,
            },
        )

        message_dictionaries = await dict_rows.fetchall()

        return [Message(**message_dict) for message_dict in message_dictionaries]


async def handle_message_from_hca(
    *,
    resource_id: pydantic.UUID4,
    catalog_service_id: pydantic.UUID4,
    message_type: str,
    raw_message: dict[str, Any],
) -> None:
    """
    update the resource state based on the received message
    """
    if message_type == "ResourceCreatedV1":
        message = hca_messages.ResourceCreatedV1.model_validate(raw_message)
        await mark_provisioned(resource_id=resource_id, external_id=message.id)
    if message_type == "ResourceDeallocatedV1":
        message = hca_messages.ResourceDeallocatedV1.model_validate(raw_message)
        await mark_deprovisioned(resource_id=resource_id)
    if message_type == "ErrorV1":
        message = hca_messages.ErrorV1.model_validate(raw_message)
        state_text = message.message
        await mark_request_denied(resource_id=resource_id, state_text=state_text)

    message = Message(
        type=message_type,
        resource_id=resource_id,
        catalog_service_id=catalog_service_id,
        queue="to_portal",
        payload=raw_message,
    )
    await add_message_to_history(message=message)


async def update_resource_state(*, message: Message) -> None:
    """
    update resource state after request has been sent to HCA
    """
    if message.type == "ResourceAllocateV1":
        await mark_request_submitted(resource_id=message.resource_id)
    elif message.type == "ResourceDeallocateV1":
        await mark_deprovisioning_request_submitted(resource_id=message.resource_id)


async def prepare_messages_for_hca(
    *, resources: list[Resource], queue: str
) -> list[Message]:
    """
    generate resource request messages for each resource in state form_submitted or deprovisioning_form_submitted
    """
    messages = []
    for resource in resources:
        quota = await get_quota(resource.catalog_quota_id)
        if quota is None:
            msg = f"Cannot find quota {resource.catalog_quota_id} for resource {resource.id}"
            raise RuntimeError(msg)

        resource_type = await get_resource_type(quota.catalog_resource_type_id)
        if resource_type is None:
            msg = f"Cannot get resource type for id {quota.catalog_resource_type_id}"
            raise RuntimeError(msg)

        resource_type_name = resource_type.json_schema["title"]

        target_entity = hca_messages.TargetEntityV1(
            group_urn_target=resource.identity_group_urn_target,
            user_id_target=resource.identity_user_id_target,
        )

        message_type: str
        payload: dict[str, Any]
        if resource.state == ResourceStateEnum.form_submitted:
            message_type = "ResourceAllocateV1"
            payload = hca_messages.ResourceAllocateV1(
                type=resource_type_name,
                target_entity=target_entity,
                specification=resource.specification,
            ).model_dump()
        elif resource.state == ResourceStateEnum.deprovisioning_form_submitted:
            if resource.external_id is None:
                msg = "Resource has no external_id set. Cannot sent prepare ResourceDeallocateV1 message"
                raise RuntimeError(msg)
            message_type = "ResourceDeallocateV1"
            payload = hca_messages.ResourceDeallocateV1(
                id=resource.external_id,
            ).model_dump()
        else:
            msg = f"Resource {resource.id} has wrong state {resource.state} and cannot be handled"
            raise ValueError(msg)

        message = Message(
            type=message_type,
            resource_id=resource.id,
            catalog_service_id=resource.catalog_service_id,
            queue=queue,
            payload=payload,
        )
        messages.append(message)
    return messages
