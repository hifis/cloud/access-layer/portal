/** @type {import('tailwindcss').Config} */

import defaultTheme from "tailwindcss/defaultTheme";

module.exports = {
    content: ["web/**/*.vue", "web/**/templates/**/*.html"],
    theme: {
        extend: {
            fontFamily: {
                sans: ['"InterVariable"', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                /*
        How to generate a color gradient:
            1) Take base color: put into HSL wheel, find starting point using L
                50  L 95 S -2
                100 L 90 S +0
                200 L 85
                300 L 75
                400 L 60
                500 L 50
                600 L 40
                700 L 30
                800 L 25
                900 L 20 S -15
            2) Check swatch and then do a manual second pass to adjust
        */
                primary: "#002864",
                secondary: "#14c8ff",
                accent: "#05e5ba",
                highlight: "#cdeefb",
                logo: "#005aa0",
                claim: "#008040",
                brightgreen: "#8cd600",
                brightorange: "#fa7833",
                brightpink: "#ed5ef2",
                blue: {
                    // biome-ignore lint/style/useNamingConvention: tailwind convention
                    DEFAULT: "#002864",
                    50: "#e6f0ff",
                    100: "#cce0ff",
                    200: "#b3d1ff",
                    300: "#80b3ff",
                    400: "#3385ff",
                    500: "#0066ff",
                    600: "#0052cc",
                    700: "#003d99",
                    800: "#003380",
                    900: "#002864",
                },
            },
            typography: ({ theme }) => ({
                hifis: {
                    css: {
                        "--tw-prose-body": theme("colors.primary"),
                        "--tw-prose-headings": theme("colors.primary"),
                        "--tw-prose-lead": theme("colors.primary"),
                        "--tw-prose-links": theme("colors.primary"),
                        "--tw-prose-bold": theme("colors.primary"),
                        "--tw-prose-counters": theme("colors.primary"),
                        "--tw-prose-bullets": theme("colors.primary"),
                        "--tw-prose-hr": theme("colors.primary"),
                        "--tw-prose-quotes": theme("colors.primary"),
                        "--tw-prose-quote-borders": theme("colors.primary"),
                        "--tw-prose-captions": theme("colors.primary"),
                        "--tw-prose-code": theme("colors.primary"),
                        "--tw-prose-pre-code": theme("colors.primary"),
                        "--tw-prose-pre-bg": theme("colors.primary"),
                        "--tw-prose-th-borders": theme("colors.primary"),
                        "--tw-prose-td-borders": theme("colors.primary"),
                        "--tw-prose-invert-body": theme("colors.white"),
                        "--tw-prose-invert-headings": theme("colors.white"),
                        "--tw-prose-invert-lead": theme("colors.white"),
                        "--tw-prose-invert-links": theme("colors.white"),
                        "--tw-prose-invert-bold": theme("colors.white"),
                        "--tw-prose-invert-counters": theme("colors.white"),
                        "--tw-prose-invert-bullets": theme("colors.white"),
                        "--tw-prose-invert-hr": theme("colors.white"),
                        "--tw-prose-invert-quotes": theme("colors.white"),
                        "--tw-prose-invert-quote-borders": theme("colors.white"),
                        "--tw-prose-invert-captions": theme("colors.white"),
                        "--tw-prose-invert-code": theme("colors.white"),
                        "--tw-prose-invert-pre-code": theme("colors.white"),
                        "--tw-prose-invert-pre-bg": "rgb(0 0 0 / 50%)",
                        "--tw-prose-invert-th-borders": theme("colors.white"),
                        "--tw-prose-invert-td-borders": theme("colors.white"),
                    },
                },
            }),
            keyframes: {
                loader: {
                    "0%": { "margin-left": 0, width: 0 },
                    "50%": { "margin-left": 0, width: "100%" },
                    "100%": { "margin-left": "100%", width: "100%" },
                },
            },
            animation: {
                loader: "loader 0.5s linear infinite",
            },
        },
    },
    plugins: [require("@tailwindcss/forms"), require("@tailwindcss/typography")],
};
