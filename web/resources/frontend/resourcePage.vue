<script lang="ts">
import { Disclosure, DisclosureButton, DisclosurePanel } from "@headlessui/vue";
import { prettifyGroupURN } from "./formatting";

// Data structures received from the view

interface ResourceData {
    user: User;
    resources: Resource[];
    services: Record<string, Service>;
    providers: Record<string, Provider>;
    resourceTypes: Record<string, ResourceType>;
}

interface User {
    id: string;
    name: string;
}

interface Resource {
    id: string;
    externalID: string | null;
    name: string;
    catalogServiceID: string;
    catalogResourceTypeID: string;
    specification: string;
    state: string;
    stateText: string | null;
    identityGroupURNTarget: string | null;
    identityUserIDTarget: string | null;
    createdAt: string;
    updatedAt: string;
}

interface ResourceType {
    id: string;
    name: string;
}

interface Service {
    id: string;
    title: string;
    generalSoftwareName: string;
    catalogProviderID: string;
}

interface Provider {
    id: string;
    title: string;
}

// Resource states which do not require auto-refresh
const INACTIVE_STATES = ["provisioned", "deprovisioned", "request_denied"];

// biome-ignore lint/style/noDefaultExport: Vue Components use default exports
export default {
    data() {
        // Load service information from data attribute
        const pageComponent = document.getElementById("resource-page");
        if (pageComponent === null) {
            throw new Error("Could not find resource-page element in DOM! Ensure it is present before trying to mount the component.");
        }

        const resourceJSON = pageComponent.dataset.resources;
        if (resourceJSON === undefined) {
            throw new Error("Resource page data attribute is missing!");
        }

        const resourceData = JSON.parse(resourceJSON) as ResourceData;

        // Load state from URL
        const currentPath = new URL(window.location.href);

        const filterOwner = decodeURIComponent(currentPath.searchParams.get("filterOwner") || "");
        const filterService = decodeURIComponent(currentPath.searchParams.get("filterService") || "");

        return {
            // Base data from data attribute
            resourceData,

            // Filter settings
            filterOwner,
            filterService,

            prettifyGroupURN,
        };
    },
    async mounted() {
        // Initial state sync to clean-up URL and set filters properly
        this.syncFilterState();

        // Start auto-refresh process
        await this.autorefreshResources();
    },
    computed: {
        filteredAndSortedServices() {
            // Filter all services which do not have resources for the currently selected filterOwner

            const filteredServiceIDs: string[] = [];

            for (const resource of this.resourceData.resources) {
                if (
                    resource.identityGroupURNTarget &&
                    resource.identityGroupURNTarget === this.filterOwner &&
                    !filteredServiceIDs.includes(resource.catalogServiceID)
                ) {
                    filteredServiceIDs.push(resource.catalogServiceID);
                }

                if (
                    resource.identityUserIDTarget &&
                    resource.identityUserIDTarget === this.filterOwner &&
                    !filteredServiceIDs.includes(resource.catalogServiceID)
                ) {
                    filteredServiceIDs.push(resource.catalogServiceID);
                }
            }

            const services: Service[] = [];
            const serviceIDs: string[] = [];

            for (const serviceID of filteredServiceIDs) {
                if (!serviceIDs.includes(serviceID)) {
                    services.push(this.resourceData.services[serviceID]);
                    serviceIDs.push(serviceID);
                }
            }

            services.sort((a, b) => this.generateCanonicalServiceName(b).localeCompare(this.generateCanonicalServiceName(a)));

            return services;
        },
        filteredAndSortedResources() {
            // Filter resources for selected owner/service combination and order descending by createdAt

            const filtered: Resource[] = [];

            for (const resource of this.resourceData.resources) {
                if (
                    resource.catalogServiceID === this.filterService &&
                    (resource.identityGroupURNTarget === this.filterOwner || resource.identityUserIDTarget === this.filterOwner)
                ) {
                    filtered.push(resource);
                }
            }

            filtered.sort((a, b) => b.createdAt.localeCompare(a.createdAt));

            return filtered;
        },
        hasPersonalResources() {
            // Does the list of resources include any personal resources?

            for (const resource of this.resourceData.resources) {
                if (resource.identityUserIDTarget === this.resourceData.user.id) {
                    return true;
                }
            }

            return false;
        },
        sortedGroupsURNsWithResources() {
            // Sort group URNs with resources by name

            const owners: string[] = [];

            for (const resource of this.resourceData.resources) {
                if (resource.identityGroupURNTarget && !owners.includes(resource.identityGroupURNTarget)) {
                    owners.push(resource.identityGroupURNTarget);
                }
            }

            owners.sort((a, b) => a.localeCompare(b));

            return owners;
        },
    },
    watch: {
        filterOwner(_newIDs, _oldIDs) {
            // Watch for changes as this parameter can be loaded from URL
            this.syncFilterState();
        },
        filterService(_newIDs, _oldIDs) {
            // Watch for changes as this parameter can be loaded from URL
            this.syncFilterState();
        },
    },
    methods: {
        syncFilterState() {
            //
            // Set the filter options to something useful given the potentially broken input loaded from URL.
            // Then, re-write URL.
            //
            // filterOwner could be unset or filter owner is set to an owner which no longer has any resources
            //    - if user has personal resources, then select personal
            //    - else select first org with resources
            // filterOwner is set to an owner which has resources but filterService is unset or filter owner is set to an owner which has resources but filterService is not present for this owner
            //    - select first service with resources
            // filterOwner is set to an owner which has resources and filterService is set to a service with resources
            //    - select owner/service
            //

            // Set filterOwner if broken
            if (
                this.filterOwner === "" ||
                !Object.values(this.resourceData.resources)
                    .flatMap((resource) => [resource.identityGroupURNTarget, resource.identityUserIDTarget])
                    .includes(this.filterOwner)
            ) {
                if (this.hasPersonalResources) {
                    // Select personal resources
                    this.filterOwner = this.resourceData.user.id;
                } else {
                    // Select the first URN with services
                    this.filterOwner = this.sortedGroupsURNsWithResources[0];
                }
            }

            // Set filterService if broken
            if (this.filterService === "" || !this.filteredAndSortedServices.map((service) => service.id).includes(this.filterService)) {
                this.filterService = this.filteredAndSortedServices[0].id;
            }

            // Sync state into query parameters
            const updatedURL = new URL(window.location.href);

            // Clear all search params
            updatedURL.search = "";

            if (this.filterOwner !== "") {
                updatedURL.searchParams.set("filterOwner", this.filterOwner);
            }

            if (this.filterService !== "") {
                updatedURL.searchParams.set("filterService", this.filterService);
            }

            updatedURL.searchParams.sort();

            window.history.replaceState({}, "", updatedURL);
        },
        generateCanonicalServiceName(service: Service): string {
            if (service.title === service.generalSoftwareName) {
                return service.title;
            }

            return `${service.generalSoftwareName} / ${service.title}`;
        },
        isRefreshNeeded(): boolean {
            // Return whether there are any in-progress resources
            for (const resource of this.resourceData.resources) {
                if (!INACTIVE_STATES.includes(resource.state)) {
                    return true;
                }
            }

            return false;
        },
        async autorefreshResources(): Promise<void> {
            // This method reloads request data and re-schedules itself

            if (!this.isRefreshNeeded()) {
                // No refresh need, return
                return;
            }

            // Reload data
            try {
                const resourcePage = await fetch(location.href, { redirect: "error" });

                if (!resourcePage.ok) {
                    // If re-authentication is needed or an error occured, reload the page instead
                    location.reload();
                }

                const parsedResponse = new DOMParser().parseFromString(await resourcePage.text(), "text/html");

                const pageComponent = parsedResponse.getElementById("resource-page");

                if (pageComponent === null) {
                    throw new Error("Could not find resource-page element in DOM!");
                }

                const resourceJSON = pageComponent.dataset.resources;
                if (resourceJSON === undefined) {
                    throw new Error("Resource page data attribute is missing!");
                }

                this.resourceData = JSON.parse(resourceJSON) as ResourceData;
            } catch (error) {
                // This could be a timeout or similar, log this instead
                console.error("Failed to refresh resource information:", error);
            }

            // Check if this needs to be re-scheduled
            if (!this.isRefreshNeeded()) {
                // No refresh need, return
                return;
            }

            // Depending on newest change of an in-progress resource set refresh interval
            const latestInProgressChange = Date.parse(
                this.resourceData.resources
                    .filter((resource) => !INACTIVE_STATES.includes(resource.state))
                    .sort((a, b) => b.updatedAt.localeCompare(a.updatedAt))[0].updatedAt,
            );

            const tenSecondsAgo = Date.now() - 10 * 1000;
            const oneMinuteAgo = Date.now() - 60 * 1000;
            const fiveMinutesAgo = Date.now() - 5 * 60 * 1000;
            const oneHourAgo = Date.now() - 60 * 60 * 1000;
            const twelveHoursAgo = Date.now() - 12 * 60 * 60 * 1000;

            let refreshIntervalMilliseconds = 5 * 60 * 1000;

            if (latestInProgressChange > twelveHoursAgo) {
                refreshIntervalMilliseconds = 60 * 1000;
            }

            if (latestInProgressChange > oneHourAgo) {
                refreshIntervalMilliseconds = 10 * 1000;
            }

            if (latestInProgressChange > fiveMinutesAgo) {
                refreshIntervalMilliseconds = 3 * 1000;
            }

            if (latestInProgressChange > oneMinuteAgo) {
                refreshIntervalMilliseconds = 1 * 1000;
            }

            if (latestInProgressChange > tenSecondsAgo) {
                refreshIntervalMilliseconds = 500;
            }

            // Re-schedule self
            setTimeout(() => this.autorefreshResources(), refreshIntervalMilliseconds);
        },
    },
    components: { Disclosure, DisclosureButton, DisclosurePanel },
};
</script>

<template>
    <section class="flex flex-row justify-end items-center mb-8 w-full">
        <div class="pl-2 flex flex-row w-auto relative dropdown">
            <select class="m-0 p-2 pr-8 mt-[-0.3rem] max-xl:mt-1 w-full h-full text-ellipsis border-none shadow-none"
                aria-label="Select Resource Workspace" v-model="filterOwner">
                <option v-if="hasPersonalResources" :value="`${resourceData.user.id}`">{{ resourceData.user.name }}'s
                    Resources</option>
                <option v-for="groupURN of sortedGroupsURNsWithResources" :value="groupURN">
                    {{ prettifyGroupURN(groupURN) }}
                </option>
            </select>
        </div>
    </section>
    <section class="flex flex-col xl:flex-row items-start content-start">
        <section class="flex flex-col flex-grow-0 w-full xl:w-1/5 xl:mr-4 max-xl:mb-4">
            <div class="flex flex-col">
                <div v-for="service of filteredAndSortedServices"
                    class="flex flex-row p-2 py-3 font-medium hover:rounded-md hover:bg-slate-200 cursor-pointer"
                    :class="{ 'bg-slate-200 rounded-md': filterService === service.id }"
                    @click="filterService = service.id" v-on:keydown.enter="filterService = service.id" tabindex="0"
                    :title="`${generateCanonicalServiceName(service)}`">
                    <div class="flex flex-row w-1/5 items-center justify-center">
                        <img class="max-w-[27px] max-h-[17px] m-0"
                            :alt="`Logo ${generateCanonicalServiceName(service)}`"
                            :src="`/services/${service.id}/logo/`" />
                    </div>
                    <div class="flex flex-col w-4/5">
                        <span
                            class="whitespace-nowrap overflow-hidden text-ellipsis">{{ generateCanonicalServiceName(service) }}</span>
                    </div>
                </div>
                <a href="/services/" class="self-center pt-6">Browse Services</a>
            </div>
        </section>
        <section class="flex flex-col xl:w-4/5 w-full">
            <section class="rounded-md shadow border py-0">
                <table class="table-auto m-0">
                    <thead>
                    </thead>
                    <tbody>
                        <Disclosure v-for="resource of filteredAndSortedResources" :key="resource.id" v-slot="{ open }">
                            <tr class="border-slate-200" :class="open && 'border-slate-50'">
                                <td class="py-4 pl-4">{{ resource.name }}</td>
                                <td>{{ resource.stateText }}</td>
                                <td class="py-4 text-right">
                                    <span v-if="resource.state === 'form_submitted'"
                                        class="p-2 rounded bg-sky-50 text-sky-800 whitespace-nowrap select-none"
                                        title="Your request has been submitted to Helmholtz Cloud and is waiting to be forwarded to the resource provider.">
                                        <span class="text-lg animate-pulse">•</span>
                                        Form Submitted
                                    </span>
                                    <span v-else-if="resource.state === 'request_submitted'"
                                        class="p-2 rounded bg-sky-50 text-sky-800 whitespace-nowrap select-none"
                                        title="Your resource request is ready to be processed by the provider.">
                                        <span class="text-lg animate-pulse">•</span>
                                        Request Submitted
                                    </span>
                                    <span v-else-if="resource.state === 'provisioned'"
                                        class="p-2 rounded bg-green-50 text-green-800 whitespace-nowrap select-none"
                                        title="Your resource has been provisioned successfully and is ready to use!">
                                        <i class="fa-solid fa-circle-check"></i>
                                        Ready
                                    </span>
                                    <span v-else-if="resource.state === 'deprovisioning_form_submitted'"
                                        class="p-2 rounded bg-slate-50 text-slate-800 whitespace-nowrap select-none"
                                        title="Your request for deprovisioning has been received and is waiting to be forwarded to the provider.">
                                        <span class="text-lg animate-pulse">•</span>
                                        Deprovisioning Form Submitted
                                    </span>
                                    <span v-else-if="resource.state === 'deprovisioning_request_submitted'"
                                        class="p-2 rounded bg-slate-50 text-slate-800 whitespace-nowrap select-none"
                                        title="Your resource request is ready to be processed by the provider.">
                                        <span class="text-lg animate-pulse">•</span>
                                        Deprovisioning Request Submitted
                                    </span>
                                    <span v-else-if="resource.state === 'request_denied'"
                                        class="p-2 rounded bg-red-50 text-red-800 whitespace-nowrap select-none"
                                        title="Your request for this resource was denied by the provider.">
                                        <i class="fa-solid fa-circle-xmark"></i>
                                        Request Denied
                                    </span>
                                    <span v-else-if="resource.state === 'deprovisioned'"
                                        class="p-2 rounded bg-slate-50 text-slate-800 whitespace-nowrap select-none"
                                        title="Your resource has been deprovisioned.">
                                        <i class="fa-solid fa-archive-box"></i>
                                        Deprovisioned
                                    </span>
                                    <span v-else
                                        class="p-2 rounded bg-slate-50 text-slate-800 whitespace-nowrap select-none"
                                        :title="`Your resource is in the state ${resource.state}.`">
                                        {{ resource.state }}
                                    </span>
                                </td>
                                <td class="text-right pr-4 w-4">
                                    <DisclosureButton>
                                        <i v-if="!open" title="Reveal Resource Information"
                                            class="fa-solid text-sm text-slate-500 fa-chevron-down"></i>
                                        <i v-if="open" title="Hide Resource Information"
                                            class="fa-solid text-sm text-slate-500 fa-chevron-up"></i>
                                    </DisclosureButton>
                                </td>
                            </tr>
                            <DisclosurePanel as="tr" class="border-slate-200">
                                <td colspan="100%">
                                    <div class="flex w-full flex-col md:flex-row px-4">
                                        <table class="flex m-0 w-full">
                                            <tbody class="w-full">
                                                <tr class="border-0">
                                                    <td><b>Created</b></td>
                                                    <td :title="resource.createdAt">
                                                        {{ new Intl.DateTimeFormat('en-DE', { dateStyle: 'medium', timeStyle: 'medium' }).format(new Date(resource.createdAt)) }}
                                                    </td>
                                                </tr>
                                                <tr class="border-0">
                                                    <td><b>Updated</b></td>
                                                    <td :title="resource.updatedAt">
                                                        {{ new Intl.DateTimeFormat('en-DE', { dateStyle: 'medium', timeStyle: 'medium' }).format(new Date(resource.updatedAt)) }}
                                                    </td>
                                                </tr>
                                                <tr class="border-0 whitespace-nowrap">
                                                    <td><b>Helmholtz Cloud ID</b></td>
                                                    <td class="font-mono">{{ resource.id }}</td>
                                                </tr>
                                                <tr v-if="resource.externalID !== null" class="border-0">
                                                    <td><b>External ID</b></td>
                                                    <td class="font-mono">{{ resource.externalID }}</td>
                                                </tr>
                                                <tr class="border-0">
                                                    <td><b>Specification</b></td>
                                                    <td class="w-full font-mono">
                                                        <pre
                                                            class="mt-0 bg-slate-900 text-white">{{ resource.specification }}</pre>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </DisclosurePanel>
                        </Disclosure>
                    </tbody>
                </table>
            </section>
        </section>
    </section>
</template>


<style scoped>
.dropdown::after {
    position: absolute;
    top: 6px;
    right: 12px;
    font: var(--fa-font-solid);
    content: "\f0d7";
}
</style>