"""
Add resources table
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import JSONB, UUID

revision = "da21b5f717ca"
down_revision = None
branch_labels = ("resources",)
depends_on = None


def upgrade() -> None:
    op.create_table(
        "resources_resources",
        sa.Column(
            "id",
            UUID,
            primary_key=True,
            nullable=False,
            comment="Resource's ID",
        ),
        sa.Column(
            "catalog_quota_id",
            UUID,
            primary_key=False,
            nullable=False,
            comment="ID of the quota, policies reference the quota",
        ),
        sa.Column(
            "catalog_service_id",
            UUID,
            primary_key=False,
            nullable=False,
            comment="ID of the service",
        ),
        sa.Column(
            "identity_user_id_requested_by",
            UUID,
            primary_key=False,
            nullable=False,
            comment="The user who started the request",
        ),
        sa.Column(
            "identity_group_urn_target",
            sa.Text,
            index=True,
            nullable=True,
            comment="When this is a group resource the group this resource was requested for, if identity_user_id_target is set then this must be NULL",
        ),
        sa.Column(
            "identity_user_id_target",
            UUID,
            index=True,
            nullable=True,
            comment="When this is a personal resource the user this resource was requested for, if identity_group_target is set then this must be NULL",
        ),
        sa.Column(
            "external_id",
            sa.Text,
            index=False,
            nullable=True,
            comment="The ID assigned by HCA for future reference, that's why this is a string",
        ),
        sa.Column(
            "name",
            sa.Text,
            index=False,
            nullable=False,
            comment="The name of the resource when displayed in the portal",
        ),
        sa.Column(
            "state",
            sa.Text,
            index=False,
            nullable=False,
            comment="The resource state",
        ),
        sa.Column(
            "state_text",
            sa.Text,
            index=False,
            nullable=True,
            comment="A text with additional information for certain states, e.g., the error message for request_denied",
        ),
        sa.Column(
            "specification",
            JSONB,
            nullable=False,
            comment="The current resource specification",
        ),
        sa.Column(
            "created_at",
            sa.DateTime(timezone=True),
            unique=False,
            index=False,
            nullable=False,
            comment="The resource creation date",
        ),
        sa.Column(
            "updated_at",
            sa.DateTime(timezone=True),
            unique=False,
            index=False,
            nullable=False,
            comment="The resource modification date",
        ),
    )

    op.create_check_constraint(
        "ck_either_personal_or_group_resource",
        "resources_resources",
        "((identity_group_urn_target IS NOT NULL) AND (identity_user_id_target IS NULL)) OR ((identity_group_urn_target IS NULL) AND (identity_user_id_target IS NOT NULL))",
        deferrable=False,
    )


def downgrade() -> None:
    op.drop_table("resources_resources")
