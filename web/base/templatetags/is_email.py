from django import template
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator

register = template.Library()


# This is a Django base class for which we do not have type annotations at the moment.
@register.filter()  # type: ignore[misc]
def is_email(text: str | None) -> bool:
    """
    Return whether the string passed is formatted like an email address.
    """

    if text is None:
        return False

    valid = True

    try:
        EmailValidator(None, None, [])(text)
    except ValidationError:
        valid = False

    return valid


register.filter("is_email", is_email)
