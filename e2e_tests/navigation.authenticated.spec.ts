import { expect } from "@playwright/test";
import { loggedInTest } from "./login";

loggedInTest("user menu works as expected when logged-in", async ({ page }) => {
    const response = await page.goto("/");
    expect(response?.status()).toBe(200);

    const userMenuButton = page.locator("#user-menu-button");
    const userMenuDropdown = page.locator("#user-menu-dropdown");

    // By default the user menu button is visible
    await expect(userMenuButton).toBeVisible();

    // The dropdown is closed and can be made visible by clicking the button
    await expect(userMenuDropdown).not.toBeVisible();
    await userMenuButton.click();
    await expect(userMenuDropdown).toBeVisible();
    await expect(userMenuDropdown).toContainText("Resource Requests");
    await expect(userMenuDropdown).toContainText("Logout");

    // Clicking anywhere closes the dropdown
    await page.getByRole("main").click();
    await expect(userMenuDropdown).not.toBeVisible();

    // Switch to phone
    await page.setViewportSize({ width: 320, height: 600 });

    // Expect button to be hidden now as the dropdown became part of the hamburger menu
    await expect(userMenuButton).not.toBeVisible();
    await expect(userMenuDropdown).not.toBeVisible();

    // Open the hamburger menu and the items should appear
    await page.locator("#hamburger-menu").click();
    await expect(userMenuDropdown).toContainText("Resource Requests");
    await expect(userMenuDropdown).toContainText("Logout");

    // Switch back to desktop
    await page.setViewportSize({ width: 1920, height: 1080 });

    // Everything should be back to the previous state
    await expect(userMenuButton).toBeVisible();
    await expect(userMenuDropdown).not.toBeVisible();
});
