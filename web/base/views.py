from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

import core.app
import core.catalog


async def index(request: HttpRequest) -> HttpResponse:
    """Landing page."""

    providers = await core.catalog.get_providers()

    return render(
        request,
        "base/index.html",
        {"providers": sorted(providers, key=lambda provider: provider.title)},
    )


async def privacy_policy(request: HttpRequest) -> HttpResponse:
    """Privacy policy page."""

    return render(request, "base/privacy_policy.html")


async def terms_of_use(request: HttpRequest) -> HttpResponse:
    """Terms of use page."""

    return render(request, "base/terms_of_use.html")


async def healthz(_request: HttpRequest) -> HttpResponse:
    """Liveness endpoint. If this does not return a 200, the pod is going to be recreated."""

    return HttpResponse(status=200)


async def readyz(_request: HttpRequest) -> HttpResponse:
    """Readiness endpoint. Can return a non-200 code if the service is overloaded. This is mainly to stop the load balancer from overloading workers."""

    core_is_ready = await core.app.is_ready()

    if core_is_ready is False:
        return HttpResponse(status=429)

    return HttpResponse(status=200)


def error400(
    request: HttpRequest,
    # Django API
    exception: Exception,  # noqa: ARG001
) -> HttpResponse:
    """400 page."""

    return render(
        request,
        "base/error.html",
        {
            "code": 400,
            "message": "Bad Request",
            "explanation": "The request you sent to the service was invalid. Please check whether you're sending standards-compliant HTTP requests.",
        },
        status=400,
    )


def error403(
    request: HttpRequest,
    # Django API
    exception: Exception,  # noqa: ARG001
) -> HttpResponse:
    """403 page."""

    return render(
        request,
        "base/error.html",
        {
            "code": 403,
            "message": "Permission Denied",
            "explanation": "You do not have permission to access this page. If you believe this to be an error, please reach out to the helpdesk linked in the menu above.",
        },
        status=403,
    )


def error404(
    request: HttpRequest,
    # Django API
    exception: Exception,  # noqa: ARG001
) -> HttpResponse:
    """404 page."""

    return render(
        request,
        "base/error.html",
        {
            "code": 404,
            "message": "Not Found",
            "explanation": "The page you tried to access could not be found. If you believe this to be an error, please reach out to the helpdesk linked in the menu above.",
        },
        status=404,
    )


def error500(request: HttpRequest) -> HttpResponse:
    """500 page."""

    return render(
        request,
        "base/error.html",
        {
            "code": 500,
            "message": "Internal Server Error",
            "explanation": "There was an internal server error while processing your request. Please reach out to the helpdesk linked in the menu above.",
        },
        status=500,
    )
