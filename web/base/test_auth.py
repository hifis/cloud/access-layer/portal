# Comparing to magic values is part of testing examples
# ruff: noqa: PLR2004

import datetime
import json
from http import HTTPStatus
from uuid import UUID, uuid4

import django.contrib.auth
import pytest
from cryptography.fernet import Fernet
from django.conf import settings
from django.contrib.auth.middleware import AuthenticationMiddleware
from django.contrib.auth.models import AnonymousUser
from django.contrib.messages.storage import default_storage
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.sessions.middleware import SessionMiddleware
from django.core.exceptions import SuspiciousOperation
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.test import RequestFactory
from django.urls import reverse
from django.utils import timezone
from mozilla_django_oidc.views import OIDCAuthenticationCallbackView

from web.base.auth import (
    OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY,
    AAIError,
    PortalErrorRedirectView,
    PortalOIDCBackend,
    aai_session_encryption_middleware,
    middleware_after_view,
)
from web.base.models import AAIProfile, PortalToken, User
from web.base.testing import TEST_CLAIMS


@pytest.mark.django_db
class TestPortalErrorRedirectView:
    # Check workaround works as expected and redirects on first try but throws on second and validates presence of session cookie
    def test_workaround(
        self: "TestPortalErrorRedirectView",
        rf: RequestFactory,
        monkeypatch: pytest.MonkeyPatch,
    ) -> None:
        request = rf.get(
            "/", query_params={"code": "secretcode", "state": "brokenstate"}
        )
        request.user = AnonymousUser()
        request.COOKIES[settings.SESSION_COOKIE_NAME] = "dummy"
        request.session = SessionStore()
        request.session["oidc_states"] = "expectedstate"

        assert OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY not in request.session

        # First try: redirect
        view = PortalErrorRedirectView()
        response = view.get(request)

        assert type(response) is HttpResponseRedirect
        assert response.status_code == HTTPStatus.FOUND
        assert response.headers["Location"] == reverse("oidc_authentication_init")

        assert OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY in request.session
        assert request.session[OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY] == 1

        # Second try: throw
        with pytest.raises(
            SuspiciousOperation,
            match="OIDC callback state not found in session `oidc_states`!",
        ):
            view.get(request)

        # Check it clears the counter on success as expected by monkeypatching the superclass function
        # We do not monkeypatch in the other cases to ensure the contract still works at the cost of some brittleness
        monkeypatch.setattr(
            OIDCAuthenticationCallbackView,
            "get",
            lambda _self, _request: "magicreturnvalue",
        )

        assert OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY in request.session
        assert request.session[OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY] == 1

        response = view.get(request)

        assert response == "magicreturnvalue"

        assert OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY not in request.session

        del request.COOKIES[settings.SESSION_COOKIE_NAME]
        response = view.get(request)

        assert response.status_code == 400
        assert "Login failed. Your Browser did not send the session cookie." in str(
            response.content
        )


@pytest.mark.django_db
class TestPortalOIDCBackend:
    # Create a new user and check that the encrypted profile information matches the claims
    def test_create_user_works_as_expected(
        self: "TestPortalOIDCBackend", rf: RequestFactory
    ) -> None:
        # Generate a request object we can use in the backend
        request = rf.get("/")
        request.user = AnonymousUser()

        # Instantiate backend and assign request
        backend = PortalOIDCBackend()
        backend.request = request

        # Create user and check it was created
        user = backend.create_user(TEST_CLAIMS)
        assert isinstance(user, User)

        # Check that backend sets the secret and that it's a string of expected length
        assert hasattr(request, "aai_middleware_new_user_secret")
        assert isinstance(request.aai_middleware_new_user_secret, str)
        assert len(request.aai_middleware_new_user_secret) == 44

        # Can decrypt profile using parameter
        aai_profile_json = Fernet(request.aai_middleware_new_user_secret).decrypt(
            user.encrypted_aai_profile
        )
        aai_profile = AAIProfile(**json.loads(aai_profile_json))

        # Check profile matches
        assert aai_profile == AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        # Session ID should be None as the backend does not know the session ID yet.
        # The middleware sets the ID once it is known later in the request flow.
        assert user.session_id is None

        # Expiry is around expected time
        difference = (
            timezone.now() + datetime.timedelta(seconds=settings.SESSION_COOKIE_AGE)
        ) - user.fallback_expire_at
        assert difference < datetime.timedelta(seconds=1)
        assert difference > datetime.timedelta(seconds=-1)

    # It should not be possible to create a new user when a user is already authenticated in this request.
    def test_create_user_raises_if_user_is_already_authenticated(
        self: "TestPortalOIDCBackend",
        rf: RequestFactory,
    ) -> None:
        user = User(
            id=uuid4(), fallback_expire_at=timezone.now() + datetime.timedelta(hours=24)
        )
        user.save()

        request = rf.get("/")
        request.user = user

        backend = PortalOIDCBackend()
        backend.request = request

        with pytest.raises(
            AAIError,
            match="Can't create user: there's already a user for this request.",
        ):
            backend.create_user(TEST_CLAIMS)

    # It should not be possible to call create_user twice in a request,
    # this would indicate a bug as the implementation does not expect this at this time.
    def test_create_user_raises_if_user_was_already_created(
        self: "TestPortalOIDCBackend",
        rf: RequestFactory,
    ) -> None:
        request = rf.get("/")
        request.user = AnonymousUser()
        # False positive: this is just test data
        request.aai_middleware_new_user_secret = "already set"  # nosec B105

        backend = PortalOIDCBackend()
        backend.request = request

        with pytest.raises(
            AAIError,
            match="Only one user can be created per request. This should never happen!",
        ):
            backend.create_user(TEST_CLAIMS)

    # Test updating claims works when re-authenticating
    def test_update_user_on_reauthentication(
        self: "TestPortalOIDCBackend",
        rf: RequestFactory,
    ) -> None:
        request = rf.get("/")
        request.user = AnonymousUser()

        backend = PortalOIDCBackend()
        backend.request = request

        # Create user and check it was created
        user = backend.create_user(TEST_CLAIMS)
        assert isinstance(user, User)

        #
        # Check if profile update works
        #

        # Set up AAI profile as if set by middleware
        request.aai_profile = AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        # Create claims updates
        updated_claims = TEST_CLAIMS.copy()
        updated_claims["name"] += "updated"
        updated_claims["email"] += "updated"

        assert updated_claims != TEST_CLAIMS

        # Store old values
        old_profile = request.aai_profile.model_copy()
        old_secret = request.aai_middleware_new_user_secret

        assert not hasattr(request, "aai_middleware_updated_user")

        backend.update_user(user, updated_claims)

        updated_profile = request.aai_profile.model_copy()

        assert old_profile != updated_profile
        assert request.aai_middleware_new_user_secret != old_secret
        assert request.aai_middleware_updated_user is True

        assert updated_profile.name == updated_claims["name"]
        assert updated_profile.email == updated_claims["email"]

        # Check updated values were persisted to DB as expected
        user.refresh_from_db()

        updated_aai_profile_json_from_db = Fernet(
            request.aai_middleware_new_user_secret
        ).decrypt(user.encrypted_aai_profile)
        updated_profile_from_db = AAIProfile(
            **json.loads(updated_aai_profile_json_from_db)
        )

        assert updated_profile_from_db != old_profile
        assert updated_profile_from_db == updated_profile

    # Test updating claims does not work if the user IDs do not match
    def test_raises_if_user_ids_do_not_match(
        self: "TestPortalOIDCBackend",
        rf: RequestFactory,
    ) -> None:
        request = rf.get("/")
        request.user = AnonymousUser()

        backend = PortalOIDCBackend()
        backend.request = request

        # Create user and check it was created
        user = backend.create_user(TEST_CLAIMS)
        assert isinstance(user, User)

        # Set up AAI profile as if set by middleware
        request.aai_profile = AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        old_profile = request.aai_profile.model_copy()

        # Create claims updates with different sub
        updated_claims = TEST_CLAIMS.copy()
        updated_claims["sub"] = str(uuid4())
        updated_claims["name"] += "updated"

        assert updated_claims != TEST_CLAIMS

        # Attempt to update profile with mismatching sub/ID
        with pytest.raises(
            AAIError,
            match="Attempted to update user using another user's OIDC claims. This is unexpected.",
        ):
            backend.update_user(user, updated_claims)

        # Assert nothing was updated
        assert request.aai_profile == old_profile

        # Attempt to update profile with None claims
        with pytest.raises(TypeError):
            backend.update_user(user, None)  # type: ignore[arg-type]

        # Assert nothing was updated
        assert request.aai_profile == old_profile

        # Attempt to update profile with broken claims
        with pytest.raises(KeyError):
            backend.update_user(user, {})

        # Assert nothing was updated
        assert request.aai_profile == old_profile

    # In this context the sub claim (voPersonID) serves as the username.
    def test_get_username(self: "TestPortalOIDCBackend") -> None:
        backend = PortalOIDCBackend()

        # Works when claims are set
        uuid = str(uuid4())
        assert backend.get_username({"sub": uuid}) == uuid

        # Raises when none
        with pytest.raises(TypeError):
            backend.get_username(None)  # type: ignore[arg-type]

        # Raises when claims are missing
        with pytest.raises(KeyError):
            backend.get_username({})

    # This method is used by the OIDC package to check whether a user is already present and
    # whether to create a new one. Only when "sub" matches between tests and the current session, we
    # return a user und update them subsequently.
    def test_filter_users_by_claims(
        self: "TestPortalOIDCBackend",
        rf: RequestFactory,
    ) -> None:
        request = rf.get("/")
        request.user = AnonymousUser()

        backend = PortalOIDCBackend()
        backend.request = request

        # Create user and check it was created
        user = backend.create_user(TEST_CLAIMS)
        assert isinstance(user, User)

        request.user = user

        # Set up AAI profile as if set by middleware
        request.aai_profile = AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        # When existing subject with matching ID returns user
        assert backend.filter_users_by_claims({
            "sub": str(request.aai_profile.vo_person_id)
        }) == [user]

        # When none returns empty list
        assert backend.filter_users_by_claims(None) == []  # type: ignore[arg-type]

        # When empty dict returns empty list
        assert backend.filter_users_by_claims({}) == []

        # When different ID returns empty list
        uuid = uuid4()
        assert str(uuid) != str(request.aai_profile.vo_person_id)

        assert backend.filter_users_by_claims({"sub": uuid}) == []

    # Verify all expected claims need to be present.
    def test_verify_claims(self: "TestPortalOIDCBackend") -> None:
        backend = PortalOIDCBackend()

        # When present true (test all used in create_user!)
        assert backend.verify_claims(TEST_CLAIMS) is True

        # When required claim is missing return false
        for claim in PortalOIDCBackend.REQUIRED_CLAIMS:
            without_required_claim = TEST_CLAIMS.copy()
            del without_required_claim[claim]
            assert backend.verify_claims(without_required_claim) is False

        # when none false
        assert backend.verify_claims(None) is False  # type: ignore[arg-type]

        # when empty false
        assert backend.verify_claims({}) is False

    # Workaround for
    # https://codebase.helmholtz.cloud/hifis/cloud/access-layer/portal/-/issues/205
    def test_converts_affiliation_string_claim(self: "TestPortalOIDCBackend") -> None:
        backend = PortalOIDCBackend()

        string_affiliation_claim = TEST_CLAIMS.copy()
        string_affiliation_claim["eduperson_scoped_affiliation"] = "staff"

        assert backend._claims_to_aai_profile(  # noqa: SLF001, unfortunately we have to test a private method here as passing the argument through HIDaH does not work
            string_affiliation_claim
        ).eduperson_scoped_affiliation == ["staff"]

    # Workaround for
    # https://codebase.helmholtz.cloud/hifis/cloud/access-layer/portal/-/issues/205
    def test_converts_entitlements_string_claim(self: "TestPortalOIDCBackend") -> None:
        backend = PortalOIDCBackend()

        string_entitlement_claim = TEST_CLAIMS.copy()
        string_entitlement_claim["eduperson_entitlement"] = "library"

        assert backend._claims_to_aai_profile(  # noqa: SLF001, unfortunately we have to test a private method here as passing the argument through HIDaH does not work
            string_entitlement_claim
        ).eduperson_entitlements == ["library"]

    # Workaround for
    # https://codebase.helmholtz.cloud/hifis/cloud/access-layer/portal/-/issues/205
    def test_converts_assurances_string_claim(self: "TestPortalOIDCBackend") -> None:
        backend = PortalOIDCBackend()

        string_assurance_claim = TEST_CLAIMS.copy()
        string_assurance_claim["eduperson_assurance"] = (
            "https://refeds.org/assurance/ATP/ePA-1m"
        )

        assert backend._claims_to_aai_profile(  # noqa: SLF001, unfortunately we have to test a private method here as passing the argument through HIDaH does not work
            string_assurance_claim
        ).eduperson_assurances == ["https://refeds.org/assurance/ATP/ePA-1m"]


@pytest.mark.django_db
class TestAAISessionEncryptionMiddleware:
    #
    # middleware_before_view
    #

    # When there's a valid request it decrypts the profile and sets it on the request
    # Basically, simulate a request creating a user and then doing a subsequent request as a logged-in user.
    def test_regular_authenticated_request_sets_aai_profile(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user and get secret from request

        # Create a new request for creating a new user
        creation_request = HttpRequest()

        # User is not logged-in yet -> user is AnonymousUser
        creation_request.user = AnonymousUser()

        # Create backend with request assigned (backend needs a request to work correctly)
        backend = PortalOIDCBackend()
        backend.request = creation_request

        # Create the user using the backend
        user = backend.create_user(TEST_CLAIMS)

        # Take secret from the request (it was set by the create_user method)
        decryption_key = creation_request.aai_middleware_new_user_secret

        # 2) Create cookie for test request and log user in

        # Pretend this is a returning user, start a new request
        request = rf.get("/")

        # Assign the newly created user to this request to pretend they logged-in
        request.user = user

        # In order to use the django login helper there needs to be a session
        request.session = SessionStore()
        django.contrib.auth.login(request, user)
        request.session.create()
        user.session_id = request.session.session_key
        user.save()

        # 3) Generate a portal token
        token_json = (
            PortalToken(
                session_key=request.session.session_key,
                user_secret=decryption_key,
            )
            .model_dump_json()
            .encode("utf-8")
        )
        encrypted_portal_token = (
            Fernet(settings.PORTAL_TOKEN_ENCRYPTION_SECRET)
            .encrypt(token_json)
            .decode("utf-8")
        )
        request.COOKIES[settings.PORTAL_TOKEN_COOKIE_NAME] = encrypted_portal_token

        # 4) Do the request
        assert not hasattr(request, "aai_profile")

        def test_view(request: HttpRequest) -> HttpResponse:
            assert hasattr(request, "aai_profile")

            # Profile gets set as expected
            assert request.aai_profile is not None
            assert request.aai_profile == AAIProfile(
                vo_person_id=UUID(TEST_CLAIMS["sub"]),
                name=TEST_CLAIMS["name"],
                email=TEST_CLAIMS["email"],
                eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
                eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
                eduperson_scoped_affiliation=TEST_CLAIMS[
                    "eduperson_scoped_affiliation"
                ],
            )

            return HttpResponse()

        aai_session_encryption_middleware(test_view)(request)

    # When user is not authenticated it sets the aai_profile to None
    def test_anonymous_user_results_in_aai_profile_set_to_none(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        request = rf.get("/")
        request.user = AnonymousUser()

        # It is not set before
        assert not hasattr(request, "aai_profile")

        response = HttpResponse()
        SessionMiddleware(lambda _req: response).process_request(request)
        AuthenticationMiddleware(lambda _req: response).process_request(request)
        aai_session_encryption_middleware(lambda _req: response)(request)

        # It's set to None afterwards
        assert hasattr(request, "aai_profile")
        assert request.aai_profile is None

    # It raises when there's a user but no (saved) session
    def test_raises_when_session_is_missing(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user
        creation_request = HttpRequest()
        creation_request.user = AnonymousUser()
        backend = PortalOIDCBackend()
        backend.request = creation_request
        user = backend.create_user(TEST_CLAIMS)

        # 2) Set user on request but keep the session unsaved so the key is None
        request = rf.get("/")
        request.user = user
        request.session = SessionStore()
        request._messages = default_storage(request)  # noqa: SLF001 testing setup

        # 4) Do the request
        with pytest.raises(AAIError, match="Can't decrypt profile without a session."):
            aai_session_encryption_middleware(lambda _req: HttpResponse())(request)

    # It raises when the portal token is missing but the user is authenticated
    def test_raises_when_token_is_missing(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user and get secret from request
        creation_request = HttpRequest()
        creation_request.user = AnonymousUser()
        backend = PortalOIDCBackend()
        backend.request = creation_request
        user = backend.create_user(TEST_CLAIMS)

        # 2) Create cookie for test request and log user in
        request = rf.get("/")
        request.user = user
        request.session = SessionStore()
        request._messages = default_storage(request)  # noqa: SLF001 testing setup
        django.contrib.auth.login(request, user)
        request.session.create()
        user.session_id = request.session.session_key
        user.save()

        # 4) Do the request
        with pytest.raises(
            AAIError, match="Can't decrypt profile without a portal token."
        ):
            aai_session_encryption_middleware(lambda _req: HttpResponse())(request)

    # It raises when the portal token has correct format but wrong password
    def test_raises_when_token_contains_wrong_password(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user
        creation_request = HttpRequest()
        creation_request.user = AnonymousUser()
        backend = PortalOIDCBackend()
        backend.request = creation_request
        user = backend.create_user(TEST_CLAIMS)

        # 2) Create cookie for test request and log user in
        request = rf.get("/")
        request.user = user
        request.session = SessionStore()
        request._messages = default_storage(request)  # noqa: SLF001 testing setup
        django.contrib.auth.login(request, user)
        request.session.create()
        user.session_id = request.session.session_key
        user.save()

        # 3) Generate a portal token with a wrong password
        token_json = (
            PortalToken(
                session_key=request.session.session_key,
                # Generate a random key, so this does not match the secret
                # used for encrypting the profile in the previous step.
                user_secret=Fernet.generate_key().decode("utf-8"),
            )
            .model_dump_json()
            .encode("utf-8")
        )
        encrypted_portal_token = (
            Fernet(settings.PORTAL_TOKEN_ENCRYPTION_SECRET)
            .encrypt(token_json)
            .decode("utf-8")
        )
        request.COOKIES[settings.PORTAL_TOKEN_COOKIE_NAME] = encrypted_portal_token

        # 4) Do the request
        assert not hasattr(request, "aai_profile")

        with pytest.raises(AAIError):
            aai_session_encryption_middleware(lambda _req: HttpResponse())(request)

        assert not hasattr(request, "aai_profile")

    # It raises when the portal token does not have a correct format
    def test_raises_when_token_has_wrong_format(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user
        creation_request = HttpRequest()
        creation_request.user = AnonymousUser()
        backend = PortalOIDCBackend()
        backend.request = creation_request
        user = backend.create_user(TEST_CLAIMS)

        # 2) Create cookie for test request and log user in
        request = rf.get("/")
        request.user = user
        request.session = SessionStore()
        request._messages = default_storage(request)  # noqa: SLF001 testing setup
        django.contrib.auth.login(request, user)
        request.session.create()
        user.session_id = request.session.session_key
        user.save()

        # 3) Generate a broken portal token
        request.COOKIES[settings.PORTAL_TOKEN_COOKIE_NAME] = "broken"

        # 4) Do the request
        assert not hasattr(request, "aai_profile")

        with pytest.raises(AAIError, match="Could not decrypt token cookie"):
            aai_session_encryption_middleware(lambda _req: HttpResponse())(request)

        assert not hasattr(request, "aai_profile")

    # It raises when the portal token session ID does not match the session ID
    def test_raises_when_token_session_key_does_not_match_actual_session_key(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user and get secret from request
        creation_request = HttpRequest()
        creation_request.user = AnonymousUser()
        backend = PortalOIDCBackend()
        backend.request = creation_request
        user = backend.create_user(TEST_CLAIMS)
        decryption_key = creation_request.aai_middleware_new_user_secret

        # 2) Create cookie for test request and log user in
        request = rf.get("/")
        request.user = user
        request.session = SessionStore()
        request._messages = default_storage(request)  # noqa: SLF001 testing setup
        django.contrib.auth.login(request, user)
        request.session.create()
        user.session_id = request.session.session_key
        user.save()

        # 3) Generate a portal token with a mismatching session key
        token_json = (
            PortalToken(
                session_key=f"mismatch-{request.session.session_key}",
                user_secret=decryption_key,
            )
            .model_dump_json()
            .encode("utf-8")
        )
        encrypted_portal_token = (
            Fernet(settings.PORTAL_TOKEN_ENCRYPTION_SECRET)
            .encrypt(token_json)
            .decode("utf-8")
        )
        request.COOKIES[settings.PORTAL_TOKEN_COOKIE_NAME] = encrypted_portal_token

        # 4) Do the request
        assert not hasattr(request, "aai_profile")

        with pytest.raises(
            AAIError,
            match="Portal token's session ID and request session ID do not match!",
        ):
            aai_session_encryption_middleware(lambda _req: HttpResponse())(request)

        assert not hasattr(request, "aai_profile")

    # When the token's session ID does not match the user's session ID it raises
    def test_raises_when_user_session_key_does_not_match_token(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user and get secret from request
        creation_request = HttpRequest()
        creation_request.user = AnonymousUser()
        backend = PortalOIDCBackend()
        backend.request = creation_request
        user = backend.create_user(TEST_CLAIMS)
        decryption_key = creation_request.aai_middleware_new_user_secret

        # 2) Create cookie for test request and log user in
        request = rf.get("/")
        request.user = user
        request.session = SessionStore()
        request._messages = default_storage(request)  # noqa: SLF001 testing setup
        django.contrib.auth.login(request, user)
        request.session.create()

        # 3) Generate an unrelated session
        another_session = SessionStore()
        another_session.create()
        user.session_id = another_session.session_key
        user.save()

        # 4) Generate a portal token with the wrong session ID
        token_json = (
            PortalToken(
                session_key=request.session.session_key,
                user_secret=decryption_key,
            )
            .model_dump_json()
            .encode("utf-8")
        )
        encrypted_portal_token = (
            Fernet(settings.PORTAL_TOKEN_ENCRYPTION_SECRET)
            .encrypt(token_json)
            .decode("utf-8")
        )
        request.COOKIES[settings.PORTAL_TOKEN_COOKIE_NAME] = encrypted_portal_token

        # 4) Do the request
        with pytest.raises(
            AAIError,
            match="Portal token does not match session linked to the user!",
        ):
            aai_session_encryption_middleware(lambda _request: HttpResponse())(request)

    # Raises when session user ID and request user ID mismatch
    def test_raises_when_session_user_id_and_request_user_id_mismatch(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create two users and get secret from request
        creation_request = HttpRequest()
        creation_request.user = AnonymousUser()
        backend = PortalOIDCBackend()
        backend.request = creation_request
        user = backend.create_user(TEST_CLAIMS)
        decryption_key = creation_request.aai_middleware_new_user_secret

        del creation_request.aai_middleware_new_user_secret
        another_user = backend.create_user(TEST_CLAIMS)

        # 2) Create cookie for test request and log user in, then assign another user
        request = rf.get("/")
        request.session = SessionStore()
        request._messages = default_storage(request)  # noqa: SLF001 testing setup
        django.contrib.auth.login(request, user)
        request.session.create()
        user.session_id = request.session.session_key
        user.save()
        request.user = another_user

        # 3) Generate a portal token as usual
        token_json = (
            PortalToken(
                session_key=request.session.session_key,
                user_secret=decryption_key,
            )
            .model_dump_json()
            .encode("utf-8")
        )
        encrypted_portal_token = (
            Fernet(settings.PORTAL_TOKEN_ENCRYPTION_SECRET)
            .encrypt(token_json)
            .decode("utf-8")
        )
        request.COOKIES[settings.PORTAL_TOKEN_COOKIE_NAME] = encrypted_portal_token

        # 4) Do the request

        # Check the request is made on behalf of the (wrong) user
        assert request.user.is_authenticated
        assert request.user.id is not None

        # Assert user ID is in session
        assert request.session[django.contrib.auth.SESSION_KEY] is not None

        # Assert the request user and the session user ID mismatch
        assert request.user.id != request.session[django.contrib.auth.SESSION_KEY]

        # Assert the old session is still in the database
        old_session_key = request.session.session_key
        assert SessionStore().exists(old_session_key)

        # Assert this is does not load an AAI profile, this should not even work
        assert not hasattr(request, "aai_profile")

        # Check it raises
        with pytest.raises(
            AAIError,
            match="Session user and authenticated user mismatch! This should never happen.",
        ):
            aai_session_encryption_middleware(lambda _req: HttpResponse())(request)

        # Check the request user gets logged out in this case as a safety-measure as this should never happen
        assert not request.user.is_authenticated
        assert not SessionStore().exists(old_session_key)
        assert not request.session.has_key(django.contrib.auth.SESSION_KEY)
        assert not hasattr(request, "aai_profile")

    # Resets session if there's a SESSION_KEY in the session and user is anonymous and sets the user to None
    # This could be the case if there's residual session data when a user was deleted from the database.
    def test_resets_residual_session_information(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user
        creation_request = HttpRequest()
        creation_request.user = AnonymousUser()
        backend = PortalOIDCBackend()
        backend.request = creation_request
        user = backend.create_user(TEST_CLAIMS)

        # 2) Create cookie for test request and log user in
        request = rf.get("/")

        # Create a session with some test data in the database
        request.session = SessionStore()
        request.session["some_value"] = "test"
        django.contrib.auth.login(request, user)
        request.session.create()

        # Pretend user was deleted but the session stayed around
        user.delete()
        request.user = AnonymousUser()

        # 4) Do the request

        # Assert preconditions

        # There's no valid profile
        assert not hasattr(request, "aai_profile")

        # But there's a session
        assert request.session.session_key is not None

        # For an anonymous user
        assert request.user.is_authenticated is False

        # There is the session with some test data
        assert request.session.has_key("some_value")
        assert request.session["some_value"] == "test"
        old_session_key = request.session.session_key

        # Check session is present
        assert SessionStore().exists(old_session_key)

        # Create a dummy view getting called below. At the time this view runs the session should
        # have been cleared by the middleware.
        def dummy_view(request: HttpRequest) -> HttpResponse:
            # Assert the session was flushed, so the user is not authenticated and the custom data is gone
            assert request.aai_profile is None
            assert request.session.session_key is None
            assert request.user.is_authenticated is False
            assert not request.session.has_key("some_value")

            return HttpResponse()

        aai_session_encryption_middleware(dummy_view)(request)

        # Check again that the user is not logged in and the data is gone
        assert request.aai_profile is None
        assert request.session.session_key is None
        assert request.user.is_authenticated is False
        assert not request.session.has_key("some_value")
        assert not SessionStore().exists(old_session_key)

    #
    # middleware_after_view
    #
    # Middleware_before_view rejects django authenticated requests without a token.
    # Therefore test function directly instead of full middleware stack.
    #

    # Adds a cookie to the session which can be used to decrypt the AAI profile and assigns user to session
    def test_sets_portal_token_cookie_and_assings_user(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user and create logged-in request to simulate return from OIDC flow
        request = rf.get("/")
        request.user = AnonymousUser()
        request.session = SessionStore()
        backend = PortalOIDCBackend()
        backend.request = request
        user = backend.create_user(TEST_CLAIMS)
        django.contrib.auth.login(request, user)

        # 2) Run this through middleware_after_view
        assert hasattr(request, "aai_middleware_new_user_secret")
        assert user.session_id is None

        response = HttpResponse()
        response = middleware_after_view(request, response)

        # Reload user as it was updated by the middleware
        user = User.objects.get(id=user.id)

        assert user.session_id is not None
        assert settings.PORTAL_TOKEN_COOKIE_NAME in response.cookies

        # Decode/parse cookie
        portal_token_json = Fernet(settings.PORTAL_TOKEN_ENCRYPTION_SECRET).decrypt(
            response.cookies[settings.PORTAL_TOKEN_COOKIE_NAME].value
        )
        portal_token = PortalToken(**json.loads(portal_token_json))

        assert portal_token.session_key == request.session.session_key
        assert user.decrypt_aai_profile(portal_token.user_secret) == AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

    # Raises if session was flushed
    def test_raises_on_unsaved_session(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user and create logged-in request to simulate return from OIDC flow
        request = rf.get("/")
        request.user = AnonymousUser()
        request.session = SessionStore()
        backend = PortalOIDCBackend()
        backend.request = request
        user = backend.create_user(TEST_CLAIMS)
        django.contrib.auth.login(request, user)

        # 2) Flush the  session and run this through middleware_after_view
        request.session.flush()

        response = HttpResponse()
        with pytest.raises(AAIError, match="Session was flushed upstream."):
            response = middleware_after_view(request, response)

    # Raises if user was already assigned
    def test_raises_if_user_already_assigned(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Create user and create logged-in request to simulate return from OIDC flow
        request = rf.get("/")
        request.user = AnonymousUser()
        request.session = SessionStore()
        backend = PortalOIDCBackend()
        backend.request = request
        user = backend.create_user(TEST_CLAIMS)
        django.contrib.auth.login(request, user)

        # 2) run this through middleware_after_view
        assert user.session_id is None
        user.session_id = request.session.session_key
        user.save()

        response = HttpResponse()
        with pytest.raises(
            AAIError,
            match="Error assigning session ID to user. Number of IDs assigned: 0 User already may already have a session assigned! This should never happen.",
        ):
            response = middleware_after_view(request, response)

    # Deletes the portal token cookie if user is logged out but user still presents the cookie
    def test_removes_cookie_if_unauthenticated(
        self: "TestAAISessionEncryptionMiddleware", rf: RequestFactory
    ) -> None:
        # 1) Prepare unauthenticated request with portal token
        request = rf.get("/")
        request.user = AnonymousUser()
        request.session = SessionStore()
        backend = PortalOIDCBackend()
        backend.request = request

        request.COOKIES[settings.PORTAL_TOKEN_COOKIE_NAME] = "123456"

        # 2) run this through middleware_after_view
        response = HttpResponse()

        response = middleware_after_view(request, response)

        assert settings.PORTAL_TOKEN_COOKIE_NAME in response.cookies
        assert response.cookies[settings.PORTAL_TOKEN_COOKIE_NAME].value == ""
        assert response.cookies[settings.PORTAL_TOKEN_COOKIE_NAME]["max-age"] == 0
        assert (
            response.cookies[settings.PORTAL_TOKEN_COOKIE_NAME]["expires"]
            == "Thu, 01 Jan 1970 00:00:00 GMT"
        )
