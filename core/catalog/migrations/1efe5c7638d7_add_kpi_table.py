"""
add_kpi_table
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = "1efe5c7638d7"
down_revision = "059b3a5504f9"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "catalog_service_kpi",
        sa.Column(
            "catalog_service_id",
            UUID,
            primary_key=True,
            nullable=False,
            comment="Service's UUID",
        ),
        sa.Column(
            "user_count",
            sa.Integer,
            unique=False,
            index=False,
            primary_key=False,
            nullable=False,
            comment="Service's active user count (may be based on corrected/adjusted values)",
        ),
    )


def downgrade() -> None:
    op.drop_table("catalog_service_kpi")
