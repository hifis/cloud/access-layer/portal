from typing import Any
from uuid import uuid4

import asgiref.sync
import django.contrib.auth
from cryptography.fernet import Fernet
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.contrib.messages.storage import default_storage
from django.contrib.sessions.backends.db import SessionStore
from django.core.handlers.asgi import ASGIRequest
from django.http import HttpRequest
from django.test import AsyncRequestFactory

from web.base.auth import PortalOIDCBackend
from web.base.models import PortalToken, User

TEST_CLAIMS: dict[str, Any] = {
    "sub": str(uuid4()),
    "email": "test.user@localhost",
    "name": "Test User",
    "eduperson_entitlement": [
        "urn:geant:localhost:group:pytest#login.localhost",
        "urn:geant:localhost:group:python#login.localhost",
    ],
    "eduperson_assurance": [
        "https://localhost/verified",
    ],
    "eduperson_scoped_affiliation": ["staff"],
}


async def create_request_with_user(
    async_rf: AsyncRequestFactory, user_claims: dict[str, Any] | None = None
) -> ASGIRequest:
    """
    Generate a request with a pre-authenticated user which can be used for testing authenticated web views.
    """

    if user_claims is None:
        user_claims = TEST_CLAIMS.copy()

    # 1) Create user and get secret from request
    creation_request = HttpRequest()
    creation_request.user = AnonymousUser()
    backend = PortalOIDCBackend()
    backend.request = creation_request
    user = await asgiref.sync.sync_to_async(backend.create_user)(user_claims)
    decryption_key = creation_request.aai_middleware_new_user_secret

    # 2) Create cookie for test request and log user in
    asgi_request: ASGIRequest = async_rf.get("/")
    asgi_request.user = user

    async def auser() -> User:
        return user

    asgi_request.auser = auser

    asgi_request.session = SessionStore()
    asgi_request._messages = default_storage(asgi_request)  # noqa: SLF001 testing setup
    await django.contrib.auth.alogin(asgi_request, user)
    await asgiref.sync.sync_to_async(asgi_request.session.create)()
    user.session_id = asgi_request.session.session_key
    await user.asave()

    # 3) Generate a portal token
    token_json = (
        PortalToken(
            session_key=asgi_request.session.session_key,
            user_secret=decryption_key,
        )
        .model_dump_json()
        .encode("utf-8")
    )
    encrypted_portal_token = (
        Fernet(settings.PORTAL_TOKEN_ENCRYPTION_SECRET)
        .encrypt(token_json)
        .decode("utf-8")
    )
    asgi_request.COOKIES[settings.PORTAL_TOKEN_COOKIE_NAME] = encrypted_portal_token

    # 4) Set AAI profile (normally the middleware would do this)
    asgi_request.aai_profile = user.decrypt_aai_profile(decryption_key)

    return asgi_request
