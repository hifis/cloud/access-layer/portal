# This file is in a subdirectory because otherwise Django is going to try to import this file if it is in management/commands

from datetime import timedelta
from io import StringIO
from uuid import uuid4

import pytest
from django.contrib.sessions.backends.db import SessionStore
from django.utils import timezone

from web.base.management.commands.clearusers import Command
from web.base.models import User


class TestClearusers:
    # does nothing if it does not have to
    @pytest.mark.django_db
    def test_does_not_do_anything_if_there_are_no_expired_users(
        self: "TestClearusers",
    ) -> None:
        output_io = StringIO()
        session = SessionStore()
        session.create()
        User(
            id=uuid4(),
            session_id=session.session_key,
            fallback_expire_at=timezone.now() + timedelta(hours=1),
        ).save()
        Command(stdout=output_io).handle()
        captured_output = output_io.getvalue()

        assert (
            captured_output
            == "Clearing expired sessions...\nOK!\nRemoving leftover expired users...\nNo leftover users found. OK!\n"
        )

    # clears old sessions
    @pytest.mark.django_db
    def test_clears_old_sessions_and_user(self: "TestClearusers") -> None:
        output_io = StringIO()
        session = SessionStore()
        session.set_expiry(timezone.now() - timedelta(hours=1))
        session.create()
        User(
            id=uuid4(),
            session_id=session.session_key,
            fallback_expire_at=timezone.now() + timedelta(hours=1),
        ).save()

        assert User.objects.count() == 1
        assert session.model.objects.count() == 1

        Command(stdout=output_io).handle()

        assert User.objects.count() == 0
        assert session.model.objects.count() == 0

        captured_output = output_io.getvalue()

        assert (
            captured_output
            == "Clearing expired sessions...\nOK!\nRemoving leftover expired users...\nNo leftover users found. OK!\n"
        )

    # clears expired users, even if their session is not expired (session could expire between session delete and user delete, highly unlikely though)
    @pytest.mark.django_db
    def test_clears_expired_users_with_session_by_fallback(
        self: "TestClearusers",
    ) -> None:
        output_io = StringIO()
        session = SessionStore()
        session.create()
        User(
            id=uuid4(),
            session_id=session.session_key,
            fallback_expire_at=timezone.now() - timedelta(hours=1),
        ).save()

        assert User.objects.count() == 1
        assert session.model.objects.count() == 1

        Command(stdout=output_io).handle()

        # User gets deleted
        assert User.objects.count() == 0

        # Session stays around but user is gone
        assert session.model.objects.count() == 1

        captured_output = output_io.getvalue()

        assert (
            captured_output
            == "Clearing expired sessions...\nOK!\nRemoving leftover expired users...\nDeleted 1 leftover user(s). This may indicate a bug in user handling!\n"
        )

    # clears users where the session is none, this can happen if the middleware or view crashes during user creation, log this
    @pytest.mark.django_db
    def test_clears_expired_users_without_session_by_fallback(
        self: "TestClearusers",
    ) -> None:
        output_io = StringIO()
        User(
            id=uuid4(),
            session_id=None,
            fallback_expire_at=timezone.now() - timedelta(hours=1),
        ).save()

        assert User.objects.count() == 1

        Command(stdout=output_io).handle()

        assert User.objects.count() == 0

        captured_output = output_io.getvalue()

        assert (
            captured_output
            == "Clearing expired sessions...\nOK!\nRemoving leftover expired users...\nDeleted 1 leftover user(s). This may indicate a bug in user handling!\n"
        )
