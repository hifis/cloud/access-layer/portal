"""portal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.urls import include, path

import web.base.views
import web.helpdesk.views
import web.resources.views
import web.services.views

urlpatterns = [
    # Base
    path("", web.base.views.index, name="index"),
    path("privacy", web.base.views.privacy_policy, name="privacy_policy"),
    path("terms-of-use", web.base.views.terms_of_use, name="terms_of_use"),
    # Services
    path("services/", web.services.views.services, name="services"),
    path(
        "services/<uuid:service_id>/_profile/",
        web.services.views.service_profile,
        name="service_profile",
    ),
    path(
        "services/<uuid:service_id>/logo/",
        web.services.views.service_logo,
        name="service_logo",
    ),
    path(
        "services/provider/<uuid:provider_id>/logo/",
        web.services.views.provider_logo,
        name="provider_logo",
    ),
    # Resource Management
    path(
        "resources/",
        web.resources.views.resources,
        name="view_resources",
    ),
    path(
        "resources/request/",
        web.resources.views.request_resource,
        name="request_resource",
    ),
    path(
        "resources/request/<uuid:service_id>/<uuid:resource_type_id>/",
        web.resources.views.request_form,
        name="resource_request_form",
    ),
    # Metrics / liveness
    path(
        "internal/healthz/",
        web.base.views.healthz,
        name="healthz",
    ),
    path(
        "internal/readyz/",
        web.base.views.readyz,
        name="readyz",
    ),
    # Send request to open support ticket
    path(
        "helpdesk/",
        web.helpdesk.views.open_support_ticket,
        name="open_support_ticket",
    ),
    # Show deprovisioning confirmation page
    path(
        "helpdesk/deprovisioning/<uuid:service_id>/confirm",
        web.helpdesk.views.confirm,
        name="confirm_deprovisioning_request",
    ),
    # Extensions
    path("oidc/", include("mozilla_django_oidc.urls")),
    path("internal/", include("django_prometheus.urls")),
]

# Custom error pages
handler400 = "web.base.views.error400"
handler403 = "web.base.views.error403"
handler404 = "web.base.views.error404"
handler500 = "web.base.views.error500"

# Auto-reload templates in development
if settings.DEBUG:
    urlpatterns += [path("__reload__/", include("django_browser_reload.urls"))]
