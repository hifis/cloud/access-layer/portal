import { VueHeadMixin, createHead } from "@unhead/vue";
import { createApp } from "vue";

import ServiceGrid from "./serviceGrid.vue";

if (document.getElementById("service-grid")) {
    const app = createApp(ServiceGrid);

    app.use(createHead());
    app.mixin(VueHeadMixin);

    app.mount("#service-grid");
}
