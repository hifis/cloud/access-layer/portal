# Comparing to magic values is part of testing examples
# ruff: noqa: PLR2004


import json
from http import HTTPStatus

import django.contrib.messages
import pydantic
import pytest
from django.contrib.auth.models import AnonymousUser
from django.contrib.messages import Message, get_messages
from django.core.handlers.asgi import ASGIRequest
from django.core.serializers.json import DjangoJSONEncoder

import core.catalog.testing
import web
from web.resources import views


async def prepare_basic_request(
    request_from_test_case: ASGIRequest,
) -> tuple[ASGIRequest, pydantic.UUID4]:
    """
    Prepare a basic request for testing.
    Also returns policy ID as it is needed by form data.
    """

    provider = core.catalog.testing.make_provider()
    await core.catalog.testing.create_or_update_provider(provider=provider)

    service = core.catalog.testing.make_service(provider_id=provider.id)
    await core.catalog.testing.create_or_update_service(service=service)

    resource_type = core.catalog.testing.make_resource_type()
    resource_type.json_schema = {"properties": {"test": {"type": "string"}}}
    resource_type.ui_schema = {}
    await core.catalog.testing.create_or_update_resource_type(
        resource_type=resource_type
    )

    quota = core.catalog.testing.make_quota(
        service_id=service.id, resource_type_id=resource_type.id
    )
    await core.catalog.testing.create_or_update_quota(quota=quota)

    policy = core.catalog.testing.make_policy(quota_id=quota.id)
    policy.target_entity = "self"
    policy.json_schema = {"properties": {}}
    policy.actor_requirements = core.catalog.PolicyActorRequirements(
        edu_person_entitlement=request_from_test_case.aai_profile.eduperson_entitlements,
        edu_person_assurance=request_from_test_case.aai_profile.eduperson_assurances,
        edu_person_scoped_affiliation=request_from_test_case.aai_profile.eduperson_scoped_affiliation,
    )
    await core.catalog.testing.create_or_update_policy(policy=policy)

    request_from_test_case.method = "POST"

    return (request_from_test_case, policy.id)


@pytest.mark.usefixtures("event_loop", "_core_database")
@pytest.mark.django_db
class TestRequestForm:
    # Test base case
    @pytest.mark.asyncio
    async def test_base_case(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        (request, policy_id) = await prepare_basic_request(request_with_user)
        request._body = web.resources.views.FormResourceRequest(  # noqa: SLF001 there is no setter
            name="Test",
            target_entity="self",
            catalog_policy_id=policy_id,
            specification={"test": "test"},
            accept_terms=True,
        ).model_dump_json()
        response = await views.request_resource(request)

        assert response.status_code == HTTPStatus.CREATED
        assert response.content == b"{}"
        assert response.headers["content-type"] == "application/json"
        assert list(get_messages(request)) == [
            Message(
                level=django.contrib.messages.SUCCESS,
                message="Thank you for your request! Next, we're forwarding your request to the service provider for review and provisionig. You can keep track of your request's status on this page.",
            )
        ]

    # Test POST is accepted, GET is rejected
    @pytest.mark.asyncio
    async def test_does_not_accept_get_request(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        response = await views.request_resource(request_with_user)

        assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED
        assert response.content == b""

    # Test user must be authenticated to call view
    @pytest.mark.asyncio
    async def test_user_must_be_authenticated(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        anonymous_user = AnonymousUser()
        request_with_user.user = anonymous_user

        async def auser() -> AnonymousUser:
            return anonymous_user

        request_with_user.auser = auser

        request_with_user.method = "POST"

        response = await views.request_resource(request_with_user)

        assert response.status_code == HTTPStatus.UNAUTHORIZED
        assert response.headers["content-type"] == "application/json"
        assert (
            response.content == b'{"status_text": "You need to sign in to continue."}'
        )

    # Test resource request must be valid (smoke test like base case, just submit wrong property type in specification)
    # Resource request validation logic is tested in core in detail
    @pytest.mark.asyncio
    async def test_resource_request_validation(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        (request, policy_id) = await prepare_basic_request(request_with_user)
        request._body = web.resources.views.FormResourceRequest(  # noqa: SLF001 there is no setter
            name="Test",
            target_entity="self",
            catalog_policy_id=policy_id,
            specification={"test": 1},
            accept_terms=True,
        ).model_dump_json()
        response = await views.request_resource(request)

        assert response.status_code == HTTPStatus.INTERNAL_SERVER_ERROR
        assert response.content == b'{"status_text": "Internal server error."}'
        assert response.headers["content-type"] == "application/json"

    # Test display name validation
    @pytest.mark.asyncio
    async def test_display_name_validation(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        (request, policy_id) = await prepare_basic_request(request_with_user)

        test_data = web.resources.views.FormResourceRequest(
            name="Test",
            target_entity="self",
            catalog_policy_id=policy_id,
            specification={"test": 1},
            accept_terms=True,
        )

        broken_data = test_data.model_dump()
        broken_data["name"] = ""

        request._body = json.dumps(broken_data, cls=DjangoJSONEncoder)  # noqa: SLF001 there is no setter

        response = await views.request_resource(request_with_user)
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert (
            response.content
            == b'{"status_text": "The data submitted is formatted incorrectly."}'
        )
        assert response.headers["content-type"] == "application/json"

    # Test checkbox validation
    @pytest.mark.asyncio
    async def test_accept_terms_validation(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        (request, policy_id) = await prepare_basic_request(request_with_user)

        test_data = web.resources.views.FormResourceRequest(
            name="Test",
            target_entity="self",
            catalog_policy_id=policy_id,
            specification={"test": 1},
            accept_terms=True,
        )

        broken_data = test_data.model_dump()
        broken_data["accept_terms"] = False

        request._body = json.dumps(broken_data, cls=DjangoJSONEncoder)  # noqa: SLF001 there is no setter

        response = await views.request_resource(request_with_user)
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert (
            response.content
            == b'{"status_text": "The data submitted is formatted incorrectly."}'
        )
        assert response.headers["content-type"] == "application/json"

    @pytest.mark.asyncio
    async def test_accept_terms_must_be_present(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        (request, policy_id) = await prepare_basic_request(request_with_user)

        test_data = web.resources.views.FormResourceRequest(
            name="Test",
            target_entity="self",
            catalog_policy_id=policy_id,
            specification={"test": 1},
            accept_terms=True,
        )

        broken_data = test_data.model_dump()
        del broken_data["accept_terms"]

        request._body = json.dumps(broken_data, cls=DjangoJSONEncoder)  # noqa: SLF001 there is no setter

        response = await views.request_resource(request_with_user)
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert (
            response.content
            == b'{"status_text": "The data submitted is formatted incorrectly."}'
        )
        assert response.headers["content-type"] == "application/json"

    @pytest.mark.asyncio
    async def test_accept_terms_type_checking(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        (request, policy_id) = await prepare_basic_request(request_with_user)

        test_data = web.resources.views.FormResourceRequest(
            name="Test",
            target_entity="self",
            catalog_policy_id=policy_id,
            specification={"test": 1},
            accept_terms=True,
        )

        broken_data = test_data.model_dump()
        broken_data["accept_terms"] = "True"

        request._body = json.dumps(broken_data, cls=DjangoJSONEncoder)  # noqa: SLF001 there is no setter

        response = await views.request_resource(request_with_user)
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert (
            response.content
            == b'{"status_text": "The data submitted is formatted incorrectly."}'
        )
        assert response.headers["content-type"] == "application/json"

    # Test quota can't be exceeded
    @pytest.mark.asyncio
    async def test_quota_limit(
        self: "TestRequestForm", request_with_user: ASGIRequest
    ) -> None:
        provider = core.catalog.testing.make_provider()
        await core.catalog.testing.create_or_update_provider(provider=provider)

        service = core.catalog.testing.make_service(provider_id=provider.id)
        await core.catalog.testing.create_or_update_service(service=service)

        resource_type = core.catalog.testing.make_resource_type()
        resource_type.json_schema = {"properties": {"limited": {"type": "integer"}}}
        resource_type.ui_schema = {}
        await core.catalog.testing.create_or_update_resource_type(
            resource_type=resource_type
        )

        quota = core.catalog.testing.make_quota(
            service_id=service.id, resource_type_id=resource_type.id
        )
        quota.quota = [core.catalog.QuotaLimit(property="limited", total=3)]
        await core.catalog.testing.create_or_update_quota(quota=quota)

        policy = core.catalog.testing.make_policy(quota_id=quota.id)
        policy.target_entity = "self"
        policy.json_schema = {"properties": {}}
        policy.actor_requirements = core.catalog.PolicyActorRequirements(
            edu_person_entitlement=request_with_user.aai_profile.eduperson_entitlements,
            edu_person_assurance=request_with_user.aai_profile.eduperson_assurances,
            edu_person_scoped_affiliation=request_with_user.aai_profile.eduperson_scoped_affiliation,
        )
        await core.catalog.testing.create_or_update_policy(policy=policy)

        request_with_user.method = "POST"

        request_with_user._body = web.resources.views.FormResourceRequest(  # noqa: SLF001 there is no setter
            name="Test",
            target_entity="self",
            catalog_policy_id=policy.id,
            specification={"limited": 4},
            accept_terms=True,
        ).model_dump_json()

        response = await views.request_resource(request_with_user)
        assert response.status_code == HTTPStatus.FORBIDDEN
