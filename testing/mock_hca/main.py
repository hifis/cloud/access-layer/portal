import logging
import uuid

from helmholtz_cloud_agent.core.main import HCAApplication
from helmholtz_cloud_agent.messages import (
    ResourceAllocateV1,
    ResourceCreatedV1,
)

MOCK_HCA_DENY_VALUE = 3

# Global logger
logging.basicConfig(
    format="%(asctime)s\t%(levelname)s\t%(filename)s\t%(funcName)s\t%(message)s"
)

logger = logging.getLogger("hca")


class ResourceAlreadyExistsError(Exception):
    def __init__(
        self: "ResourceAlreadyExistsError",
        message: str = "Resource already exists",
    ) -> None:
        self.message = message
        super().__init__(self.message)

    def __str__(self: "ResourceAlreadyExistsError") -> str:
        return f"{self.message}"


app = HCAApplication(run_once=True)


# a simple message handler that replies randomly with ResourceCreatedV1 or returns an exception dependent on the testProperty field.
@app.handle(message_type="ResourceAllocateV1")  # type: ignore[misc]
def resource_allocate_v1(
    correlation_id: str, payload: ResourceAllocateV1
) -> ResourceCreatedV1:
    msg = f"Got ResourceAllocateV1 request for {correlation_id} payload: {payload}"
    logger.info(msg)

    if payload.specification["testProperty"] == MOCK_HCA_DENY_VALUE:
        raise ResourceAlreadyExistsError

    resource_id = str(uuid.uuid4())
    return ResourceCreatedV1(id=resource_id)


def start_mock_hca() -> None:
    app.run()
