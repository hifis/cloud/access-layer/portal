# Comparing to magic values is part of testing examples
# ruff: noqa: PLR2004


from uuid import UUID

import pytest

import core.identity


class TestActorIdentity:
    def test_serialization(self: "TestActorIdentity") -> None:
        test_identity = core.identity.ActorIdentity(
            vo_person_id=UUID("2ee824b9-8681-4d44-b6e1-a17db75852d3"),
            edu_person_entitlement=["urn:geant:example.com:team#login.helmholtz.de"],
            edu_person_assurance=["https://localhost/assurance"],
            edu_person_scoped_affiliation=["staff"],
        )

        # Test regular serialization
        assert (
            test_identity.model_dump_json()
            == '{"vo_person_id":"2ee824b9-8681-4d44-b6e1-a17db75852d3","edu_person_entitlement":["urn:geant:example.com:team#login.helmholtz.de"],"edu_person_assurance":["https://localhost/assurance"],"edu_person_scoped_affiliation":["staff"]}'
        )

        # test aliases mirroring the Helmholtz ID output
        assert (
            test_identity.model_dump_json(by_alias=True)
            == '{"voPersonID":"2ee824b9-8681-4d44-b6e1-a17db75852d3","eduPersonEntitlement":["urn:geant:example.com:team#login.helmholtz.de"],"eduPersonAssurance":["https://localhost/assurance"],"eduPersonScopedAffiliation":["staff"]}'
        )

    def test_group_memberships(self: "TestActorIdentity") -> None:
        # Test Empty and None case
        test_identity = core.identity.ActorIdentity(
            vo_person_id=UUID("2ee824b9-8681-4d44-b6e1-a17db75852d3"),
            edu_person_entitlement=[],
            edu_person_assurance=["https://localhost/assurance"],
            edu_person_scoped_affiliation=["staff"],
        )

        assert (
            test_identity.get_group_memberships(
                authoritative_directory="login.helmholtz.de"
            )
            == []
        )

        test_identity = core.identity.ActorIdentity(
            vo_person_id=UUID("2ee824b9-8681-4d44-b6e1-a17db75852d3"),
            edu_person_entitlement=None,
            edu_person_assurance=["https://localhost/assurance"],
            edu_person_scoped_affiliation=["staff"],
        )

        assert (
            test_identity.get_group_memberships(
                authoritative_directory="login.helmholtz.de"
            )
            == []
        )

        # Test a variety of formats
        test_identity = core.identity.ActorIdentity(
            vo_person_id=UUID("2ee824b9-8681-4d44-b6e1-a17db75852d3"),
            edu_person_entitlement=[
                # Broken formats
                "urn:geant:example.com:team1",
                "urn:geant:example.com:team2#",
                # Wrong authority
                "urn:geant:example.com:login.helmholtz.de:team3#hidah",
                "urn:geant:example.com:team4#hidah",
                # OK
                "urn:geant:example.com:team5#login.helmholtz.de",
                "urn:geant:example.com:team6#login.helmholtz.de",
            ],
            edu_person_assurance=["https://localhost/assurance"],
            edu_person_scoped_affiliation=["staff"],
        )

        assert test_identity.get_group_memberships(
            authoritative_directory="login.helmholtz.de"
        ) == [
            "urn:geant:example.com:team5",
            "urn:geant:example.com:team6",
        ]


@pytest.mark.usefixtures("_context_database")
@pytest.mark.asyncio
class TestGroupImport:
    @staticmethod
    @pytest.mark.parametrize(
        "urn",
        [
            # missing namespace
            ("broken"),
            # wrong namespace
            ("broken:urn"),
            # contains group but missing namespace and group
            ("broken:urn:group"),
            # wrong namespace
            ("broken:urn:group:abc"),
            # double ::
            ("urn:geant::group:subgroup:team"),
            # ends with :
            ("urn:geant:example.com:group:subgroup:team:"),
            # entitlement
            (
                "urn:geant:example.com:helmholz.de:demo:more:group:subgroup:moresubgroup:extra:team#login.helmholtz.de"
            ),
            # none
            None,
            # empty string
            "",
        ],
    )
    async def test_broken_urns_are_not_imported(urn: str) -> None:
        with pytest.raises(RuntimeError, match="Stopping import! Invalid urn."):
            await core.identity.import_group_urns([urn])

    @staticmethod
    @pytest.mark.parametrize(
        ("first_import", "second_import", "expected_db_state"),
        [
            # test add
            (
                [
                    "urn:geant:example.com:group:test",
                    "urn:geant:example.com:group:test2",
                ],
                None,
                [
                    "urn:geant:example.com:group:test",
                    "urn:geant:example.com:group:test2",
                ],
            ),
            # test remove
            (
                [
                    "urn:geant:example.com:group:test",
                    "urn:geant:example.com:group:test2",
                ],
                ["urn:geant:example.com:group:test"],
                ["urn:geant:example.com:group:test"],
            ),
            # test update
            (
                [
                    "urn:geant:example.com:group:test",
                    "urn:geant:example.com:group:test2",
                ],
                [
                    "urn:geant:example.com:group:test",
                    "urn:geant:example.com:group:test3",
                ],
                [
                    "urn:geant:example.com:group:test",
                    "urn:geant:example.com:group:test3",
                ],
            ),
            # test []
            (
                [],
                None,
                [],
            ),
            (
                [
                    "urn:geant:example.com:group:test",
                    "urn:geant:example.com:group:test2",
                ],
                [],
                [],
            ),
        ],
    )
    async def test_working_import(
        first_import: list[str],
        second_import: list[str] | None,
        expected_db_state: list[str],
    ) -> None:
        # Imports
        await core.identity.import_group_urns(first_import)

        if second_import is not None:
            await core.identity.import_group_urns(second_import)

        # Check
        assert (await core.identity.get_group_urns()) == expected_db_state
