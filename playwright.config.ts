import type { PlaywrightTestConfig } from "@playwright/test";
const config: PlaywrightTestConfig = {
    testDir: "e2e_tests",
    reporter: process.env.CI ? [["list"], ["junit", { outputFile: "./report.xml" }]] : "list",
    retries: 2,
    fullyParallel: true,
    use: {
        viewport: { width: 1920, height: 1080 },
        video: {
            mode: "retain-on-failure",
            size: { width: 1920, height: 1080 },
        },
        screenshot: "only-on-failure",
        ignoreHTTPSErrors: false,
        baseURL: process.env.PLAYWRIGHT_BASE_URL ? process.env.PLAYWRIGHT_BASE_URL : "http://localhost:8000",
        httpCredentials: {
            username: process.env.PLAYWRIGHT_BASIC_AUTH_USER ? process.env.PLAYWRIGHT_BASIC_AUTH_USER : "",
            password: process.env.PLAYWRIGHT_BASIC_AUTH_PASSWORD ? process.env.PLAYWRIGHT_BASIC_AUTH_PASSWORD : "",
        },
    },
};

// biome-ignore lint/style/noDefaultExport: This is how Playwright is configured
export default config;
