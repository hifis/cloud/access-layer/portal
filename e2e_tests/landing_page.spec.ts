import { expect, test } from "@playwright/test";

test("Test basic landing page contents", async ({ page }) => {
    const response = await page.goto("/");
    expect(response?.status()).toBe(200);

    await expect(page).toHaveTitle("Cloud Platform for Science - Helmholtz Cloud");

    const headline = await page.locator("h1");
    await expect(headline).toHaveText("Power your research with the cloud built for science.");

    const hero = await headline.locator("..");
    await expect(hero).toContainText("Take your research project from proposal to publication with cloud-based tools.");

    const allButton = await hero.getByText("Browse Services").locator("..");
    await expect(allButton).toHaveAttribute("href", "/services/");
});
