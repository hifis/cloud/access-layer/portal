import { expect, test } from "@playwright/test";

test("Test terms of use page contents", async ({ page }) => {
    const response = await page.goto("/terms-of-use");
    expect(response?.status()).toBe(200);

    await expect(page).toHaveTitle("Terms of Use - Helmholtz Cloud");

    const englishSection = await page.locator("#english");
    await expect(englishSection).toContainText("Terms of Use");

    const germanSection = await page.locator("#german");
    await expect(germanSection).toContainText("Nutzungsbedingungen");
});
