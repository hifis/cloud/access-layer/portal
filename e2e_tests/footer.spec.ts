import { expect, test } from "@playwright/test";

test("footer", async ({ page }) => {
    const response = await page.goto("/");
    expect(response?.status()).toBe(200);

    const nav = await page.locator("footer");

    await expect(nav.getByText("Imprint")).toHaveAttribute("href", "https://www.desy.de/imprint");
    await expect(nav.getByText("Privacy")).toHaveAttribute("href", "/privacy");
    await expect(nav.getByText("Declaration of Accessibility")).toHaveAttribute(
        "href",
        "https://www.desy.de/declaration_of_accessibility/index_eng.html",
    );
});
