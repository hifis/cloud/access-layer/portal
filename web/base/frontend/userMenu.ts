export function setupUserMenu() {
    // This function sets up the event handlers for the user menu dropdown.

    const button = document.getElementById("user-menu-button");

    if (button !== null) {
        button.addEventListener("click", toggleMenu);
        button.addEventListener("keypress", (event: KeyboardEvent) => {
            if (event.key === "Enter") {
                toggleMenu();
            }
        });
        document.addEventListener("click", (event) => {
            // This is triggered whenever some other area of the page is clicked
            if (!button.contains(event.target as Node)) {
                closeMenu();
            }
        });
    }
}

function toggleMenu() {
    if (document.getElementById("user-menu-dropdown")?.classList.contains("lg:hidden")) {
        document.getElementById("user-menu-dropdown")?.classList.remove("lg:hidden");
    } else {
        document.getElementById("user-menu-dropdown")?.classList.add("lg:hidden");
    }
}

function closeMenu() {
    if (!document.getElementById("user-menu-dropdown")?.classList.contains("lg:hidden")) {
        document.getElementById("user-menu-dropdown")?.classList.add("lg:hidden");
    }
}
