"""
add the rest of the catalog entities
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = "af948fa666a0"
down_revision = "006161f359c0"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "catalog_provider",
        sa.Column(
            "id", UUID, primary_key=True, nullable=False, comment="Provider's UUID"
        ),
        sa.Column(
            "title",
            sa.Text,
            nullable=False,
            comment="Title text",
        ),
        sa.Column(
            "logo_filename",
            sa.Text,
            nullable=True,
            comment="The logo's filename",
        ),
        sa.Column(
            "logo_content_type",
            sa.Text,
            nullable=True,
            comment="The logo's content type",
        ),
        sa.Column(
            "logo_data",
            sa.LargeBinary,
            nullable=True,
            comment="The logo's binary data",
        ),
    )

    op.create_table(
        "catalog_service",
        sa.Column(
            "id",
            UUID,
            primary_key=True,
            nullable=False,
            comment="Service's UUID (UID in plony)",
        ),
        sa.Column(
            "plony_at_id",
            sa.Text,
            unique=True,
            index=True,
            nullable=False,
            comment="Plony @id",
        ),
        sa.Column(
            "plony_review_state",
            sa.Text,
            nullable=False,
            comment="Service's plony review state (service_online means it's ready)",
        ),
        sa.Column(
            "title",
            sa.Text,
            nullable=False,
            comment="Title text",
        ),
        sa.Column(
            "description",
            sa.Text,
            nullable=False,
            comment="Description",
        ),
        sa.Column(
            "general_long_description",
            sa.Text,
            nullable=False,
            comment="A long-form description of the service",
        ),
        sa.Column(
            "general_short_text_cloud_portal",
            sa.Text,
            nullable=False,
            comment="A short description of the service for use by the portal",
        ),
        sa.Column(
            "general_link_service_usage",
            sa.Text,
            nullable=False,
            comment="The primary link for users to get started with the service (e.g. sign-up page)",
        ),
        sa.Column(
            "general_software_name",
            sa.Text,
            nullable=False,
            comment="The comma-separated name(s) of the software the service is based on",
        ),
        sa.Column(
            "backup_strategy",
            sa.Text,
            nullable=False,
            comment="Description of the service's backup strategy",
        ),
        sa.Column(
            "communication_contact_user_support",
            sa.Text,
            nullable=False,
            comment="User support contact details (e.g. an email address)",
        ),
        sa.Column(
            "service_lvl_description",
            sa.Text,
            nullable=True,
            comment="Description of the service level (e.g. uptime...)",
        ),
        sa.Column(
            "user_limitations",
            sa.Text,
            nullable=False,
            comment="Description of limits placed on the service usage (e.g. quotas)",
        ),
        sa.Column(
            "user_enablement",
            sa.Text,
            nullable=False,
            comment="Description of how to access the service (e.g. special signup procedure)",
        ),
        sa.Column(
            "data_storage_location",
            sa.Text,
            nullable=False,
            comment="Description of the physical location service data is stored at",
        ),
        sa.Column(
            "catalog_provider_id",
            UUID,
            # Prevent deleting providers if they have services attached
            sa.ForeignKey("catalog_provider.id", ondelete="RESTRICT"),
            # There are no services without providers
            nullable=False,
            comment="The service's provider's UUID",
            index=True,
        ),
        sa.Column(
            "general_service_logo_filename",
            sa.Text,
            nullable=True,
            comment="The logo's filename",
        ),
        sa.Column(
            "general_service_logo_content_type",
            sa.Text,
            nullable=True,
            comment="The logo's content type",
        ),
        sa.Column(
            "general_service_logo_data",
            sa.LargeBinary,
            nullable=True,
            comment="The logo's binary data",
        ),
    )

    op.create_table(
        "catalog_service_keyword",
        sa.Column(
            "catalog_service_id",
            UUID,
            # Remove linking table entry when service is deleted
            sa.ForeignKey("catalog_service.id", ondelete="CASCADE"),
            primary_key=True,
            nullable=False,
            comment="The service the keyword should be linked to",
        ),
        sa.Column(
            "catalog_keyword_id",
            UUID,
            # Remove linking table entry when keyword is deleted
            sa.ForeignKey("catalog_keyword.id", ondelete="CASCADE"),
            primary_key=True,
            nullable=False,
            comment="The UUID of the keyword",
        ),
    )


def downgrade() -> None:
    op.drop_table("catalog_service_keyword")
    op.drop_table("catalog_service")
    op.drop_table("catalog_provider")
