# This file is in a subdirectory because otherwise Django is going to try to import this file if it is in templatetags.

from web.base.templatetags.is_email import is_email


def test_detects_email_format() -> None:
    assert is_email("test@example.com") is True
    assert is_email("test@desy") is False
    assert is_email("test") is False
    assert is_email(None) is False
