"""
make_service_link_optional
"""

from alembic import op

# revision identifiers, used by Alembic.
revision = "443b140e7762"
down_revision = "af948fa666a0"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column("catalog_service", "general_link_service_usage", nullable=True)


def downgrade() -> None:
    op.execute("DELETE FROM catalog_service WHERE general_link_service_usage is NULL;")
    op.alter_column("catalog_service", "general_link_service_usage", nullable=False)
