from datetime import UTC, datetime, timedelta

import pytest

import core.app
import core.maintenance


@pytest.mark.usefixtures("_context_database")
class TestMaintenanceMessage:
    @staticmethod
    @pytest.mark.asyncio
    async def test_maintenance_message() -> None:
        # Check all the basic cases of maintenance message
        # (None set, set but expired, overwrite an expired message...)

        # At the beginning there's no message
        message = await core.maintenance.get_current_maintenance_message()
        assert message is None

        # Does not return a message if it is expired
        one_hour_ago = datetime.now(tz=UTC) - timedelta(hours=1)
        await core.maintenance.set_maintenance_message("test expired", one_hour_ago)

        message = await core.maintenance.get_current_maintenance_message()
        assert message is None

        # Returns a message if it is not expired
        one_hour_from_now = datetime.now(tz=UTC) + timedelta(hours=1)
        await core.maintenance.set_maintenance_message("test", one_hour_from_now)

        message = await core.maintenance.get_current_maintenance_message()
        assert message is not None
        assert message.show_until == one_hour_from_now
        assert message.message == "test"
