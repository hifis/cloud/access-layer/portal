<script lang="ts">
// Multi-step form for requesting resources

import { type Schema, type ValidationResult, Validator } from "@cfworker/json-schema";
import { RadioGroup, RadioGroupOption } from "@headlessui/vue";
import { JsonForms, type JsonFormsChangeEvent } from "@jsonforms/vue";
import { vanillaRenderers } from "@jsonforms/vue-vanilla";
import type { ErrorObject } from "ajv";

import { prettifyGroupURN } from "./formatting";

import type { UUID } from "node:crypto";
import type { ControlElement, Layout, UISchemaElement } from "@jsonforms/core";

// Data structures received from the view
interface FormData {
    service: Service;
    resourceType: ResourceType;
    policies: Policy[];
    quotas: Quota[];
    groups: string[];
}

interface ResourceType {
    id: UUID;
    name: string;
    jsonSchema: Schema;
    uiSchema: Layout;
}

interface Service {
    title: string;
}

interface Policy {
    id: string;
    name: string;
    targetEntity: string;
    catalogQuotaID: string;
    jsonSchema: Schema;
}

interface Quota {
    id: string;
    name: string;
}

export enum FormStep {
    PickTarget = 0,
    PickPolicy = 1,
    EnterRequestData = 2,
}

const renderers = [...vanillaRenderers];

function convertLocalValidationResult(result: ValidationResult): ErrorObject[] {
    // Converts between the validation library and the format JSON Forms expects
    // https://jsonforms.io/docs/validation/

    return result.errors.map((info) => {
        return {
            instancePath: info.instanceLocation.replace("#", ""),
            message: info.keyword === "pattern" ? "Your input does not match the required format." : info.error,
            schemaPath: "",
            keyword: info.keyword,
            params: {},
        };
    });
}

// biome-ignore lint/style/noDefaultExport: Vue Components use default exports
export default {
    // biome-ignore lint/complexity/noExcessiveCognitiveComplexity: Splitting this into functions would make it harder to read.
    data() {
        // Load form information from data attribute
        const formComponent = document.getElementById("request-form");
        if (formComponent === null) {
            throw new Error("Could not find form-component element in DOM! Ensure it is present before trying to mount the component.");
        }

        const formDataJSON = formComponent.dataset.formData;
        if (formDataJSON === undefined) {
            throw new Error("Form component data attribute is missing!");
        }

        const formData = JSON.parse(formDataJSON) as FormData;

        const allGroups = new Set(formData.groups);

        // Collect all possible target entities
        const allTargets: Set<string> = new Set();

        for (const policy of formData.policies) {
            if (policy.targetEntity === "self") {
                // 'self', simply add it

                allTargets.add(policy.targetEntity);
            } else if (policy.targetEntity.endsWith(":")) {
                // Wildcard
                // Collect all group URNs starting with the wildcard

                for (const group of allGroups) {
                    if (group.startsWith(policy.targetEntity)) {
                        allTargets.add(group);
                    }
                }
            } else if (allGroups.has(policy.targetEntity)) {
                // Fully qualified URN, check if exists and then add it

                allTargets.add(policy.targetEntity);
            }
        }

        // Collect all group targets (excluding 'self')
        const allGroupTargets: string[] = [];
        for (const target of allTargets) {
            if (target !== "self") {
                allGroupTargets.push(target);
            }
        }

        // Collect all quotas by ID
        const quotasByID: Map<string, Quota> = new Map();
        for (const quota of formData.quotas) {
            quotasByID.set(quota.id, quota);
        }

        return {
            //
            // Backend data input
            //

            // The ResourceType this request is about
            resourceType: formData.resourceType,

            // The service this request is about
            service: formData.service,

            // All policies available to the user
            policies: formData.policies,

            // All quotas available to the user
            quotasByID,

            // All possible target URNs for the user's request including 'self'
            allTargets,

            // All possible target URNs for the user's request excluding 'self'
            allGroupTargets,

            //
            // Form State
            //

            currentStep: FormStep.PickTarget,

            // PickTarget step
            pickTargetSelectedOption: "",
            pickTargetGroupSelection: "",

            // PickPolicy step
            pickPolicySelection: "",

            // EnterRequestData
            // Validators are empty at the beginning as they get set by watch method
            // when EnterRequestData step has been reached.

            // Request form data
            enterRequestDataName: "",
            enterRequestDataFormData: null as Record<string, unknown> | null,
            enterRequestDataCheckboxAcceptTermsChecked: false,
            enterRequestDataSendingInProgress: false,

            // JSON schema validators for resource's specification
            enterRequestDataResourceTypeSchemaValidator: new Validator({}),
            enterRequestDataPolicySchemaValidator: new Validator({}),

            // Error state
            enterRequestDataValidationErrors: [] as ErrorObject[],
            enterRequestDataHasMissingFields: false,
            enterRequestDataSubmitErrorText: "",

            // Form state tracking for validation de-bouncing
            enterRequestDataJSONFormRenderers: Object.freeze(renderers),
            enterRequestDataMemoizedData: "",

            //
            // Types/Vue-specific data
            //

            // Form step enum's first and last step
            // Needs to be updated when a step is added in the beginning/end
            firstStep: FormStep.PickTarget,
            lastStep: FormStep.EnterRequestData,

            // Add to data so Vue knows about the definitions
            FormStep,
            prettifyGroupURN,
        };
    },
    computed: {
        pickTargetTargetEntity(): string {
            // If a valid selection was made in the PickTarget step this returns the selected target
            // Used for validating the step and building request data

            // Return self or URN if it is a valid target group
            if (this.pickTargetSelectedOption === "self" && this.allTargets.has("self")) {
                return "self";
            }

            if (this.pickTargetSelectedOption === "group" && this.allGroupTargets.includes(this.pickTargetGroupSelection)) {
                return this.pickTargetGroupSelection;
            }

            return "";
        },
        pickPolicyAvailablePolicies(): Policy[] {
            // Return the policies available to the user after they selected the pickTargetTargetEntity

            // In case there's no selection but this should not happen anyway
            if (this.pickTargetTargetEntity === "") {
                return [];
            }

            return this.policies.filter((policy) => {
                // Wildcard
                if (policy.targetEntity.endsWith(":") && this.pickTargetTargetEntity.startsWith(policy.targetEntity)) {
                    return true;
                }

                // 'self' and regular URNs
                if (policy.targetEntity === this.pickTargetTargetEntity) {
                    return true;
                }

                return false;
            });
        },
        enterRequestDataPolicy(): Policy | null {
            // Return the second validation layer for policy restrictions

            if (this.pickPolicySelection === "") {
                // Nothing picket yet
                return null;
            }

            const policyIndex = this.pickPolicyAvailablePolicies.findIndex((policy) => policy.id === this.pickPolicySelection);

            if (policyIndex === -1) {
                // Invalid policy picked
                return null;
            }

            return this.pickPolicyAvailablePolicies[policyIndex];
        },
        isEnterRequestDataNameValid(): boolean {
            if (this.enterRequestDataName.match("^[a-zA-Z0-9- ]+$") !== null) {
                return true;
            }
            return false;
        },
        schemaWithMergedDescriptions(): object {
            // Returns a JSON schema which merges the ResourceType's property
            // descriptions with the selected policy's descriptions to enabe a more
            // user-friendly display of field descriptions.

            const schemaCopy = JSON.parse(JSON.stringify(this.resourceType.jsonSchema));

            if (!this.enterRequestDataPolicy?.jsonSchema) {
                // No policy selected, so return copy
                return schemaCopy;
            }

            // Collect all top-level descriptions in a quota
            const quotaDescriptions = new Map<string, string>();

            for (const [name, info] of Object.entries(this.enterRequestDataPolicy.jsonSchema.properties as object)) {
                if (Object.keys(info).includes("description")) {
                    quotaDescriptions.set(name, info.description);
                }
            }

            // Merge
            quotaDescriptions.forEach((description, property) => {
                schemaCopy.properties[property].description =
                    `${schemaCopy.properties[property].description}. Based on your permissions ${description}.`;
            });

            return schemaCopy;
        },
        isCurrentStepValid(): boolean {
            // Validate the form input of the current step

            let isValid = false;

            switch (this.currentStep) {
                case FormStep.PickTarget: {
                    if (this.pickTargetTargetEntity !== "") {
                        isValid = true;
                    }
                    break;
                }
                case FormStep.PickPolicy: {
                    if (this.pickPolicyAvailablePolicies.map((policy) => policy.id).includes(this.pickPolicySelection)) {
                        isValid = true;
                    }
                    break;
                }
                case FormStep.EnterRequestData: {
                    if (
                        this.isEnterRequestDataNameValid &&
                        this.enterRequestDataValidationErrors.length === 0 &&
                        this.enterRequestDataCheckboxAcceptTermsChecked === true &&
                        !this.enterRequestDataSendingInProgress
                    ) {
                        isValid = true;
                    }
                    break;
                }
                default: {
                    console.error("Error: tried to validate unknown form step.");
                    isValid = false;
                    break;
                }
            }

            return isValid;
        },
    },
    watch: {
        // biome-ignore lint/complexity/noExcessiveCognitiveComplexity: Splitting this would make it harder to read
        currentStep(newStep: FormStep, oldStep: FormStep) {
            if (newStep === FormStep.PickPolicy) {
                // Skip quota/policy step in case there's only one available

                if (this.pickPolicyAvailablePolicies.length === 1) {
                    this.pickPolicySelection = this.pickPolicyAvailablePolicies[0].id;

                    // Depending on direction skip one step ahead or back
                    if (newStep > oldStep) {
                        this.stepForward();
                    } else {
                        this.stepBackward();
                    }
                }
            }

            if (newStep === FormStep.EnterRequestData) {
                // Merge required fields so the form renders all '*' as expected in cases
                // where a field is not required by the resource type but the policy.
                //
                // Get required properties on both schemas if they exists and merge into de-duplicated array.
                this.resourceType.jsonSchema.required = Array.from(
                    new Set(
                        ((this.resourceType.jsonSchema.required as string[]) || []).concat(
                            (this.enterRequestDataPolicy?.jsonSchema.required as string[]) || [],
                        ),
                    ),
                );

                // Normally, one would use Ajv for validation, however, as it relies on
                // code generation and is blocked by our CSP, we use the cfworker implementation.
                this.enterRequestDataResourceTypeSchemaValidator = new Validator(this.resourceType.jsonSchema, "2020-12", false);
                this.enterRequestDataPolicySchemaValidator = new Validator(this.enterRequestDataPolicy?.jsonSchema as Schema, "2020-12", false);

                // If this is the first time we advance to this step (user may have used the back button)
                // Fill-in the schemas' default values
                if (this.enterRequestDataFormData === null) {
                    this.enterRequestDataFormData = {};

                    // Add values from ResourceType schema
                    for (const [propertyName, propertyDefinition] of Object.entries(this.resourceType.jsonSchema.properties as Schema)) {
                        if (propertyDefinition.default !== undefined) {
                            this.enterRequestDataFormData[propertyName] = propertyDefinition.default;
                        }
                    }

                    // Add values from policy's schema (overrides any defaults from the ResourceType)
                    for (const [propertyName, propertyDefinition] of Object.entries(this.enterRequestDataPolicy?.jsonSchema.properties as Schema)) {
                        if (propertyDefinition.default !== undefined) {
                            this.enterRequestDataFormData[propertyName] = propertyDefinition.default;
                        }
                    }
                }
            }
        },
    },
    methods: {
        stepForward() {
            // Try to step the form forward

            if (this.isCurrentStepValid && this.currentStep !== this.lastStep) {
                this.currentStep++;
            }
        },
        stepBackward() {
            // Try to step the form backward

            if (this.currentStep > this.firstStep) {
                this.currentStep--;
            } else {
                // Go back in browser history as the form is already at the beginning
                window.history.back();
            }
        },
        async sendRequest() {
            // Try sending the resource request

            if (this.currentStep !== FormStep.EnterRequestData || !this.isCurrentStepValid) {
                // The form is either not in the last step or not valid.
                return;
            }

            const requestFormData = {
                name: this.enterRequestDataName,
                // biome-ignore lint/style/useNamingConvention: The endpoint expects Python-style payloads
                target_entity: this.pickTargetTargetEntity,
                // biome-ignore lint/style/useNamingConvention: The endpoint expects Python-style payloads
                catalog_policy_id: this.enterRequestDataPolicy?.id,
                specification: this.enterRequestDataFormData,
                // biome-ignore lint/style/useNamingConvention: The endpoint expects Python-style payloads
                accept_terms: this.enterRequestDataCheckboxAcceptTermsChecked,
            };

            // Reset the error text
            this.enterRequestDataSubmitErrorText = "";

            try {
                this.enterRequestDataSendingInProgress = true;

                const response = await fetch("/resources/request/", {
                    method: "POST",
                    mode: "same-origin",
                    headers: {
                        "Content-Type": "application/json",
                        "X-CSRFToken": (document.getElementsByName("csrfmiddlewaretoken")[0] as HTMLInputElement).value,
                    },
                    body: JSON.stringify(requestFormData),
                });

                if (response.status === 201) {
                    // Everything worked. Redirect.
                    window.location.replace("/resources/");
                    return;
                }

                // Something went wrong
                const responseJson = await response.json();
                this.enterRequestDataSubmitErrorText = responseJson.status_text;
            } catch (_error) {
                // Fetch or JSON parsing failed
                this.enterRequestDataSubmitErrorText = "An error occured while sending your request. If this persists, please contact support.";
            }

            // Enable button again
            this.enterRequestDataSendingInProgress = false;
        },
        enterRequestDataOnFormChange(event: JsonFormsChangeEvent) {
            // Validate data input

            // Filter properties whose value is undefined as they are not supported in validation
            const filteredEventData = Object.fromEntries(
                Object.entries(event.data).filter(([_key, value]: [string, unknown]) => {
                    return value !== undefined;
                }),
            );

            // Memoize data so we don't get into an infinite re-validation loop
            const serializedData = JSON.stringify(filteredEventData);
            if (serializedData === this.enterRequestDataMemoizedData) {
                return;
            }
            this.enterRequestDataMemoizedData = serializedData;

            // Update data attribute
            this.enterRequestDataFormData = filteredEventData;

            const resourceTypeResult = this.enterRequestDataResourceTypeSchemaValidator.validate(filteredEventData);
            const policyResult = this.enterRequestDataPolicySchemaValidator?.validate(filteredEventData);

            const resourceTypeErrors = convertLocalValidationResult(resourceTypeResult);
            const policyErrors = convertLocalValidationResult(policyResult);

            this.enterRequestDataValidationErrors = resourceTypeErrors.concat(policyErrors);

            // Check if there are any missing field errors
            const missingFieldErrorIndex = this.enterRequestDataValidationErrors.findIndex((err) => err.keyword === "required");
            this.enterRequestDataHasMissingFields = missingFieldErrorIndex !== -1;
        },
        lookupLabelInUISchema(property: string) {
            // Try to find a property's control label in the UI schema, returns null if not found
            return this.findControlLabelInUISchema(property, this.resourceType.uiSchema);
        },
        // biome-ignore lint/complexity/noExcessiveCognitiveComplexity: Runtime type checking makes this necessar<y
        findControlLabelInUISchema(property: string, schemaElement: UISchemaElement): string | null {
            // Recursively search a UI schema for a control label, returns null if not found

            if (this.schemaElementIsControlElement(schemaElement) && (schemaElement as ControlElement).scope === `#/properties/${property}`) {
                // Label found, return label if exists and should be displayed (it can be false, string or a label description)
                if (schemaElement.label === undefined || typeof schemaElement.label === "boolean") {
                    // Technically this also hides labels where the label is true but there's not much use in a field labelled like that
                    return null;
                }

                // Chekc whether it is a simple string
                if (typeof schemaElement.label === "string") {
                    return schemaElement.label;
                }

                // Check whether it is a label description that is supposed to be displayed
                if (schemaElement.label.show && typeof schemaElement.label.text === "string") {
                    return schemaElement.label.text;
                }

                return null;
            }

            if (
                this.schemaElementIsLayout(schemaElement) &&
                Object.keys(schemaElement).includes("elements") &&
                Array.isArray(schemaElement.elements)
            ) {
                // These elements may contain control elements, recursively search these
                for (const element of schemaElement.elements) {
                    const foundLabel = this.findControlLabelInUISchema(property, element);

                    if (foundLabel !== null) {
                        return foundLabel;
                    }
                }
            }

            return null;
        },
        schemaElementIsControlElement(schemaElement: UISchemaElement): schemaElement is ControlElement {
            return schemaElement.type === "Control";
        },
        schemaElementIsLayout(schemaElement: UISchemaElement): schemaElement is Layout {
            return ["HorizontalLayout", "VerticalLayout", "Group", "Categorization", "Category"].includes(schemaElement.type);
        },
    },
    components: { RadioGroup, RadioGroupOption, JsonForms },
};
</script>

<template>
    <div class="container mx-auto flex flex-col items-center w-full lg:w-1/2 lg:border lg:rounded-md lg:shadow">
        <button class="flex p-2 self-start m-6 no-underline" @click="stepBackward()" @keypress.enter="stepBackward()">
            <span class="font-medium"><i class="fa-solid fa-chevron-left"></i> Go Back</span>
        </button>
        <div class="flex flex-col p-6 md:p-12">
            <div class="flex flex-col items-center">
                <div v-if="currentStep === FormStep.PickTarget">
                    <h1 class="tex-center mt-0 text-2xl">Who do you want to request {{ resourceType.name }} for?</h1>
                    <RadioGroup v-model="pickTargetSelectedOption"
                        class="mt-16 grid grid-cols-2 gap-4 select-none px-8 lg:px-16">
                        <RadioGroupOption value="self" v-slot="{ active, checked }">
                            <div class="flex flex-col justify-center items-center p-4 transition-all hover:shadow-md cursor-pointer border-2 shadow-sm rounded-md"
                                :class="{ 'border-primary': checked, 'shadow-md': active }" value="self"
                                title="Request resource for myself.">
                                <i class="fa-solid fa-user text-[5rem]"></i>
                                <span class="whitespace-nowrap mt-6 text-xl font-semibold">Myself</span>
                            </div>
                        </RadioGroupOption>
                        <RadioGroupOption value="group" v-slot="{ active, checked }">
                            <div class="flex flex-col justify-center items-center p-4 transition-all hover:shadow-md cursor-pointer border-2 shadow-sm rounded-md"
                                :class="{ 'border-primary': checked, 'shadow-md': active }" value="group"
                                title="Request resource for a group.">
                                <i class="fa-solid fa-users text-[5rem]"></i>
                                <span class="whitespace-nowrap mt-6 text-xl font-semibold">A Group</span>
                            </div>
                        </RadioGroupOption>
                    </RadioGroup>
                    <div class="pt-16  min-h-[8rem] flex flex-col justify-center text-center">
                        <span v-if="pickTargetSelectedOption === 'self' && !allTargets.has('self')">You can't request
                            this
                            resource as a personal resource.</span>
                        <span v-if="pickTargetSelectedOption === 'group' && allGroupTargets.length === 0">There are no
                            groups
                            you're allowed to request this resource for.</span>
                        <select v-if="pickTargetSelectedOption === 'group' && allGroupTargets.length > 0"
                            v-model="pickTargetGroupSelection">
                            <option value="" disabled>Please select a group...</option>
                            <option v-for="targetGroup in allGroupTargets" :value="targetGroup">
                                {{ prettifyGroupURN(targetGroup as string) }}
                            </option>
                        </select>
                    </div>
                </div>
                <div v-if="currentStep === FormStep.PickPolicy">
                    <h1 class="text-center mt-0 text-2xl">Which set of permissions do you want to use?</h1>
                    <p class="text-center">
                        Please select the option below that best fits your use-case.
                    </p>
                    <RadioGroup v-model="pickPolicySelection"
                        class="mt-16 grid grid-cols-1 gap-4 select-none px-8 lg:px-16">
                        <RadioGroupOption v-for="policy in pickPolicyAvailablePolicies" :value="policy.id"
                            v-slot="{ active, checked }">
                            <div class="flex flex-col p-4 transition-all hover:shadow-md cursor-pointer border-2 shadow-sm rounded-md"
                                :class="{ 'border-primary': checked, 'shadow-md': active }" value="self">
                                <span class="font-semibold">{{ policy.name }} on quota
                                    {{ quotasByID.get(policy.catalogQuotaID)?.name }}</span>
                                <ul class="m-0">
                                    <li v-for="(info, name) in policy.jsonSchema.properties">
                                        <span class="font-semibold">{{ lookupLabelInUISchema(name as string) || name }}:
                                        </span>
                                        <span>{{ (info as Schema).description }}</span>
                                    </li>
                                </ul>
                            </div>
                        </RadioGroupOption>
                    </RadioGroup>
                </div>
                <div class="flex flex-col items-center w-full" v-if="currentStep === FormStep.EnterRequestData">
                    <h1 class="mt-0 mb-8 text-center text-2xl">Please fill in your resource's details</h1>
                    <div class="self-center w-full xl:w-2/3 2xl:w-1/2">
                        <h2 class="text-xl">Details for {{ resourceType.name }}</h2>
                        <json-forms :data="enterRequestDataFormData" :renderers="enterRequestDataJSONFormRenderers"
                            :schema="schemaWithMergedDescriptions" :uischema="resourceType.uiSchema"
                            validation-mode="NoValidation" :additional-errors="enterRequestDataValidationErrors"
                            :config="{ showUnfocusedDescription: true }" @change="enterRequestDataOnFormChange" />
                        <h2 class="text-xl">Helmholtz Cloud</h2>
                        <label for="enter-request-data-name" class="label">Display name on Helmholtz
                            Cloud *</label>
                        <input id="enter-request-data-name" class="wrapper" type="text"
                            v-model="enterRequestDataName" />
                        <span class="description"
                            :class="{ error: !isEnterRequestDataNameValid && enterRequestDataName.length > 0 }">
                            The resource's display name in your Hemholtz Cloud account. Allowed characters: a-Z, 0-9, -
                            <span v-if="!isEnterRequestDataNameValid && enterRequestDataName.length > 0">
                                The name you entered is not valid.
                            </span>
                        </span>
                        <div class="mt-12 flex flex-row">
                            <input type="checkbox" id="checkbox-accept-legal-text" class="mt-1"
                                v-model="enterRequestDataCheckboxAcceptTermsChecked" />
                            <label for="checkbox-accept-legal-text" class="pl-2">I have read and agree to the&nbsp;<a
                                    href="/terms-of-use" target="_blank">terms
                                    of use</a>. I confirm to use the Helmholtz Cloud
                                resources for neither private nor economic purposes as defined by EU state aid law
                                (EU-Beihilfenrecht).</label>
                        </div>
                        <div class="mt-8 min-h-8"
                            :class="{ error: (enterRequestDataHasMissingFields || !isEnterRequestDataNameValid) || enterRequestDataSubmitErrorText }">
                            <span v-if="enterRequestDataHasMissingFields || !isEnterRequestDataNameValid">Please fill
                                all fields marked with '*'.</span>
                            <span v-if="enterRequestDataSubmitErrorText">{{ enterRequestDataSubmitErrorText }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-16 flex flex-col justify-center text-center">
                <button v-if="currentStep !== lastStep" @click="stepForward()" @keypress.enter="stepForward()"
                    :disabled="!isCurrentStepValid">
                    <hifis-button :variant="isCurrentStepValid ? 'default' : 'disabled'">Continue</hifis-button>
                </button>
                <button v-if="currentStep === lastStep" @click="sendRequest()" @keypress.enter="sendRequest()"
                    :disabled="!isCurrentStepValid">
                    <hifis-button :variant="isCurrentStepValid ? 'default' : 'disabled'">
                        <span v-if="!enterRequestDataSendingInProgress">Send Request</span>
                        <span v-if="enterRequestDataSendingInProgress">Sending...</span>
                    </hifis-button>
                </button>
            </div>
        </div>
    </div>
</template>
