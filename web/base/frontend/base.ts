import { createApp } from "vue";

import HamburgerMenu from "./hamburgerMenu.vue";
import "./customElements.ts";
import { setupUserMenu } from "./userMenu.ts";

setupUserMenu();

if (document.getElementById("hamburger-menu")) {
    createApp(HamburgerMenu).mount("#hamburger-menu");
}
