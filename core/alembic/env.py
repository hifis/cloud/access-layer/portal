import os
from logging.config import fileConfig

from alembic import context
from sqlalchemy import engine_from_config, pool

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)


connect_url = os.getenv("PORTAL_CORE_DATABASE_URL")
if connect_url is None:
    error_message = (
        "Could not find PORTAL_CORE_DATABASE_URL in environment. Please configure it."
    )
    raise RuntimeError(error_message)

# Translate to psycopg3 URL
# The 'postgresql://' prefix gets mapped to the psycopg2 adapter by sqlalchemy.
# Since this is sqlalchemy-specific behaviour we map the 'standard' DB URL format to the specific one.
if connect_url.startswith("postgresql://"):
    connect_url = connect_url.replace("postgresql://", "postgresql+psycopg://", 1)

config.set_main_option("sqlalchemy.url", connect_url)

target_metadata = None


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """

    config_section = config.get_section(config.config_ini_section)

    if config_section is None:
        error_message = "Error: config_section is None!"
        raise RuntimeError(error_message)

    connectable = engine_from_config(
        config_section,
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(connection=connection, target_metadata=target_metadata)

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
