export function prettifyGroupURN(urn: string): string {
    // Format group URN into human-readable format
    return urn.replace("urn:geant:", "").replace(":group:", " / ").replace(":", " / ");
}
