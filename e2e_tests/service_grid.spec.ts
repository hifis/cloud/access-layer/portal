import { expect, test } from "@playwright/test";

// Run this file in parallel as loading the page takes a while
test.describe.configure({ mode: "parallel" });

test("basic grid contents", async ({ page }) => {
    const response = await page.goto("/services/");
    expect(response?.status()).toBe(200);

    const serviceGrid = await page.locator("#service-grid");

    await expect(page).toHaveTitle("Services - Helmholtz Cloud");

    // Legal text
    await expect(page.getByTestId("usage-note")).toContainText(
        "You may not use the services for private or economic purposes as defined by EU state aid law (EU Beihilfenrecht). Read more in our terms of use.",
    );

    // Smoke test that it rendered services
    await expect(serviceGrid).toContainText("Helmholtz Codebase");
});

test("text search", async ({ page }) => {
    const response = await page.goto("/services/");
    expect(response?.status()).toBe(200);

    const serviceGrid = await page.locator("#service-grid");
    const searchField = await serviceGrid.getByPlaceholder("Search services...");

    await expect(serviceGrid).not.toContainText("Sort by Relevance");

    // Find one specific service by name
    await searchField.fill("rancher");
    await expect(serviceGrid).toContainText("Rancher managed Kubernetes");

    // Check that sorting button switches
    await expect(serviceGrid).toContainText("Sort by Relevance");

    // Service can be found even if there's a typo
    await searchField.fill("rncher");
    await expect(serviceGrid).toContainText("Rancher managed Kubernetes");

    // Find one specific service description content
    await searchField.fill("rancher.com");
    await expect(serviceGrid).toContainText("Rancher managed Kubernetes");

    // To be sure that the previous assertions are working test that entering nonsense returns an empty result
    await searchField.fill("randomtextfortesting");
    await expect(serviceGrid).not.toContainText("Rancher managed Kubernetes");
});

test("provider filter", async ({ page }) => {
    let response = await page.goto("/services/");
    expect(response?.status()).toBe(200);

    let serviceGrid = await page.locator("#service-grid");
    const search = await page.getByRole("search");

    // Choose one provider
    const allServicesCount = await serviceGrid.locator("div.grid>a:visible").count();
    await search.getByText("DESY").click();

    // Validate it found fewer services
    const providerFilterCount = await serviceGrid.locator("div.grid>a:visible").count();
    expect(providerFilterCount).toBeLessThan(allServicesCount);

    // Reset
    await search.getByText("DESY").click();
    const resetFilterCount = await serviceGrid.locator("div.grid>a:visible").count();

    expect(resetFilterCount).toEqual(allServicesCount);

    // URL parsing works
    await search.getByText("DESY").click();

    response = await page.reload();
    expect(response?.status()).toBe(200);

    serviceGrid = await page.locator("#service-grid");

    const foundServices = await serviceGrid.locator("div.grid>a:visible");
    expect(await foundServices.count()).toBe(providerFilterCount);
});

test("category filter", async ({ page }) => {
    let response = await page.goto("/services/");
    expect(response?.status()).toBe(200);

    let serviceGrid = await page.locator("#service-grid");
    const search = await page.getByRole("search");

    // Choose one category
    const allServicesCount = await serviceGrid.locator("div.grid>a:visible").count();
    await search.getByText("Communication").click();

    // Validate it found fewer services
    const categoryFilterCount = await serviceGrid.locator("div.grid>a:visible").count();
    expect(categoryFilterCount).toBeLessThan(allServicesCount);

    // Reset
    await search.getByText("Communication").click();
    const resetFilterCount = await serviceGrid.locator("div.grid>a:visible").count();

    expect(resetFilterCount).toEqual(allServicesCount);

    // URL parsing works
    await search.getByText("Communication").click();

    response = await page.reload();
    expect(response?.status()).toBe(200);

    serviceGrid = await page.locator("#service-grid");

    const foundServices = await serviceGrid.locator("div.grid>a:visible");
    expect(await foundServices.count()).toBe(categoryFilterCount);
});

test("reset filters button", async ({ page }) => {
    const response = await page.goto("/services/");
    expect(response?.status()).toBe(200);

    // Check the button is not there
    expect(await page.getByText("Reset Filters").count()).toBe(0);

    const serviceGrid = await page.locator("#service-grid");
    const search = await page.getByRole("search");

    const allServicesCount = await serviceGrid.locator("div.grid>a:visible").count();

    // Choose one category
    await search.getByText("Communication").click();

    const categoryFilterCount = await serviceGrid.locator("div.grid>a:visible").count();
    expect(categoryFilterCount).toBeLessThan(allServicesCount);

    // Check the button is there after a category was selected
    expect(await page.getByText("Reset Filters").count()).toBe(1);

    // Click the button
    const resetFiltersButton = await serviceGrid.getByText("Reset Filters");
    await resetFiltersButton.click();

    // Check the button is gone and search is reset
    expect(await page.getByText("Reset Filters").count()).toBe(0);

    const resetFilterCount = await serviceGrid.locator("div.grid>a:visible").count();
    expect(resetFilterCount).toEqual(allServicesCount);
});

test("service modal", async ({ page }) => {
    const response = await page.goto("/services/");
    expect(response?.status()).toBe(200);

    const serviceGrid = await page.locator("#service-grid");
    const modal = await serviceGrid.getByRole("dialog");

    // Clicking the links changes the URL, title and opens the modal
    await expect(page).toHaveTitle("Services - Helmholtz Cloud");

    const cardTitle = await serviceGrid.locator("div.grid>a:visible").first().locator("span.text-xl").textContent();

    await serviceGrid.locator("div.grid>a:visible").first().click();

    await expect(page).not.toHaveTitle("Services - Helmholtz Cloud");

    // Check the title contains the card title (smoke test)
    await expect(await modal.locator("h2").textContent()).toContain(cardTitle);

    // biome-ignore lint/performance/useTopLevelRegex: Run only once per test run
    expect(page.url()).toMatch(/.*\/services\/\?serviceID=[0-9a-f\-+]/);

    await expect(modal).toContainText("Go to service");
    await expect(modal).toContainText("Provide Feedback");
});

test("sorting", async ({ page }) => {
    let response = await page.goto("/services/");
    expect(response?.status()).toBe(200);

    let serviceGrid = await page.locator("#service-grid");
    const sortSelect = await serviceGrid.getByLabel("Service Sorting");

    // Sort and check services are re-ordered
    const firstServicePopularity = await serviceGrid.locator("div.grid>a:visible").first().getAttribute("aria-labelledby");

    await sortSelect.selectOption({ label: "Sort by Name" });

    const firstServiceTitle = await serviceGrid.locator("div.grid>a:visible").first().getAttribute("aria-labelledby");

    expect(firstServicePopularity).not.toEqual(firstServiceTitle);

    // biome-ignore lint/performance/useTopLevelRegex: only run once per test run
    expect(page.url()).toMatch(/\/services\/\?sortByAttribute=title/);

    await sortSelect.selectOption({ label: "Sort by Popularity" });

    const firstServicePopularityAgain = await serviceGrid.locator("div.grid>a:visible").first().getAttribute("aria-labelledby");

    // biome-ignore lint/performance/useTopLevelRegex: only run once per test run
    expect(page.url()).toMatch(/\/services\//);

    expect(firstServicePopularity).toEqual(firstServicePopularityAgain);

    await sortSelect.selectOption({ label: "Sort by Name" });

    // Check URL parsing works
    // biome-ignore lint/performance/useTopLevelRegex: only run once per test run
    expect(page.url()).toMatch(/\/services\/\?sortByAttribute=title/);

    response = await page.reload();

    // biome-ignore lint/performance/useTopLevelRegex: only run once per test run
    expect(page.url()).toMatch(/\/services\/\?sortByAttribute=title/);
    expect(response?.status()).toBe(200);

    serviceGrid = await page.locator("#service-grid");

    const firstServiceAfterReload = await serviceGrid.locator("div.grid>a:visible").first().getAttribute("aria-labelledby");
    expect(firstServiceAfterReload).toEqual(firstServiceTitle);
});
