import datetime
from uuid import uuid4

import pydantic
import pytest

import core.catalog.testing
import core.identity.testing
import core.resources
from core.resources.resources import (
    QuotaExceededError,
    Resource,
    ResourceRequestError,
    ResourceStateEnum,
)

TEST_GROUP_URN = "urn:geant:helmholtz.de:group:helmholtz.cloud:test"


class ExampleData(pydantic.BaseModel):
    actor_identity: core.identity.ActorIdentity
    provider: core.catalog.Provider
    service: core.catalog.Service
    resource_type: core.catalog.ResourceType
    quota: core.catalog.Quota
    policy: core.catalog.Policy
    resource: Resource | None


def make_resource() -> Resource:
    return core.resources.Resource(
        id=uuid4(),
        catalog_quota_id=uuid4(),
        catalog_service_id=uuid4(),
        identity_user_id_requested_by=uuid4(),
        identity_group_urn_target=None,
        identity_user_id_target=uuid4(),
        external_id=None,
        name="Test Resource",
        state=ResourceStateEnum.form_submitted,
        state_text=None,
        specification={},
        created_at=datetime.datetime.now(tz=datetime.UTC),
        updated_at=datetime.datetime.now(tz=datetime.UTC),
    )


async def create_example_data(*, do_not_create_resource: bool = False) -> ExampleData:
    await core.identity.import_group_urns([
        # Main testing URN
        TEST_GROUP_URN,
        # Subteams for group tests
        f"{TEST_GROUP_URN}:team",
        f"{TEST_GROUP_URN}:team2",
        f"{TEST_GROUP_URN}:wrongassertion",
    ])

    actor_identity = core.identity.testing.make_actor_identity()
    actor_identity.edu_person_entitlement = [f"{TEST_GROUP_URN}#localhost"]
    actor_identity.edu_person_assurance = ["test_assurance"]
    actor_identity.edu_person_scoped_affiliation = ["test_affiliation"]

    provider = core.catalog.testing.make_provider()

    await core.catalog.testing.create_or_update_provider(provider=provider)

    service = core.catalog.testing.make_service(provider_id=provider.id)
    await core.catalog.testing.create_or_update_service(service=service)

    resource_type = core.catalog.testing.make_resource_type()
    resource_type.json_schema = {
        "title": "TestResourceType",
        "description": "Specification of a Test Resource",
        "type": "object",
        "properties": {
            "testProperty": {
                "description": "Test Property",
                "type": "integer",
                "default": 1,
                "minimum": 1,
                "maximum": 5,
            },
            "testPropertyForPolicy": {
                "description": "Test Property",
                "type": "string",
            },
        },
        "required": [
            "testProperty",
        ],
    }

    await core.catalog.testing.create_or_update_resource_type(
        resource_type=resource_type
    )

    quota = core.catalog.testing.make_quota(
        service_id=service.id, resource_type_id=resource_type.id
    )
    quota.quota = [
        core.catalog.QuotaLimit(property="testProperty", total=1_000),
    ]
    await core.catalog.testing.create_or_update_quota(quota=quota)

    policy = core.catalog.testing.make_policy(quota_id=quota.id)
    policy.actor_requirements = core.catalog.PolicyActorRequirements(
        edu_person_entitlement=[f"{TEST_GROUP_URN}#localhost"],
        edu_person_assurance=["test_assurance"],
        edu_person_scoped_affiliation=["test_affiliation"],
    )
    policy.target_entity = "self"
    policy.json_schema = {
        "type": "object",
        "properties": {
            "testProperty": {
                "description": "Count",
                "type": "integer",
                "minimum": 2,
                "maximum": 4,
            },
            "testPropertyForPolicy": {"type": "string", "pattern": "^exact$"},
        },
        "required": ["testProperty", "testPropertyForPolicy"],
    }

    await core.catalog.testing.create_or_update_policy(policy=policy)

    if do_not_create_resource:
        resource = None
    else:
        resource = await core.resources.request_resource(
            actor_identity=actor_identity,
            catalog_policy_id=policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

    return ExampleData(
        actor_identity=actor_identity,
        provider=provider,
        service=service,
        resource_type=resource_type,
        quota=quota,
        policy=policy,
        resource=resource,
    )


class TestResourceValidation:
    @staticmethod
    @pytest.mark.asyncio
    async def test_target_validation() -> None:
        # valid when identity_group_urn_target is set
        core.resources.Resource.model_validate(
            make_resource().model_dump()
            | {
                "identity_group_urn_target": "urn:geant:helmholtz.de:group:DESY",
                "identity_user_id_target": None,
            }
        )

        # valid when identity_user_id_target is set
        make_resource().identity_user_id_target = uuid4()

        # invalid when neither identity_user_id_target nor identity_group_urn_target are set
        with pytest.raises(
            ValueError,
            match="Either identity_group_urn_target or identity_user_id_target must be set",
        ):
            core.resources.Resource.model_validate(
                make_resource().model_dump()
                | {
                    "identity_group_urn_target": None,
                    "identity_user_id_target": None,
                }
            )

        # invalid when both identity_user_id_target and identity_group_urn_target are set
        with pytest.raises(
            ValueError,
            match="Either identity_group_urn_target or identity_user_id_target must be set",
        ):
            core.resources.Resource.model_validate(
                make_resource().model_dump()
                | {
                    "identity_group_urn_target": "urn:geant:helmholtz.de:group:DESY",
                    "identity_user_id_target": uuid4(),
                }
            )


@pytest.mark.load_extra_schemas("catalog", "identity")
@pytest.mark.usefixtures("_context_database")
class TestRequestResources:
    @staticmethod
    @pytest.mark.asyncio
    async def test_request_resource() -> None:
        example_data = await create_example_data()

        assert example_data.resource is not None

        # Check ID attributes are assigned correctly
        assert example_data.resource.catalog_quota_id == example_data.quota.id
        assert example_data.resource.catalog_service_id == example_data.service.id
        assert (
            example_data.resource.identity_user_id_requested_by
            == example_data.actor_identity.vo_person_id
        )
        assert example_data.resource.identity_group_urn_target is None
        assert (
            example_data.resource.identity_user_id_target
            == example_data.actor_identity.vo_person_id
        )

        db_resource = await core.resources.get_resource_by_id(
            resource_id=example_data.resource.id
        )

        assert db_resource is not None
        assert example_data.resource == db_resource
        assert db_resource.created_at == db_resource.updated_at
        assert db_resource.state == ResourceStateEnum.form_submitted

    @staticmethod
    @pytest.mark.asyncio
    async def test_check_policy_exists() -> None:
        example_data = await create_example_data()
        random_uuid = uuid4()

        assert random_uuid != example_data.policy.id

        with pytest.raises(
            ResourceRequestError,
            match="The policy does not exist.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=uuid4(),
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_check_quota_referenced_by_policy_exists() -> None:
        example_data = await create_example_data()

        random_uuid = uuid4()

        assert random_uuid != example_data.policy.catalog_quota_id

        example_data.policy.catalog_quota_id = random_uuid

        await core.catalog.testing.create_or_update_policy(policy=example_data.policy)

        with pytest.raises(
            ResourceRequestError,
            match="The quota does not exist - this should never happen. The referential integrity between policy and quota is broken.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_check_resource_type_referenced_by_quota_exists() -> None:
        example_data = await create_example_data()

        random_uuid = uuid4()

        assert random_uuid != example_data.quota.catalog_resource_type_id

        example_data.quota.catalog_resource_type_id = random_uuid

        await core.catalog.testing.create_or_update_quota(quota=example_data.quota)

        with pytest.raises(
            ResourceRequestError,
            match="The resource type does not exist - this should never happen. The referential integrity between quota and resource type is broken.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_check_policy_is_available_to_actor() -> None:
        # Check that actor would be valid, then change profile to make it no longer valid for policy

        example_data = await create_example_data()
        assert (
            example_data.actor_identity.edu_person_entitlement
            == example_data.policy.actor_requirements.edu_person_entitlement
        )

        original_entitlement = (
            example_data.policy.actor_requirements.edu_person_entitlement
        )

        # Base case works
        await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

        # edu_person_entitlement is checked for correctness even if empty or none
        assert example_data.policy.actor_requirements.edu_person_entitlement is not None

        example_data.actor_identity.edu_person_entitlement = [
            example_data.policy.actor_requirements.edu_person_entitlement[0]
            + "intentionally changed for test"
        ]

        with pytest.raises(
            ResourceRequestError,
            match="The policy is not available to this actor.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        example_data.actor_identity.edu_person_entitlement = []

        with pytest.raises(
            ResourceRequestError,
            match="The policy is not available to this actor.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        example_data.actor_identity.edu_person_entitlement = None

        with pytest.raises(
            ResourceRequestError,
            match="The policy is not available to this actor.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        example_data.actor_identity.edu_person_entitlement = original_entitlement

        # Base case works again
        await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

        # edu_person_assurance is checked for correctness even if empty or none
        assert (
            example_data.actor_identity.edu_person_assurance
            == example_data.policy.actor_requirements.edu_person_assurance
        )

        assert example_data.policy.actor_requirements.edu_person_assurance is not None

        original_assurance = example_data.policy.actor_requirements.edu_person_assurance

        example_data.actor_identity.edu_person_assurance = [
            example_data.policy.actor_requirements.edu_person_assurance[0]
            + "intentionally changed for test"
        ]

        with pytest.raises(
            ResourceRequestError,
            match="The policy is not available to this actor.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        example_data.actor_identity.edu_person_assurance = []

        with pytest.raises(
            ResourceRequestError,
            match="The policy is not available to this actor.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        example_data.actor_identity.edu_person_assurance = None

        with pytest.raises(
            ResourceRequestError,
            match="The policy is not available to this actor.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        example_data.actor_identity.edu_person_assurance = original_assurance

        # Base case works again
        await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

    @staticmethod
    @pytest.mark.asyncio
    async def test_check_resource_type_schema() -> None:
        # JSON Schema validation smoke test, more cases should be covered by the schema library's tests

        example_data = await create_example_data()

        # Check type
        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass resource type schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": "test",
                    "testPropertyForPolicy": "exact",
                },
            )

        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass resource type schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": 2,
                },
            )

        # Check required
        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass resource type schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={},
            )

        # Check not none
        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass resource type schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": None,
                },
            )

        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass resource type schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": None,
                },
            )

        # Check unevaluated properties are not allowed
        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass resource type schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "unevaluatedProperty": 2,
                },
            )

        # Check minimum
        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass resource type schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 0,
                },
            )

        # Check maximum
        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass resource type schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 6,
                },
            )

        # Base case works
        await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

    @staticmethod
    @pytest.mark.asyncio
    async def test_check_policy_schema() -> None:
        # JSON Schema validation smoke test, more cases should be covered by the schema library's tests

        example_data = await create_example_data()

        # Check required
        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass policy schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                },
            )

        # Check policy minimum
        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass policy schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 1,
                },
            )

        # Check policy maximum
        with pytest.raises(
            ResourceRequestError,
            match="The resource specification did not pass policy schema validation.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 4,
                },
            )

        # Base case works
        await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

    @staticmethod
    @pytest.mark.asyncio
    async def test_check_target_entity() -> None:
        example_data = await create_example_data()

        assert example_data.policy.target_entity == "self"

        # Base case works for personal resource
        await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

        # Requesting a group resource for a personal policy is not allowed
        with pytest.raises(
            ResourceRequestError,
            match="The requested target entity does not match the policy.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity=TEST_GROUP_URN,
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        # Base case works for group resource
        example_data.policy.target_entity = TEST_GROUP_URN
        await core.catalog.testing.create_or_update_policy(policy=example_data.policy)

        assert example_data.actor_identity.edu_person_entitlement is not None
        assert example_data.actor_identity.edu_person_entitlement[0].startswith(
            TEST_GROUP_URN
        )

        await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity=TEST_GROUP_URN,
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

        # Requesting a personal resource for a group policy is not allowed
        with pytest.raises(
            ResourceRequestError,
            match="'self' is not a valid target_entity for this policy.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        # Using a non-matching URN is not allowed
        with pytest.raises(
            ResourceRequestError,
            match="The requested target entity does not match the policy.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity=TEST_GROUP_URN + ":subgroup",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        # Wildcard works
        unrelated_urn = "urn:geant:helmholtz.de:group:helmholtz.cloud:unrelated"
        await core.identity.import_group_urns([
            TEST_GROUP_URN,
            TEST_GROUP_URN + ":subgroup:subsubgroup",
            unrelated_urn,
        ])
        example_data.policy.target_entity = TEST_GROUP_URN + ":"
        await core.catalog.testing.create_or_update_policy(policy=example_data.policy)

        await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity=TEST_GROUP_URN + ":subgroup:subsubgroup",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

        # Wildcard on group not covered is not allowed
        with pytest.raises(
            ResourceRequestError,
            match="The requested target entity does not match the policy.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity=unrelated_urn,
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        await core.identity.import_group_urns([TEST_GROUP_URN])

        # Wildcard on missing group is not allowed
        with pytest.raises(
            ResourceRequestError,
            match="The group selected as target_entity does not exist.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity=TEST_GROUP_URN + ":subgroup",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        example_data.policy.target_entity = TEST_GROUP_URN
        await core.catalog.testing.create_or_update_policy(policy=example_data.policy)

        # Actor without vo_person_id is not allowed

        example_data.actor_identity.vo_person_id = None

        with pytest.raises(
            ResourceRequestError,
            match="Actor does not have vo_person_id!",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity=TEST_GROUP_URN,
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_check_quota_limits() -> None:
        example_data = await create_example_data(do_not_create_resource=True)

        # Exceeding the quota with the first request is not allowed
        example_data.quota.quota = [
            core.catalog.QuotaLimit(property="testProperty", total=2)
        ]
        await core.catalog.testing.create_or_update_quota(quota=example_data.quota)

        with pytest.raises(
            QuotaExceededError,
            match="Request would exceed quota.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 3,
                    "testPropertyForPolicy": "exact",
                },
            )

        # Exceeding the quota with one more request is not allowed
        await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

        with pytest.raises(
            QuotaExceededError,
            match="Request would exceed quota.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": 2,
                    "testPropertyForPolicy": "exact",
                },
            )

        # Passing negative values is not allowed and should never happen as quota has validations on import
        example_data.resource_type.json_schema["properties"]["testProperty"] = {
            "type": "integer"
        }
        await core.catalog.testing.create_or_update_resource_type(
            resource_type=example_data.resource_type
        )

        example_data.policy.json_schema["properties"]["testProperty"] = {
            "type": "integer"
        }
        await core.catalog.testing.create_or_update_policy(policy=example_data.policy)

        with pytest.raises(
            ResourceRequestError,
            match="The value passed in the specification is less than zero but is also limited by a quota! This should never happen.",
        ):
            await core.resources.request_resource(
                actor_identity=example_data.actor_identity,
                catalog_policy_id=example_data.policy.id,
                target_entity="self",
                name="Test Resource",
                specification={
                    "testProperty": -1,
                    "testPropertyForPolicy": "exact",
                },
            )

        # Base case works with new data
        new_example_data = await create_example_data(do_not_create_resource=True)

        await core.resources.request_resource(
            actor_identity=new_example_data.actor_identity,
            catalog_policy_id=new_example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

    @staticmethod
    @pytest.mark.asyncio
    async def test_get_resource_by_id() -> None:
        created_resource = (await create_example_data()).resource
        assert created_resource is not None

        db_resource = await core.resources.get_resource_by_id(
            resource_id=created_resource.id
        )

        assert db_resource is not None
        assert created_resource.id == db_resource.id

        db_resource_with_wrong_id = await core.resources.get_resource_by_id(
            resource_id=uuid4()
        )

        assert db_resource_with_wrong_id is None

    @staticmethod
    @pytest.mark.asyncio
    async def test_get_resources_by_state() -> None:
        created_resource = (await create_example_data()).resource

        db_resources = await core.resources.get_resources_by_state(
            state=ResourceStateEnum.form_submitted
        )

        assert created_resource in db_resources
        assert all(
            resource.state == ResourceStateEnum.form_submitted
            for resource in db_resources
        )

    @staticmethod
    @pytest.mark.asyncio
    async def test_mark_request_submitted() -> None:
        created_resource = (await create_example_data()).resource
        assert created_resource is not None

        await core.resources.mark_request_submitted(created_resource.id)

        updated_resource = await core.resources.get_resource_by_id(created_resource.id)

        assert updated_resource is not None
        assert created_resource.id == updated_resource.id
        assert updated_resource.state == ResourceStateEnum.request_submitted
        assert created_resource.updated_at < updated_resource.updated_at

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'form_submitted' state",
        ):
            await core.resources.mark_request_submitted(uuid4())

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'form_submitted' state",
        ):
            await core.resources.mark_request_submitted(created_resource.id)

    @staticmethod
    @pytest.mark.asyncio
    async def test_mark_request_denied() -> None:
        created_resource = (await create_example_data()).resource
        assert created_resource is not None

        await core.resources.mark_request_submitted(created_resource.id)

        state_text = "user unknown"
        await core.resources.mark_request_denied(
            created_resource.id, state_text=state_text
        )

        updated_resource = await core.resources.get_resource_by_id(created_resource.id)

        assert updated_resource is not None
        assert created_resource.id == updated_resource.id
        assert updated_resource.state == ResourceStateEnum.request_denied
        assert created_resource.updated_at < updated_resource.updated_at
        assert updated_resource.state_text == state_text

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'request_submitted' state",
        ):
            await core.resources.mark_provisioned(uuid4(), None)

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'request_submitted' state",
        ):
            await core.resources.mark_provisioned(created_resource.id, None)

    @staticmethod
    @pytest.mark.asyncio
    async def test_mark_provisioned() -> None:
        created_resource = (await create_example_data()).resource
        assert created_resource is not None

        await core.resources.mark_request_submitted(created_resource.id)

        await core.resources.mark_provisioned(created_resource.id, None)

        updated_resource = await core.resources.get_resource_by_id(
            resource_id=created_resource.id
        )

        assert updated_resource is not None
        assert created_resource.id == updated_resource.id
        assert updated_resource.state == ResourceStateEnum.provisioned
        assert created_resource.updated_at < updated_resource.updated_at

        created_resource_with_external_id = (await create_example_data()).resource
        assert created_resource_with_external_id is not None

        await core.resources.mark_request_submitted(
            created_resource_with_external_id.id
        )
        await core.resources.mark_provisioned(
            created_resource_with_external_id.id, "external_id"
        )
        updated_resource_with_external_id = await core.resources.get_resource_by_id(
            resource_id=created_resource_with_external_id.id
        )

        assert updated_resource_with_external_id is not None
        assert updated_resource_with_external_id.external_id == "external_id"

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'request_submitted' state",
        ):
            await core.resources.mark_provisioned(uuid4(), None)

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'request_submitted' state",
        ):
            await core.resources.mark_provisioned(created_resource.id, None)

    @staticmethod
    @pytest.mark.asyncio
    async def test_mark_deprovisioning_form_submitted() -> None:
        created_resource = (await create_example_data()).resource
        assert created_resource is not None

        await core.resources.mark_request_submitted(created_resource.id)

        await core.resources.mark_provisioned(created_resource.id, None)

        await core.resources.mark_deprovisioning_form_submitted(created_resource.id)

        updated_resource = await core.resources.get_resource_by_id(
            resource_id=created_resource.id
        )

        assert updated_resource is not None
        assert created_resource.id == updated_resource.id
        assert updated_resource.state == ResourceStateEnum.deprovisioning_form_submitted
        assert created_resource.updated_at < updated_resource.updated_at

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'provisioned' state",
        ):
            await core.resources.mark_deprovisioning_form_submitted(uuid4())

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'provisioned' state",
        ):
            await core.resources.mark_deprovisioning_form_submitted(created_resource.id)

    @staticmethod
    @pytest.mark.asyncio
    async def test_mark_deprovisioning_request_submitted() -> None:
        created_resource = (await create_example_data()).resource
        assert created_resource is not None

        await core.resources.mark_request_submitted(created_resource.id)

        await core.resources.mark_provisioned(created_resource.id, None)

        await core.resources.mark_deprovisioning_form_submitted(created_resource.id)

        await core.resources.mark_deprovisioning_request_submitted(created_resource.id)

        updated_resource = await core.resources.get_resource_by_id(
            resource_id=created_resource.id
        )

        assert updated_resource is not None
        assert created_resource.id == updated_resource.id
        assert (
            updated_resource.state == ResourceStateEnum.deprovisioning_request_submitted
        )
        assert created_resource.updated_at < updated_resource.updated_at

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'deprovisioning_form_submitted' state",
        ):
            await core.resources.mark_deprovisioning_request_submitted(uuid4())

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'deprovisioning_form_submitted' state",
        ):
            await core.resources.mark_deprovisioning_request_submitted(
                created_resource.id
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_mark_deprovisioned() -> None:
        created_resource = (await create_example_data()).resource
        assert created_resource is not None

        await core.resources.mark_request_submitted(created_resource.id)

        await core.resources.mark_provisioned(created_resource.id, None)

        await core.resources.mark_deprovisioning_form_submitted(created_resource.id)

        await core.resources.mark_deprovisioning_request_submitted(created_resource.id)

        await core.resources.mark_deprovisioned(created_resource.id)

        updated_resource = await core.resources.get_resource_by_id(
            resource_id=created_resource.id
        )

        assert updated_resource is not None
        assert created_resource.id == updated_resource.id
        assert updated_resource.state == ResourceStateEnum.deprovisioned
        assert created_resource.updated_at < updated_resource.updated_at

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'deprovisioning_request_submitted' state",
        ):
            await core.resources.mark_deprovisioned(uuid4())

        with pytest.raises(
            RuntimeError,
            match="Resource not updated. Either id does not exist or is not in 'deprovisioning_request_submitted' state",
        ):
            await core.resources.mark_deprovisioned(created_resource.id)


@pytest.mark.load_extra_schemas("catalog", "identity")
@pytest.mark.usefixtures("_context_database")
class TestResourceByState:
    @staticmethod
    @pytest.mark.asyncio
    async def test_get_resources_by_state() -> None:
        empty_resources = await core.resources.get_resources_by_state(
            state=ResourceStateEnum.deprovisioned
        )

        assert empty_resources == []

        created_resource = (await create_example_data()).resource
        assert created_resource is not None

        db_resources = await core.resources.get_resources_by_state(
            state=ResourceStateEnum.form_submitted
        )

        assert db_resources == [created_resource]


@pytest.mark.load_extra_schemas("catalog", "identity")
@pytest.mark.usefixtures("_context_database")
class TestGetResources:
    @staticmethod
    @pytest.mark.asyncio
    async def test_get_user_resources() -> None:
        # Test user resource only returns resources a user may access due to either group membership or personal resource

        example_data = await create_example_data()

        # Remove quota limits so we can create a bunch of resources
        example_data.quota.quota = []
        await core.catalog.testing.create_or_update_quota(quota=example_data.quota)

        actor_identity = example_data.actor_identity

        # User is not member of team2 and wrongassertion is asserted by hidah, not localhost
        actor_identity.edu_person_entitlement = [
            f"{TEST_GROUP_URN}#localhost",
            f"{TEST_GROUP_URN}:team#localhost",
            f"{TEST_GROUP_URN}:wrongassertion#hidah",
        ]

        #
        # Test variants
        #

        # - Resource which belongs to the user
        user_resource = example_data.resource
        assert user_resource is not None
        assert user_resource.identity_user_id_target == actor_identity.vo_person_id

        # - Resource which belongs to a group the user is a member of

        # Add policy which allows to create resources for group
        group_policy = core.catalog.Policy(**example_data.policy.model_dump())
        group_policy.id = uuid4()
        group_policy.target_entity = f"{TEST_GROUP_URN}:team"
        await core.catalog.testing.create_or_update_policy(policy=group_policy)

        group_resource = await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=group_policy.id,
            target_entity=f"{TEST_GROUP_URN}:team",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

        # - Resource which belongs to different user
        other_users_resource = (await create_example_data()).resource

        # - Resource which belongs to a group the user is not a member of
        other_example_data = await create_example_data(do_not_create_resource=True)
        other_example_data.policy.target_entity = f"{TEST_GROUP_URN}:team2"

        await core.catalog.testing.create_or_update_policy(
            policy=other_example_data.policy
        )

        other_group_resource = await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=other_example_data.policy.id,
            target_entity=f"{TEST_GROUP_URN}:team2",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

        # - Resource which belongs to a group the user is a member of but where group membership has been asserted by the wrong authority
        wrong_assertion_example_data = await create_example_data(
            do_not_create_resource=True
        )
        wrong_assertion_example_data.policy.target_entity = (
            f"{TEST_GROUP_URN}:wrongassertion"
        )

        await core.catalog.testing.create_or_update_policy(
            policy=wrong_assertion_example_data.policy
        )

        wrong_assertion_group_resource = await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=wrong_assertion_example_data.policy.id,
            target_entity=f"{TEST_GROUP_URN}:wrongassertion",
            name="Test Resource",
            specification={
                "testProperty": 2,
                "testPropertyForPolicy": "exact",
            },
        )

        # Test data has been set up, run the query

        user_resources = await core.resources.get_user_resources(
            actor_identity=actor_identity, authoritative_directory="localhost"
        )

        # Assertions

        # User should only be able to see the group resource and their personal resource
        assert len(user_resources) == 2  # noqa: PLR2004 this is a constant for this test case
        assert user_resource in user_resources
        assert group_resource in user_resources

        # They should not be able to see other people's personal resources
        assert other_users_resource not in user_resources

        # They should not be able to see other group's resources
        assert other_group_resource not in user_resources

        # They should not be able to see resources of groups with the wrong authoritative directory
        assert wrong_assertion_group_resource not in user_resources
