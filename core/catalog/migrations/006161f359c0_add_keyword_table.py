"""
add keyword table
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = "006161f359c0"
down_revision = None
branch_labels = ("catalog",)
depends_on = None


def upgrade() -> None:
    op.create_table(
        "catalog_keyword",
        sa.Column("id", UUID, primary_key=True, nullable=False, comment="Plony UID"),
        sa.Column(
            "plony_at_id",
            sa.Text,
            unique=True,
            index=True,
            nullable=False,
            comment="Plony @id",
        ),
        sa.Column(
            "title",
            sa.Text,
            unique=False,
            index=True,
            nullable=False,
            comment="Title text",
        ),
        sa.Column(
            "is_visible",
            sa.Boolean,
            nullable=False,
            comment="Whether this keyword should be visible",
        ),
        sa.Column(
            "is_category",
            sa.Boolean,
            nullable=False,
            comment="Whether this keyword is a category",
        ),
    )


def downgrade() -> None:
    op.drop_table("catalog_keyword")
