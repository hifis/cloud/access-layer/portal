import uuid
from typing import Any
from uuid import uuid4

import aio_pika
import pytest

import core
from core.catalog import testing
from core.cloud_agent.cloud_agent import (
    RabbitMQConfig,
    lookup_requests_and_send_to_queue,
    on_message_from_hca,
)
from core.cloud_agent.message_handlers import (
    Message,
    add_message_to_history,
    get_messages_from_history,
    prepare_messages_for_hca,
)
from core.resources.resources import (
    ResourceStateEnum,
)
from core.resources.test_resources import create_example_data
from portalctl import start_mock_hca

# property values in resource request that have to be set so that the mock HCA sends either ResourceCreatedV1 or ErrorV1 messages
MOCK_HCA_ACCEPT_VALUE = 2
MOCK_HCA_DENY_VALUE = 3


def make_message(patch: dict[str, Any] | None = None) -> Message:
    if patch is None:
        patch = {}

    return Message(
        type="ResourceCreatedV1",
        resource_id=uuid4(),
        catalog_service_id=uuid4(),
        queue="to_hca",
        payload={},
    )


async def consume_cloud_agent_message(vhost: str) -> None:
    """consumes one message from the mock-provider vhost"""
    config = RabbitMQConfig()  # type: ignore[call-arg]

    connection = await aio_pika.connect_robust(
        host=config.host,
        port=config.port,
        login=config.username,
        password=config.password,
        ssl=config.use_ssl,
        virtualhost=vhost,
        reconnect_interval=config.reconnect_interval,
    )

    channel = await connection.channel()
    queue = await channel.get_queue(config.receive_queue)
    async with queue.iterator() as q:
        async for message in q:
            await on_message_from_hca(message)
            return


async def send_resource_request_to_queue(vhost: str) -> None:
    """lookup all form_submitted resources and send to hca once"""
    config = RabbitMQConfig()  # type: ignore[call-arg]
    connections: dict[
        str,
        tuple[aio_pika.abc.AbstractRobustConnection, aio_pika.abc.AbstractChannel],
    ] = {}

    connection = await aio_pika.connect_robust(
        host=config.host,
        port=config.port,
        login=config.username,
        password=config.password,
        ssl=config.use_ssl,
        virtualhost=vhost,
        reconnect_interval=config.reconnect_interval,
    )
    channel = await connection.channel()
    connections[vhost] = (connection, channel)

    await lookup_requests_and_send_to_queue(config, connections)


@pytest.mark.load_extra_schemas("catalog", "resources")
@pytest.mark.usefixtures("_context_database")
class TestCloudAgentMessageHistory:
    @staticmethod
    @pytest.mark.asyncio
    async def test_message_to_history() -> None:
        message = make_message()
        await add_message_to_history(message=message)

        db_messages = await get_messages_from_history(
            message_type=message.type,
            resource_id=message.resource_id,
            catalog_service_id=message.catalog_service_id,
            queue=message.queue,
        )

        assert len(db_messages) == 1
        assert message == db_messages[0]


@pytest.mark.load_extra_schemas("catalog", "resources", "identity")
@pytest.mark.usefixtures("_context_database")
class TestCloudAgentMessageHandling:
    @staticmethod
    @pytest.mark.asyncio
    async def test_prepare_messages_for_hca() -> None:
        example_data = await create_example_data()
        if example_data is None:
            pytest.fail("example_data was not created")

        resource = example_data.resource
        if resource is None:
            pytest.fail("resource was not created")

        # test message preparation for ResourceAllocateV1
        messages = await prepare_messages_for_hca(
            resources=[
                resource,
            ],
            queue="to_hca",
        )

        assert len(messages) == 1
        assert messages[0].type == "ResourceAllocateV1"
        assert messages[0].catalog_service_id == example_data.service.id
        assert messages[0].queue == "to_hca"
        assert messages[0].resource_id == resource.id

        # test message preparation for ResourceDeallocateV1
        resource.state = ResourceStateEnum.deprovisioning_form_submitted
        resource.external_id = "external_id"
        messages = await prepare_messages_for_hca(
            resources=[
                resource,
            ],
            queue="to_hca",
        )

        assert messages[0].type == "ResourceDeallocateV1"

        # test message preparation for ResourceDeallocateV1 if no external_id has been set
        resource.state = ResourceStateEnum.deprovisioning_form_submitted
        resource.external_id = None

        with pytest.raises(
            RuntimeError,
            match="Resource has no external_id set. Cannot sent prepare ResourceDeallocateV1 message",
        ):
            messages = await prepare_messages_for_hca(
                resources=[
                    resource,
                ],
                queue="to_hca",
            )

        # test message preparation for if resource has wrong state
        resource.state = ResourceStateEnum.provisioned
        resource.external_id = "external_id"

        with pytest.raises(
            ValueError,
            match=f"Resource {resource.id} has wrong state {resource.state} and cannot be handled",
        ):
            messages = await prepare_messages_for_hca(
                resources=[
                    resource,
                ],
                queue="to_hca",
            )

        # test message preparation if no quota can be found
        resource = (await create_example_data()).resource
        if resource is None:
            pytest.fail("resource was not created")

        resource.catalog_quota_id = uuid4()

        with pytest.raises(
            RuntimeError,
            match=f"Cannot find quota {resource.catalog_quota_id} for resource {resource.id}",
        ):
            await prepare_messages_for_hca(
                resources=[
                    resource,
                ],
                queue="to_hca",
            )
        provider = testing.make_provider()
        await testing.create_or_update_provider(provider=provider)

        service = testing.make_service(provider_id=provider.id)
        await testing.create_or_update_service(service=service)

        resource_type = testing.make_resource_type()
        await testing.create_or_update_resource_type(resource_type=resource_type)

        quota = testing.make_quota(
            service_id=service.id, resource_type_id=resource_type.id
        )
        await testing.create_or_update_quota(quota=quota)

        # test message preparation if no rabbitmq vhos name mapping is available
        resource = (await create_example_data()).resource
        if resource is None:
            pytest.fail("resource was not created")

        # test message preparation if no resource type can be found
        resource = example_data.resource
        if resource is None:
            pytest.fail("resource was not created")
        example_data.quota.catalog_resource_type_id = uuid4()

        await testing.create_or_update_quota(quota=example_data.quota)

        with pytest.raises(
            RuntimeError,
            match=f"Cannot get resource type for id {example_data.quota.catalog_resource_type_id}",
        ):
            await prepare_messages_for_hca(
                resources=[
                    resource,
                ],
                queue="to_hca",
            )


@pytest.mark.load_extra_schemas("catalog", "resources", "identity")
@pytest.mark.usefixtures("_context_database")
class TestCloudAgentWithRabbitMQAndHCA:
    @staticmethod
    @pytest.mark.asyncio
    async def test_send_and_receive_messages_provisioned() -> None:
        example_data = await create_example_data(do_not_create_resource=True)

        provider = core.catalog.testing.make_provider()

        await core.catalog.testing.create_or_update_provider(provider=provider)

        # create a service with the ID expected by the mock HCA handler
        service = core.catalog.testing.make_service(provider_id=provider.id)
        service.id = uuid.UUID("1be91786-b7e7-4fa3-81d9-1b95dd03cd52")

        await core.catalog.testing.create_or_update_service(service=service)

        # update the quota with this service
        quota = example_data.quota
        quota.catalog_service_id = service.id
        await core.catalog.testing.create_or_update_quota(quota=quota)

        assert example_data is not None

        resource = await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": MOCK_HCA_ACCEPT_VALUE,
                "testPropertyForPolicy": "exact",
            },
        )
        assert resource is not None

        # check if the resource has been created and is in form_submitted
        db_resource = await core.resources.get_resource_by_id(resource.id)
        assert db_resource is not None
        assert db_resource.state == ResourceStateEnum.form_submitted

        # send the resource to RabbitMQ
        await send_resource_request_to_queue(vhost=str(quota.catalog_service_id))

        db_messages = await get_messages_from_history(
            message_type="ResourceAllocateV1",
            resource_id=resource.id,
            catalog_service_id=quota.catalog_service_id,
            queue="to_hca",
        )

        assert len(db_messages) == 1

        # check if the resource state has been updated to request_submitted
        db_resource = await core.resources.get_resource_by_id(resource.id)
        assert db_resource is not None
        assert db_resource.state == ResourceStateEnum.request_submitted

        # run the mock HCA to handle the request and send a response back
        start_mock_hca()

        # get the response from RabbitMQ
        await consume_cloud_agent_message(vhost=str(quota.catalog_service_id))

        db_messages = await get_messages_from_history(
            message_type="ResourceCreatedV1",
            resource_id=resource.id,
            catalog_service_id=quota.catalog_service_id,
            queue="to_portal",
        )

        assert len(db_messages) == 1

        # check that the resource has been updated correctly. The mock HCA randomly sends successful and failure responses.
        db_resource = await core.resources.get_resource_by_id(resource.id)
        assert db_resource is not None
        assert db_resource.state == ResourceStateEnum.provisioned

    @staticmethod
    @pytest.mark.asyncio
    async def test_send_and_receive_messages_denied() -> None:
        example_data = await create_example_data(do_not_create_resource=True)

        provider = core.catalog.testing.make_provider()

        await core.catalog.testing.create_or_update_provider(provider=provider)

        # create a service with the ID expected by the mock HCA handler
        service = core.catalog.testing.make_service(provider_id=provider.id)
        service.id = uuid.UUID("1be91786-b7e7-4fa3-81d9-1b95dd03cd52")

        await core.catalog.testing.create_or_update_service(service=service)

        # update the quota with this service
        quota = example_data.quota
        quota.catalog_service_id = service.id
        await core.catalog.testing.create_or_update_quota(quota=quota)

        assert example_data is not None

        resource = await core.resources.request_resource(
            actor_identity=example_data.actor_identity,
            catalog_policy_id=example_data.policy.id,
            target_entity="self",
            name="Test Resource",
            specification={
                "testProperty": MOCK_HCA_DENY_VALUE,
                "testPropertyForPolicy": "exact",
            },
        )
        assert resource is not None

        # check if the resource has been created and is in form_submitted
        db_resource = await core.resources.get_resource_by_id(resource.id)
        assert db_resource is not None
        assert db_resource.state == ResourceStateEnum.form_submitted

        # send the resource to RabbitMQ
        await send_resource_request_to_queue(vhost=str(quota.catalog_service_id))

        db_messages = await get_messages_from_history(
            message_type="ResourceAllocateV1",
            resource_id=resource.id,
            catalog_service_id=quota.catalog_service_id,
            queue="to_hca",
        )

        assert len(db_messages) == 1

        # check if the resource state has been updated to request_submitted
        db_resource = await core.resources.get_resource_by_id(resource.id)
        assert db_resource is not None
        assert db_resource.state == ResourceStateEnum.request_submitted

        # run the mock HCA to handle the request and send a response back
        start_mock_hca()

        # get the response from RabbitMQ
        await consume_cloud_agent_message(vhost=str(quota.catalog_service_id))

        db_messages = await get_messages_from_history(
            message_type="ErrorV1",
            resource_id=resource.id,
            catalog_service_id=quota.catalog_service_id,
            queue="to_portal",
        )

        assert len(db_messages) == 1
        # check that the resource has been updated correctly. The mock HCA randomly sends successful and failure responses.
        db_resource = await core.resources.get_resource_by_id(resource.id)
        assert db_resource is not None
        assert db_resource.state == ResourceStateEnum.request_denied
        assert db_resource.state_text == "Resource already exists"
