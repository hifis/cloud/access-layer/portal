from django import template
from django.utils.safestring import SafeString, mark_safe
from markdown_it import MarkdownIt

register = template.Library()


class PortalMarkdownIt(MarkdownIt):
    # The function must be named this way as it is named like this in the package's base class
    def validateLink(self: "PortalMarkdownIt", url: str) -> bool:  # noqa: N802
        """Only allow https or mailto links whil would also pass regular validation."""
        return (url.startswith(("https://", "mailto:"))) and super().validateLink(url)


# This is a Django base class for which we do not have type annotations at the moment.
@register.filter(needs_autoescape=True)  # type: ignore[misc]
def render_markdown(
    text: str | None,
    autoescape: bool = True,  # noqa: ARG001,FBT001,FBT002 part of Django
) -> SafeString:
    """
    Render markdown. Only allow a set of allowlisted tags.
    """

    if text is None:
        return SafeString("")

    # Default markdown is supposed to be safe
    #
    # https://markdown-it-py.readthedocs.io/en/latest/other.html#security
    # https://markdown-it-py.readthedocs.io/en/latest/using.html#the-parser
    #
    # "html": False is passed for good measure even though it should already be set by the preset
    #
    markdown = PortalMarkdownIt("js-default", {"html": False}).disable("image")
    rendered_html = markdown.render(text)

    # Should be safe to mark_safe as HTML is disabled: https://markdown-it-py.readthedocs.io/en/latest/other.html#security
    return mark_safe(rendered_html)  # nosec B308, B703


register.filter("render_markdown", render_markdown)
