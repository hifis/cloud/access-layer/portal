// biome-ignore lint/correctness/noNodejsModules: Test files are not client-side
import { URL } from "node:url";
import { expect } from "@playwright/test";
import { loggedInTest } from "./login";

loggedInTest("login/logout cookie handling works as expected", async ({ page, context, baseURL }) => {
    const response = await page.goto("/");
    expect(response?.status()).toBe(200);

    // Check user is logged-in
    let navbar = await page.locator("nav");
    await expect(navbar).toContainText("Logout");

    // Check only expected cookies are set

    // Check for undefined, once to make the test fail
    // Second for the type checker
    expect(baseURL).not.toBeUndefined();
    if (baseURL === undefined) {
        throw new Error("baseURL can't be undefined!");
    }

    const testServerDomain = new URL(baseURL).hostname;
    let portalCookies = (await context.cookies()).filter((cookie) => cookie.domain === testServerDomain);
    let portalCookieNames = portalCookies.map((cookie) => cookie.name).sort();
    expect(
        JSON.stringify(portalCookieNames).localeCompare('["__Host-csrftoken","__Host-hidah","__Host-portal_token","__Host-sessionid"]') === 0,
    ).toBeTruthy();

    // Click logout
    await page.locator("#user-menu-button").click();
    await page.locator('#user-menu-dropdown input[value="Logout"]').click();

    // Check user was logged out and cookies were deleted
    navbar = await page.locator("nav");
    await expect(navbar).toContainText("Sign in");

    portalCookies = (await context.cookies()).filter((cookie) => cookie.domain === testServerDomain);
    portalCookieNames = portalCookies.map((cookie) => cookie.name).sort();
    expect(JSON.stringify(portalCookieNames).localeCompare('["__Host-csrftoken","__Host-hidah"]') === 0).toBeTruthy();
});

loggedInTest("client gets logged-out when modifying cookie", async ({ page, context, baseURL }) => {
    let response = await page.goto("/");
    expect(response?.status()).toBe(200);

    // Check user is logged-in
    let navbar = await page.locator("nav");
    await expect(navbar).toContainText("Logout");

    // Check only expected cookies are set

    // Check for undefined, once to make the test fail
    // Second for the type checker
    expect(baseURL).not.toBeUndefined();
    if (baseURL === undefined) {
        throw new Error("baseURL can't be undefined!");
    }

    const testServerDomain = new URL(baseURL).hostname;
    let portalCookies = (await context.cookies()).filter((cookie) => cookie.domain === testServerDomain);
    let portalCookieNames = portalCookies.map((cookie) => cookie.name).sort();
    expect(portalCookieNames).toEqual(["__Host-csrftoken", "__Host-hidah", "__Host-portal_token", "__Host-sessionid"]);

    // Modify cookie and access site again
    await context.addCookies([
        {
            name: "__Host-portal_token",
            value: "broken",
            secure: true,
            domain: testServerDomain,
            path: "/",
        },
    ]);
    response = await page.goto("/");
    expect(response?.status()).toBe(500);
    expect((await response?.allHeaders())?.["set-cookie"]).toContain(
        '__Host-sessionid=""; expires=Thu, 01 Jan 1970 00:00:00 GMT; Max-Age=0; Path=/;',
    );

    // Check user was logged out and cookies were deleted
    navbar = await page.locator("nav");
    await expect(navbar).toContainText("Sign in");
    await expect(page.locator("#message-1")).toHaveText(
        "You were signed out due to an error. Please try signing in again. If the problem persists, please contact support.",
    );

    portalCookies = (await context.cookies()).filter((cookie) => cookie.domain === testServerDomain);
    portalCookieNames = portalCookies.map((cookie) => cookie.name).sort();

    // User is logged-out (token cookie is still present as it does not get deleted immediately as the middleware
    // crashes before a repsonse is available)
    expect(portalCookieNames).toEqual(["__Host-csrftoken", "__Host-hidah", "__Host-portal_token"]);

    // Reload page and the token should be gone
    response = await page.reload();
    expect(response?.status()).toBe(200);
    expect((await response?.allHeaders())?.["set-cookie"]).toContain(
        '__Host-portal_token=""; expires=Thu, 01 Jan 1970 00:00:00 GMT; Max-Age=0; Path=/',
    );

    portalCookies = (await context.cookies()).filter((cookie) => cookie.domain === testServerDomain);
    portalCookieNames = portalCookies.map((cookie) => cookie.name).sort();

    expect(portalCookieNames).toEqual(["__Host-csrftoken", "__Host-hidah"]);

    navbar = await page.locator("nav");
    await expect(navbar).toContainText("Sign in");
});
