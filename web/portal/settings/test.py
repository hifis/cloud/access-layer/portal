import os
import secrets

import cryptography.fernet
from django.core.checks.security.base import SECRET_KEY_INSECURE_PREFIX

import core.app

# Patch environment before settings are loaded and core app is booted
os.environ["PORTAL_CORE_DATABASE_URL"] = core.app.require_envvar(
    "PORTAL_CORE_TEST_DATABASE_URL"
)

os.environ["DJANGO_SECRET_KEY"] = SECRET_KEY_INSECURE_PREFIX + secrets.token_urlsafe(64)

os.environ["PORTAL_TOKEN_ENCRYPTION_SECRET"] = (
    cryptography.fernet.Fernet.generate_key().decode("utf-8")
)


from .base import *  # noqa: F403 | Importing all base settings after patching is the intended effect here.
