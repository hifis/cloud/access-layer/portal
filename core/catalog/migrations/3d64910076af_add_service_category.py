"""
add_service_category

Add the service's primary category to the service profile.
First, set this to an empty string, then add data on next plony import.
"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "3d64910076af"
down_revision = "c6741e959354"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "catalog_service",
        sa.Column(
            "category",
            sa.Text,
            unique=False,
            index=False,
            nullable=True,
            comment="Service's primary category",
        ),
    )
    op.execute("UPDATE catalog_service SET category = '';")
    op.alter_column("catalog_service", "category", nullable=False)


def downgrade() -> None:
    op.drop_column("catalog_service", "category")
