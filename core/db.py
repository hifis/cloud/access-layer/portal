import asyncio
import threading
from collections.abc import AsyncIterator
from contextlib import asynccontextmanager

import psycopg
import psycopg_pool
from psycopg.rows import TupleRow


class Database:
    """Class providing psycopg connections for access to the core Postgres DB."""

    def __init__(
        self: "Database", db_url: str, min_pool_size: int = 1, max_pool_size: int = 4
    ) -> None:
        self.__min_pool_size = min_pool_size
        self.__max_pool_size = max_pool_size
        self.__db_url = db_url
        self.__thread_locals = threading.local()

    async def get_pool(self: "Database") -> psycopg_pool.AsyncConnectionPool:
        """
        Only use this if you need raw access to the pool, which normally should not
        be necessary!

        Try to fetch the connection pool for the current thread. Unfortunately this
        is necessary since Django uses a new thread for each request in development.
        When deploying with uvicorn a single thread and therefore single async loop is
        used so that the pool can be re-used.
        """

        # Ensure this thread has a local pool creation lock
        if not hasattr(self.__thread_locals, "pool_creation_lock"):
            self.__thread_locals.pool_creation_lock = asyncio.Lock()

        if not hasattr(self.__thread_locals, "pgpool"):
            async with self.__thread_locals.pool_creation_lock:
                # Re-check condition as there could have been concurrent attempts
                # at creating a pool asynchronously
                if not hasattr(self.__thread_locals, "pgpool"):
                    self.__thread_locals.pgpool = psycopg_pool.AsyncConnectionPool(
                        conninfo=self.__db_url,
                        min_size=self.__min_pool_size,
                        max_size=self.__max_pool_size,
                        # Lower timeout, this should happen within a few seconds and not half a minute
                        timeout=5,
                        # Set to something big, but set a limit so we do not run out of memory due to queued requests
                        max_waiting=1_000,
                        # Seconds before a connection gets closed
                        max_lifetime=60,
                        # Seconds before an idle connection gets closed
                        max_idle=10,
                        # New default
                        # https://www.psycopg.org/psycopg3/docs/api/pool.html#psycopg_pool.AsyncConnectionPool
                        open=False,
                    )

                    # Open the pool
                    await self.__thread_locals.pgpool.open(wait=True)

        if self.__thread_locals.pgpool is None:
            error_message = "Connection pool is None. This should never happen!"
            raise RuntimeError(error_message)

        return self.__thread_locals.pgpool  # type:ignore[no-any-return]

    @asynccontextmanager
    async def get_connection_with_transaction(
        self: "Database",
    ) -> AsyncIterator[psycopg.AsyncConnection[TupleRow]]:
        """Use this to retrieve a connection from the pool."""

        pool = await self.get_pool()

        # Get a connection from the pool and start a transaction
        async with pool.connection() as connection, connection.transaction():
            yield connection


#
# Global database connection
#

DB: Database


def boot(
    postgres_url: str, postgres_min_pool_size: int, postgres_max_pool_size: int
) -> None:
    """Initialize database connection. Called by core.app.boot."""

    global DB  # noqa: PLW0603

    DB = Database(postgres_url, postgres_min_pool_size, postgres_max_pool_size)


async def is_healthy() -> bool:
    """Returns true if the core database pool is ready and not overloaded."""

    if DB is None:
        return False

    result = False

    async with DB.get_connection_with_transaction() as connection:
        db_result = await (await connection.execute("SELECT true")).fetchone()

        if db_result is not None and db_result[0] is True:
            result = True

    return result
