# Comparing to magic values is part of testing examples
# ruff: noqa: PLR2004

import asyncio
import ssl
from uuid import uuid4

import httpx
import pytest
from pytest_httpx import HTTPXMock

from core.catalog import ServiceLink
from core.monitoring.availability import (
    AvailabilityClient,
    get_availability_history_for_service,
    get_service_availabilities,
    get_service_availability,
    get_unavailable_services,
    update_availabilities,
)

MOCK_SERVICES = [
    ServiceLink(catalog_service_id=uuid4(), service_link="https://test_url"),
    ServiceLink(catalog_service_id=uuid4(), service_link="https://test_url"),
    ServiceLink(catalog_service_id=uuid4(), service_link="https://test_url"),
]


@pytest.mark.usefixtures("_context_database")
class TestAvailabilitySuccessful:
    @staticmethod
    @pytest.mark.asyncio
    async def test_availability_successful(httpx_mock: HTTPXMock) -> None:
        """checks that a single successful service availability is updated correctly"""
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )
        assert availability_db is None

        client = AvailabilityClient(retries=0, delay=1, timeout=1)
        # mock a 200 response
        httpx_mock.add_response(status_code=200)

        # check the availability and update the database
        availability = await client.check_availability(MOCK_SERVICES[0])

        await update_availabilities([
            availability,
        ])

        # get the availability info from DB and check the correct status code
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert availability_db is not None
        assert availability_db.status_code == 200


@pytest.mark.usefixtures("_context_database")
class TestAvailabilityError:
    @staticmethod
    @pytest.mark.asyncio
    async def test_availability_error(httpx_mock: HTTPXMock) -> None:
        """Checks that a service connection error is handled correclty"""
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )
        assert availability_db is None

        client = AvailabilityClient(retries=0, delay=1, timeout=1)

        httpx_mock.add_exception(httpx.ConnectError("Error"))

        availability = await client.check_availability(MOCK_SERVICES[0])

        await update_availabilities([
            availability,
        ])

        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert availability_db is not None
        assert availability_db.status_code == -1


@pytest.mark.usefixtures("_context_database")
class TestAvailabilityTimeout:
    @staticmethod
    @pytest.mark.asyncio
    async def test_availability_timeout(httpx_mock: HTTPXMock) -> None:
        """Checks that a service timeout is handled correctly"""
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )
        assert availability_db is None

        client = AvailabilityClient(retries=0, delay=1, timeout=1)

        httpx_mock.add_exception(httpx.ConnectTimeout("Timeout"))

        availability = await client.check_availability(MOCK_SERVICES[0])

        await update_availabilities([
            availability,
        ])

        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert availability_db is not None
        assert availability_db.status_code == -2


@pytest.mark.usefixtures("_context_database")
class TestAvailabilityRemoteProtocolError:
    @staticmethod
    @pytest.mark.asyncio
    async def test_availability_remote_protocol_error(httpx_mock: HTTPXMock) -> None:
        """Checks that a remote protocol error is handled correctly"""
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )
        assert availability_db is None

        client = AvailabilityClient(retries=0, delay=1, timeout=1)

        httpx_mock.add_exception(httpx.RemoteProtocolError("Remote Protocol Error"))

        availability = await client.check_availability(MOCK_SERVICES[0])

        await update_availabilities([
            availability,
        ])

        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert availability_db is not None
        assert availability_db.status_code == -3


@pytest.mark.usefixtures("_context_database")
class TestAvailabilityReadTimeout:
    @staticmethod
    @pytest.mark.asyncio
    async def test_availability_read_timeout(httpx_mock: HTTPXMock) -> None:
        """Checks that a read timeout is handled correctly"""
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )
        assert availability_db is None

        client = AvailabilityClient(retries=0, delay=1, timeout=1)

        httpx_mock.add_exception(httpx.ReadTimeout("Read Timeout"))

        availability = await client.check_availability(MOCK_SERVICES[0])

        await update_availabilities([
            availability,
        ])

        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert availability_db is not None
        assert availability_db.status_code == -4


@pytest.mark.usefixtures("_context_database")
class TestAvailabilitySSLCertVerificationError:
    @staticmethod
    @pytest.mark.asyncio
    async def test_availability_ssl_verification_error(httpx_mock: HTTPXMock) -> None:
        """Checks that a SSLCertVerificationError is handled correctly"""
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )
        assert availability_db is None

        client = AvailabilityClient(retries=0, delay=1, timeout=1)

        httpx_mock.add_exception(ssl.SSLCertVerificationError())

        availability = await client.check_availability(MOCK_SERVICES[0])

        await update_availabilities([
            availability,
        ])

        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert availability_db is not None
        assert availability_db.status_code == -5


@pytest.mark.usefixtures("_context_database")
class TestAvailabilityUpdate:
    @staticmethod
    @pytest.mark.asyncio
    async def test_availabilities_successful(httpx_mock: HTTPXMock) -> None:
        """Updates the availabilities for all mock services and checks that they are added correctly to the database"""
        availabilities_db = await get_service_availabilities()
        assert availabilities_db == []

        client = AvailabilityClient(retries=0, delay=1, timeout=1)

        # run the checks for all services
        check_tasks = []
        for service in MOCK_SERVICES:
            # mock a 200 response
            httpx_mock.add_response(status_code=200)
            check_tasks.append(client.check_availability(service))

        # Wait for tasks to complete
        availabilities = await asyncio.gather(*check_tasks)

        # update the database
        await update_availabilities(availabilities)
        # check that all availabilities have been added to the DB
        availabilities_db = await get_service_availabilities()

        assert len(availabilities) == len(MOCK_SERVICES)


@pytest.mark.usefixtures("_context_database")
class TestAvailabilityUnavailable:
    @staticmethod
    @pytest.mark.asyncio
    async def test_availabilities_only_unavailable(httpx_mock: HTTPXMock) -> None:
        """Updates all three mock services of with only one is unavailable.
        Checks that get_unavailable services only returns the one service"""
        availabilities_db = await get_service_availabilities()
        assert availabilities_db == []

        client = AvailabilityClient(retries=0, delay=1, timeout=1)
        availabilities = []
        # mock a 200 response
        httpx_mock.add_response(status_code=200)

        # check the availability and update the database
        availability = await client.check_availability(MOCK_SERVICES[0])
        availabilities.append(availability)
        # check the availability and update the database
        httpx_mock.add_response(status_code=200)
        availability = await client.check_availability(MOCK_SERVICES[1])
        availabilities.append(availability)

        # mock a 200 response
        httpx_mock.add_exception(httpx.ConnectError("Error"))

        # check the availability and update the database
        availability = await client.check_availability(MOCK_SERVICES[2])
        availabilities.append(availability)
        await update_availabilities(availabilities)

        # update the database
        await update_availabilities(availabilities)
        # check that all availabilities have been added to the DB
        availabilities_db = await get_service_availabilities()

        assert len(availabilities) == len(MOCK_SERVICES)
        # check that only unavailable services are returned
        unavailable_services = await get_unavailable_services()

        assert len(unavailable_services) == 1
        assert unavailable_services[0].status_code == -1


@pytest.mark.usefixtures("_context_database")
class TestAvailabilityHistory:
    @staticmethod
    @pytest.mark.asyncio
    async def test_availability_history(httpx_mock: HTTPXMock) -> None:
        """Updates the availability multiple times for a single service and checks that the history table is filled correctly"""
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )
        assert availability_db is None

        history_db = await get_availability_history_for_service(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert len(history_db) == 0

        client = AvailabilityClient(retries=0, delay=1, timeout=1)
        # mock a 200 response
        httpx_mock.add_response(status_code=200)
        # check availability and update db
        availability = await client.check_availability(MOCK_SERVICES[0])

        await update_availabilities([
            availability,
        ])
        # check availability has been added to db
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert availability_db is not None
        assert availability_db.status_code == 200
        # mock a connection error
        httpx_mock.add_exception(httpx.ConnectError("Error"))
        # check availability and update db
        availability = await client.check_availability(MOCK_SERVICES[0])

        await update_availabilities([
            availability,
        ])
        # check availability has been added to db
        availability_db = await get_service_availability(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert availability_db is not None
        assert availability_db.status_code == -1
        # check previous availability info has been added to history
        history_db = await get_availability_history_for_service(
            MOCK_SERVICES[0].catalog_service_id
        )

        assert len(history_db) == 1
        assert history_db[0].status_code == 200
