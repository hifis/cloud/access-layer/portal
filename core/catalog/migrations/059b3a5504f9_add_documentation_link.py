"""
add_documentation_link
"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "059b3a5504f9"
down_revision = "3d64910076af"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "catalog_service",
        sa.Column(
            "general_link_documentation",
            sa.Text,
            unique=False,
            index=False,
            nullable=True,
            comment="Link to the service's documentation.",
        ),
    )


def downgrade() -> None:
    op.drop_column("catalog_service", "general_link_documentation")
