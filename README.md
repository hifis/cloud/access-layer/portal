<div align="center"><img src="web/base/static/images/nav_logo.svg" alt="logo" /></div>

## Overview

The [Helmholtz Cloud Portal](https://helmholtz.cloud) is a web applications where users can discover and manage HIFIS cloud services. You can see how the portal fits into the general HIFIS architecture [here](https://hifis.net/doc/service-integration/).

## Useful commands

### Development

#### Development setup
The development setup is container-based. You need `vscode`, `podman` and the latest `docker-compose`. To get started, go to `/dev/dev_env.example`, copy it to `/dev/dev_env` and fill-in the values. Never ever commit this file (it should be in `.gitignore` anyway). To modify the portal, you need Plony access if you want to display services in your local environment. Feel free to ask the portal development team. To pull the portal development container, you need to request read-access to the container registry or build it yourself using the `Containerfile` in `/dev`. You need to modify the `docker-compose.yml` in that case.

Next, start the container setup in `/dev` with `docker-compose up`, stop it and `up` it again. You can then launch vscode and install the [Dev Containers Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers). Then, open the repository's directory in vscode and click the reopen in dev container button at the bottom right. As a final first-time setup step open a terminal within vscode, run `setup_portal`. This should set up the DB and pull service metadata from plony so you can see the service catalog.
Now, you can launch the portal dev server by clicking on the run and debug menu in the sidebar and launching the `Development Server` configuration. This is going to boot the Django development server as well as the frontend build pipeline for JS/CSS assets. You can also set breakpoints in the Python code now! Finally, access [https://localhost:8000](https://localhost:8000) in your local browser and you should be seeing the portal. To see what's going on in the database, open [https://localhost:8081](https://localhost:8080). When you make changes to templates etc. your browser should auto-reload the page!

When you want to run this setup in the future: after booting your machine run `docker-compose up` to start the database etc., then open vscode, re-open in dev container if necessary and start the `Development Server` run configuration.

#### Fresh container and database
Run `setup_portal` to install the python dependencies and prepare the database by running the migrations.

Note: if you're starting with an empty database you may need to restart the containers after the first start, so the DB Web UI is loaded properly.

#### Servers
- `dev` to start up the application server and build watchers

##### Manual control
- `run_portal` to start the Django server
- `run_tailwind` to start the CSS build watcher
- `run_vite` to start the JS build watcher

#### Building a release
- `deno run build`

#### Running end-to-end tests
- `deno run test` You may need to install additional dependencies to install these tests. Accessibility tests may fail when running this locally.

#### Database Migrations
Be sure to set `branch_labels` when starting a new context in `core`. We are working with [multiple heads](https://alembic.sqlalchemy.org/en/latest/branches.html#working-with-branch-labels) with one head per context in `core`. Every head starts at the `base` revision (`down_revision = None`) and is labelled with the context's name.

- `alembic revision -m "YOUR_DESCRIPTION" --version-path=core/YOUR_CONTEXT/migrations --head CURRENT_HEAD_REVISION_OF_CONTEXT` to add a new DB migration to the core
- `alembic upgrade heads` to migrate all heads to their latest revision
- `alembic downgrade YOUR_CONTEXT@-1` to downgrade a context by one revision
- `alembic upgrade YOUR_CONTEXT@+1` to upgrade a context by one revision
- `alembic heads` to list the current context's revisions. Add `--verbose` for more information.
- `alembic history` to list DB migration history. Add `--verbose` for more information.

#### How to start a new Django application

The Django 'frontend' is grouped into apps. When you want to start a new one:

```bash
mkdir web/your_app_name
python manage.py startapp your_app_name web/your_app_name
```

change `name = "your_app_name"` to `name = "web.your_app_name"` in `apps.py`. Then, you can install your new app in `web/portal/settings/base.py` by adding it to `INSTALLED_APPS`.

You can remove the admin, model and migration files as they are not needed in this project. The admin is not used and the core database is managed by the core module and alembic. Then, you're ready to go! You could start linking urls to the app's views in the global URl file in `web/portal/urls.py` now. You can use the other apps as inpiration on how to structure your directories for static files etc. Remember that `web/base` and `web/portal` are special cases as they contain mostly global boilerplate code you might not need in your own app.

### Production
- `DJANGO_SETTINGS_MODULE=web.portal.settings.production gunicorn web.portal.asgi:application -k uvicorn.workers.UvicornWorker` to start the ASGI server

- Log out everyone
```python
python manage.py shell

>>> from django.contrib.sessions.models import Session
>>> Session.objects.all().delete()

(2, {'base.User': 1, 'sessions.Session': 1})
```

## Design Paradigm

The Cloud Portal is a Python application split into two main components. The web application and the core. The web application is a Django application doing the session handling / OIDC with sprinkles of Vue.js providing interactivity where needed. The core can be considered a completely separate application based on standard Python tooling such as pydantic, alembic and psycopg. Think of the web application as a web interface to the core (similar to 'Phoenix is not your application' if you're familiar with the Elixir programming language). Python was chosen because of its wide adoption in the scientific community, thus facilitating community contributions.

The reasoning behind the split between the core and web application is that this way the core business logic of cloud management stays separate from the presentation logic of the web interface. If in the future e.g. JSON API access to HIFIS resource management needs to be implemented or a new frontend technology emerges, it merely forms another 'frontend' interacting with the core APIs for provisioning resources and so on.

### Core
The core, as the name suggests, contains the Portal's core business logic. For example, it syncs the service catalog with Plony, coordinates resource provisioning and monitors external services. By tapping into Python's emerging async and typing ecosystem the design is intended to provide a balance between safety, performance and Python's flexibility.

The core is divided into domain contexts inspired by the domain-driven-design philosophy (e.g. the service catalog or monitoring). Ideally, this makes cross-domain interdependencies explicit with a clear public API for each context. When interacting with core APIs, users can rely on type-checked pydantic objects providing basic type-safety. If the need arises to replace a context's functionality with a 'faster' language later on, the Python API can be kept in place and serve as a facade for an external service implemented in e.g. Rust or Go.

There are some 'cross-cutting-resources' such as access to Postgres (contexts use separate tables though) or central external services such as Plony or message queues, which are needed by multiple contexts. These are located at the root of the core package. In order to initialize the core you simply call the `core.app.boot(...)` function to set up all the connection pools and access to external resources needed for running all core functionality. For an example, you can look at the `portalctl.py` utility which offers a CLI for some core tasks. The Django app basically does the same thing when loading its settings file, booting up the core so core APIs can be used by views.

#### Development Guidelines for Core Contexts
- When starting a new context consider its scope carefully
- API Design:
    - Contexts have one canonical entry point for their public API: `core.context_name` everything below this root namespace shall be considered private and *must not* be called from outside that context
        - Why?
            - 1. this leads to a clearly defined external API for this context
            - 2. this allows for some freedom when moving things around within a context as long as you keep the API and its semantics compatible
    - Each public method a context offers must have type hints and sufficient documentation
    - That API must be covered by tests
    - When receiving or returning data beyond primitive types, they must be passed as pydantic structs
- Database:
    - Put migrations into a migrations subdirectory inside the context directory. You can link it in the alembic config.
    - Prefix all table names with the context name: `contextname_tablename`
    - Avoid integration between contexts on the database level. Remember, any context may be moved into a network service with completely different storage. Your only point of interaction with other contexts is by calling their API. Therefore:
        - Avoid `JOIN`s between contexts, the table may move outside the DB
        - Do not rely on cascading deletes between contexts, you can't count on internal deletion semantics of other contexts
        - When storing IDs, always prefix their name with the context ID even within the same context (e.g. `catalog_service_id`), otherwise IDs names may become confusing when storing another context's ID in your table, exception: the ID column of the table itself

### Web Application
The web application provides a web interface to the core functions. While it is built on top of the Django ecosystem, the heavy lifting with regards to business logic is done by the core. Use of the ORM is mostly limited to OIDC/session handling. The web application's database is separate from the core database. It's probably not a good idea to have two migration tools (alembic for core, django for web) modifying the same database.
The UI can be styled using Tailwind CSS and Vue.js can be used if interactivity is needed. However, the portal is not a single page application. The UI can be tested with Playwright end-to-end tests.

#### Development Guidelines for the Web Application
- Prefer server-rendered UI when possible to reduce JavaScript/TypeScript complexity
- When creating a new view, be sure to cover it with Playwright accessibility tests
- You can use the other apps as a template when creating a new app. Also see the guide on apps in another section of this file.

#### Helmholtz ID Profile Information
At the moment Helmholtz ID profile information is stored encrypted in the database and tied to the session. The main implementation can be found in `web/base/auth.py`. On login the portal fetches profile information from the Helmholtz ID, creates a temporary user and encrypts the profile information using a random key. The key is then handed to the user in a cookie. This way the portal can decrypt profile information when the user sends the key along with a request. After the session ends, the portal can delete that data. This is supposed to prevent the build-up of profile information of users who may not perform advanced actions (such as requesting resources) in the database. The design is expected to evolve as requirements continue to develop.

### CLI
The second interface to the appplication is the `portalctl.py` file offering a CLI interface for common tasks such as importing data from Plony or setting the maintenance message.

## Configuration

### Core

| Variable | Description |
| -- | -- |
|`PORTAL_CORE_DATABASE_URL`|The database URL for the core part of the portal.|
|`PORTAL_CORE_TEST_DATABASE_URL`|The database URL used during testing.|
|`PORTAL_CORE_POSTGRES_MIN_POOL_SIZE`|The minimum number of postgres connections to keep open for the core part of the portal. Optional, default: 10|
|`PORTAL_CORE_POSTGRES_MAX_POOL_SIZE`|The minimum number of postgres connections to keep open for the core part of the portal. Optional, default: 20|
|`PORTAL_CORE_DATABASE_URL`|The database URL for the core part of the portal.|
|`EXCEPTION_TRACKING_ENABLED`|Set to `True` to enable exception tracking.|
|`EXCEPTION_TRACKING_DSN`|The endpoint/access token for submitting exception tracking data.|
|`EXCEPTION_TRACKING_CA_CERT`|CA certificate of the exception tracking endpoint.|
|`EXCEPTION_TRACKING_ENVIRONMENT`|The environment to tag exception tracking events with.|
|`EXCEPTION_TRACKING_RELEASE`|The version to tag exception tracking events with.|

### portalctl.py

| Variable | Description |
| -- | -- |
|`PLONY_BASE_URL`|Base URL of the Plony instance.|
|`PLONY_USER`|Plony user|
|`PLONY_PASSWORD`|Plony password|

### Web

| Variable | Description |
| -- | -- |
|`PORTAL_WEB_DATABASE_URL`|The database URL for the web (Django) part of the portal.|
|`HELMHOLTZ_ID_CLIENT_ID`|Client ID to use for the OIDC flow.|
|`HELMHOLTZ_ID_CLIENT_SECRET`|Client secret to use for the OIDC flow.|
|`HELMHOLTZ_ID_AUTHORIZATION_ENDPOINT`|OIDC authorization endpoint the user gets forwarded to.|
|`HELMHOLTZ_ID_JWKS_ENDPOINT`|OIDC JWKs endpoint for token signing keys.|
|`HELMHOLTZ_ID_TOKEN_ENDPOINT`|OIDC token endpoint for retrieving tokens after the user has completed the login process.|
|`HELMHOLTZ_ID_USERINFO_ENDPOINT`|OIDC userinfo endpoint used for retrieving user profile information.|
|`HELMHOLTZ_ID_AUTHORITATIVE_DIRECTORY`|The authoritative directory to use for e.g. checking profile assurances. You never need to changes this to something different than `login.helmholtz.de` outside of testing scenarios (e.g. HIDaH).|
|`PORTAL_TOKEN_ENCRYPTION_SECRET`|The server secret used for signing/encrypting the user key before handing it to the user. Generate with `python -c "import cryptography.fernet; print(cryptography.fernet.Fernet.generate_key().decode('utf-8'))"`.|
|`DJANGO_SECRET_KEY`|The Django [secret key](https://docs.djangoproject.com/en/4.1/ref/settings/#secret-key).|
|`DJANGO_SETTINGS_MODULE`|The settings module of the web portal `web.portal.settings.development`.|
|`LOG_LEVEL`|The log level of the console output of the Django server.|
|`ALLOWED_HOSTS`|The hostnames django should serve the requests for.|
|`ENABLE_HSTS_PRELOAD`|Whether to enable [HSTS preload](https://docs.djangoproject.com/en/5.0/ref/settings/#std-setting-SECURE_HSTS_PRELOAD).|
|`HELPDESK_ENDPOINT`|URL to helpdesk instance.|
|`HELPDESK_PATH`|API endpoint to use for creating helpdesk tickets.|
|`HELPDESK_TOKEN`|Authentication token to use when connecting to helpdesk.|
|`HELPDESK_DEPROV_GROUP`|Group to create deprovisioning tickets in.|
|`HELPDESK_DEPROV_MESSAGE`|Message which should be put into the deprovisioning ticket.|
|`HELPDESK_DEPROV_TITLE`|Title of deprovisioning tickets.|
|`HELPDESK_FEEDBACK_GROUP`|Group to create feedback tickets in.|
|`HELPDESK_TYPE`|Ticket article type to use when creating helpdesk tickets.|
|`PLAYWRIGHT_BASE_URL`|Base URL end to end tests are going to connect to.|
|`PLAYWRIGHT_BASIC_AUTH_USER`|If the server to be tested is protected by basic auth, supply the user here.|
|`PLAYWRIGHT_BASIC_AUTH_PASSWORD`|If the server to be tested is protected by basic auth, supply the password here.|
|`PORTAL_CLOUD_AGENT_HOST`|RabbitMQ hostname for the cloud agent receiver/sender.|
|`PORTAL_CLOUD_AGENT_PORT`|RabbitMQ port for the cloud agent receiver/sender.|
|`PORTAL_CLOUD_AGENT_USE_SSL`|Enable SSL transfer encryption for RabbitMQ connections.|
|`PORTAL_CLOUD_AGENT_USERNAME`|Username to authenticate to RabbitMQ.|
|`PORTAL_CLOUD_AGENT_PASSWORD`|Password to authenticate to RabbitMQ.|
|`PORTAL_CLOUD_RECEIVE_QUEUE`|RabbitMQ queue used to receive messages from cloud agent.|
|`PORTAL_CLOUD_SEND_QUEUE`|RabbitMQ queue used to send messages to cloud agent.|
|`PORTAL_CLOUD_AGENT_RECONNECT_INTERVAL`|Interval in seconds to wait until next reconnect attempt.|
