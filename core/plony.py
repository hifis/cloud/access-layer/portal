"""Module for communicating with plony."""

import asyncio
import json
import typing
from pathlib import Path
from typing import Any

import httpx
import pydantic
from pydantic import BaseModel, ConfigDict, Field


class PlonyLink(BaseModel):
    at_id: pydantic.HttpUrl = Field(..., alias="@id")
    title: str


class PlonyImageLink(BaseModel):
    download: pydantic.HttpUrl
    filename: str
    content_type: str = Field(..., alias="content-type")


class PlonyTokenizedString(BaseModel):
    title: str
    token: str


class PlonyKeyword(BaseModel):
    uid: pydantic.UUID4 = Field(..., alias="UID")
    at_id: pydantic.HttpUrl = Field(..., alias="@id")
    title: str
    is_visible: bool = Field(..., alias="isVisible")
    is_category: bool = Field(..., alias="isCategory")


class PlonyInstitution(BaseModel):
    uid: pydantic.UUID4 = Field(..., alias="UID")
    at_id: pydantic.HttpUrl = Field(..., alias="@id")
    title: str
    abbreviation: str
    url: pydantic.HttpUrl | None
    is_service_provider: bool
    logo: PlonyImageLink | None


class PlonyService(BaseModel):
    uid: pydantic.UUID4 = Field(..., alias="UID")
    at_id: pydantic.HttpUrl = Field(..., alias="@id")
    title: str
    description: str
    service_category: PlonyTokenizedString | None
    backup_strategy: str
    general_software_name: str
    general_short_text_cloud_portal: str
    general_long_description: str
    general_link_service_usage: pydantic.HttpUrl | None
    general_documentation: pydantic.HttpUrl | None
    communication_contact_user_support: str
    service_lvl_description: str | None
    user_limitations: str
    user_access_description: str
    review_state: str
    other_accept_publish_to_portal: PlonyTokenizedString
    data_storage_location: PlonyTokenizedString
    service_provider: list[PlonyLink]
    general_service_logo: PlonyImageLink | None
    service_keywords: list[PlonyLink]


class PlonyNumericFieldDefinition(BaseModel):
    unit: str
    display_name_unit: str


class PlonyStringFieldDefinition(BaseModel):
    max_length: int


class PlonyPropertyDefinition(BaseModel):
    display_name: str
    description: str
    required: bool
    numeric_field: PlonyNumericFieldDefinition | None
    string_field: PlonyStringFieldDefinition | None


class PlonyResourceType(BaseModel):
    id: pydantic.UUID4
    name: str
    description: str
    json_schema: dict[str, Any]
    ui_schema: dict[str, Any]


class PlonyQuotaLimit(BaseModel):
    property: str
    total: int


class PlonyQuota(BaseModel):
    id: pydantic.UUID4
    service_id: pydantic.UUID4
    resource_type_id: pydantic.UUID4
    name: str
    quota: list[PlonyQuotaLimit]


class PlonyPolicyActorRequirements(BaseModel):
    # Strict so it does not have surprising effects when users try to match on unsupported fields
    model_config = ConfigDict(extra="forbid", strict=True)

    edu_person_entitlement: list[str] = Field(
        ..., alias="eduPersonEntitlement", min_length=1
    )
    edu_person_assurance: list[str] | None = Field(..., alias="eduPersonAssurance")
    edu_person_scoped_affiliation: list[str] | None = Field(
        ..., alias="eduPersonScopedAffiliation"
    )


class PlonyPolicy(BaseModel):
    id: pydantic.UUID4
    quota_id: pydantic.UUID4
    actor_requirements: PlonyPolicyActorRequirements
    target_entity: str
    name: str
    review_state: str
    json_schema: dict[str, Any]
    time_seconds: int


class Client:
    """An async API client for plony. Remember to close the client with aclose() once you're done.
    In case something goes wrong it throws an exception. Caution: this client does not support multiple batches.
    If a response would return more than 1000 items, it raises."""

    # Size of response batches
    # https://plonerestapi.readthedocs.io/en/latest/querystringsearch.html#batch-start-b-start
    BATCH_SIZE = 1_000

    # Mock data location while Plony API is not ready yet
    __MOCK_DATA_DIR = Path(__file__).parent / "../mock_data/plony"

    def __init__(
        self: "Client",
        username: str,
        password: str,
        base_url: str = "https://plony.helmholtz-berlin.de",
    ) -> None:
        self.plony_auth_cookie_cache_lock = asyncio.Lock()
        self.plony_auth_cookie_cache: dict[str, str] = {}
        self.username = username
        self.password = password
        self.client = httpx.AsyncClient(
            base_url=base_url,
            headers={
                "user-agent": "plony_importer (Cloud Portal)",
                "accept": "application/json",
            },
        )

    async def aclose(self: "Client") -> None:
        """Use this method to close the client once you're done."""
        await self.client.aclose()

    async def list_services(self: "Client") -> list[PlonyService]:
        """List service data available at plony."""
        response = await self.client.post(
            "/services/@querystring-search",
            json={
                "fullobjects": 1,
                "b_size": Client.BATCH_SIZE,
                "query": [
                    {
                        "i": "portal_type",
                        "o": "plone.app.querystring.operation.selection.any",
                        "v": ["Service"],
                    }
                ],
            },
            headers={"accept": "application/json"},
            auth=(self.username, self.password),
        )

        response_data = Client.__parse_and_validate_response(response)["items"]

        return [PlonyService(**item) for item in response_data]

    async def list_institutions(self: "Client") -> list[PlonyInstitution]:
        """List institutions available at plony."""
        response = await self.client.post(
            "/@querystring-search",
            json={
                "fullobjects": 1,
                "b_size": Client.BATCH_SIZE,
                "query": [
                    {
                        "i": "portal_type",
                        "o": "plone.app.querystring.operation.selection.any",
                        "v": ["Institution"],
                    }
                ],
            },
            headers={"accept": "application/json"},
            auth=(self.username, self.password),
        )

        response_data = Client.__parse_and_validate_response(response)["items"]

        return [PlonyInstitution(**item) for item in response_data]

    async def list_keywords(self: "Client") -> list[PlonyKeyword]:
        """List keywords available at plony."""
        response = await self.client.post(
            "/service_keywords/@querystring-search",
            json={
                "fullobjects": 1,
                "b_size": Client.BATCH_SIZE,
                "query": [
                    {
                        "i": "portal_type",
                        "o": "plone.app.querystring.operation.selection.any",
                        "v": ["ServiceKeyword"],
                    }
                ],
            },
            headers={"accept": "application/json"},
            auth=(self.username, self.password),
        )

        response_data = Client.__parse_and_validate_response(response)["items"]

        return [PlonyKeyword(**item) for item in response_data]

    async def list_resource_types(self: "Client") -> list[PlonyResourceType]:
        """List resource types available at plony."""

        with Path.open(
            self.__MOCK_DATA_DIR / "resource_types.json", mode="r"
        ) as mock_file:
            response_data = json.load(mock_file)

        return [PlonyResourceType(**item) for item in response_data]

    async def list_quotas(self: "Client") -> list[PlonyQuota]:
        """List resource types available at plony."""

        with Path.open(self.__MOCK_DATA_DIR / "quotas.json", mode="r") as mock_file:
            response_data = json.load(mock_file)

        return [PlonyQuota(**item) for item in response_data]

    async def list_policies(self: "Client") -> list[PlonyPolicy]:
        """List policies available at plony."""

        with Path.open(self.__MOCK_DATA_DIR / "policies.json", mode="r") as mock_file:
            response_data = json.load(mock_file)

        return [PlonyPolicy(**item) for item in response_data]

    async def get_image(self: "Client", url: str, content_type: str) -> bytes:
        """Download an image from plony."""
        cookie = await self.__get_plony_auth_cookie()

        response = await self.client.get(
            url,
            headers={**self.client.headers, "accept": content_type},
            cookies=cookie,
        )

        response.raise_for_status()

        return response.content

    async def __get_plony_auth_cookie(self: "Client") -> dict[str, str]:
        """Some requests require cookie auth. This method retrieves an auth cookie. Cookies are cached."""

        if self.plony_auth_cookie_cache:
            return self.plony_auth_cookie_cache

        # Try to acquire lock on cache.
        # It can happen that multiple async tasks are trying to set the cookie at the same time.
        # Therefore, re-check cache once lock has been acquired.

        async with self.plony_auth_cookie_cache_lock:
            if self.plony_auth_cookie_cache:
                return self.plony_auth_cookie_cache

            response = await self.client.post(
                "/@login",
                json={
                    "login": self.username,
                    "password": self.password,
                },
                auth=(self.username, self.password),
            )

            response.raise_for_status()

            self.plony_auth_cookie_cache = {"plone_auth": response.json()["token"]}

        return self.plony_auth_cookie_cache

    @staticmethod
    def __parse_and_validate_response(response: httpx.Response) -> dict[str, Any]:
        """
        Validates response code and structure, returns parsed response, raises on error.
        """
        response.raise_for_status()
        response_data: dict[str, typing.Any] = response.json()

        if response_data["items_total"] > Client.BATCH_SIZE:
            error_message = f"Response would return more than {Client.BATCH_SIZE} items. This client does not support this yet. Please add batching to the client."
            raise RuntimeError(error_message)

        return response_data
