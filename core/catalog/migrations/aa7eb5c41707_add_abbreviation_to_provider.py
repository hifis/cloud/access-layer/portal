"""
add_provider_url

This migration works with existing data present, abbreviations are set to a blank value.
To avoid pydantic errors at runtime, a plony import must be run immediately afterwards so the table contains valid data.
"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "aa7eb5c41707"
down_revision = "738f38a6dd8a"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "catalog_provider",
        sa.Column(
            "abbreviation",
            sa.Text,
            unique=False,
            index=False,
            nullable=True,
            comment="Abbreviation",
        ),
    )
    op.execute("UPDATE catalog_provider SET abbreviation = '';")
    op.alter_column("catalog_provider", "abbreviation", nullable=False)


def downgrade() -> None:
    op.drop_column("catalog_provider", "abbreviation")
