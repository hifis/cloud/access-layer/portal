import { defineCustomElement } from "vue";

import Button from "./custom_elements/button.vue";
import Loader from "./custom_elements/loader.vue";
import Message from "./custom_elements/message.vue";

// Elements
const ButtonElement = defineCustomElement(Button);
const LoaderElement = defineCustomElement(Loader);
const MessageElement = defineCustomElement(Message);

// Element Registration
customElements.define("hifis-button", ButtonElement);
customElements.define("hifis-loader", LoaderElement);
customElements.define("hifis-message", MessageElement);
