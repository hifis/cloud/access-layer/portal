## Container-based Development Environment

### Getting Started

This folder keeps all configuration files needed for a Container-based development environment. First, copy the file `dev_env.example` to `dev_env`. You can leave the values as is. Once you want to do some more advanced testing you can replace them with values for the testing systems.

Getting the environment running is a simple as running:

```
> podman-compose up -d
```

The development container will mount this repository so you can make your changes on your host system and use the container to build, test and run the Cloud Portal front and backend.

