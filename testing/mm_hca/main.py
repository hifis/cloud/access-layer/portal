import logging
import time
import uuid

from helmholtz_cloud_agent.core.main import HCAApplication
from helmholtz_cloud_agent.messages import (
    ResourceAllocateV1,
    ResourceCreatedV1,
)
from pydantic import BaseModel, TypeAdapter, ValidationError


class MmTeamResourceSpecV1(BaseModel):
    team_name: str
    team_slug: str
    invite_only: bool


# Global logger
logging.basicConfig(
    format="%(asctime)s\t%(levelname)s\t%(filename)s\t%(funcName)s\t%(message)s"
)

logger = logging.getLogger("hca")


class TeamMustBeInviteOnlyError(Exception):
    def __init__(
        self: "TeamMustBeInviteOnlyError",
        message: str = "The team must be invite only",
    ) -> None:
        self.message = message
        super().__init__(self.message)

    def __str__(self: "TeamMustBeInviteOnlyError") -> str:
        return f"{self.message}"


app = HCAApplication()


# a simple message handler for the Mattermost team use case that is used in the e2e tests
# it returns a ResourceCreatedV1 if invite_only is true otherwise it return a TeamMustBeInviteOnly exception
@app.handle(message_type="ResourceAllocateV1")  # type: ignore[misc]
def resource_allocate_v1(
    correlation_id: str, payload: ResourceAllocateV1
) -> ResourceCreatedV1:
    msg = f"Got ResourceAllocateV1 request for {correlation_id} payload: {payload}"
    logger.info(msg)

    # Wait 3 seconds so that Playwright can check the request_submitted state in the resource table
    time.sleep(3)

    spec: MmTeamResourceSpecV1
    try:
        dataclass_validator = TypeAdapter(MmTeamResourceSpecV1)
        spec = dataclass_validator.validate_python(payload.specification)

    except ValidationError:
        logger.exception("cannot validate request specification")

    if spec.invite_only:
        resource_id = str(uuid.uuid4())
        return ResourceCreatedV1(id=resource_id)
    else:
        raise TeamMustBeInviteOnlyError


def start_mm_hca() -> None:
    logger.setLevel(level=logging.DEBUG)
    app.run()
