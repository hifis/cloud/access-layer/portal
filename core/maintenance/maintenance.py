import datetime

import psycopg.rows
import pydantic

import core.db

# Magic UUID as there's only one message
MESSAGE_UUID = "c6507a25-55b6-4618-9885-1b964c14105f"


class MaintenanceMessage(pydantic.BaseModel):
    id: pydantic.UUID4
    show_until: datetime.datetime
    message: str


async def set_maintenance_message(message: str, show_until: datetime.date) -> None:
    """
    Set the maintenance message.
    """

    insert_message_query = """
    INSERT INTO maintenance_message (id, show_until, message)
    VALUES (%(id)s, %(show_until)s, %(message)s)
    ON CONFLICT (id) DO UPDATE SET show_until = EXCLUDED.show_until, message = EXCLUDED.message
    """

    async with core.db.DB.get_connection_with_transaction() as connection:
        await connection.execute(
            insert_message_query,
            {"id": MESSAGE_UUID, "show_until": show_until, "message": message},
        )


async def get_current_maintenance_message() -> MaintenanceMessage | None:
    """
    Get current maintenance message if there's one.
    """

    async with core.db.DB.get_connection_with_transaction() as connection:
        message_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(MaintenanceMessage)
        ).execute(
            "SELECT id, show_until, message FROM maintenance_message WHERE id = %(message_id)s AND show_until > now()",
            {"message_id": MESSAGE_UUID},
        )

        return await message_rows.fetchone()
