from django.apps import AppConfig


# This is a Django base class for which we do not have type annotations at the moment.
class ServicesConfig(AppConfig):  # type: ignore[misc]
    default_auto_field = "django.db.models.BigAutoField"
    name = "web.services"
