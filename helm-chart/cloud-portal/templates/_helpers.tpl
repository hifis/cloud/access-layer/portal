{{/* Generate basic labels */}}
{{- define "hifisCloudPortal.labels" }}
  labels:
    app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
    app.kubernetes.io/instance: {{ .Release.Name | quote }}
    app.kubernetes.io/version: {{ .Chart.AppVersion }}
    helm.sh/chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
{{- end }}

{{/* define common environment variables */}}
{{- define "hifisCloudPortal.envVariables" }}
env:
- name: PORTAL_WEB_DATABASE_URL
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-database-credentials
      key: web
- name: PORTAL_CORE_DATABASE_URL
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-database-credentials
      key: core
- name: DJANGO_SETTINGS_MODULE
  value: {{ .Values.config.django.settingsModule | quote }}
- name: DJANGO_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-django-secret
      key: secret_key
- name: PORTAL_TOKEN_ENCRYPTION_SECRET
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-portal-token-encryption-secret
      key: portal_token_encryption_secret
- name: HELMHOLTZ_ID_AUTHORIZATION_ENDPOINT
  value: {{ .Values.config.helmholtzID.authorizationEndpoint | quote }}
- name: HELMHOLTZ_ID_JWKS_ENDPOINT
  value: {{ .Values.config.helmholtzID.jwksEndpoint | quote }}
- name: HELMHOLTZ_ID_TOKEN_ENDPOINT
  value: {{ .Values.config.helmholtzID.tokenEndpoint | quote }}
- name: HELMHOLTZ_ID_USERINFO_ENDPOINT
  value: {{ .Values.config.helmholtzID.userinfoEndpoint | quote }}
- name: HELMHOLTZ_ID_AUTHORITATIVE_DIRECTORY
  value: {{ .Values.config.helmholtzID.authoritativeDirectory | quote }}
- name: HELMHOLTZ_ID_CLIENT_ID
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-helmholtz-id-credentials
      key: client_id
- name: HELMHOLTZ_ID_CLIENT_SECRET
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-helmholtz-id-credentials
      key: client_secret
- name: PLONY_USER
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-plony-credentials
      key: user
- name: PLONY_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-plony-credentials
      key: password
- name: PLONY_BASE_URL
  value: {{ .Values.config.plony.baseUrl | quote }}
- name: HELPDESK_ENDPOINT
  value: {{ .Values.config.helpdesk.endpoint | quote }}
- name: HELPDESK_PATH
  value: {{ .Values.config.helpdesk.path | quote }}
- name: HELPDESK_DEPROV_GROUP
  value: {{ .Values.config.helpdesk.deprovision.group | quote }}
- name: HELPDESK_DEPROV_MESSAGE
  value: {{ .Values.config.helpdesk.deprovision.message | quote }}
- name: HELPDESK_DEPROV_TITLE
  value: {{ .Values.config.helpdesk.deprovision.title | quote }}
- name: HELPDESK_FEEDBACK_GROUP
  value: {{ .Values.config.helpdesk.feedback.group | quote }}
- name: HELPDESK_TYPE
  value: {{ .Values.config.helpdesk.type | quote }}
- name: HELPDESK_TOKEN
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-helpdesk-credentials
      key: token
- name: LOG_LEVEL
  value: {{ .Values.logging.level | quote }}
- name: INTERNAL_POD_IP
  valueFrom:
    fieldRef:
      fieldPath: status.podIP
- name: ALLOWED_HOSTS
  value: $(INTERNAL_POD_IP),{{ .Values.config.allowedHosts}}
- name: ENABLE_HSTS_PRELOAD
  value: {{ .Values.config.django.enableHSTSPreload | quote }}
{{ if .Values.config.exceptionTracking.enabled }}
- name: EXCEPTION_TRACKING_ENABLED
  value: "True"
- name: EXCEPTION_TRACKING_DSN
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-exception-tracking-dsn
      key: dsn
- name: EXCEPTION_TRACKING_ENVIRONMENT
  value: {{ .Values.config.exceptionTracking.environment | quote }}
- name: EXCEPTION_TRACKING_RELEASE
  value: {{ .Values.config.exceptionTracking.release | default .Values.image.tag | quote }}
- name: EXCEPTION_TRACKING_CA_CERT
  value: "/certs/ca.pem"
{{ end }}

- name: PORTAL_CLOUD_AGENT_HOST
  value: {{ .Values.config.cloudAgent.host | quote }}
- name: PORTAL_CLOUD_AGENT_PORT
  value: {{ .Values.config.cloudAgent.port | quote }}
- name: PORTAL_CLOUD_AGENT_USE_SSL
  value: {{ .Values.config.cloudAgent.useSSL | quote }}
- name: PORTAL_CLOUD_AGENT_RECEIVE_QUEUE
  value: {{ .Values.config.cloudAgent.receiveQueue | quote }}
- name: PORTAL_CLOUD_AGENT_SEND_QUEUE
  value: {{ .Values.config.cloudAgent.sendQueue | quote }}
- name: PORTAL_CLOUD_AGENT_RECONNECT_INTERVAL
  value: {{ .Values.config.cloudAgent.reconnectInterval | quote }}
- name: PORTAL_CLOUD_AGENT_USERNAME
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-rabbitmq-credentials
      key: username
- name: PORTAL_CLOUD_AGENT_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-rabbitmq-credentials
      key: password
{{- end }}

{{- define "hifisCloudPortal.exceptionTrackingCaVolume" }}
- name: ca-volume
  configMap:
    name: {{ .Release.Name }}-exception-tracking-ca-cert
{{- end }}

{{- define "hifisCloudPortal.exceptionTrackingCaVolumeMount" }}
- name: ca-volume
  mountPath: /certs
  readOnly: true
{{- end }}
