import asyncio
from collections.abc import Callable, Coroutine
from typing import Any

from django.http import HttpRequest, HttpResponse
from django.utils.decorators import sync_and_async_middleware


# Decorator from Django which does not have type annotations
@sync_and_async_middleware  # type: ignore[misc]
def set_security_headers(
    get_response: Callable[[HttpRequest], HttpResponse],
) -> (
    Callable[[Coroutine[Any, Any, HttpResponse]], HttpResponse]
    | Callable[[HttpRequest], HttpResponse]
):
    """
    This middleware sets a few best-practice security headers.
    The policies are restrictive by default.
    """

    def put_headers(response: HttpResponse) -> HttpResponse:
        response.headers["content-security-policy"] = (
            "default-src 'none'; connect-src 'self'; img-src 'self'; media-src 'self'; style-src 'self'; script-src 'self'; font-src 'self'; base-uri 'none'; form-action 'self'; frame-ancestors 'none'; upgrade-insecure-requests;"
        )

        response.headers["feature-policy"] = (
            "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; battery 'none'; camera 'none'; display-capture 'none'; document-domain 'none'; encrypted-media 'none'; fullscreen 'none'; gamepad 'none'; geolocation 'none'; gyroscope 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; payment 'none'; picture-in-picture 'none'; publickey-credentials-get 'none'; speaker-selection 'none'; usb 'none'; screen-wake-lock 'none'; web-share 'none'; xr-spatial-tracking 'none';"
        )

        response.headers["cross-origin-resource-policy"] = "same-origin"
        response.headers["cross-origin-embedder-policy"] = "require-corp"

        response.headers["x-xss-protection"] = "1; mode=block"
        response.headers["x-frame-options"] = "DENY"

        return response

    if asyncio.iscoroutinefunction(get_response):

        async def run_middleware(request: HttpRequest) -> HttpResponse:
            response: HttpResponse = await get_response(request)

            return put_headers(response)

    else:
        # Mypy does not like conditional function definitions
        def run_middleware(request: HttpRequest) -> HttpResponse:  # type: ignore[misc]
            response: HttpResponse = get_response(request)
            return put_headers(response)

    return run_middleware
