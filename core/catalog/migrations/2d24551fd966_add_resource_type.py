"""
Add resource type data model
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import JSONB, UUID

# revision identifiers, used by Alembic.
revision = "2d2455fd966"
down_revision = "aa7eb5c41707"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "catalog_resource_type",
        sa.Column(
            "id", UUID, primary_key=True, nullable=False, comment="ResourceType's UUID"
        ),
        sa.Column(
            "resource_type_json",
            JSONB,
            nullable=False,
            comment="The resource type",
        ),
    )

    op.create_index(
        index_name="catalog_resource_type_json_gin",
        table_name="catalog_resource_type",
        columns=["resource_type_json"],
        postgresql_using="gin",
    )


def downgrade() -> None:
    op.drop_table("catalog_resource_type")
