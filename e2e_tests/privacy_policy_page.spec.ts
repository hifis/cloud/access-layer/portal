import { expect, test } from "@playwright/test";

test("Test privacy policy page contents", async ({ page }) => {
    const response = await page.goto("/privacy");
    expect(response?.status()).toBe(200);

    await expect(page).toHaveTitle("Privacy Policy - Helmholtz Cloud");

    const headline = await page.locator("h1");
    await expect(headline).toContainText("Privacy Policy");
});
