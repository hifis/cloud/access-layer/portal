import asyncio
import datetime
import json
import uuid

import aio_pika
from pydantic_settings import BaseSettings, SettingsConfigDict
from sentry_sdk import capture_exception

from core.catalog.catalog import get_quotas
from core.cloud_agent.message_handlers import (
    add_message_to_history,
    handle_message_from_hca,
    prepare_messages_for_hca,
    update_resource_state,
)
from core.resources.resources import (
    ResourceStateEnum,
    get_resources_by_state,
)


class RabbitMQConfig(BaseSettings):
    host: str
    port: int
    use_ssl: bool
    username: str
    password: str
    receive_queue: str
    send_queue: str
    reconnect_interval: float = 3.0

    model_config = SettingsConfigDict(env_prefix="portal_cloud_agent_")


async def __setup_connection(
    *, vhost: str, config: RabbitMQConfig
) -> aio_pika.abc.AbstractRobustConnection:
    """create a connection to the rabbitmq vhost"""
    return await aio_pika.connect_robust(
        host=config.host,
        port=config.port,
        login=config.username,
        password=config.password,
        ssl=config.use_ssl,
        virtualhost=vhost,
        reconnect_interval=config.reconnect_interval,
    )


async def lookup_requests_and_send_to_queue(
    config: RabbitMQConfig,
    connections: dict[
        str,
        tuple[aio_pika.abc.AbstractRobustConnection, aio_pika.abc.AbstractChannel],
    ],
) -> None:
    form_submitted_resources = await get_resources_by_state(
        ResourceStateEnum.form_submitted
    )
    deprovisioning_form_submitted = await get_resources_by_state(
        ResourceStateEnum.deprovisioning_form_submitted
    )
    messages = await prepare_messages_for_hca(
        resources=form_submitted_resources + deprovisioning_form_submitted,
        queue=config.send_queue,
    )

    for message in messages:
        _, channel = connections[str(message.catalog_service_id)]
        await channel.default_exchange.publish(
            message=aio_pika.Message(
                body=json.dumps(message.model_dump(mode="json")["payload"]).encode(),
                correlation_id=str(message.resource_id),
                headers={"service_id": str(message.catalog_service_id)},
                type=message.type,
                delivery_mode=aio_pika.DeliveryMode.PERSISTENT,  # makes the broker persist the message to disk so it survives broker restarts
            ),
            routing_key=message.queue,
        )
        await add_message_to_history(message=message)
        await update_resource_state(message=message)


async def send_resource_requests_to_queue() -> None:
    """lookup all form_submitted resources and send to hca"""
    config = RabbitMQConfig()  # type: ignore[call-arg]
    vhosts = {str(quota.catalog_service_id) for quota in await get_quotas()}
    connections: dict[
        str,
        tuple[aio_pika.abc.AbstractRobustConnection, aio_pika.abc.AbstractChannel],
    ] = {}
    for vhost in vhosts:
        connection = await __setup_connection(vhost=vhost, config=config)
        channel = await connection.channel()
        connections[vhost] = (connection, channel)

    while True:
        next_run = datetime.datetime.now(tz=datetime.UTC) + datetime.timedelta(
            seconds=1
        )
        await lookup_requests_and_send_to_queue(config, connections)

        await asyncio.sleep(
            (next_run - datetime.datetime.now(tz=datetime.UTC)).total_seconds()
        )


async def on_message_from_hca(message: aio_pika.abc.AbstractIncomingMessage) -> None:
    """callback used to handle messages received from HCA"""
    # Check if the message body is not empty. Discard message otherwise.
    if not message.body:
        msg = "Received an empty message body from HCA"
        capture_exception(RuntimeError(msg))
        await message.ack()
        return

    # Check if the message body is valid JSON. Discard message otherwise.
    try:
        raw_message = json.loads(message.body)
    except json.JSONDecodeError as e:
        capture_exception(e)
        await message.ack()
        return

    # Check if service_id is in the message headers. Discard message otherwise.
    if "service_id" not in message.headers:
        msg = "Received a message without service_id in headers from HCA"
        capture_exception(RuntimeError(msg))
        await message.ack()
        return

    # Check that service_id is a valid UUID. Discard message otherwise
    try:
        catalog_service_id = uuid.UUID(str(message.headers["service_id"]))
    except ValueError as e:
        capture_exception(e)
        await message.ack()
        return

    # Check that the message type is set. Discard message otherwise.
    if message.type is None:
        msg = "Received an message without a type from HCA"
        capture_exception(RuntimeError(msg))
        await message.ack()
        return

    # Check that message_id is a valid UUID. Discard message otherwise
    try:
        resource_id = uuid.UUID(message.message_id)
    except ValueError as e:
        capture_exception(e)
        await message.ack()
        return

    await handle_message_from_hca(
        resource_id=resource_id,
        catalog_service_id=catalog_service_id,
        message_type=message.type,
        raw_message=raw_message,
    )

    await message.ack()


async def start_cloud_agent_receivers() -> None:
    """Setup consumers for each vhosts"""
    config = RabbitMQConfig()  # type: ignore[call-arg]

    vhosts = {str(quota.catalog_service_id) for quota in await get_quotas()}

    amqp_handles = []

    for vhost in vhosts:
        connection = await __setup_connection(vhost=vhost, config=config)

        channel = await connection.channel()
        queue = await channel.get_queue(config.receive_queue)

        consumer = await queue.consume(callback=on_message_from_hca)

        amqp_handles.append((connection, channel, queue, consumer))

    # Wait indefinitely with consumers running in background
    await asyncio.Future()
