"""
switch_to_user_access_description

Change column name 'user_enablement' to 'user_access_description' as it was changed in Plony.
"""

from alembic import op

# revision identifiers, used by Alembic.
revision = "9474bebdea5c"
down_revision = "1efe5c7638d7"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column(
        "catalog_service",
        "user_enablement",
        nullable=False,
        new_column_name="user_access_description",
    )


def downgrade() -> None:
    op.alter_column(
        "catalog_service",
        "user_access_description",
        nullable=False,
        new_column_name="user_enablement",
    )
