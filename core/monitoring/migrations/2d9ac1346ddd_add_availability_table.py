"""
add availability table
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = "2d9ac1346ddd"
down_revision = None
branch_labels = ("monitoring",)
depends_on = None


def upgrade() -> None:
    op.create_table(
        "monitoring_availability",
        sa.Column(
            "service_id",
            UUID,
            primary_key=True,
            nullable=False,
            comment="Service's UUID",
        ),
        sa.Column(
            "status_code",
            sa.INT,
            nullable=False,
            comment="HTTP status code",
        ),
        sa.Column(
            "status_text",
            sa.Text,
            nullable=True,
            comment="The status text in case of error",
        ),
        sa.Column(
            "date",
            sa.DateTime,
            nullable=False,
            comment="The test date",
        ),
    )


def downgrade() -> None:
    op.drop_table("monitoring_availability")
