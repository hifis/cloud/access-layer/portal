import asyncio
import json
import re
from typing import Any
from uuid import UUID

import jsonschema
import jsonschema.exceptions
import jsonschema.validators
import psycopg
import pydantic
from pydantic import BaseModel, ConfigDict, Field, NonNegativeInt, field_validator

import core.db
import core.identity
import core.kpi
from core.identity import GROUP_URN_PATTERN
from core.plony import Client as PlonyClient
from core.plony import PlonyInstitution, PlonyService

# List of JSON Schema types supported in ResourceTypes and Policies
# To keep complexity low in the beginning, e.g. nested objects are not supported
SUPPORTED_RESOURCE_JSON_SCHEMA_TYPES = ["integer", "string", "boolean"]


class Keyword(BaseModel):
    id: pydantic.UUID4
    plony_at_id: pydantic.HttpUrl
    title: str
    is_visible: bool
    is_category: bool


class ServiceLink(BaseModel):
    catalog_service_id: pydantic.UUID4
    service_link: str


class Provider(BaseModel):
    id: pydantic.UUID4
    title: str
    abbreviation: str
    url: pydantic.HttpUrl
    logo_content_type: str | None
    logo_filename: str | None
    logo_data: bytes | None


class ServiceKPIs(BaseModel):
    catalog_service_id: pydantic.UUID4
    user_count: int = Field(strict=True, ge=0)


class Service(BaseModel):
    id: pydantic.UUID4
    plony_at_id: pydantic.HttpUrl
    plony_review_state: str
    title: str
    description: str
    category: str
    general_long_description: str
    general_short_text_cloud_portal: str
    general_link_service_usage: pydantic.HttpUrl | None
    general_link_documentation: pydantic.HttpUrl | None
    general_software_name: str
    backup_strategy: str
    communication_contact_user_support: str
    service_lvl_description: str | None
    user_limitations: str
    user_access_description: str
    data_storage_location: str
    catalog_provider_id: pydantic.UUID4
    general_service_logo_data: bytes | None
    general_service_logo_content_type: str | None
    general_service_logo_filename: str | None


class ResourceType(BaseModel):
    id: pydantic.UUID4
    name: str = Field(..., min_length=1)
    description: str = Field(..., min_length=1)
    json_schema: dict[str, Any]
    ui_schema: dict[str, Any]

    @field_validator("json_schema")
    @classmethod
    def must_be_valid_json_schema(
        cls: type["ResourceType"], schema: dict[str, Any]
    ) -> dict[str, Any]:
        try:
            # Create a strict validator for draft 2020-12 schemas
            jsonschema.Draft202012Validator(
                jsonschema.Draft202012Validator.META_SCHEMA
                | {"unevaluatedProperties": False}
            ).validate(schema)
        except jsonschema.exceptions.ValidationError as e:
            msg = "error validating JSON schema"
            raise ValueError(msg, e.message) from e

        return schema

    @field_validator("json_schema")
    @classmethod
    def must_not_use_refs(
        cls: type["BaseModel"], schema: dict[str, Any]
    ) -> dict[str, Any]:
        schema_string = json.dumps(schema)

        if (
            "$ref" in schema_string
            or "$recursiveRef" in schema_string
            or "$dynamicRef" in schema_string
        ):
            msg = "Found ref, recursiveRef or dynamicRef in json_schema which is not permitted in MVP phase"
            raise ValueError(msg)

        return schema

    @field_validator("json_schema")
    @classmethod
    def must_only_use_supported_types(
        cls: type["ResourceType"], schema: dict[str, Any]
    ) -> dict[str, Any]:
        if "properties" in schema:
            for schema_property in schema["properties"].values():
                if schema_property["type"] not in SUPPORTED_RESOURCE_JSON_SCHEMA_TYPES:
                    msg = "resource schema contains unsupported type"
                    raise ValueError(msg)

        return schema


class DBResourceType(BaseModel):
    id: pydantic.UUID4
    resource_type_json: str


class QuotaLimit(BaseModel):
    property: str = Field(..., min_length=1)
    total: NonNegativeInt


class Quota(BaseModel):
    id: pydantic.UUID4
    catalog_service_id: pydantic.UUID4
    catalog_resource_type_id: pydantic.UUID4
    name: str
    quota: list[QuotaLimit]


class DBQuota(BaseModel):
    id: pydantic.UUID4
    quota_json: str


class PolicyActorRequirements(BaseModel):
    # Strict so it does not have surprising effects when users try to match on unsupported fields
    model_config = ConfigDict(extra="forbid", strict=True)

    edu_person_entitlement: list[str] | None
    edu_person_assurance: list[str] | None
    edu_person_scoped_affiliation: list[str] | None


class Policy(BaseModel):
    id: pydantic.UUID4
    catalog_quota_id: pydantic.UUID4
    actor_requirements: PolicyActorRequirements
    target_entity: str
    name: str
    json_schema: dict[str, Any]
    time_seconds: int

    @field_validator("json_schema")
    @classmethod
    def must_be_valid_json_schema(
        cls: type["Policy"], schema: dict[str, Any]
    ) -> dict[str, Any]:
        try:
            # Create a strict validator for draft 2020-12 schemas
            jsonschema.Draft202012Validator(
                jsonschema.Draft202012Validator.META_SCHEMA
                | {"unevaluatedProperties": False}
            ).validate(schema)
        except jsonschema.exceptions.ValidationError as e:
            msg = "error validating JSON schema"
            raise ValueError(msg, e.message) from e

        return schema

    @field_validator("json_schema")
    @classmethod
    def must_not_use_refs(
        cls: type["BaseModel"], schema: dict[str, Any]
    ) -> dict[str, Any]:
        schema_string = json.dumps(schema)

        if (
            "$ref" in schema_string
            or "$recursiveRef" in schema_string
            or "$dynamicRef" in schema_string
        ):
            msg = "Found ref, recursiveRef or dynamicRef in json_schema which is not permitted in MVP phase"
            raise ValueError(msg)

        return schema

    @field_validator("json_schema")
    @classmethod
    def must_only_use_supported_types(
        cls: type["BaseModel"], schema: dict[str, Any]
    ) -> dict[str, Any]:
        if "properties" in schema:
            for schema_property in schema["properties"].values():
                if schema_property["type"] not in SUPPORTED_RESOURCE_JSON_SCHEMA_TYPES:
                    msg = "resource schema contains unsupported type"
                    raise ValueError(msg)

        return schema

    @field_validator("target_entity")
    @classmethod
    def must_contain_valid_target_entity(
        cls: type["BaseModel"], target_entity: str
    ) -> str:
        if (
            # Personal resource
            target_entity == "self"
            # Group URN with wildcard (ignore last character)
            or (
                target_entity.endswith(":")
                and re.match(GROUP_URN_PATTERN, target_entity[:-1])
            )
            # Group URN
            or re.match(GROUP_URN_PATTERN, target_entity)
        ):
            return target_entity
        else:
            msg = "target_entity has invalid format"
            raise ValueError(msg)


class DBPolicy(BaseModel):
    id: pydantic.UUID4
    policy_json: str


class ServiceValidationError(Exception):
    pass


class QuotaValidationError(Exception):
    pass


class PolicyValidationError(Exception):
    pass


async def get_keywords() -> list[Keyword]:
    """Return all keywords stored in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        return await _get_keywords(connection=connection)


async def get_all_service_keyword_ids() -> dict[UUID, list[UUID]]:
    """Get all service's keyword IDs."""

    result_rows = []

    async with core.db.DB.get_connection_with_transaction() as connection:
        dictionary_rows = await connection.cursor(
            row_factory=psycopg.rows.dict_row
        ).execute(
            "SELECT catalog_service_id, catalog_keyword_id FROM catalog_service_keyword"
        )

        result_rows = await dictionary_rows.fetchall()

    mapping: dict[UUID, list[UUID]] = {}

    for result in result_rows:
        if result["catalog_service_id"] not in mapping:
            mapping[result["catalog_service_id"]] = [result["catalog_keyword_id"]]
        else:
            mapping[result["catalog_service_id"]].append(result["catalog_keyword_id"])

    return mapping


async def get_providers() -> list[Provider]:
    """Return all providers stored in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        return await _get_providers(connection=connection)


async def get_provider(provider_id: UUID) -> Provider | None:
    """Return provider with this UUID from the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        provider_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(Provider)
        ).execute(
            "SELECT * FROM catalog_provider WHERE id = %(provider_id)s",
            {"provider_id": provider_id},
        )

        return await provider_rows.fetchone()


async def get_services() -> list[Service]:
    """Return all services stored in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        return await _get_services(connection=connection)


async def get_service(service_id: UUID) -> Service | None:
    """Return service with this UUID from the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        service_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(Service)
        ).execute(
            "SELECT * FROM catalog_service WHERE id = %(service_id)s",
            {"service_id": service_id},
        )

        return await service_rows.fetchone()


async def get_service_links() -> list[ServiceLink]:
    """Return the general_link_service_usage for all services"""

    result = []

    async with core.db.DB.get_connection_with_transaction() as connection:
        dictionary_rows = await connection.cursor(
            row_factory=psycopg.rows.dict_row
        ).execute("SELECT id, general_link_service_usage FROM catalog_service")

        result = await dictionary_rows.fetchall()

    return [
        ServiceLink(
            catalog_service_id=UUID(str(row["id"])),
            service_link=str(row["general_link_service_usage"]),
        )
        for row in result
    ]


async def get_resource_type(resource_type_id: UUID) -> ResourceType | None:
    """Return resource type with this UUID from the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        resource_type_rows = await connection.cursor(
            row_factory=psycopg.rows.dict_row
        ).execute(
            "SELECT resource_type_json FROM catalog_resource_type WHERE id = %(resource_type_id)s",
            {"resource_type_id": resource_type_id},
        )

        json_dict = await resource_type_rows.fetchone()

    if json_dict is None:
        return None

    return ResourceType.model_validate(json_dict["resource_type_json"])


async def get_resource_types() -> list[ResourceType]:
    """Return all resource types stored in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        return await _get_resource_types(connection=connection)


async def get_resource_types_for_service(service_id: UUID) -> list[ResourceType]:
    """Return all resource types with a quota for this service ID."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        dict_rows = await connection.cursor(row_factory=psycopg.rows.dict_row).execute(
            """SELECT resource_type_json
            FROM catalog_resource_type
            WHERE id IN
                (SELECT DISTINCT (quota_json->>'catalog_resource_type_id')::uuid
                    FROM catalog_quota WHERE quota_json->>'catalog_service_id' = %(service_id)s)""",
            {"service_id": str(service_id)},
        )

        result = await dict_rows.fetchall()

    return [ResourceType.model_validate(row["resource_type_json"]) for row in result]


async def get_policies_available_to_actor(
    *,
    actor_identity: core.identity.ActorIdentity,
    service_id: UUID,
    resource_type_id: UUID,
) -> list[Policy]:
    """Get policies on a service which are available to the auth context."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        dict_rows = await connection.cursor(row_factory=psycopg.rows.dict_row).execute(
            """SELECT policy_json
               FROM catalog_policy
               WHERE
               policy_json->'catalog_quota_id' IN (
                   SELECT DISTINCT quota_json->'id'
                   FROM catalog_quota
                   WHERE
                   quota_json->>'catalog_service_id' = %(service_id)s
                   AND
                   quota_json->>'catalog_resource_type_id' = %(resource_type_id)s
               )
               AND
               policy_json->'actor_requirements' <@ %(actor_identity)s""",
            {
                "service_id": str(service_id),
                "resource_type_id": str(resource_type_id),
                "actor_identity": actor_identity.model_dump_json(),
            },
        )

        result = await dict_rows.fetchall()

    return [Policy.model_validate(row["policy_json"]) for row in result]


async def get_quota(quota_id: UUID) -> Quota | None:
    """Return quota with this UUID from the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        dict_rows = await connection.cursor(row_factory=psycopg.rows.dict_row).execute(
            "SELECT quota_json FROM catalog_quota WHERE id = %(quota_id)s",
            {"quota_id": quota_id},
        )

        json_dict = await dict_rows.fetchone()

    if json_dict is None:
        return None

    return Quota.model_validate(json_dict["quota_json"])


async def get_quotas() -> list[Quota]:
    """Return all quotas stored in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        dict_rows = await connection.cursor(row_factory=psycopg.rows.dict_row).execute(
            "SELECT quota_json FROM catalog_quota"
        )

        result = await dict_rows.fetchall()

    return [Quota.model_validate(row["quota_json"]) for row in result]


async def get_policy(policy_id: UUID) -> Policy | None:
    """Return policy with this UUID from the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        dict_rows = await connection.cursor(row_factory=psycopg.rows.dict_row).execute(
            "SELECT * FROM catalog_policy WHERE id = %(policy_id)s",
            {"policy_id": policy_id},
        )

        json_dict = await dict_rows.fetchone()

    if json_dict is None:
        return None

    return Policy.model_validate(json_dict["policy_json"])


async def get_policies() -> list[Policy]:
    """Return all policies stored in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        dict_rows = await connection.cursor(row_factory=psycopg.rows.dict_row).execute(
            "SELECT policy_json FROM catalog_policy"
        )

        result = await dict_rows.fetchall()

    return [Policy.model_validate(row["policy_json"]) for row in result]


async def get_service_kpis() -> list[ServiceKPIs]:
    """Return all service KPIs stored in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        kpi_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(ServiceKPIs)
        ).execute("SELECT * FROM catalog_service_kpi")

        return await kpi_rows.fetchall()


async def import_kpis(*, kpi_map: core.kpi.KPIMap) -> None:
    """Replace all KPI information with the supplied list."""

    kpi_info = [
        {"catalog_service_id": catalog_service_id, "user_count": kpis.user_count}
        for catalog_service_id, kpis in kpi_map.items()
    ]

    catalog_service_ids = kpi_map.keys()

    # Save new KPI values
    async with core.db.DB.get_connection_with_transaction() as connection:
        if len(kpi_info) > 0:
            # Insert/update values and delete any entries which are now missing
            await connection.cursor().executemany(
                """
                INSERT INTO catalog_service_kpi (catalog_service_id, user_count)
                VALUES (%(catalog_service_id)s, %(user_count)s)
                ON CONFLICT (catalog_service_id)
                DO UPDATE SET user_count = %(user_count)s;
                """,
                kpi_info,
            )

            await connection.execute(
                "DELETE FROM catalog_service_kpi WHERE NOT catalog_service_id = ANY(%(catalog_service_ids)s);",
                {"catalog_service_ids": list(catalog_service_ids)},
            )
        else:
            # If there are no KPIs, delete everything
            await connection.execute("DELETE FROM catalog_service_kpi;")


# Plony importer, it is what it is
async def run_plony_import(  # noqa: PLR0915, PLR0912
    *, plony_client: PlonyClient, include_in_review_services: bool
) -> None:
    # Gather data and then store it in DB

    # Download keywords
    plony_keywords = await plony_client.list_keywords()
    keywords = {
        str(plony_keyword.uid): Keyword(
            id=plony_keyword.uid,
            plony_at_id=plony_keyword.at_id,
            title=plony_keyword.title,
            is_visible=plony_keyword.is_visible,
            is_category=plony_keyword.is_category,
        )
        for plony_keyword in plony_keywords
    }

    # Download providers (institutions which are service providers)
    plony_providers = await plony_client.list_institutions()
    plony_providers = [
        provider for provider in plony_providers if provider.is_service_provider
    ]

    # Download provider logos, it's logo data with the provider's UUID as key
    provider_logo_tasks = [
        _download_provider_logo(plony_client, plony_provider)
        for plony_provider in plony_providers
        if plony_provider.logo is not None
    ]

    # Wait for tasks to complete
    provider_logo_results = await asyncio.gather(*provider_logo_tasks)
    provider_logos = dict(provider_logo_results)

    providers = {}
    for plony_provider in plony_providers:
        if plony_provider.url is None:
            message = "Provider URL can't be None!"
            raise RuntimeError(message)

        providers[str(plony_provider.uid)] = Provider(
            id=plony_provider.uid,
            title=plony_provider.title,
            abbreviation=plony_provider.abbreviation,
            url=plony_provider.url,
            logo_filename=plony_provider.logo.filename if plony_provider.logo else None,
            logo_content_type=plony_provider.logo.content_type
            if plony_provider.logo
            else None,
            logo_data=provider_logos.get(plony_provider.uid, None),
        )

    # Download resource types
    plony_resource_types = await plony_client.list_resource_types()

    resource_types = {
        str(resource_type.id): resource_type
        for resource_type in [
            ResourceType.model_validate(plony_resource_type.model_dump())
            for plony_resource_type in plony_resource_types
        ]
    }

    # Download services
    plony_services = await plony_client.list_services()

    # Perform a quick check of the data, then filter for review state
    for plony_service in plony_services:
        if plony_service.review_state == "service_online":
            # False positive
            if (
                plony_service.other_accept_publish_to_portal.token != "yes"  # nosec B105
            ):
                error_message = f"Service {plony_service.title} / {plony_service.uid} has review_state set to 'service_online' and other_accept_publish_to_portal not set to 'yes'. This should not happen."
                raise ServiceValidationError(error_message)

            if plony_service.general_link_service_usage is None:
                error_message = f"Service {plony_service.title} / {plony_service.uid} has review_state set to 'service_online' and no general_link_service_usage. This should not happen."
                raise ServiceValidationError(error_message)

    plony_services = [
        plony_service
        for plony_service in plony_services
        # False positive
        if plony_service.other_accept_publish_to_portal.token == "yes"  # nosec B105
        and (
            plony_service.review_state == "service_online"
            or (
                include_in_review_services
                and (
                    plony_service.review_state
                    in {"service_card_review", "integration_completed"}
                )
            )
        )
    ]

    # Download service logos in parallel
    service_logo_tasks = [
        _download_service_logo(plony_client, plony_service)
        for plony_service in plony_services
        if plony_service.general_service_logo is not None
    ]

    # Wait for tasks to complete
    service_logo_results = await asyncio.gather(*service_logo_tasks)
    service_logos = dict(service_logo_results)

    # Find provider ID of each service
    provider_at_id_to_uid = {
        plony_provider.at_id: plony_provider.uid for plony_provider in plony_providers
    }

    # TODO: import all possible service provider for a service  # noqa: FIX002, TD002, TD003
    service_provider_ids: dict[pydantic.UUID4, pydantic.UUID4] = {}
    for plony_service in plony_services:
        service_provider_ids[plony_service.uid] = provider_at_id_to_uid[
            plony_service.service_provider[0].at_id
        ]

    services = {
        str(plony_service.uid): Service(
            id=plony_service.uid,
            plony_at_id=plony_service.at_id,
            plony_review_state=plony_service.review_state,
            title=plony_service.title,
            description=plony_service.description,
            category=plony_service.service_category.title
            if plony_service.service_category
            else "No Category",
            general_long_description=plony_service.general_long_description,
            general_short_text_cloud_portal=plony_service.general_short_text_cloud_portal,
            general_link_service_usage=plony_service.general_link_service_usage,
            general_link_documentation=plony_service.general_documentation,
            general_software_name=plony_service.general_software_name,
            backup_strategy=plony_service.backup_strategy,
            communication_contact_user_support=plony_service.communication_contact_user_support,
            service_lvl_description=plony_service.service_lvl_description,
            user_access_description=plony_service.user_access_description,
            user_limitations=plony_service.user_limitations,
            data_storage_location=plony_service.data_storage_location.title,
            catalog_provider_id=service_provider_ids[plony_service.uid],
            general_service_logo_filename=plony_service.general_service_logo.filename
            if plony_service.general_service_logo
            else None,
            general_service_logo_content_type=plony_service.general_service_logo.content_type
            if plony_service.general_service_logo
            else None,
            general_service_logo_data=service_logos.get(plony_service.uid, None),
        )
        for plony_service in plony_services
    }

    # Download quotas
    plony_quotas = await plony_client.list_quotas()

    quotas = {
        str(plony_quota.id): Quota(
            id=plony_quota.id,
            catalog_service_id=plony_quota.service_id,
            catalog_resource_type_id=plony_quota.resource_type_id,
            name=plony_quota.name,
            quota=[QuotaLimit(**limit.model_dump()) for limit in plony_quota.quota],
        )
        for plony_quota in plony_quotas
    }

    # Validate quotas
    for quota in quotas.values():
        # Ensure referenced service exists
        quota_service = services.get(str(quota.catalog_service_id), None)
        if quota_service is None:
            message = f"Service referenced by quota does not exist (Quota: {quota.id}, Service: {quota.catalog_service_id})"
            raise QuotaValidationError(message)

        # Ensure referenced resource type exists
        quota_resource_type = resource_types.get(
            str(quota.catalog_resource_type_id), None
        )

        if quota_resource_type is None:
            message = f"ResourceType referenced by quota does not exist (Quota: {quota.id}, ResourceType: {quota.catalog_resource_type_id})"
            raise QuotaValidationError(message)

        # Ensure ensure property exists on resource type
        for limit in quota.quota:
            if limit.property not in quota_resource_type.json_schema["properties"]:
                message = f"Property referenced by quota does not exist on resource type (Quota: {quota.id}, ResourceType: {quota.catalog_resource_type_id})"
                raise QuotaValidationError(message)

            # Ensure property referenced by quota is of type integer
            if (
                quota_resource_type.json_schema["properties"][limit.property].get(
                    "type", None
                )
                != "integer"
            ):
                message = f"Property referenced by quota is not of type integer (Quota: {quota.id}, ResourceType: {quota.catalog_resource_type_id})"
                raise QuotaValidationError(message)

            # Ensure it has either minimum >= 0 or exclusiveMinimum > 0
            # Otherwise people could break quotas by submitting requests with negative values in limited fields
            minimum = quota_resource_type.json_schema["properties"][limit.property].get(
                "minimum", None
            )
            exclusive_minimum = quota_resource_type.json_schema["properties"][
                limit.property
            ].get("exclusiveMinimum", None)

            if minimum is None and exclusive_minimum is None:
                message = f"Property referenced by quota must have either minimum >= 0 or exclusiveMinimum > 0 set (Quota: {quota.id}, ResourceType: {quota.catalog_resource_type_id})"
                raise QuotaValidationError(message)

            if minimum is not None and not minimum >= 0:
                message = f"Property referenced by quota must have minimum >= 0 if exclusiveMinimum is set (Quota: {quota.id}, ResourceType: {quota.catalog_resource_type_id})"
                raise QuotaValidationError(message)

            if exclusive_minimum is not None and not exclusive_minimum > 0:
                message = f"Property referenced by quota must have exclusiveMinimum > 0 if exclusiveMinimum is set (Quota: {quota.id}, ResourceType: {quota.catalog_resource_type_id})"
                raise QuotaValidationError(message)

    # Download policies
    plony_policies = await plony_client.list_policies()

    policies = {
        str(plony_policy.id): Policy(
            id=plony_policy.id,
            catalog_quota_id=plony_policy.quota_id,
            actor_requirements=PolicyActorRequirements(
                **plony_policy.actor_requirements.model_dump()
            ),
            target_entity=plony_policy.target_entity,
            name=plony_policy.name,
            json_schema=plony_policy.json_schema,
            time_seconds=plony_policy.time_seconds,
        )
        for plony_policy in plony_policies
    }

    # Validate policies
    for policy in policies.values():
        # Ensure referenced quota exists
        policy_quota = quotas.get(str(policy.catalog_quota_id), None)

        if policy_quota is None:
            message = f"Quota referenced by policy does not exist (Policy: {policy.id}, Quota: {policy.catalog_quota_id})"
            raise PolicyValidationError(message)

        # Ensure referenced properties exist on resource type
        resource_type = resource_types[str(policy_quota.catalog_resource_type_id)]

        for policy_property_name, policy_property in policy.json_schema[
            "properties"
        ].items():
            if policy_property_name not in resource_type.json_schema["properties"]:
                message = f"Property referenced by policy does not exist on resource type (Policy: {policy.id}, Quota: {policy.catalog_quota_id})"
                raise PolicyValidationError(message)

            if (
                policy_property["type"]
                != resource_type.json_schema["properties"][policy_property_name]["type"]
            ):
                message = f"Types of properties do not match between policy and resource type (Policy: {policy.id}, Quota: {policy.catalog_quota_id})"
                raise PolicyValidationError(message)

    async with core.db.DB.get_connection_with_transaction() as connection:
        #
        # Import keywords
        #

        # Insert or update keywords by ID
        old_keyword_ids = {
            keyword.id for keyword in await _get_keywords(connection=connection)
        }

        for keyword in keywords.values():
            await _create_or_update_keyword(keyword=keyword, connection=connection)

        new_keyword_ids = {keyword.id for keyword in keywords.values()}

        keywords_to_delete = old_keyword_ids.difference(new_keyword_ids)

        for keyword_id in keywords_to_delete:
            await _delete_keyword(keyword_id=keyword_id, connection=connection)

        # Import providers
        old_provider_ids = {
            provider.id for provider in await _get_providers(connection=connection)
        }

        for provider in providers.values():
            await _create_or_update_provider(provider=provider, connection=connection)

        new_provider_ids = {provider.id for provider in providers.values()}

        #
        # Import ResourceTypes
        #

        # Import ResourceTypes
        old_resource_type_ids = {
            resource_type.id
            for resource_type in await _get_resource_types(connection=connection)
        }

        for resource_type in resource_types.values():
            await _create_or_update_resource_type(
                resource_type=resource_type, connection=connection
            )

        new_resource_type_ids = {
            resource_type.id for resource_type in resource_types.values()
        }

        # Delete any resource types no longer present
        resource_types_to_delete = old_resource_type_ids.difference(
            new_resource_type_ids
        )

        for resource_type_id in resource_types_to_delete:
            await _delete_resource_type(
                resource_type_id=resource_type_id, connection=connection
            )

        # Import Quotas, note: do not clean-up/delete quotas for now!
        # Deleting quotas could render existing resources quota-less
        for quota in quotas.values():
            await _create_or_update_quota(quota=quota, connection=connection)

        # Import Policies
        old_policy_ids = {
            policy.id for policy in await _get_policies(connection=connection)
        }

        for policy in policies.values():
            await _create_or_update_policy(policy=policy, connection=connection)

        new_policy_ids = {policy.id for policy in policies.values()}

        # Delete any resource types no longer present
        policies_to_delete = old_policy_ids.difference(new_policy_ids)

        for policy_id in policies_to_delete:
            await _delete_policy(policy_id=policy_id, connection=connection)

        #
        # Import services
        #

        old_service_ids = {
            service.id for service in await _get_services(connection=connection)
        }

        for service in services.values():
            await _create_or_update_service(service=service, connection=connection)

        new_service_ids = {service.id for service in services.values()}

        services_to_delete = old_service_ids.difference(new_service_ids)

        for service_id in services_to_delete:
            await _delete_service(service_id=service_id, connection=connection)

        # Delete any providers no longer present
        # This needs to be done after services have been cleaned-up since you'd need to remove services first before you can remove a provider.
        providers_to_delete = old_provider_ids.difference(new_provider_ids)

        for provider_id in providers_to_delete:
            await _delete_provider(provider_id=provider_id, connection=connection)

        #
        # Link services to keywords
        #

        insert_service_keyword_query = """
        INSERT INTO catalog_service_keyword (catalog_service_id, catalog_keyword_id)
        VALUES (%(catalog_service_id)s, (SELECT id FROM catalog_keyword WHERE plony_at_id = %(catalog_keyword_id)s))
        ON CONFLICT (catalog_service_id, catalog_keyword_id) DO NOTHING
        """
        insert_service_keyword_params = [
            {
                "catalog_service_id": plony_service.uid,
                "catalog_keyword_id": plony_keyword.at_id.unicode_string(),
            }
            for plony_service in plony_services
            for plony_keyword in plony_service.service_keywords
        ]

        await connection.cursor().executemany(
            insert_service_keyword_query, insert_service_keyword_params
        )

        # Delete any keyword links no longer present
        for plony_service in plony_services:
            if len(plony_service.service_keywords) > 0:
                plony_ids = [
                    str(keyword_link.at_id)
                    for keyword_link in plony_service.service_keywords
                ]

                await connection.execute(
                    """
                    DELETE FROM catalog_service_keyword
                    WHERE catalog_keyword_id
                        NOT IN (SELECT id FROM catalog_keyword WHERE plony_at_id = ANY(%(plony_ids)s))
                        AND catalog_service_id = %(service_id)s
                    """,
                    {"plony_ids": plony_ids, "service_id": plony_service.uid},
                )
            else:
                await connection.execute(
                    "DELETE FROM catalog_service_keyword WHERE catalog_service_id = %(service_id)s",
                    {"service_id": plony_service.uid},
                )


async def _get_providers(
    connection: psycopg.AsyncConnection[psycopg.rows.TupleRow],
) -> list[Provider]:
    """Return all providers stored in the database."""

    provider_rows = await connection.cursor(
        row_factory=psycopg.rows.class_row(Provider)
    ).execute("SELECT * FROM catalog_provider")

    return await provider_rows.fetchall()


async def _create_or_update_provider(
    *, provider: Provider, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    await connection.execute(
        """INSERT INTO catalog_provider
                (id, title, abbreviation, url, logo_content_type, logo_filename, logo_data)
               VALUES
                (%(id)s, %(title)s, %(abbreviation)s, %(url)s, %(logo_content_type)s, %(logo_filename)s, %(logo_data)s)
               ON CONFLICT (id)
               DO UPDATE SET
                title = %(title)s, abbreviation = %(abbreviation)s, url = %(url)s, logo_content_type = %(logo_content_type)s, logo_filename = %(logo_filename)s, logo_data = %(logo_data)s;""",
        provider.model_dump() | {"url": str(provider.url)},
    )


async def _get_resource_types(
    *, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> list[ResourceType]:
    dict_rows = await connection.cursor(row_factory=psycopg.rows.dict_row).execute(
        "SELECT resource_type_json FROM catalog_resource_type"
    )

    result = await dict_rows.fetchall()

    return [ResourceType.model_validate(row["resource_type_json"]) for row in result]


async def _delete_resource_type(
    *,
    resource_type_id: UUID,
    connection: psycopg.AsyncConnection[psycopg.rows.TupleRow],
) -> None:
    result_cursor = await connection.execute(
        """DELETE FROM catalog_resource_type WHERE id = %(resource_type_id)s""",
        {"resource_type_id": resource_type_id},
    )

    if result_cursor.statusmessage != "DELETE 1":
        msg = "ResourceType could not be deleted!"
        raise RuntimeError(msg)


async def _create_or_update_resource_type(
    *,
    resource_type: ResourceType,
    connection: psycopg.AsyncConnection[psycopg.rows.TupleRow],
) -> None:
    await connection.execute(
        """INSERT INTO catalog_resource_type
            (id, resource_type_json)
            VALUES
            (%(id)s, %(resource_type_json)s)
            ON CONFLICT (id)
            DO UPDATE SET
            resource_type_json = %(resource_type_json)s;""",
        {"id": resource_type.id, "resource_type_json": resource_type.model_dump_json()},
    )


async def _create_or_update_quota(
    *, quota: Quota, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    await connection.execute(
        """INSERT INTO catalog_quota
            (id, quota_json)
            VALUES
            (%(id)s, %(quota_json)s)
            ON CONFLICT (id)
            DO UPDATE SET
            quota_json = %(quota_json)s;""",
        {"id": quota.id, "quota_json": quota.model_dump_json()},
    )


async def _get_policies(
    *, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> list[Policy]:
    dict_rows = await connection.cursor(row_factory=psycopg.rows.dict_row).execute(
        "SELECT policy_json FROM catalog_policy"
    )

    result = await dict_rows.fetchall()

    return [Policy.model_validate(row["policy_json"]) for row in result]


async def _delete_policy(
    *, policy_id: UUID, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    result_cursor = await connection.execute(
        """DELETE FROM catalog_policy WHERE id = %(policy_id)s""",
        {"policy_id": policy_id},
    )

    if result_cursor.statusmessage != "DELETE 1":
        msg = "Policy could not be deleted!"
        raise RuntimeError(msg)


async def _create_or_update_policy(
    *, policy: Policy, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    await connection.execute(
        """INSERT INTO catalog_policy
            (id, policy_json)
            VALUES
            (%(id)s, %(policy_json)s)
            ON CONFLICT (id)
            DO UPDATE SET
            policy_json = %(policy_json)s;""",
        {"id": policy.id, "policy_json": policy.model_dump_json()},
    )


async def _delete_provider(
    *, provider_id: UUID, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    result_cursor = await connection.execute(
        """DELETE FROM catalog_provider WHERE id = %(provider_id)s""",
        {"provider_id": provider_id},
    )

    if result_cursor.statusmessage != "DELETE 1":
        msg = "Provider could not be deleted!"
        raise RuntimeError(msg)


async def _create_or_update_keyword(
    *, keyword: Keyword, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    await connection.execute(
        """INSERT INTO catalog_keyword
            (id, plony_at_id, title, is_visible, is_category)
            VALUES
            (%(id)s, %(plony_at_id)s, %(title)s, %(is_visible)s, %(is_category)s)
            ON CONFLICT (id)
            DO UPDATE SET
            plony_at_id = %(plony_at_id)s, title = %(title)s, is_visible = %(is_visible)s, is_category = %(is_category)s;""",
        keyword.model_dump(mode="json"),
    )


async def _delete_keyword(
    *, keyword_id: UUID, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    result_cursor = await connection.execute(
        """DELETE FROM catalog_keyword WHERE id = %(keyword_id)s""",
        {"keyword_id": keyword_id},
    )

    if result_cursor.statusmessage != "DELETE 1":
        msg = "Keyword could not be deleted!"
        raise RuntimeError(msg)


async def _get_keywords(
    *, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> list[Keyword]:
    """Return all keywords stored in the database."""

    keyword_rows = await connection.cursor(
        row_factory=psycopg.rows.class_row(Keyword)
    ).execute("SELECT * FROM catalog_keyword")

    return await keyword_rows.fetchall()


async def _get_services(
    *, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> list[Service]:
    """Return all services stored in the database."""

    service_rows = await connection.cursor(
        row_factory=psycopg.rows.class_row(Service)
    ).execute("SELECT * FROM catalog_service")

    return await service_rows.fetchall()


async def _delete_service(
    *, service_id: UUID, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    result_cursor = await connection.execute(
        """DELETE FROM catalog_service WHERE id = %(service_id)s""",
        {"service_id": service_id},
    )

    if result_cursor.statusmessage != "DELETE 1":
        msg = "Service could not be deleted!"
        raise RuntimeError(msg)


async def _create_or_update_service(
    *, service: Service, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    await connection.execute(
        """INSERT INTO catalog_service
                (id,
                plony_at_id,
                plony_review_state,
                title,
                description,
                category,
                general_long_description,
                general_short_text_cloud_portal,
                general_link_service_usage,
                general_link_documentation,
                general_software_name,
                backup_strategy,
                communication_contact_user_support,
                service_lvl_description,
                user_limitations,
                user_access_description,
                data_storage_location,
                catalog_provider_id,
                general_service_logo_data,
                general_service_logo_content_type,
                general_service_logo_filename)
               VALUES
                (
                %(id)s,
                %(plony_at_id)s,
                %(plony_review_state)s,
                %(title)s,
                %(description)s,
                %(category)s,
                %(general_long_description)s,
                %(general_short_text_cloud_portal)s,
                %(general_link_service_usage)s,
                %(general_link_documentation)s,
                %(general_software_name)s,
                %(backup_strategy)s,
                %(communication_contact_user_support)s,
                %(service_lvl_description)s,
                %(user_limitations)s,
                %(user_access_description)s,
                %(data_storage_location)s,
                %(catalog_provider_id)s,
                %(general_service_logo_data)s,
                %(general_service_logo_content_type)s,
                %(general_service_logo_filename)s
                )
               ON CONFLICT (id)
               DO UPDATE SET
                plony_at_id = %(plony_at_id)s,
                plony_review_state = %(plony_review_state)s,
                title = %(title)s,
                description = %(description)s,
                category = %(category)s,
                general_long_description = %(general_long_description)s,
                general_short_text_cloud_portal = %(general_short_text_cloud_portal)s,
                general_link_service_usage = %(general_link_service_usage)s,
                general_link_documentation = %(general_link_documentation)s,
                general_software_name = %(general_software_name)s,
                backup_strategy = %(backup_strategy)s,
                communication_contact_user_support = %(communication_contact_user_support)s,
                service_lvl_description = %(service_lvl_description)s,
                user_limitations = %(user_limitations)s,
                user_access_description = %(user_access_description)s,
                data_storage_location = %(data_storage_location)s,
                catalog_provider_id = %(catalog_provider_id)s,
                general_service_logo_data = %(general_service_logo_data)s,
                general_service_logo_content_type = %(general_service_logo_content_type)s,
                general_service_logo_filename = %(general_service_logo_filename)s;""",
        service.model_dump()
        | {
            "plony_at_id": str(service.plony_at_id),
            "general_link_service_usage": str(service.general_link_service_usage),
            "general_link_documentation": str(service.general_link_documentation)
            if service.general_link_documentation
            else None,
        },
    )


async def _download_service_logo(
    plony_client: PlonyClient, plony_service: PlonyService
) -> tuple[pydantic.UUID4, bytes]:
    if plony_service.general_service_logo is None:
        error_message = f"ERROR: could not import services as at least one is missing a logo: {plony_service.title}"
        raise ServiceValidationError(error_message)

    response = await plony_client.get_image(
        plony_service.general_service_logo.download.unicode_string(),
        plony_service.general_service_logo.content_type,
    )

    return (plony_service.uid, response)


async def _download_provider_logo(
    plony_client: PlonyClient, plony_provider: PlonyInstitution
) -> tuple[pydantic.UUID4, bytes]:
    if plony_provider.logo is None:
        error_message = f"ERROR: could not import providers as at least one is missing a logo: {plony_provider.title}"
        raise ServiceValidationError(error_message)

    response = await plony_client.get_image(
        plony_provider.logo.download.unicode_string(),
        plony_provider.logo.content_type,
    )

    return (plony_provider.uid, response)
