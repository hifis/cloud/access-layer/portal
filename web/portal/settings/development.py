from .base import *  # noqa: F403 | Importing all base settings is the intended effect here.

DEBUG = True

# Tailwind configuration
INTERNAL_IPS = ["127.0.0.1"]
INSTALLED_APPS += ["django_browser_reload"]  # noqa: F405
MIDDLEWARE += ["django_browser_reload.middleware.BrowserReloadMiddleware"]  # noqa: F405
