"""
add_context_name_to_service
"""

from alembic import op

# revision identifiers, used by Alembic.
revision = "d8c6f4d4fc96"
down_revision = "0af8d4bf9b88"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column(
        "monitoring_availability", "service_id", new_column_name="catalog_service_id"
    )
    op.alter_column(
        "monitoring_availability_history",
        "service_id",
        new_column_name="catalog_service_id",
    )


def downgrade() -> None:
    op.alter_column(
        "monitoring_availability", "catalog_service_id", new_column_name="service_id"
    )
    op.alter_column(
        "monitoring_availability_history",
        "catalog_service_id",
        new_column_name="service_id",
    )
