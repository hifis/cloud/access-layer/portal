import os
from importlib import import_module

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from web.base.models import User


class Command(BaseCommand):  # type: ignore[misc]
    help = "Clear expired sessions and leftover users"

    def handle(self: "BaseCommand", *_args: None, **_options: None) -> None:
        self.stdout.write("Clearing expired sessions...")
        import_module(settings.SESSION_ENGINE).SessionStore.clear_expired()
        self.stdout.write("OK!")

        self.stdout.write("Removing leftover expired users...")
        (_, deleted_objects) = User.objects.filter(
            fallback_expire_at__lte=timezone.now()
        ).delete()

        cleared_users = 0
        if "base.User" in deleted_objects:
            cleared_users = deleted_objects["base.User"]

        if cleared_users > 0:
            message = f"Deleted {cleared_users} leftover user(s). This may indicate a bug in user handling!"

            if os.getenv("EXCEPTION_TRACKING_ENABLED") == "True":
                # Manually report this
                from sentry_sdk import capture_exception

                capture_exception(RuntimeError(message))

            self.stdout.write(message)
        else:
            self.stdout.write("No leftover users found. OK!")
