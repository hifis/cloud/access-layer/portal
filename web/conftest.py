import os
import typing
from pathlib import Path

import alembic.command
import alembic.config
import pytest
import pytest_asyncio
from django.core.handlers.asgi import ASGIRequest
from django.test import AsyncRequestFactory

import core.app
import core.catalog.test_catalog
import core.plony
from web.base.testing import TEST_CLAIMS, create_request_with_user


@pytest_asyncio.fixture
async def request_with_user(
    request: pytest.FixtureRequest, async_rf: AsyncRequestFactory
) -> ASGIRequest:
    """
    Fixture for creating an authenticated request.
    """

    marker = request.node.get_closest_marker("user_claims")

    user_claims = TEST_CLAIMS.copy() if marker is None else marker.args[0]

    return await create_request_with_user(user_claims=user_claims, async_rf=async_rf)


@pytest_asyncio.fixture
async def _core_database() -> typing.AsyncGenerator[None]:
    """This function is run once per test class (see below).
    It migrates the complete core database, yields to the tests and then cleans up."""

    alembic_ini_path = Path(__file__).parent / "../alembic.ini"
    alembic_config = alembic.config.Config(alembic_ini_path)

    os.environ["PORTAL_CORE_DATABASE_URL"] = core.app.require_envvar(
        "PORTAL_CORE_TEST_DATABASE_URL"
    )

    alembic.command.upgrade(alembic_config, "heads")

    # Run tests below
    yield

    # Clean up (down-migrate)
    alembic.command.downgrade(alembic_config, "base")
