"""
Add quota model
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import JSONB, UUID

# revision identifiers, used by Alembic.
revision = "c6741e959354"
down_revision = "da4aad48c87f"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "catalog_quota",
        sa.Column("id", UUID, primary_key=True, nullable=False, comment="Quota's UUID"),
        sa.Column(
            "quota_json",
            JSONB,
            nullable=False,
            comment="The quota",
        ),
    )

    op.create_index(
        index_name="catalog_quota_json_gin",
        table_name="catalog_quota",
        columns=["quota_json"],
        postgresql_using="gin",
    )


def downgrade() -> None:
    op.drop_table("catalog_quota")
