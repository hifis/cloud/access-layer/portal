import logging
import os

import core.db


def boot(postgres_url: str) -> None:
    """Initialize core dependencies. Do this before using the core modules."""

    #
    # Initialize exception tracking if enabled
    #

    log = logging.getLogger(__name__)

    if os.getenv("EXCEPTION_TRACKING_ENABLED") == "True":
        log.info("Loading Exception Tracking.")

        import sentry_sdk
        from sentry_sdk.integrations.django import DjangoIntegration
        from sentry_sdk.integrations.httpx import HttpxIntegration

        sentry_sdk.init(
            dsn=require_envvar("EXCEPTION_TRACKING_DSN"),
            environment=require_envvar("EXCEPTION_TRACKING_ENVIRONMENT"),
            release=require_envvar("EXCEPTION_TRACKING_RELEASE"),
            ca_certs=os.getenv("EXCEPTION_TRACKING_CA_CERT", None),
            integrations=[DjangoIntegration(), HttpxIntegration()],
            # Do not trace, prometheus does this better at the moment until there's spans in the web UI
            traces_sample_rate=0.0,
            # Reduce data sent
            max_request_body_size="never",
            send_default_pii=False,
            include_local_variables=False,
            send_client_reports=False,
        )

    #
    # Load core config
    #

    postgres_min_pool_size = int(os.getenv("PORTAL_CORE_POSTGRES_MIN_POOL_SIZE", "1"))
    postgres_max_pool_size = int(os.getenv("PORTAL_CORE_POSTGRES_MAX_POOL_SIZE", "4"))

    core.db.boot(postgres_url, postgres_min_pool_size, postgres_max_pool_size)


async def is_ready() -> bool:
    """Returns whether the core is able to handle requests at this point in time."""

    db_status = await core.db.is_healthy()

    return db_status


def require_envvar(var: str) -> str:
    """Try to get variable from environment. Raise if not found."""
    value = os.getenv(var)
    if value is None:
        error_message = (
            f"ERROR: Missing configuration. Could not find {var} in environment!"
        )
        raise RuntimeError(error_message)

    return value
