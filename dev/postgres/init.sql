-- Initialize the two DBs for development
-- There is a password to prevent accidental LAN access in case of improperly configured container ports.

-- The Django testing setup creates the database automatically, therefore it needs CREATEDB
CREATE ROLE portal_web LOGIN CREATEDB PASSWORD 'QxGhR7kQbJxQwoA9iXGHWm2UUFdCgyZl';
CREATE DATABASE portal_web OWNER portal_web ENCODING UTF8;

CREATE ROLE portal_core LOGIN PASSWORD 'w8fnBJmuHiPdijy0sHQrSMnGhGFNojoR';
CREATE DATABASE portal_core OWNER portal_core ENCODING UTF8;

CREATE ROLE portal_core_test LOGIN PASSWORD 'gN8aFnmQJWMX5xkFkrFD79kV41AO1aEA';
CREATE DATABASE portal_core_test OWNER portal_core_test ENCODING UTF8;