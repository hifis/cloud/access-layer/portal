"""
Add policy model
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import JSONB, UUID

# revision identifiers, used by Alembic.
revision = "da4aad48c87f"
down_revision = "2d2455fd966"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "catalog_policy",
        sa.Column(
            "id", UUID, primary_key=True, nullable=False, comment="Policy's UUID"
        ),
        sa.Column(
            "policy_json",
            JSONB,
            nullable=False,
            comment="The policy",
        ),
    )

    op.create_index(
        index_name="catalog_policy_json_gin",
        table_name="catalog_policy",
        columns=["policy_json"],
        postgresql_using="gin",
    )


def downgrade() -> None:
    op.drop_table("catalog_policy")
