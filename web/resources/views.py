import asyncio
import json
import re
from http import HTTPStatus
from typing import Any, Literal
from uuid import UUID

import pydantic
from asgiref.sync import sync_to_async
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.http import (
    Http404,
    HttpRequest,
    HttpResponse,
    JsonResponse,
)
from django.shortcuts import render
from django.views.decorators.http import require_POST

import core.catalog
import core.identity
import core.resources


class FormResourceRequest(pydantic.BaseModel):
    """This represents the data sent by the resource request form."""

    model_config = pydantic.ConfigDict(strict=True)

    name: str = pydantic.Field(..., min_length=1, pattern=r"^[0-9a-zA-z- ]+$")
    target_entity: str
    catalog_policy_id: pydantic.UUID4
    specification: dict[str, Any]
    accept_terms: Literal[True]

    @pydantic.field_validator("target_entity")
    @classmethod
    def must_contain_valid_target_entity(
        cls: type["FormResourceRequest"], target_entity: str
    ) -> str:
        if (
            # Personal resource
            target_entity == "self"
            # Group URN
            or re.match(core.identity.GROUP_URN_PATTERN, target_entity)
        ):
            return target_entity
        else:
            msg = "target_entity has invalid format"
            raise ValueError(msg)


@login_required  # type: ignore[misc]
async def resources(request: HttpRequest) -> HttpResponse:
    """Personal resource list."""

    user_resources = await core.resources.get_user_resources(
        actor_identity=request.aai_profile.to_actor_identity(),
        authoritative_directory=settings.AUTHORITATIVE_DIRECTORY,
    )

    if len(user_resources) == 0:
        # User does not have any resources, show default page
        return render(request, "resources/resources_no_requests.html")

    # Load all user resource's services into a dict indexed by service's id
    services = {
        service.id: service
        for service in await asyncio.gather(*[
            core.catalog.get_service(catalog_service_id)
            for catalog_service_id in {
                resource.catalog_service_id for resource in user_resources
            }
        ])
        if service is not None
    }

    # Load all user resource's quotas into a dict indexed by quota's id
    quotas = {
        quota.id: quota
        for quota in await asyncio.gather(*[
            core.catalog.get_quota(catalog_quota_id)
            for catalog_quota_id in {
                resource.catalog_quota_id for resource in user_resources
            }
        ])
        if quota is not None
    }

    # Load all user resource's resource types into a dict indexed by resource type's id
    resource_types = {
        resource_type.id: resource_type
        for resource_type in await asyncio.gather(*[
            core.catalog.get_resource_type(catalog_resource_type_id)
            for catalog_resource_type_id in {
                quotas[resource.catalog_quota_id].catalog_resource_type_id
                for resource in user_resources
            }
        ])
        if resource_type is not None
    }

    # Load providers
    providers = {
        provider.id: provider
        for provider in await core.catalog.get_providers()
        if provider is not None
    }

    # Attributes to output to JSON in case the structs contain something confidential in the future
    template_data = {
        "user": {
            "id": request.aai_profile.vo_person_id,
            "name": request.aai_profile.name,
        },
        "resources": [
            {
                "id": str(resource.id),
                "externalID": resource.external_id,
                "name": resource.name,
                "catalogServiceID": resource.catalog_service_id,
                "catalogResourceTypeID": quotas[
                    resource.catalog_quota_id
                ].catalog_resource_type_id,
                "specification": resource.specification,
                "state": resource.state,
                "stateText": resource.state_text,
                "identityGroupURNTarget": resource.identity_group_urn_target,
                "identityUserIDTarget": resource.identity_user_id_target,
                "createdAt": resource.created_at,
                "updatedAt": resource.updated_at,
            }
            for resource in user_resources
        ],
        "resourceTypes": {
            str(resource_type_id): {
                "id": str(resource_type_id),
                "name": resource_type.name,
            }
            for (resource_type_id, resource_type) in resource_types.items()
        },
        "services": {
            str(service_id): {
                "id": str(service_id),
                "title": service.title,
                "generalSoftwareName": service.general_software_name,
                "catalogProviderID": service.catalog_provider_id,
            }
            for (service_id, service) in services.items()
        },
        "providers": {
            str(provider_id): {"id": str(provider_id), "title": provider.title}
            for (provider_id, provider) in providers.items()
        },
    }

    resource_json = json.dumps(template_data, cls=DjangoJSONEncoder)

    return render(
        request,
        "resources/resources.html",
        {"resource_json": resource_json},
    )


# Django annotations do not have types
@require_POST  # type: ignore[misc]
async def request_resource(request: HttpRequest) -> JsonResponse:
    """Receive and validate resource request sent using the form."""

    if not await sync_to_async(lambda: request.user.is_authenticated)():
        return JsonResponse(
            {"status_text": "You need to sign in to continue."},
            status=HTTPStatus.UNAUTHORIZED,
        )

    try:
        form_data = FormResourceRequest.model_validate_json(request.body)
    except ValueError:
        return JsonResponse(
            {"status_text": "The data submitted is formatted incorrectly."},
            status=HTTPStatus.BAD_REQUEST,
        )

    # Try creating resource object if quota has enough room available
    try:
        await core.resources.request_resource(
            actor_identity=request.aai_profile.to_actor_identity(),
            catalog_policy_id=form_data.catalog_policy_id,
            target_entity=form_data.target_entity,
            name=form_data.name,
            specification=form_data.specification,
        )
    except core.resources.ResourceRequestError:
        return JsonResponse(
            {"status_text": "Internal server error."},
            status=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    except core.resources.QuotaExceededError:
        return JsonResponse(
            {
                "status_text": "Your resource request would exceed the quota's overall limit. Please contact support."
            },
            status=HTTPStatus.FORBIDDEN,
        )

    messages.add_message(
        request=request,
        message="Thank you for your request! Next, we're forwarding your request to the service provider for review and provisionig. You can keep track of your request's status on this page.",
        level=messages.SUCCESS,
    )

    return JsonResponse({}, status=201)


@login_required  # type: ignore[misc]
async def request_form(
    request: HttpRequest, resource_type_id: UUID, service_id: UUID
) -> HttpResponse:
    """Resource request form."""

    service = await core.catalog.get_service(service_id)
    if service is None:
        error_message = "Service not found."
        raise Http404(error_message)

    resource_type = await core.catalog.get_resource_type(resource_type_id)
    if resource_type is None:
        error_message = "Resource type not found."
        raise Http404(error_message)

    policies_available_to_user = await core.catalog.get_policies_available_to_actor(
        actor_identity=request.aai_profile.to_actor_identity(),
        service_id=service.id,
        resource_type_id=resource_type.id,
    )

    if len(policies_available_to_user) == 0:
        # User does not have any policies for this combination, so render fallback page
        return render(
            request,
            "resources/request_no_access.html",
            {"service": service, "resource_type": resource_type},
        )

    quota_ids_available_to_user = {
        policy.catalog_quota_id for policy in policies_available_to_user
    }

    quota_tasks = [
        core.catalog.get_quota(quota_id) for quota_id in quota_ids_available_to_user
    ]
    quotas_available_to_user = await asyncio.gather(*quota_tasks)

    if None in quotas_available_to_user or len(quotas_available_to_user) != len(
        quota_ids_available_to_user
    ):
        # Not covered by tests at this time due to the structure of the get_policies_available_to_actor query
        # Should this change this check should catch potential errors
        msg = "Could not retrieve all quotas available to user. This should never happen and may hint at a referential integrity issue between policies and quotas!"
        raise RuntimeError(msg)

    # TODO: only return URNs which match the policies  # noqa: TD002, FIX002, TD003
    all_groups = await core.identity.get_group_urns()

    # Manually build return data so we don't accidentally send future non-public fields
    return render(
        request,
        "resources/request.html",
        {
            "form_data": json.dumps(
                {
                    "service": {"title": service.title},
                    "resourceType": {
                        "id": resource_type.id,
                        "name": resource_type.name,
                        "jsonSchema": resource_type.json_schema,
                        "uiSchema": resource_type.ui_schema,
                    },
                    "policies": [
                        {
                            "id": policy.id,
                            "name": policy.name,
                            "targetEntity": policy.target_entity,
                            "catalogQuotaID": policy.catalog_quota_id,
                            "jsonSchema": policy.json_schema,
                        }
                        for policy in policies_available_to_user
                    ],
                    "quotas": [
                        {
                            "id": quota.id,
                            "name": quota.name,
                        }
                        for quota in quotas_available_to_user
                        if quota is not None
                    ],
                    "groups": all_groups,
                },
                cls=DjangoJSONEncoder,
            )
        },
    )
