"""
add_messages_history_table
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import JSONB, UUID

# revision identifiers, used by Alembic.
revision = "f0d7020b593d"
down_revision = None
branch_labels = ("cloud_agent",)
depends_on = None


def upgrade() -> None:
    op.create_table(
        "cloud_agent_message_history",
        sa.Column(
            "type",
            sa.Text,
            primary_key=False,
            nullable=False,
            comment="The type of the message",
        ),
        sa.Column(
            "resource_id",
            UUID,
            primary_key=False,
            nullable=False,
            comment="The resource_id of the message",
        ),
        sa.Column(
            "catalog_service_id",
            UUID,
            primary_key=False,
            nullable=False,
            comment="The service_id of the service that receives/sends the message",
        ),
        sa.Column(
            "queue",
            sa.Text,
            primary_key=False,
            nullable=False,
            comment="The queue the message was sent/received to/from (to_hca/to_portal)",
        ),
        sa.Column(
            "payload",
            JSONB,
            nullable=False,
            comment="The JSON payload of the message",
        ),
        sa.Column(
            "created_at",
            sa.DateTime(timezone=True),
            unique=False,
            index=False,
            nullable=False,
            comment="The date the message was sent/received",
        ),
    )


def downgrade() -> None:
    op.drop_table("cloud_agent_message_history")
