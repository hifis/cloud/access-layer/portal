"""Module for checking service availabilities."""

import asyncio
from collections.abc import Sequence
from datetime import UTC, datetime
from ssl import SSLCertVerificationError

import httpx
import psycopg.rows
import pydantic

import core.db
from core.catalog import ServiceLink, get_service_links


class Availability(pydantic.BaseModel):
    catalog_service_id: pydantic.UUID4
    status_code: int
    status_text: str | None
    date: datetime


class AvailabilityClient:
    """An async client to check service availabilities. It will try to access the service at the given link.
    If the connection fails or times out it will wait for 'delay' seconds and then tries again. It will repeat
    this for the amount given in 'retries'. The timeout can be configured with 'timeout' in seconds.
    """

    def __init__(
        self: "AvailabilityClient", retries: int, delay: int, timeout: int
    ) -> None:
        self.retries = retries
        self.delay = delay
        self.timeout = timeout

    async def check_availability(
        self: "AvailabilityClient", link: ServiceLink
    ) -> Availability:
        status_code = 0
        status_text = None

        for _ in range(self.retries + 1):
            try:
                async with httpx.AsyncClient() as async_client:
                    r = await async_client.get(
                        link.service_link, timeout=self.timeout, follow_redirects=True
                    )
                    status_code = r.status_code
                break
            except httpx.ConnectError as ex:
                status_code = -1
                status_text = str(ex)
            except httpx.ConnectTimeout as ex:
                status_code = -2
                status_text = str(ex)
            except httpx.RemoteProtocolError as ex:
                status_code = -3
                status_text = str(ex)
            except httpx.ReadTimeout as ex:
                status_code = -4
                status_text = str(ex)
            except SSLCertVerificationError as ex:
                status_code = -5
                status_text = str(ex)
            await asyncio.sleep(self.delay)

        return Availability(
            catalog_service_id=link.catalog_service_id,
            status_code=status_code,
            status_text=status_text,
            date=datetime.now(tz=UTC),
        )


async def get_service_availability(
    catalog_service_id: pydantic.UUID4,
) -> Availability | None:
    """Return availability for a given service"""
    async with core.db.DB.get_connection_with_transaction() as connection:
        availability_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(Availability)
        ).execute(
            "SELECT * FROM monitoring_availability WHERE catalog_service_id = %(catalog_service_id)s",
            {"catalog_service_id": catalog_service_id},
        )

        return await availability_rows.fetchone()


async def get_service_availabilities() -> list[Availability]:
    """Return latest availability checker status for all services"""

    async with core.db.DB.get_connection_with_transaction() as connection:
        availability_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(Availability)
        ).execute(
            "SELECT * FROM monitoring_availability",
        )

        return await availability_rows.fetchall()


async def get_unavailable_services() -> list[Availability]:
    """Return only unavailable services"""

    async with core.db.DB.get_connection_with_transaction() as connection:
        availability_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(Availability)
        ).execute(
            "SELECT * FROM monitoring_availability WHERE status_code != 200",
        )

        return await availability_rows.fetchall()


async def get_availability_history_for_service(
    catalog_service_id: pydantic.UUID4,
) -> list[Availability]:
    """Return all availability history for a given service"""

    async with core.db.DB.get_connection_with_transaction() as connection:
        availability_rows = await connection.cursor(
            row_factory=psycopg.rows.class_row(Availability)
        ).execute(
            "SELECT * FROM monitoring_availability_history WHERE catalog_service_id = %(catalog_service_id)s",
            {"catalog_service_id": catalog_service_id},
        )

        return await availability_rows.fetchall()


async def run_availability_checker(client: AvailabilityClient) -> None:
    """check availabilties for all services and update the database"""
    # Get service links from database
    links = await get_service_links()

    # Prepare tasks to test the service link
    check_tasks = [client.check_availability(link) for link in links]

    # Wait for tasks to complete
    availabilities = await asyncio.gather(*check_tasks)

    await update_availabilities(availabilities)


async def update_availabilities(availabilities: list[Availability]) -> None:
    """Update the database with the given availabilities"""
    previous_availabilities: dict[pydantic.UUID4, Availability] = {
        item.catalog_service_id: item for item in await get_service_availabilities()
    }

    async with core.db.DB.get_connection_with_transaction() as connection:
        # check if the availability has changed from the last check and update history table
        for availability in availabilities:
            previous = previous_availabilities.get(availability.catalog_service_id)
            if (previous is not None) and (
                previous.status_code != availability.status_code
            ):
                await _insert_history(previous, connection)
        # Update the availabilities in the database
        await _upsert_items(availabilities, connection)


async def _upsert_items(
    items: Sequence[Availability],
    connection: psycopg.AsyncConnection[psycopg.rows.TupleRow],
) -> None:
    number_of_items = len(items)

    if number_of_items == 0:
        return

    query = """INSERT INTO "monitoring_availability" (catalog_service_id, status_code, status_text, date)
               VALUES (%(catalog_service_id)s, %(status_code)s, %(status_text)s, %(date)s)
               ON CONFLICT ("catalog_service_id")
               DO UPDATE
               SET status_text = EXCLUDED.status_text, status_code = EXCLUDED.status_code, date = EXCLUDED.date"""

    await connection.cursor().executemany(query, [item.model_dump() for item in items])


async def _insert_history(
    item: Availability, connection: psycopg.AsyncConnection[psycopg.rows.TupleRow]
) -> None:
    query = """INSERT INTO "monitoring_availability_history"
              (catalog_service_id, status_code, status_text, date)
              VALUES (%(catalog_service_id)s, %(status_code)s, %(status_text)s, %(date)s)"""

    await connection.execute(query, item.model_dump())
