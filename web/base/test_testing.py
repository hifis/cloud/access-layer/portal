from typing import Any
from uuid import UUID, uuid4

import asgiref.sync
import pytest
from django.http import HttpRequest, HttpResponse
from django.test import AsyncRequestFactory

from web.base import testing
from web.base.auth import (
    aai_session_encryption_middleware,
)
from web.base.models import AAIProfile
from web.base.testing import TEST_CLAIMS


@pytest.mark.django_db
class TestCreateRequestWithUser:
    def test_create_request_with_user_defaults(
        self: "TestCreateRequestWithUser", async_rf: AsyncRequestFactory
    ) -> None:
        request = asgiref.sync.async_to_sync(testing.create_request_with_user)(
            async_rf=async_rf, user_claims=None
        )

        assert request.aai_profile == AAIProfile(
            vo_person_id=UUID(TEST_CLAIMS["sub"]),
            name=TEST_CLAIMS["name"],
            email=TEST_CLAIMS["email"],
            eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
            eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
            eduperson_scoped_affiliation=TEST_CLAIMS["eduperson_scoped_affiliation"],
        )

        del request.aai_profile

        assert not hasattr(request, "aai_profile")

        def test_view(request: HttpRequest) -> HttpResponse:
            assert hasattr(request, "aai_profile")

            # Profile gets set as expected
            assert request.aai_profile is not None
            assert request.aai_profile == AAIProfile(
                vo_person_id=UUID(TEST_CLAIMS["sub"]),
                name=TEST_CLAIMS["name"],
                email=TEST_CLAIMS["email"],
                eduperson_entitlements=TEST_CLAIMS["eduperson_entitlement"],
                eduperson_assurances=TEST_CLAIMS["eduperson_assurance"],
                eduperson_scoped_affiliation=TEST_CLAIMS[
                    "eduperson_scoped_affiliation"
                ],
            )

            return HttpResponse()

        aai_session_encryption_middleware(test_view)(request)

    def test_create_request_with_custom_claims(
        self: "TestCreateRequestWithUser", async_rf: AsyncRequestFactory
    ) -> None:
        custom_claims: dict[str, Any] = {
            "sub": str(uuid4()),
            "email": TEST_CLAIMS["email"] + "custom",
            "name": TEST_CLAIMS["name"] + "custom",
            "eduperson_entitlement": [
                TEST_CLAIMS["eduperson_entitlement"][0] + "custom",
                TEST_CLAIMS["eduperson_entitlement"][1] + "custom",
            ],
            "eduperson_assurance": [
                TEST_CLAIMS["eduperson_assurance"][0] + "custom",
            ],
            "eduperson_scoped_affiliation": [
                TEST_CLAIMS["eduperson_scoped_affiliation"][0] + "custom"
            ],
        }

        request = asgiref.sync.async_to_sync(testing.create_request_with_user)(
            async_rf=async_rf, user_claims=custom_claims
        )

        assert request.aai_profile == AAIProfile(
            vo_person_id=UUID(custom_claims["sub"]),
            name=custom_claims["name"],
            email=custom_claims["email"],
            eduperson_entitlements=custom_claims["eduperson_entitlement"],
            eduperson_assurances=custom_claims["eduperson_assurance"],
            eduperson_scoped_affiliation=custom_claims["eduperson_scoped_affiliation"],
        )

        del request.aai_profile

        assert not hasattr(request, "aai_profile")

        def test_view(request: HttpRequest) -> HttpResponse:
            assert hasattr(request, "aai_profile")

            # Profile gets set as expected
            assert request.aai_profile is not None
            assert request.aai_profile == AAIProfile(
                vo_person_id=UUID(custom_claims["sub"]),
                name=custom_claims["name"],
                email=custom_claims["email"],
                eduperson_entitlements=custom_claims["eduperson_entitlement"],
                eduperson_assurances=custom_claims["eduperson_assurance"],
                eduperson_scoped_affiliation=custom_claims[
                    "eduperson_scoped_affiliation"
                ],
            )

            return HttpResponse()

        aai_session_encryption_middleware(test_view)(request)
