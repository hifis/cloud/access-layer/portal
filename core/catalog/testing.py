"""
The factories here can be used for setting up data for testing in other
contexts but must not be used for regular business logic.
"""

import typing
from uuid import UUID, uuid4

from pydantic import HttpUrl

import core.app
import core.catalog
import core.catalog.catalog
import core.db
import core.plony
from core.catalog.catalog import (
    _create_or_update_keyword,
    _create_or_update_policy,
    _create_or_update_provider,
    _create_or_update_quota,
    _create_or_update_resource_type,
    _create_or_update_service,
    _delete_keyword,
    _delete_policy,
    _delete_provider,
    _delete_resource_type,
    _delete_service,
)


def make_keyword() -> core.catalog.Keyword:
    uuid = uuid4()
    return core.catalog.Keyword(
        id=uuid,
        plony_at_id=HttpUrl(
            f"https://plony-test.helmholtz-berlin.de/keyword?uuid={uuid}"
        ),
        title="Keyword",
        is_visible=True,
        is_category=False,
    )


def make_plony_keyword(
    patch: dict[str, typing.Any] | None = None,
) -> core.plony.PlonyKeyword:
    if patch is None:
        patch = {}

    uuid = uuid4()
    return core.plony.PlonyKeyword.model_validate(
        {
            "title": "Keyword",
            "@id": f"https://plony-test.helmholtz-berlin.de/keyword?uuid={uuid}",
            "UID": uuid,
            "isVisible": True,
            "isCategory": False,
        }
        | patch
    )


def make_provider() -> core.catalog.Provider:
    return core.catalog.Provider(
        id=uuid4(),
        title="Provider",
        abbreviation="p",
        url=HttpUrl("https://localhost"),
        logo_content_type="image/svg+xml",
        logo_filename="logo.svg",
        logo_data=b"<svg></svg>",
    )


def make_plony_institution(
    uuid: str, patch: dict[str, typing.Any] | None = None
) -> core.plony.PlonyInstitution:
    # uuid is a parameter because of hardcoded logos at the moment

    if patch is None:
        patch = {}

    return core.plony.PlonyInstitution.model_validate(
        {
            "title": "Institution",
            "abbreviation": "I",
            "@id": f"https://plony-test.helmholtz-berlin.de/institution?uuid={uuid}",
            "UID": uuid,
            "url": "https://localhost.local",
            "is_service_provider": True,
            "logo": {
                "filename": "logo.svg",
                "download": "https://localhost/svg_image",
                "content-type": "image/svg+xml",
            },
        }
        | patch
    )


def make_plony_service(
    provider: core.plony.PlonyInstitution,
    keywords: list[core.plony.PlonyKeyword] | None = None,
    patch: dict[str, typing.Any] | None = None,
) -> core.plony.PlonyService:
    if keywords is None:
        keywords = []

    if patch is None:
        patch = {}

    uuid = uuid4()
    return core.plony.PlonyService.model_validate(
        {
            "title": "service",
            "description": "description",
            "service_category": {"title": "Cloud Service", "token": "cloudService"},
            "backup_strategy": "backup_strategy",
            "general_software_name": "general_software_name",
            "general_short_text_cloud_portal": "general_short_text_cloud_portal",
            "general_long_description": "general_long_description",
            "general_link_service_usage": f"https://plony-test.helmholtz-berlin.de/service_usage?service={uuid}",
            "general_documentation": "https://localhost",
            "communication_contact_user_support": "communication_contact_user_support",
            "service_lvl_description": "optional service_lvl_description",
            "user_limitations": "user_limitations",
            "user_access_description": "user_access_description",
            "review_state": "service_online",
            "other_accept_publish_to_portal": {"title": "Yes", "token": "yes"},
            "data_storage_location": {
                "title": "location title",
                "token": "tokenstring",
            },
            "service_provider": [
                {
                    "title": provider.title,
                    "@id": provider.at_id,
                },
            ],
            "general_service_logo": {
                "filename": "logo.svg",
                "download": "https://localhost/svg_image",
                "content-type": "image/svg+xml",
            },
            "service_keywords": [
                {
                    "title": keyword.title,
                    "@id": keyword.at_id,
                }
                for keyword in keywords
            ],
            "UID": uuid,
            "@id": f"https://plony-test.helmholtz-berlin.de/service?uuid={uuid}",
        }
        | patch
    )


def make_service(
    provider_id: UUID,
) -> core.catalog.Service:
    uuid = uuid4()
    return core.catalog.Service(
        id=uuid,
        plony_at_id=HttpUrl(
            f"https://plony-test.helmholtz-berlin.de/service?uuid={uuid}"
        ),
        catalog_provider_id=provider_id,
        title="service",
        description="description",
        category="Cloud Service",
        backup_strategy="backup_strategy",
        general_software_name="general_software_name",
        general_short_text_cloud_portal="general_short_text_cloud_portal",
        general_long_description="general_long_description",
        general_link_service_usage=HttpUrl(
            f"https://plony-test.helmholtz-berlin.de/service_usage?service={uuid}"
        ),
        general_link_documentation=HttpUrl("https://localhost"),
        communication_contact_user_support="communication_contact_user_support",
        service_lvl_description="optional service_lvl_description",
        user_limitations="user_limitations",
        user_access_description="user_access_description",
        plony_review_state="service_online",
        general_service_logo_filename="logo.svg",
        general_service_logo_data=b"<svg></svg>",
        general_service_logo_content_type="btext/image+svg",
        data_storage_location="in center",
    )


def make_plony_resource_type(
    patch: dict[str, typing.Any] | None = None,
) -> core.plony.PlonyResourceType:
    if patch is None:
        patch = {}

    return core.plony.PlonyResourceType.model_validate(
        {
            "id": uuid4(),
            "name": "Compute",
            "description": "A Virtual Machine",
            "json_schema": {
                "$schema": "https://json-schema.org/draft/2020-12/schema",
                "$id": "https://codebase.helmholtz.cloud/...",
                "title": "ComputeResourceV1",
                "description": "Specification of a Virtual Machine",
                "type": "object",
                "properties": {
                    "name": {
                        "description": "Your machine's desired name (only a-z, 0-9, length between 3 and 30 characters)",
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 30,
                        "pattern": "^[a-z0-9]+$",
                    },
                    "flavor": {
                        "description": "Your favorite ice cream flavor",
                        "type": "string",
                        "default": "vanilla",
                        "oneOf": [
                            {"const": "vanilla", "title": "Vanilla"},
                            {"const": "chocolate", "title": "Chocolate"},
                            {"const": "strawberry", "title": "Strawberry"},
                        ],
                    },
                    "cpu": {
                        "description": "Number of CPU cores",
                        "type": "integer",
                        "default": 2,
                        "minimum": 1,
                        "enum": [1, 2, 4, 8, 16],
                    },
                    "ram": {
                        "description": "RAM in MB",
                        "type": "integer",
                        "default": 2048,
                        "minimum": 512,
                        "maximum": 131072,
                    },
                    "storage": {
                        "description": "Persistent storage space in GB",
                        "type": "integer",
                        "default": 64,
                        "minimum": 16,
                        "maximum": 4096,
                    },
                },
                "required": ["name", "flavor", "cpu", "ram", "storage"],
            },
            "ui_schema": {
                "type": "VerticalLayout",
                "elements": [
                    {
                        "type": "Control",
                        "scope": "#/properties/name",
                        "options": {"restrict": True},
                    },
                    {"type": "Control", "scope": "#/properties/flavor"},
                    {"type": "Control", "scope": "#/properties/cpu"},
                    {
                        "type": "Control",
                        "scope": "#/properties/ram",
                        "options": {"slider": True},
                    },
                    {
                        "type": "Control",
                        "scope": "#/properties/storage",
                        "options": {"slider": True},
                    },
                ],
            },
        }
        | patch
    )


def make_resource_type() -> core.catalog.ResourceType:
    return core.catalog.ResourceType(
        id=uuid4(),
        name="Compute",
        description="A Virtual Machine",
        json_schema={
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "$id": "https://codebase.helmholtz.cloud/...",
            "title": "ComputeResourceV1",
            "description": "Specification of a Virtual Machine",
            "type": "object",
            "properties": {
                "name": {
                    "description": "Your machine's desired name (only a-z, 0-9, length between 3 and 30 characters)",
                    "type": "string",
                    "minLength": 3,
                    "maxLength": 30,
                    "pattern": "^[a-z0-9]+$",
                },
                "flavor": {
                    "description": "Your favorite ice cream flavor",
                    "type": "string",
                    "default": "vanilla",
                    "oneOf": [
                        {"const": "vanilla", "title": "Vanilla"},
                        {"const": "chocolate", "title": "Chocolate"},
                        {"const": "strawberry", "title": "Strawberry"},
                    ],
                },
                "cpu": {
                    "description": "Number of CPU cores",
                    "type": "integer",
                    "default": 2,
                    "minimum": 1,
                    "enum": [1, 2, 4, 8, 16],
                },
                "ram": {
                    "description": "RAM in MB",
                    "type": "integer",
                    "default": 2048,
                    "minimum": 512,
                    "maximum": 131072,
                },
                "storage": {
                    "description": "Persistent storage space in GB",
                    "type": "integer",
                    "default": 64,
                    "minimum": 16,
                    "maximum": 4096,
                },
            },
            "required": ["name", "flavor", "cpu", "ram", "storage"],
        },
        ui_schema={
            "type": "VerticalLayout",
            "elements": [
                {
                    "type": "Control",
                    "scope": "#/properties/name",
                    "options": {"restrict": True},
                },
                {"type": "Control", "scope": "#/properties/flavor"},
                {"type": "Control", "scope": "#/properties/cpu"},
                {
                    "type": "Control",
                    "scope": "#/properties/ram",
                    "options": {"slider": True},
                },
                {
                    "type": "Control",
                    "scope": "#/properties/storage",
                    "options": {"slider": True},
                },
            ],
        },
    )


def make_plony_quota(
    patch: dict[str, typing.Any] | None = None,
) -> core.plony.PlonyQuota:
    if patch is None:
        patch = {}

    return core.plony.PlonyQuota.model_validate(
        {
            "id": uuid4(),
            "service_id": "a32d228f-a8d7-468b-8a19-6c2705b7b47c",
            "resource_type_id": "b3e9b179-b26c-4c65-95dd-07d76a9d5c93",
            "name": "Cool Quota",
            "quota": [
                {"property": "cpu", "total": 16},
                {"property": "ram", "total": 64},
                {"property": "storage", "total": 128},
            ],
        }
        | patch
    )


def make_quota(service_id: UUID, resource_type_id: UUID) -> core.catalog.Quota:
    return core.catalog.Quota(
        id=uuid4(),
        catalog_service_id=service_id,
        catalog_resource_type_id=resource_type_id,
        name="Cool Quota",
        quota=[
            core.catalog.QuotaLimit(property="cpu", total=128),
            core.catalog.QuotaLimit(property="ram", total=64_000),
            core.catalog.QuotaLimit(property="storage", total=10_000),
        ],
    )


def make_policy(
    quota_id: UUID,
) -> core.catalog.Policy:
    return core.catalog.Policy(
        id=uuid4(),
        catalog_quota_id=quota_id,
        name="Amazing Policy",
        actor_requirements=core.catalog.PolicyActorRequirements(
            edu_person_entitlement=[
                "urn:geant:helmholtz.de:group:DESY#login.helmholtz.de"
            ],
            edu_person_assurance=["https://refed.sorg/assurance/ATP/ePA-1m"],
            edu_person_scoped_affiliation=["staff@login.helmholtz.de"],
        ),
        target_entity="urn:geant:helmholtz.de:group:DLR:",
        json_schema={
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "$id": "https://codebase.helmholtz.cloud/...",
            "title": "MyPolicyLimitsForHIFISV1",
            "description": "Additional validation for this policy",
            "type": "object",
            "properties": {
                "name": {
                    "description": "your machine's name must start with 'hifis'",
                    "type": "string",
                    "pattern": "^hifis[a-z0-9]+$",
                },
                "ram": {
                    "description": "you may allocate up to 8192 MB of RAM",
                    "type": "integer",
                    "maximum": 8192,
                },
                "storage": {
                    "description": "your machine's local disk size may not exceed 1024 GB",
                    "type": "integer",
                    "maximum": 1024,
                },
            },
        },
        time_seconds=9999,
    )


def make_plony_policy(
    patch: dict[str, typing.Any] | None = None,
) -> core.plony.PlonyPolicy:
    if patch is None:
        patch = {}

    uuid = uuid4()
    return core.plony.PlonyPolicy.model_validate(
        {
            "id": uuid,
            "name": "Amazing Policy",
            "review_state": "active",
            "actor_requirements": {
                "eduPersonEntitlement": [
                    "urn:geant:helmholtz.de:group:DESY#login.helmholtz.de"
                ],
                "eduPersonAssurance": ["https://refed.sorg/assurance/ATP/ePA-1m"],
                "eduPersonScopedAffiliation": ["staff@login.helmholtz.de"],
            },
            "target_entity": "urn:geant:helmholtz.de:group:DLR:",
            "json_schema": {
                "$schema": "https://json-schema.org/draft/2020-12/schema",
                "$id": "https://codebase.helmholtz.cloud/...",
                "title": "MyPolicyLimitsForHIFISV1",
                "description": "Additional validation for this policy",
                "type": "object",
                "properties": {
                    "name": {
                        "description": "your machine's name must start with 'hifis'",
                        "type": "string",
                        "pattern": "^hifis[a-z0-9]+$",
                    },
                    "ram": {
                        "description": "you may allocate up to 8192 MB of RAM",
                        "type": "integer",
                        "maximum": 8192,
                    },
                    "storage": {
                        "description": "your machine's local disk size may not exceed 1024 GB",
                        "type": "integer",
                        "maximum": 1024,
                    },
                },
            },
            "time_seconds": 9999,
        }
        | patch
    )


async def create_or_update_keyword(*, keyword: core.catalog.catalog.Keyword) -> None:
    """Create or update a keyword in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _create_or_update_keyword(keyword=keyword, connection=connection)


async def delete_keyword(*, keyword_id: UUID) -> None:
    """Delete a keyword by id."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _delete_keyword(keyword_id=keyword_id, connection=connection)


async def create_or_update_policy(*, policy: core.catalog.catalog.Policy) -> None:
    """Create or update a policy in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _create_or_update_policy(policy=policy, connection=connection)


async def delete_policy(*, policy_id: UUID) -> None:
    """Delete a policy by ID."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _delete_policy(policy_id=policy_id, connection=connection)


async def create_or_update_resource_type(
    *, resource_type: core.catalog.catalog.ResourceType
) -> None:
    """Create or update a resource_type in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _create_or_update_resource_type(
            resource_type=resource_type, connection=connection
        )


async def delete_resource_type(*, resource_type_id: UUID) -> None:
    """Delete a resource_type by ID."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _delete_resource_type(
            resource_type_id=resource_type_id, connection=connection
        )


async def create_or_update_quota(*, quota: core.catalog.catalog.Quota) -> None:
    """Create or update a quota in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _create_or_update_quota(quota=quota, connection=connection)


async def create_or_update_service(*, service: core.catalog.catalog.Service) -> None:
    """Create or update a service in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _create_or_update_service(service=service, connection=connection)


async def delete_service(*, service_id: UUID) -> None:
    """Delete a service by ID."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _delete_service(service_id=service_id, connection=connection)


async def create_or_update_provider(*, provider: core.catalog.catalog.Provider) -> None:
    """Create or update a provider in the database."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _create_or_update_provider(provider=provider, connection=connection)


async def delete_provider(*, provider_id: UUID) -> None:
    """Delete a provider by ID."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        await _delete_provider(provider_id=provider_id, connection=connection)
