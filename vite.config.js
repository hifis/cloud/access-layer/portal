import vue from "@vitejs/plugin-vue";
import { defineConfig } from "vite";

// biome-ignore lint/style/noDefaultExport: This is how Vite is configured
export default defineConfig({
    plugins: [
        // Config options: https://github.com/vitejs/vite/tree/main/packages/plugin-vue
        vue(),
    ],
    base: "/static/",
    build: {
        outDir: "web/static",
        manifest: false,
        rollupOptions: {
            // Use JS file as entrypoint instead of index.html in regular vue workflow
            input: "web/base/static_entrypoints/base.ts",
            // Output files without hash in name so they can be included using static helpers (avoids having to use a manifest parser in Django)
            output: {
                entryFileNames: "[name].js",
                assetFileNames: "[name].[ext]",
            },
        },
    },
});
