"""
Add quota lock handles.
This is a helper table for acquiring locks on quota IDs for
preventing parallel resource requests from exceeding a quota.
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = "5f79c677ed6f"
down_revision = "da21b5f717ca"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "resources_catalog_quota_lock_handles",
        sa.Column(
            "catalog_quota_id",
            UUID,
            primary_key=True,
            nullable=False,
            comment="ID of the quota to be locked for requests",
        ),
    )


def downgrade() -> None:
    op.drop_table("resources_catalog_quota_lock_handles")
