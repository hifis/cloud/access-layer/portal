# Comparing to magic values is part of testing examples
# ruff: noqa: PLR2004

import copy
import typing
from dataclasses import dataclass
from uuid import UUID, uuid4

import pytest
import pytest_asyncio
from pydantic import HttpUrl

import core.app
import core.catalog
import core.identity.testing
import core.kpi
import core.plony
from core.catalog import testing

#
# Mock data
#


@dataclass
class ClientMockDataset:
    keywords: list[core.plony.PlonyKeyword]
    institutions: list[core.plony.PlonyInstitution]
    services: list[core.plony.PlonyService]
    resource_types: list[core.plony.PlonyResourceType]
    quotas: list[core.plony.PlonyQuota]
    policies: list[core.plony.PlonyPolicy]
    images: dict[str, bytes]


MOCK_INSTITUTIONS = [
    testing.make_plony_institution("da8c3952-d539-4228-9c93-f7968cd8ba5a"),
    testing.make_plony_institution("ac6860b7-8f77-499e-9872-b340ffd772e0"),
    # Non-provider institution without URL
    testing.make_plony_institution(
        "5ce7d675-c2eb-406b-a6af-66ccdb3375e4",
        {"is_service_provider": False, "url": None},
    ),
]

MOCK_KEYWORDS = [
    testing.make_plony_keyword(),
    testing.make_plony_keyword(),
    # Invisible Keyword
    testing.make_plony_keyword({"isVisible": False}),
    # Category
    testing.make_plony_keyword({"isCategory": True}),
    testing.make_plony_keyword(),
]

MOCK_RESOURCE_TYPES = [
    testing.make_plony_resource_type(),
    testing.make_plony_resource_type(),
    testing.make_plony_resource_type(),
]

MOCK_SERVICES = [
    testing.make_plony_service(
        provider=MOCK_INSTITUTIONS[0], keywords=[MOCK_KEYWORDS[0]]
    ),
    testing.make_plony_service(
        provider=MOCK_INSTITUTIONS[0],
        keywords=[MOCK_KEYWORDS[0], MOCK_KEYWORDS[4]],
        patch={"service_category": None},
    ),
    testing.make_plony_service(
        provider=MOCK_INSTITUTIONS[0],
        patch={
            "review_state": "integration_in_progress",
            "general_link_service_usage": None,
            "general_documentation": None,
            "service_lvl_description": None,
            "general_service_logo": None,
            "service_category": None,
        },
    ),
    testing.make_plony_service(
        provider=MOCK_INSTITUTIONS[0],
        patch={"review_state": "integration_planned"},
    ),
    testing.make_plony_service(
        provider=MOCK_INSTITUTIONS[0],
        patch={"review_state": "integration_in_progress"},
    ),
    testing.make_plony_service(
        provider=MOCK_INSTITUTIONS[0],
        patch={
            "review_state": "integration_in_progress",
            "other_accept_publish_to_portal": {"title": "No", "token": "no"},
        },
    ),
    testing.make_plony_service(
        provider=MOCK_INSTITUTIONS[0],
        patch={"review_state": "service_card_review"},
    ),
    testing.make_plony_service(
        provider=MOCK_INSTITUTIONS[0],
        patch={"review_state": "integration_completed"},
    ),
    testing.make_plony_service(
        provider=MOCK_INSTITUTIONS[0],
        patch={
            "review_state": "integration_completed",
            "other_accept_publish_to_portal": {"title": "No", "token": "no"},
        },
    ),
]

MOCK_QUOTAS = [
    testing.make_plony_quota(
        patch={
            "service_id": MOCK_SERVICES[0].uid,
            "resource_type_id": MOCK_RESOURCE_TYPES[0].id,
        }
    ),
    testing.make_plony_quota(
        patch={
            "service_id": MOCK_SERVICES[0].uid,
            "resource_type_id": MOCK_RESOURCE_TYPES[0].id,
        }
    ),
    testing.make_plony_quota(
        patch={
            "service_id": MOCK_SERVICES[0].uid,
            "resource_type_id": MOCK_RESOURCE_TYPES[0].id,
        }
    ),
]


MOCK_POLICIES = [
    testing.make_plony_policy(
        patch={
            "quota_id": MOCK_QUOTAS[0].id,
        }
    ),
    testing.make_plony_policy(
        patch={
            "quota_id": MOCK_QUOTAS[0].id,
        }
    ),
    testing.make_plony_policy(
        patch={
            "quota_id": MOCK_QUOTAS[0].id,
        }
    ),
]

MOCK_IMAGES = {
    "https://localhost/png_image": b"\x89\x50\x4e\x47\x0d\x0a\x1a\x0a",
    "https://localhost/svg_image": b"<svg></svg>",
}


PLONY_DATA = ClientMockDataset(
    keywords=MOCK_KEYWORDS,
    institutions=MOCK_INSTITUTIONS,
    services=MOCK_SERVICES,
    resource_types=MOCK_RESOURCE_TYPES,
    quotas=MOCK_QUOTAS,
    policies=MOCK_POLICIES,
    images=MOCK_IMAGES,
)

# Test data is organized so that:
# 1) One entity gets updated
# 2) One entity gets removed
# 3) One entity gets added
# 4) Any special cases/filters are covered

UPDATED_PLONY_DATA = copy.deepcopy(PLONY_DATA)

UPDATED_PLONY_DATA.keywords[0].title = "Updated Keyword"
UPDATED_PLONY_DATA.keywords[1].uid = uuid4()
UPDATED_PLONY_DATA.keywords[1].at_id = HttpUrl(
    UPDATED_PLONY_DATA.keywords[1].at_id.unicode_string() + "_new"
)

UPDATED_PLONY_DATA.institutions[0].title = "Updated Institution"
UPDATED_PLONY_DATA.institutions[0].logo = None
UPDATED_PLONY_DATA.institutions[1].uid = uuid4()
UPDATED_PLONY_DATA.institutions[1].at_id = HttpUrl(
    UPDATED_PLONY_DATA.institutions[1].at_id.unicode_string() + "_new"
)
UPDATED_PLONY_DATA.institutions[0].logo = core.plony.PlonyImageLink.model_validate(
    {
        "filename": "logo.png",
        "download": "https://localhost/png_image",
        "content-type": "image/png",
    },
)

UPDATED_PLONY_DATA.resource_types[0].json_schema["title"] = "Updated ResourceType"
UPDATED_PLONY_DATA.resource_types[1].id = uuid4()

UPDATED_PLONY_DATA.services[0].title = "Updated Service"
UPDATED_PLONY_DATA.services[0].general_service_logo = None
UPDATED_PLONY_DATA.services[1].uid = uuid4()
UPDATED_PLONY_DATA.services[1].at_id = HttpUrl(
    UPDATED_PLONY_DATA.services[1].at_id.unicode_string() + "_new"
)
UPDATED_PLONY_DATA.services[
    1
].general_service_logo = core.plony.PlonyImageLink.model_validate(
    {
        "filename": "logo.png",
        "download": "https://localhost/png_image",
        "content-type": "image/png",
    },
)

UPDATED_PLONY_DATA.quotas[0].name = "Updated Quota Name"
UPDATED_PLONY_DATA.quotas[1].id = uuid4()

UPDATED_PLONY_DATA.policies[0].json_schema["title"] = "Updated Policy"
UPDATED_PLONY_DATA.policies[1].id = uuid4()


class MockPlonyClient:
    def __init__(self: "MockPlonyClient") -> None:
        self.data = copy.deepcopy(PLONY_DATA)

    def set_mock_data(self: "MockPlonyClient", data: ClientMockDataset) -> None:
        self.data = copy.deepcopy(data)

    async def list_services(self: "MockPlonyClient") -> list[core.plony.PlonyService]:
        return copy.deepcopy(self.data.services)

    async def list_institutions(
        self: "MockPlonyClient",
    ) -> list[core.plony.PlonyInstitution]:
        return copy.deepcopy(self.data.institutions)

    async def list_keywords(self: "MockPlonyClient") -> list[core.plony.PlonyKeyword]:
        return copy.deepcopy(self.data.keywords)

    async def list_resource_types(
        self: "MockPlonyClient",
    ) -> list[core.plony.PlonyResourceType]:
        return copy.deepcopy(self.data.resource_types)

    async def list_quotas(
        self: "MockPlonyClient",
    ) -> list[core.plony.PlonyQuota]:
        return copy.deepcopy(self.data.quotas)

    async def list_policies(
        self: "MockPlonyClient",
    ) -> list[core.plony.PlonyPolicy]:
        return copy.deepcopy(self.data.policies)

    async def get_image(self: "MockPlonyClient", url: str, _content_type: str) -> bytes:
        return copy.deepcopy(self.data.images[url])


@pytest.fixture
def plony_client() -> MockPlonyClient:
    """Mock plony client."""

    return MockPlonyClient()


@pytest.mark.usefixtures("_context_database")
class TestKeywordImport:
    @staticmethod
    @pytest.mark.asyncio
    async def test_keyword_import(plony_client: MockPlonyClient) -> None:
        # At the beginning there are no keywords in the database
        keywords = await core.catalog.get_keywords()

        assert len(keywords) == 0

        # It imports all keywords
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        keywords = await core.catalog.get_keywords()

        assert len(keywords) == 5
        assert keywords[0].title == "Keyword"

        # It properly updates a keyword's title
        plony_client.set_mock_data(UPDATED_PLONY_DATA)
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        keywords = await core.catalog.get_keywords()

        assert len(keywords) == 5
        assert keywords[0].title == "Updated Keyword"

        # It raises when provider has no URL
        plony_client.set_mock_data(UPDATED_PLONY_DATA)
        plony_client.data.institutions[0].url = None

        with pytest.raises(RuntimeError, match="Provider URL can't be None!"):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )


@pytest.mark.usefixtures("_context_database")
class TestProviderImport:
    @staticmethod
    @pytest.mark.asyncio
    async def test_provider_import(plony_client: MockPlonyClient) -> None:
        # At the beginning there are no providers in the database
        providers = await core.catalog.get_providers()

        assert len(providers) == 0

        # It imports all providers
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        providers = await core.catalog.get_providers()

        assert len(providers) == 2
        assert providers[0].id == MOCK_INSTITUTIONS[0].uid
        assert providers[1].id == MOCK_INSTITUTIONS[1].uid

        # It properly updates a provider's title
        plony_client.set_mock_data(UPDATED_PLONY_DATA)
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        providers = await core.catalog.get_providers()

        assert len(providers) == 2
        assert providers[0].title == "Updated Institution"


@pytest.mark.usefixtures("_context_database")
class TestProviderConstraint:
    @staticmethod
    @pytest.mark.asyncio
    async def test_provider_constraint(plony_client: MockPlonyClient) -> None:
        # At the beginning there are no providers in the database
        providers = await core.catalog.get_providers()

        assert len(providers) == 0

        # It imports all providers
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        providers = await core.catalog.get_providers()

        assert len(providers) == 2

        # It does not allow deleting a provider with services attached (DESY)
        data_without_desy = copy.deepcopy(UPDATED_PLONY_DATA)
        del data_without_desy.institutions[0]

        plony_client.set_mock_data(data_without_desy)

        # It should raise because it can't find the provider for the service
        with pytest.raises(KeyError):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )


class TestStructValidation:
    @staticmethod
    def test_resource_type_validation() -> None:
        # It works with a valid schema
        example = testing.make_plony_resource_type()

        core.catalog.ResourceType.model_validate(example.model_dump())

        # It does not work with an invalid schema
        example = testing.make_plony_resource_type({
            "json_schema": {"random": "schema"}
        })

        with pytest.raises(
            ValueError,
            match="1 validation error for ResourceType\njson_schema\n  Value error, \\('error validating JSON schema', \"Unevaluated properties are not allowed \\('random' was unexpected\\)\"\\)",
        ):
            core.catalog.ResourceType.model_validate(example.model_dump())

        # It does not work with an empty name
        example = testing.make_plony_resource_type({"name": ""})

        with pytest.raises(
            ValueError,
            match="1 validation error for ResourceType\nname\n  String should have at least 1 character",
        ):
            core.catalog.ResourceType.model_validate(example.model_dump())

        # It does not work with an empty description
        example = testing.make_plony_resource_type({"description": ""})

        with pytest.raises(
            ValueError,
            match="1 validation error for ResourceType\ndescription\n  String should have at least 1 character",
        ):
            core.catalog.ResourceType.model_validate(example.model_dump())

        # Restrict JSON schema types
        resource_type_with_unsupported_property = (
            testing.make_plony_resource_type().model_dump()
        )
        resource_type_with_unsupported_property["json_schema"]["properties"][
            "extra"
        ] = {"type": "object"}

        with pytest.raises(
            ValueError,
            match="1 validation error for ResourceType\njson_schema\n  Value error, resource schema contains unsupported type",
        ):
            core.catalog.ResourceType.model_validate(
                resource_type_with_unsupported_property
            )

        # Disallow $ref, $recursiveRef and $dynamicRef in MVP phase
        ref_test = testing.make_plony_resource_type().model_dump()
        ref_test["json_schema"]["properties"]["reftest"] = {"$ref": "http://localhost"}

        with pytest.raises(
            ValueError,
            match="1 validation error for ResourceType\njson_schema\n  Value error, Found ref, recursiveRef or dynamicRef in json_schema which is not permitted in MVP phase",
        ):
            core.catalog.ResourceType.model_validate(ref_test)

        recursive_ref_test = testing.make_plony_resource_type().model_dump()
        recursive_ref_test["json_schema"]["properties"]["reftest"] = {
            "$recursiveRef": "http://localhost"
        }

        with pytest.raises(
            ValueError,
            match="1 validation error for ResourceType\njson_schema\n  Value error, Found ref, recursiveRef or dynamicRef in json_schema which is not permitted in MVP phase",
        ):
            core.catalog.ResourceType.model_validate(recursive_ref_test)

        dynamic_ref_test = testing.make_plony_resource_type().model_dump()
        dynamic_ref_test["json_schema"]["properties"]["reftest"] = {
            "$dynamicRef": "http://localhost"
        }

        with pytest.raises(
            ValueError,
            match="1 validation error for ResourceType\njson_schema\n  Value error, Found ref, recursiveRef or dynamicRef in json_schema which is not permitted in MVP phase",
        ):
            core.catalog.ResourceType.model_validate(dynamic_ref_test)

    # Test plony policy validation
    @staticmethod
    def test_plony_policy_validation() -> None:
        # It does not allow extra attributes, prevent people trying to match policies on unsupported attributes

        # Default case works
        testing.make_plony_policy(
            patch={
                "quota_id": uuid4(),
                "actor_requirements": {
                    "eduPersonEntitlement": ["1", "2"],
                    "eduPersonAssurance": ["3", "4"],
                    "eduPersonScopedAffiliation": ["5", "6"],
                },
            }
        )

        # It does not allow extra attributes
        with pytest.raises(
            ValueError,
            match="1 validation error for PlonyPolicy\nactor_requirements.unexpected\n  Extra inputs are not permitted",
        ):
            testing.make_plony_policy(
                patch={
                    "quota_id": uuid4(),
                    "actor_requirements": {
                        "eduPersonEntitlement": ["1", "2"],
                        "eduPersonAssurance": ["3", "4"],
                        "eduPersonScopedAffiliation": ["5", "6"],
                        "unexpected": ["test"],
                    },
                }
            )

        # Must contain at least one eduPersonEntitlement
        with pytest.raises(
            ValueError,
            match="1 validation error for PlonyPolicy\nactor_requirements.eduPersonEntitlement\n  List should have at least 1 item after validation, not 0",
        ):
            testing.make_plony_policy(
                patch={
                    "quota_id": uuid4(),
                    "actor_requirements": {
                        "eduPersonEntitlement": [],
                        "eduPersonAssurance": ["3", "4"],
                        "eduPersonScopedAffiliation": ["5", "6"],
                    },
                }
            )

    # Test policy validation
    @staticmethod
    def test_policy_validation() -> None:
        # Test default case works and verify PolicyActorRequirements is still the correct type to test for Policy
        # If the type if Policy.actor_requirements changes this is supposed to break as a reminder to test for property validity
        example_policy = core.catalog.Policy(
            id=uuid4(),
            catalog_quota_id=uuid4(),
            actor_requirements=core.catalog.PolicyActorRequirements(
                edu_person_entitlement=["test"],
                edu_person_assurance=[],
                edu_person_scoped_affiliation=[],
            ),
            target_entity="self",
            name="Example Policy",
            json_schema={},
            time_seconds=60,
        )

        assert (
            type(example_policy.actor_requirements)
            is core.catalog.PolicyActorRequirements
        )

        # Check that it's a valid JSON schema
        example = testing.make_plony_policy({"quota_id": uuid4()})

        core.catalog.Policy.model_validate(
            # In PlonyPolicy quota_id does not have the catalog_ prefix
            example.model_dump() | {"catalog_quota_id": uuid4()}
        )

        # It does not work with an invalid schema
        example = testing.make_plony_policy({
            "quota_id": uuid4(),
            "json_schema": {"random": "schema"},
        })

        with pytest.raises(
            ValueError,
            match="1 validation error for Policy\njson_schema\n  Value error, \\('error validating JSON schema', \"Unevaluated properties are not allowed \\('random' was unexpected\\)\"\\)",
        ):
            core.catalog.Policy.model_validate(
                # In PlonyPolicy quota_id does not have the catalog_ prefix
                example.model_dump() | {"catalog_quota_id": uuid4()}
            )

        # Check it only uses restricted types
        policy_with_unsupported_property = testing.make_plony_policy({
            "quota_id": uuid4()
        }).model_dump()

        policy_with_unsupported_property["json_schema"]["properties"]["extra"] = {
            "type": "object"
        }

        with pytest.raises(
            ValueError,
            match="1 validation error for Policy\njson_schema\n  Value error, resource schema contains unsupported type",
        ):
            # Add catalog_quota_id as it is named differently in PlonyPolicy
            core.catalog.Policy.model_validate(
                policy_with_unsupported_property | {"catalog_quota_id": uuid4()}
            )

        # Only allows valid URNs and/or self in target_entity
        valid_policy = testing.make_plony_policy({"quota_id": uuid4()}).model_dump() | {
            "catalog_quota_id": uuid4()
        }
        core.catalog.Policy.model_validate(valid_policy)

        core.catalog.Policy.model_validate(valid_policy | {"target_entity": "self"})
        core.catalog.Policy.model_validate(
            valid_policy | {"target_entity": "urn:geant:example.com:group:test"}
        )
        core.catalog.Policy.model_validate(
            valid_policy | {"target_entity": "urn:geant:example.com:group:test:"}
        )

        with pytest.raises(
            ValueError,
            match="Value error, target_entity has invalid format",
        ):
            core.catalog.Policy.model_validate(
                valid_policy | {"target_entity": "example_not_an_urn"}
            )

        with pytest.raises(
            ValueError,
            match="Value error, target_entity has invalid format",
        ):
            core.catalog.Policy.model_validate(
                valid_policy | {"target_entity": "urn:geant:example.com:group::double"}
            )

        with pytest.raises(
            ValueError,
            match="Value error, target_entity has invalid format",
        ):
            core.catalog.Policy.model_validate(valid_policy | {"target_entity": ":"})

        with pytest.raises(
            ValueError,
            match="Value error, target_entity has invalid format",
        ):
            core.catalog.Policy.model_validate(valid_policy | {"target_entity": ""})

        # Disallow $ref, $recursiveRef and $dynamicRef in MVP phase
        ref_test = testing.make_plony_policy({"quota_id": uuid4()}).model_dump() | {
            "catalog_quota_id": uuid4()
        }
        ref_test["json_schema"]["properties"]["reftest"] = {"$ref": "http://localhost"}

        with pytest.raises(
            ValueError,
            match="1 validation error for Policy\njson_schema\n  Value error, Found ref, recursiveRef or dynamicRef in json_schema which is not permitted in MVP phase",
        ):
            core.catalog.Policy.model_validate(ref_test)

        recursive_ref_test = testing.make_plony_policy({
            "quota_id": uuid4()
        }).model_dump() | {"catalog_quota_id": uuid4()}
        recursive_ref_test["json_schema"]["properties"]["reftest"] = {
            "$recursiveRef": "http://localhost"
        }

        with pytest.raises(
            ValueError,
            match="1 validation error for Policy\njson_schema\n  Value error, Found ref, recursiveRef or dynamicRef in json_schema which is not permitted in MVP phase",
        ):
            core.catalog.Policy.model_validate(recursive_ref_test)

        dynamic_ref_test = testing.make_plony_policy({
            "quota_id": uuid4()
        }).model_dump() | {"catalog_quota_id": uuid4()}
        dynamic_ref_test["json_schema"]["properties"]["reftest"] = {
            "$dynamicRef": "http://localhost"
        }

        with pytest.raises(
            ValueError,
            match="1 validation error for Policy\njson_schema\n  Value error, Found ref, recursiveRef or dynamicRef in json_schema which is not permitted in MVP phase",
        ):
            core.catalog.Policy.model_validate(dynamic_ref_test)

    # Test actor requirements validation
    @staticmethod
    def test_actor_requirements_validation() -> None:
        # Does not work with empty ActorRequirements
        with pytest.raises(
            ValueError,
            match="3 validation errors for PolicyActorRequirements\nedu_person_entitlement\n  Field required",
        ):
            core.catalog.PolicyActorRequirements.model_validate({})

        # Does validate with unexpected atributes in ActorRequirements
        with pytest.raises(
            ValueError,
            match="1 validation error for PolicyActorRequirements\nanother\n  Extra inputs are not permitted",
        ):
            core.catalog.PolicyActorRequirements.model_validate({
                "edu_person_entitlement": ["1", "2", "3"],
                "edu_person_assurance": ["4"],
                "edu_person_scoped_affiliation": ["5"],
                "another": ["attribute"],
            })

    @staticmethod
    def test_quota_limit_validation() -> None:
        # Test default case works and verify QuotaLimit is still the correct type to test for Quota
        # If the type if Quota.quota changes this is supposed to break as a reminder to test for property validity
        example_quota = core.catalog.Quota(
            id=uuid4(),
            name="Test",
            catalog_resource_type_id=uuid4(),
            catalog_service_id=uuid4(),
            quota=[core.catalog.QuotaLimit(property="test", total=3)],
        )
        assert type(example_quota.quota[0]) is core.catalog.QuotaLimit

        # It does not allow empty property names
        with pytest.raises(
            ValueError,
            match="1 validation error for QuotaLimit\nproperty\n  String should have at least 1 character",
        ):
            core.catalog.QuotaLimit.model_validate({"property": "", "total": 10})

        # It does not allow negative limits
        with pytest.raises(
            ValueError,
            match="1 validation error for QuotaLimit\ntotal\n  Input should be greater than or equal to 0",
        ):
            core.catalog.QuotaLimit.model_validate({"property": "test", "total": -3})


@pytest.mark.usefixtures("_context_database")
class TestResourceTypeImport:
    @staticmethod
    @pytest.mark.asyncio
    async def test_resource_type_import(plony_client: MockPlonyClient) -> None:
        # At the beginning there are no resource_types in the database
        resource_types = await core.catalog.get_resource_types()

        assert len(resource_types) == 0

        # It imports all resource_types
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        resource_types = await core.catalog.get_resource_types()

        assert len(resource_types) == 3
        assert resource_types[0].id == MOCK_RESOURCE_TYPES[0].id
        assert resource_types[1].id == MOCK_RESOURCE_TYPES[1].id

        # It properly updates a resource_type's title
        plony_client.set_mock_data(UPDATED_PLONY_DATA)
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        resource_types = await core.catalog.get_resource_types()

        assert len(resource_types) == 3
        assert resource_types[0].json_schema["title"] == "Updated ResourceType"


@pytest.mark.usefixtures("_context_database")
class TestGetResourceType:
    @staticmethod
    @pytest.mark.asyncio
    async def test_get_resource_type(plony_client: MockPlonyClient) -> None:
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        existing_resource_type = await core.catalog.get_resource_type(
            MOCK_RESOURCE_TYPES[0].id
        )

        assert existing_resource_type is not None
        assert existing_resource_type.id == MOCK_RESOURCE_TYPES[0].id

        missing_resource_type = await core.catalog.get_resource_type(uuid4())

        assert missing_resource_type is None


@pytest.mark.usefixtures("_context_database")
class TestGetResourceTypeForService:
    @staticmethod
    @pytest.mark.asyncio
    async def test_get_resource_type_for_service(plony_client: MockPlonyClient) -> None:
        # At first, there are no resource types for this service
        resource_types = await core.catalog.get_resource_types_for_service(
            MOCK_SERVICES[0].uid
        )

        assert len(resource_types) == 0

        # Run import
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        # Now there should be one
        resource_types = await core.catalog.get_resource_types_for_service(
            MOCK_SERVICES[0].uid
        )

        assert len(resource_types) == 1

        # Delete it (normally this would also require deletion of the quota)
        await testing.delete_resource_type(resource_type_id=resource_types[0].id)

        resource_types = await core.catalog.get_resource_types_for_service(
            MOCK_SERVICES[0].uid
        )

        assert len(resource_types) == 0

        # Re-run import, create a new resource type and a new service and check they don't overlap
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        resource_types = await core.catalog.get_resource_types_for_service(
            MOCK_SERVICES[0].uid
        )

        assert len(resource_types) == 1
        assert resource_types[0].id == MOCK_RESOURCE_TYPES[0].id

        new_resource_type = testing.make_resource_type()
        await testing.create_or_update_resource_type(resource_type=new_resource_type)

        new_service = testing.make_service(provider_id=MOCK_INSTITUTIONS[0].uid)
        await testing.create_or_update_service(service=new_service)

        new_quota = testing.make_quota(
            service_id=new_service.id, resource_type_id=new_resource_type.id
        )
        await testing.create_or_update_quota(quota=new_quota)

        old_service_resource_types = await core.catalog.get_resource_types_for_service(
            MOCK_SERVICES[0].uid
        )
        assert len(old_service_resource_types) == 1
        assert old_service_resource_types[0].id == MOCK_RESOURCE_TYPES[0].id

        new_service_resource_types = await core.catalog.get_resource_types_for_service(
            service_id=new_service.id
        )
        assert len(new_service_resource_types) == 1
        assert new_service_resource_types[0].id == new_resource_type.id

        assert new_resource_type.id != MOCK_RESOURCE_TYPES[0].id

        # Check that multiple resource types per service work as expected:
        # add another resource type to new_service and check that they don't overlap
        another_new_resource_type = testing.make_resource_type()
        await testing.create_or_update_resource_type(
            resource_type=another_new_resource_type
        )

        another_new_quota = testing.make_quota(
            service_id=new_service.id, resource_type_id=another_new_resource_type.id
        )
        await testing.create_or_update_quota(quota=another_new_quota)

        old_service_resource_types = await core.catalog.get_resource_types_for_service(
            MOCK_SERVICES[0].uid
        )
        assert len(old_service_resource_types) == 1
        assert old_service_resource_types[0].id == MOCK_RESOURCE_TYPES[0].id

        new_service_resource_types = await core.catalog.get_resource_types_for_service(
            service_id=new_service.id
        )
        assert len(new_service_resource_types) == 2

        new_service_resource_types_ids = {
            resource_type.id for resource_type in new_service_resource_types
        }
        assert len(new_service_resource_types_ids) == 2

        assert MOCK_RESOURCE_TYPES[0].id not in new_service_resource_types_ids
        assert new_resource_type.id in new_service_resource_types_ids
        assert another_new_resource_type.id in new_service_resource_types_ids

        # Delete resource types and the function should return 0 resource types
        # (normally this would require also deleting the quota)
        await testing.delete_resource_type(resource_type_id=new_resource_type.id)
        await testing.delete_resource_type(
            resource_type_id=another_new_resource_type.id
        )

        resource_types_after_deletion = (
            await core.catalog.get_resource_types_for_service(service_id=new_service.id)
        )
        assert len(resource_types_after_deletion) == 0


@pytest.mark.usefixtures("_context_database")
class TestQuotaImport:
    @staticmethod
    @pytest.mark.asyncio
    async def test_quota_import(plony_client: MockPlonyClient) -> None:
        # At the beginning there are no quotas in the database
        quotas = await core.catalog.get_quotas()

        assert len(quotas) == 0

        # It imports all quotas
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        quotas = await core.catalog.get_quotas()

        assert len(quotas) == 3
        assert quotas[0].id == MOCK_QUOTAS[0].id
        assert quotas[1].id == MOCK_QUOTAS[1].id

        # It properly updates a quota's name
        plony_client.set_mock_data(UPDATED_PLONY_DATA)
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        quotas = await core.catalog.get_quotas()

        # It does not delete quotas when they are deleted from Plony to prevent resources without quotas
        assert len(quotas) == 4

        # It updates a quota's name
        updated_quota = await core.catalog.get_quota(MOCK_QUOTAS[0].id)

        assert updated_quota is not None
        assert updated_quota.name == "Updated Quota Name"

    @staticmethod
    @pytest.mark.asyncio
    async def test_quota_import_service_exists_validation(
        plony_client: MockPlonyClient,
    ) -> None:
        # Blocks import of quota where the service does not exist
        plony_client.data.quotas[0].service_id = uuid4()

        with pytest.raises(
            core.catalog.QuotaValidationError,
            match="Service referenced by quota does not exist",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_quota_import_resource_type_exists_validation(
        plony_client: MockPlonyClient,
    ) -> None:
        # Blocks import of quota where the resource type does not exist
        plony_client.data.quotas[0].resource_type_id = uuid4()

        with pytest.raises(
            core.catalog.QuotaValidationError,
            match="ResourceType referenced by quota does not exist",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_quota_import_property_exists_validation(
        plony_client: MockPlonyClient,
    ) -> None:
        # Blocks import of quota where quote references property which does not exist on resource type
        plony_client.data.quotas[0].quota[0].property = "does-not-exist"

        with pytest.raises(
            core.catalog.QuotaValidationError,
            match="Property referenced by quota does not exist on resource type",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_quota_import_referenced_property_is_integer_validation(
        plony_client: MockPlonyClient,
    ) -> None:
        # Blocks import of quota where referenced property is not an integer on resource type
        plony_client.data.quotas[0].quota[0].property = "flavor"

        with pytest.raises(
            core.catalog.QuotaValidationError,
            match="Property referenced by quota is not of type integer",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_quota_import_minimum_constraint_set(
        plony_client: MockPlonyClient,
    ) -> None:
        # Test a minimum value must be set
        del plony_client.data.resource_types[0].json_schema["properties"]["cpu"][
            "minimum"
        ]

        with pytest.raises(
            core.catalog.QuotaValidationError,
            match="Property referenced by quota must have either minimum >= 0 or exclusiveMinimum > 0 set",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_quota_import_minimum_gte_0(
        plony_client: MockPlonyClient,
    ) -> None:
        # Test minimum must be >= 0
        plony_client.data.resource_types[0].json_schema["properties"]["cpu"][
            "minimum"
        ] = -1

        with pytest.raises(
            core.catalog.QuotaValidationError,
            match="Property referenced by quota must have minimum >= 0 if exclusiveMinimum is set",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_quota_import_exclusive_minimum_gt_0(
        plony_client: MockPlonyClient,
    ) -> None:
        # Test exclusiveMinimum must be > 0
        del plony_client.data.resource_types[0].json_schema["properties"]["cpu"][
            "minimum"
        ]
        plony_client.data.resource_types[0].json_schema["properties"]["cpu"][
            "exclusiveMinimum"
        ] = 0

        with pytest.raises(
            core.catalog.QuotaValidationError,
            match="Property referenced by quota must have exclusiveMinimum > 0 if exclusiveMinimum is set",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )


@pytest.mark.usefixtures("_context_database")
class TestPolicyImport:
    @staticmethod
    @pytest.mark.asyncio
    async def test_policy_import(plony_client: MockPlonyClient) -> None:
        # At the beginning there are no policies in the database
        policies = await core.catalog.get_policies()

        assert len(policies) == 0

        # It imports all policies
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        policies = await core.catalog.get_policies()

        assert len(policies) == 3
        assert policies[0].id == MOCK_POLICIES[0].id
        assert policies[1].id == MOCK_POLICIES[1].id

        # It properly updates a policy's title
        plony_client.set_mock_data(UPDATED_PLONY_DATA)
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        policies = await core.catalog.get_policies()

        assert len(policies) == 3
        assert policies[0].json_schema["title"] == "Updated Policy"

    @staticmethod
    @pytest.mark.asyncio
    async def test_policy_import_quota_exists_validation(
        plony_client: MockPlonyClient,
    ) -> None:
        # Does not import policy where quota does not exist
        plony_client.data.policies[0].quota_id = uuid4()

        with pytest.raises(
            core.catalog.PolicyValidationError,
            match="Quota referenced by policy does not exist",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_policy_import_property_exists_validation(
        plony_client: MockPlonyClient,
    ) -> None:
        # Does not import policy where property does not exist on resource type
        plony_client.data.policies[0].json_schema["properties"] = (
            plony_client.data.policies[0].json_schema["properties"]
            | {"random_value": {"type": "string"}}
        )

        with pytest.raises(
            core.catalog.PolicyValidationError,
            match="Property referenced by policy does not exist on resource type",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

    @staticmethod
    @pytest.mark.asyncio
    async def test_policy_import_property_type_validation(
        plony_client: MockPlonyClient,
    ) -> None:
        # Does not import policy where property has different type on resource type
        plony_client.data.policies[0].json_schema["properties"]["name"]["type"] = (
            "integer"
        )

        with pytest.raises(
            core.catalog.PolicyValidationError,
            match="Types of properties do not match between policy and resource type",
        ):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )


@pytest.mark.usefixtures("_context_database")
class TestGetPolicy:
    @staticmethod
    @pytest.mark.asyncio
    async def test_get_policy(plony_client: MockPlonyClient) -> None:
        # It imports all policies
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        existing_policy = await core.catalog.get_policy(MOCK_POLICIES[0].id)

        assert existing_policy is not None
        assert existing_policy.id == MOCK_POLICIES[0].id

        missing_policy = await core.catalog.get_policy(uuid4())

        assert missing_policy is None


@pytest.mark.usefixtures("_context_database")
class TestServiceImport:
    @staticmethod
    @pytest.mark.asyncio
    async def test_service_import(plony_client: MockPlonyClient) -> None:
        # At the beginning there are no services in the database
        services = await core.catalog.get_services()

        assert len(services) == 0

        # It imports all services
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        services = await core.catalog.get_services()

        assert len(services) == 2
        assert services[0].id == MOCK_SERVICES[0].uid
        assert services[1].id == MOCK_SERVICES[1].uid

        # It properly updates a service's title
        plony_client.set_mock_data(UPDATED_PLONY_DATA)
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        services = await core.catalog.get_services()

        assert len(services) == 2
        assert services[0].title == "Updated Service"
        assert services[1].id != MOCK_SERVICES[1].uid
        assert services[1].id == UPDATED_PLONY_DATA.services[1].uid


@pytest.mark.usefixtures("_context_database")
class TestBrokenServices:
    @staticmethod
    @pytest.mark.asyncio
    async def test_exceptions() -> None:
        # Import a service which is marked as `service_online` but does not
        # have `other_accept_publish_to_portal` set to `yes`
        plony_client = MockPlonyClient()
        institution = testing.make_plony_institution(str(uuid4()))

        service = testing.make_plony_service(
            institution,
            patch={
                "review_state": "service_online",
                "other_accept_publish_to_portal": {"title": "No", "token": "no"},
            },
        )

        plony_client.set_mock_data(
            ClientMockDataset(
                keywords=[],
                institutions=[institution],
                services=[service],
                resource_types=[],
                quotas=[],
                policies=[],
                images=copy.deepcopy(MOCK_IMAGES),
            )
        )

        with pytest.raises(core.catalog.ServiceValidationError):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

        # Test with an online service with empty general_link_service_usage
        plony_client = MockPlonyClient()
        institution = testing.make_plony_institution(str(uuid4()))

        service = testing.make_plony_service(
            institution,
            patch={
                "review_state": "service_online",
                "general_link_service_usage": None,
            },
        )

        plony_client.set_mock_data(
            ClientMockDataset(
                keywords=[],
                institutions=[institution],
                services=[service],
                resource_types=[],
                quotas=[],
                policies=[],
                images=copy.deepcopy(MOCK_IMAGES),
            )
        )

        with pytest.raises(core.catalog.ServiceValidationError):
            await core.catalog.run_plony_import(
                plony_client=typing.cast(core.plony.Client, plony_client),
                include_in_review_services=False,
            )

        # Test with an online service without primary category
        # Check category property gets mapped to default value
        plony_client = MockPlonyClient()
        institution = testing.make_plony_institution(str(uuid4()))

        service = testing.make_plony_service(
            institution,
            patch={
                "service_category": None,
            },
        )

        plony_client.set_mock_data(
            ClientMockDataset(
                keywords=[],
                institutions=[institution],
                services=[service],
                resource_types=[],
                quotas=[],
                policies=[],
                images=copy.deepcopy(MOCK_IMAGES),
            )
        )

        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        services = await core.catalog.get_services()

        assert len(services) == 1

        # Test value gets mapped properly
        assert services[0].category == "No Category"


@pytest.mark.usefixtures("_context_database")
class TestIntegrationImport:
    @staticmethod
    @pytest.mark.asyncio
    async def test_service_import_integration(plony_client: MockPlonyClient) -> None:
        # Run the regular import, then run it again with integration settings
        # The integration import should add more services

        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        services = await core.catalog.get_services()
        assert len(services) == 2

        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=True,
        )

        services = await core.catalog.get_services()
        assert len(services) == 4


@pytest.mark.usefixtures("_context_database")
class TestAccessMethods:
    @staticmethod
    @pytest.mark.asyncio
    async def test_service_import_access(plony_client: MockPlonyClient) -> None:
        # Run import, then check if the keywords are linked correctly and accessible with
        # get_all_service_keyword_ids
        await core.catalog.run_plony_import(
            plony_client=typing.cast(core.plony.Client, plony_client),
            include_in_review_services=False,
        )

        # This is a bit tricky as plony stores the keyowrd links usind the @id and not the UID
        # Therefore, we need to resolve the @id to UID first
        all_keywords = await core.catalog.get_keywords()
        keyword_map = {keyword.plony_at_id: keyword for keyword in all_keywords}

        service = plony_client.data.services[0]

        keyword_ids = [
            keyword_map[keyword.at_id].id for keyword in service.service_keywords
        ]

        # Assert there actually is a keyword in case someone fiddles with the mock data
        assert len(keyword_ids) > 0

        mapping = await core.catalog.get_all_service_keyword_ids()

        assert set(mapping[service.uid]) == set(keyword_ids)

        #
        # Get service from DB
        #

        # Try to find a service
        existing_service = await core.catalog.get_service(MOCK_SERVICES[0].uid)
        assert existing_service is not None
        assert existing_service.id == MOCK_SERVICES[0].uid

        # Find a non-existent service
        missing_service = await core.catalog.get_service(uuid4())
        assert missing_service is None

        #
        # Get provider from DB
        #

        # Try to find a provider
        existing_provider = await core.catalog.get_provider(MOCK_INSTITUTIONS[0].uid)
        assert existing_provider is not None
        assert existing_provider.id == MOCK_INSTITUTIONS[0].uid

        # Find a non-existent service
        missing_provider = await core.catalog.get_provider(uuid4())
        assert missing_provider is None

        #
        # Get links to services
        #

        services = await core.catalog.get_services()
        service_links = await core.catalog.get_service_links()
        assert len(service_links) == len(services)
        assert services[0].general_link_service_usage is not None
        assert (
            service_links[0].service_link
            == services[0].general_link_service_usage.unicode_string()
        )


@pytest_asyncio.fixture
async def policy_access_fixture() -> tuple[
    core.catalog.ResourceType,
    core.catalog.Service,
    core.catalog.Quota,
]:
    provider = testing.make_provider()

    await testing.create_or_update_provider(provider=provider)

    service = testing.make_service(provider_id=provider.id)
    await testing.create_or_update_service(service=service)

    resource_type = testing.make_resource_type()
    resource_type.json_schema = {
        "title": "TestResourceType",
        "description": "Specification of a Test Resource",
        "type": "object",
        "properties": {
            "testProperty": {
                "description": "Test Property",
                "type": "integer",
                "default": 1,
            }
        },
        "required": [
            "testProperty",
        ],
    }

    await testing.create_or_update_resource_type(resource_type=resource_type)

    quota = testing.make_quota(service_id=service.id, resource_type_id=resource_type.id)
    quota.quota = [
        core.catalog.QuotaLimit(property="testProperty", total=1_000),
    ]
    await testing.create_or_update_quota(quota=quota)

    return (resource_type, service, quota)


@pytest.mark.usefixtures("_context_database")
class TestPolicyAccess:
    quota_id = None

    @pytest.mark.parametrize(
        (
            "policy_entitlement",
            "policy_assurance",
            "policy_scoped_affiliation",
            "actor_entitlement",
            "actor_assurance",
            "actor_scoped_affiliation",
            "should_be_available_to_actor",
        ),
        [
            # Wildcard
            (
                [],
                [],
                [],
                [],
                [],
                [],
                True,
            ),
            # Exact match
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                True,
            ),
            # Actor has extra entitlements
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                [
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:a",
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:test",
                ],
                ["assurance"],
                ["staff"],
                True,
            ),
            # Actor has extra assurances
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["another_assurance", "assurance"],
                ["staff"],
                True,
            ),
            # Actor has extra scope affilications
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["another_assurance", "assurance"],
                ["staff", "member"],
                True,
            ),
            # Policy requires multiple entitlements
            (
                [
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:a",
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:test",
                ],
                ["assurance"],
                ["staff"],
                [
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:a",
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:test",
                ],
                ["another_assurance", "assurance"],
                ["staff"],
                True,
            ),
            (
                [
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:a",
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:test",
                ],
                ["assurance"],
                ["staff"],
                [
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:a",
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:b",
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:test",
                ],
                ["another_assurance", "assurance"],
                ["staff"],
                True,
            ),
            # Policy requires multiple assurances
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["another_assurance", "assurance"],
                ["staff"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["another_assurance", "assurance"],
                ["staff"],
                True,
            ),
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["another_assurance", "assurance"],
                ["staff"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["unrelated_assurance", "another_assurance", "assurance"],
                ["staff"],
                True,
            ),
            # Policy requires multiple scoped assurances
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["another_assurance", "assurance"],
                ["staff", "member"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["another_assurance", "assurance"],
                ["staff", "member"],
                True,
            ),
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["another_assurance", "assurance"],
                ["staff", "member"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["unrelated_assurance", "another_assurance", "assurance"],
                ["staff", "member", "employee"],
                True,
            ),
            # No specific assurance required by policy
            (
                [],
                [],
                [],
                [],
                ["assurance"],
                [],
                True,
            ),
            # No specific scoped affiliation required by policy
            (
                [],
                [],
                [],
                [],
                [],
                ["staff"],
                True,
            ),
            # Specific assurance may not be required but the field must be present
            (
                [],
                [],
                [],
                [],
                None,
                [],
                False,
            ),
            # Specific scoped affiliation may not be required but the field must be present
            (
                [],
                [],
                [],
                [],
                [],
                None,
                False,
            ),
            # No specific entitlement required by policy
            (
                [],
                [],
                [],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                [],
                True,
            ),
            # Specific entitlement may not be required but the field must be present
            (
                [],
                [],
                [],
                None,
                [],
                [],
                False,
            ),
            # Actor must have entitlement set to None
            (
                None,
                [],
                [],
                None,
                [],
                [],
                True,
            ),
            (
                None,
                [],
                [],
                [],
                [],
                [],
                False,
            ),
            (
                None,
                [],
                [],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                [],
                [],
                False,
            ),
            # Actor must have assurance set to None
            (
                [],
                None,
                [],
                [],
                None,
                [],
                True,
            ),
            (
                [],
                None,
                [],
                [],
                [],
                [],
                False,
            ),
            (
                [],
                None,
                [],
                [],
                ["assurance"],
                [],
                False,
            ),
            # Actor must have scoped affiliation set to None
            (
                [],
                [],
                None,
                [],
                [],
                None,
                True,
            ),
            (
                [],
                [],
                None,
                [],
                [],
                [],
                False,
            ),
            (
                [],
                [],
                None,
                [],
                [],
                ["staff"],
                False,
            ),
            # Actor is missing entitlement, assurances and scoped affiliation
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                [],
                [],
                [],
                False,
            ),
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                None,
                None,
                None,
                False,
            ),
            # Actor is missing required entitlement
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                [
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:a",
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:b",
                ],
                ["assurance"],
                ["staff"],
                False,
            ),
            (
                [
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:test",
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:test_extra",
                ],
                ["assurance"],
                ["staff"],
                [
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:test",
                    "urn:geant:helmholtz.de:group:helmholtz.cloud:a",
                ],
                ["assurance"],
                ["staff"],
                False,
            ),
            # Actor is missing required assurance
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["unrelated_assurance"],
                ["staff"],
                False,
            ),
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance", "another_assurance"],
                ["staff"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["unrelated_assurance", "assurance"],
                ["staff"],
                False,
            ),
            # Actor is missing required scoped affiliation
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["member"],
                False,
            ),
            (
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff", "member"],
                ["urn:geant:helmholtz.de:group:helmholtz.cloud:test"],
                ["assurance"],
                ["staff", "employee"],
                False,
            ),
        ],
    )
    @pytest.mark.asyncio
    async def test_get_policies_available_to_actor(  # noqa: PLR0913 we need that many parameters
        self: "TestPolicyAccess",
        policy_access_fixture: tuple[
            core.catalog.ResourceType,
            core.catalog.Service,
            core.catalog.Quota,
        ],
        policy_entitlement: list[str],
        policy_assurance: list[str] | None,
        policy_scoped_affiliation: list[str] | None,
        actor_entitlement: list[str] | None,
        actor_assurance: list[str] | None,
        actor_scoped_affiliation: list[str] | None,
        should_be_available_to_actor: bool,  # noqa: FBT001 that's how parametrization works, kwargs would make this less readable
    ) -> None:
        (resource_type, service, quota) = policy_access_fixture
        policy = testing.make_policy(quota_id=quota.id)
        policy.actor_requirements = core.catalog.PolicyActorRequirements(
            edu_person_entitlement=policy_entitlement,
            edu_person_assurance=policy_assurance,
            edu_person_scoped_affiliation=policy_scoped_affiliation,
        )
        await testing.create_or_update_policy(policy=policy)

        test_identity = core.identity.testing.make_actor_identity()
        test_identity.edu_person_entitlement = actor_entitlement
        test_identity.edu_person_assurance = actor_assurance
        test_identity.edu_person_scoped_affiliation = actor_scoped_affiliation

        policies_available_to_actor = (
            await core.catalog.get_policies_available_to_actor(
                actor_identity=test_identity,
                service_id=service.id,
                resource_type_id=resource_type.id,
            )
        )

        if should_be_available_to_actor:
            assert policy.id in [
                available_policy.id for available_policy in policies_available_to_actor
            ]
        else:
            assert len(policies_available_to_actor) == 0


@pytest.mark.usefixtures("_context_database")
@pytest.mark.asyncio
class TestKPIImport:
    @staticmethod
    @pytest.mark.parametrize(
        ("first_import", "second_import", "expected_db_state"),
        [
            # test add
            (
                {
                    UUID("6d54617d-a006-4618-86d5-281ec853c068"): core.kpi.KPIs(
                        user_count=1
                    ),
                    UUID("36d4d7be-293c-40b9-9669-c03bc16d6c78"): core.kpi.KPIs(
                        user_count=2
                    ),
                },
                None,
                [
                    core.catalog.ServiceKPIs(
                        catalog_service_id="6d54617d-a006-4618-86d5-281ec853c068",
                        user_count=1,
                    ),
                    core.catalog.ServiceKPIs(
                        catalog_service_id="36d4d7be-293c-40b9-9669-c03bc16d6c78",
                        user_count=2,
                    ),
                ],
            ),
            # test remove
            (
                {
                    UUID("6d54617d-a006-4618-86d5-281ec853c068"): core.kpi.KPIs(
                        user_count=1
                    ),
                    UUID("36d4d7be-293c-40b9-9669-c03bc16d6c78"): core.kpi.KPIs(
                        user_count=2
                    ),
                },
                {
                    UUID("6d54617d-a006-4618-86d5-281ec853c068"): core.kpi.KPIs(
                        user_count=1
                    ),
                    UUID("b874cc29-58b1-49f5-86e2-7cb9a4babe19"): core.kpi.KPIs(
                        user_count=3
                    ),
                },
                [
                    core.catalog.ServiceKPIs(
                        catalog_service_id="6d54617d-a006-4618-86d5-281ec853c068",
                        user_count=1,
                    ),
                    core.catalog.ServiceKPIs(
                        catalog_service_id="b874cc29-58b1-49f5-86e2-7cb9a4babe19",
                        user_count=3,
                    ),
                ],
            ),
            # test update
            (
                {
                    UUID("6d54617d-a006-4618-86d5-281ec853c068"): core.kpi.KPIs(
                        user_count=1
                    ),
                    UUID("36d4d7be-293c-40b9-9669-c03bc16d6c78"): core.kpi.KPIs(
                        user_count=2
                    ),
                },
                {
                    UUID("6d54617d-a006-4618-86d5-281ec853c068"): core.kpi.KPIs(
                        user_count=3
                    ),
                    UUID("36d4d7be-293c-40b9-9669-c03bc16d6c78"): core.kpi.KPIs(
                        user_count=4
                    ),
                },
                [
                    core.catalog.ServiceKPIs(
                        catalog_service_id="6d54617d-a006-4618-86d5-281ec853c068",
                        user_count=3,
                    ),
                    core.catalog.ServiceKPIs(
                        catalog_service_id="36d4d7be-293c-40b9-9669-c03bc16d6c78",
                        user_count=4,
                    ),
                ],
            ),
            # test []
            (
                {},
                None,
                [],
            ),
            (
                {
                    UUID("6d54617d-a006-4618-86d5-281ec853c068"): core.kpi.KPIs(
                        user_count=1
                    ),
                    UUID("36d4d7be-293c-40b9-9669-c03bc16d6c78"): core.kpi.KPIs(
                        user_count=2
                    ),
                },
                {},
                [],
            ),
        ],
    )
    async def test_working_import(
        first_import: dict[UUID, core.kpi.KPIs],
        second_import: dict[UUID, core.kpi.KPIs] | None,
        expected_db_state: list[core.catalog.ServiceKPIs],
    ) -> None:
        # Imports
        await core.catalog.import_kpis(
            kpi_map=core.kpi.KPIMapAdapter.validate_python(first_import)
        )

        if second_import is not None:
            await core.catalog.import_kpis(
                kpi_map=core.kpi.KPIMapAdapter.validate_python(second_import)
            )

        # Check
        assert (await core.catalog.get_service_kpis()) == expected_db_state
