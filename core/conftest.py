import os
import typing
from pathlib import Path

import alembic.command
import alembic.config
import pytest_asyncio
from _pytest.fixtures import FixtureRequest

import core.app
import core.db


@pytest_asyncio.fixture
async def _context_database(
    request: FixtureRequest,
) -> typing.AsyncGenerator[None]:
    """This function is run once per test class (see below).
    It loads the database, yields to the tests and then cleans up."""

    # Boot the core app
    core.app.boot(postgres_url=core.app.require_envvar("PORTAL_CORE_TEST_DATABASE_URL"))

    alembic_ini_path = Path(__file__).parent / "../alembic.ini"

    # Scope migrations to requesting context
    alembic_migrations_path = Path(request.path).parent / "migrations"

    alembic_config = alembic.config.Config(alembic_ini_path)

    os.environ["PORTAL_CORE_DATABASE_URL"] = core.app.require_envvar(
        "PORTAL_CORE_TEST_DATABASE_URL"
    )

    load_extra_schemas = request.node.get_closest_marker("load_extra_schemas")

    if load_extra_schemas is not None:
        extra_schema_paths = ":".join([
            str(Path(__file__).parent / f"{context_name}/migrations")
            for context_name in load_extra_schemas.args
        ])

        alembic_config.set_main_option(
            "version_locations",
            f"{alembic_migrations_path}:{extra_schema_paths}",
        )
        alembic.command.upgrade(alembic_config, "heads")
    else:
        alembic_config.set_main_option(
            "version_locations", str(alembic_migrations_path)
        )
        alembic.command.upgrade(alembic_config, "head")

    # Run tests below
    yield

    # Close the connection pool cleanly
    await (await core.db.DB.get_pool()).close(timeout=0)

    # Clean up (down-migrate catalog)
    alembic.command.downgrade(alembic_config, "base")
