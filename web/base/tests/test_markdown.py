# This file is in a subdirectory because otherwise Django is going to try to import this file if it is in templatetags.

import pytest
from django.utils.safestring import SafeString

from web.base.templatetags.markdown import render_markdown


def test_works_with_none() -> None:
    result = render_markdown(None)

    assert isinstance(result, SafeString)
    assert result == ""


def test_works_with_empty_string() -> None:
    result = render_markdown("")

    assert isinstance(result, SafeString)
    assert result == ""


def test_images_are_blocked() -> None:
    result = render_markdown("![Alt Image](https://localhost)")

    assert isinstance(result, SafeString)
    assert result == '<p>!<a href="https://localhost">Alt Image</a></p>\n'


def test_data_images_are_blocked() -> None:
    result = render_markdown("![Alt Image](data:image/png;test)")

    assert isinstance(result, SafeString)
    assert result == "<p>![Alt Image](data:image/png;test)</p>\n"


@pytest.mark.parametrize(
    ("input_text", "expected_output"),
    [
        ("[test](ssh://localhost)", "<p>[test](ssh://localhost)</p>\n"),
        (
            "[test](javascript:alert('hello'))",
            "<p>[test](javascript:alert('hello'))</p>\n",
        ),
        ("[test](vbscript:test)", "<p>[test](vbscript:test)</p>\n"),
        ("[test](file:///etc/test)", "<p>[test](file:///etc/test)</p>\n"),
        ("[test](file://etc/test)", "<p>[test](file://etc/test)</p>\n"),
        ("[test](data:1234)", "<p>[test](data:1234)</p>\n"),
        ("<ssh://localhost>", "<p>&lt;ssh://localhost&gt;</p>\n"),
        ("<javascript:alert('hello')>", "<p>&lt;javascript:alert('hello')&gt;</p>\n"),
        ("<vbscript:test>", "<p>&lt;vbscript:test&gt;</p>\n"),
        ("<file:///etc/test>", "<p>&lt;file:///etc/test&gt;</p>\n"),
        ("<file://etc/test>", "<p>&lt;file://etc/test&gt;</p>\n"),
        ("<data:1234>", "<p>&lt;data:1234&gt;</p>\n"),
    ],
)
def test_dangerous_links_are_blocked(input_text: str, expected_output: str) -> None:
    # This should not result in HTML but leave the input_text as is
    result = render_markdown(input_text)

    assert isinstance(result, SafeString)
    assert result == expected_output


def test_html_is_blocked() -> None:
    result = render_markdown(
        '<script>alert("hello");</script><img src="https://localhost" />'
    )

    assert isinstance(result, SafeString)
    assert (
        result
        == "<p>&lt;script&gt;alert(&quot;hello&quot;);&lt;/script&gt;&lt;img src=&quot;https://localhost&quot; /&gt;</p>\n"
    )


def test_renders_basic_markdown_as_expected() -> None:
    markdown = """
# Headline
## Another headline

*italic* **bold**
_italic_ __bold__

[Link](https://localhost)
[Link](test@localhost)
[Link](mailto:user@localhost?subject=Important&body=Hello)
<https://localhost>
<test@localhost>
<mailto:user@localhost?subject=Important&body=Hello>
![Alt Image](https://localhost)
https://localhost
test@localhost
javascript:alert("hello")

- List A
- List B

* List C
* List D

1. List 1
2. List 2

1) List 3
2) List 4

Direct list:
- No
- Blank
- Line

---

***

`code`

> quote

```
pre
```

|Header A| Header B|
|-|-|
|One|Two|
|Three|Four|
"""

    result = render_markdown(markdown)

    expected_result = """<h1>Headline</h1>
<h2>Another headline</h2>
<p><em>italic</em> <strong>bold</strong>
<em>italic</em> <strong>bold</strong></p>
<p><a href="https://localhost">Link</a>
[Link](test@localhost)
<a href="mailto:user@localhost?subject=Important&amp;body=Hello">Link</a>
<a href="https://localhost">https://localhost</a>
<a href="mailto:test@localhost">test@localhost</a>
<a href="mailto:user@localhost?subject=Important&amp;body=Hello">mailto:user@localhost?subject=Important&amp;body=Hello</a>
!<a href="https://localhost">Alt Image</a>
https://localhost
test@localhost
javascript:alert(&quot;hello&quot;)</p>
<ul>
<li>List A</li>
<li>List B</li>
</ul>
<ul>
<li>List C</li>
<li>List D</li>
</ul>
<ol>
<li>List 1</li>
<li>List 2</li>
</ol>
<ol>
<li>List 3</li>
<li>List 4</li>
</ol>
<p>Direct list:</p>
<ul>
<li>No</li>
<li>Blank</li>
<li>Line</li>
</ul>
<hr>
<hr>
<p><code>code</code></p>
<blockquote>
<p>quote</p>
</blockquote>
<pre><code>pre
</code></pre>
<table>
<thead>
<tr>
<th>Header A</th>
<th>Header B</th>
</tr>
</thead>
<tbody>
<tr>
<td>One</td>
<td>Two</td>
</tr>
<tr>
<td>Three</td>
<td>Four</td>
</tr>
</tbody>
</table>
"""

    assert isinstance(result, SafeString)
    assert result == expected_result
