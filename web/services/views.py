import json
from typing import Any
from uuid import UUID

from django.core.cache import caches
from django.core.serializers.json import DjangoJSONEncoder
from django.http import Http404, HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render

import core.catalog
import core.monitoring


async def services(request: HttpRequest) -> HttpResponse:
    """Service catalog."""

    services = await core.catalog.get_services()
    keywords = await core.catalog.get_keywords()
    service_keywords = await core.catalog.get_all_service_keyword_ids()
    providers = await core.catalog.get_providers()

    unavailable_services = {
        availability.catalog_service_id
        for availability in await core.monitoring.get_unavailable_services()
    }

    category_list = list({service.category for service in services})
    software_list = list({service.general_software_name for service in services})

    keyword_titles = {keyword.id: keyword.title for keyword in keywords}

    provider_map = {
        str(provider.id): {
            "id": provider.id,
            "title": provider.title,
            "abbreviation": provider.abbreviation,
        }
        for provider in providers
    }

    user_counts = {
        service_kpis.catalog_service_id: service_kpis.user_count
        for service_kpis in await core.catalog.get_service_kpis()
    }

    # Attributes to output to JSON in case the structs contain something confidential in the future
    public_service_data: dict[str, Any] = {
        "services": {
            str(service.id): {
                "profile": {
                    "id": service.id,
                    "title": service.title,
                    "category": service.category,
                    "isAvailable": service.id not in unavailable_services,
                    "generalShortTextCloudPortal": service.general_short_text_cloud_portal,
                    "generalSoftwareName": service.general_software_name,
                    "catalogProviderID": service.catalog_provider_id,
                    "generalLinkServiceUsage": service.general_link_service_usage.unicode_string()
                    if service.general_link_service_usage is not None
                    else None,
                    "userCount": user_counts.get(service.id, 0),
                    # Simple bulk text fields for frontend text search
                    "searchProvider": f"{provider_map[str(service.catalog_provider_id)]['title']} {provider_map[str(service.catalog_provider_id)]['abbreviation']}",
                    "searchKeywords": " ".join([
                        keyword_titles[keyword_id]
                        for keyword_id in service_keywords.get(service.id, [])
                    ]),
                    "searchText": "\n".join([
                        service.description,
                        service.general_long_description,
                        service.general_software_name,
                        service.backup_strategy,
                        service.communication_contact_user_support,
                        service.service_lvl_description or "",
                        service.user_limitations,
                        service.user_access_description,
                        service.data_storage_location,
                    ]),
                },
            }
            for service in services
        },
        "categories": category_list,
        "software": software_list,
        "providers": provider_map,
    }

    # Filter out providers without services
    provider_ids_with_services: set[str] = {
        str(service["profile"]["catalogProviderID"])
        for service in public_service_data["services"].values()
    }

    for unused_id in (
        set(public_service_data["providers"].keys()) - provider_ids_with_services
    ):
        del public_service_data["providers"][unused_id]

    return render(
        request,
        "services/services.html",
        {
            "service_json": json.dumps(public_service_data, cls=DjangoJSONEncoder),
        },
    )


async def service_profile(request: HttpRequest, service_id: UUID) -> HttpResponse:
    """Service profile partial."""

    service = await core.catalog.get_service(service_id)
    if service is None:
        error_message = "Service not found."
        raise Http404(error_message)

    provider = await core.catalog.get_provider(service.catalog_provider_id)

    resource_types = await core.catalog.get_resource_types_for_service(service.id)

    return render(
        request,
        "services/_service_profile.html",
        {"service": service, "provider": provider, "resource_types": resource_types},
    )


async def service_logo(_request: HttpRequest, service_id: UUID) -> HttpResponse:
    """Show a service's logo"""

    # Retrieve logo from in-memory cache as getting it from DB can be rather slow
    cache = caches["service-logos"]

    cached_logo = await cache.aget(str(service_id))
    if cached_logo is None:
        service = await core.catalog.get_service(service_id)
        if service is None:
            error_message = "Service not found."
            raise Http404(error_message)

        cached_logo = {
            "data": service.general_service_logo_data,
            "content_type": service.general_service_logo_content_type,
        }

        await cache.aset(str(service.id), cached_logo, 3600)

    if cached_logo["data"] is None:
        return HttpResponseRedirect("/static/images/fallback_logo.png")

    return HttpResponse(
        cached_logo["data"],
        content_type=cached_logo["content_type"],
        # Add private for good measure
        headers={"cache-control": "max-age=3600, private"},
    )


async def provider_logo(_request: HttpRequest, provider_id: UUID) -> HttpResponse:
    """Show a provider's logo"""

    # Retrieve logo from in-memory cache as getting it from DB can be rather slow
    cache = caches["provider-logos"]

    cached_logo = await cache.aget(str(provider_id))
    if cached_logo is None:
        provider = await core.catalog.get_provider(provider_id)
        if provider is None:
            error_message = "Provider not found."
            raise Http404(error_message)

        cached_logo = {
            "data": provider.logo_data,
            "content_type": provider.logo_content_type,
        }

        await cache.aset(str(provider.id), cached_logo, 3600)

    if cached_logo["data"] is None:
        error_message = "Provider has no logo."
        raise Http404(error_message)

    return HttpResponse(
        cached_logo["data"],
        content_type=cached_logo["content_type"],
        # Add private for good measure
        headers={"cache-control": "max-age=3600, private"},
    )
