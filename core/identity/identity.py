import re

import psycopg.rows
from pydantic import UUID4, BaseModel, Field

import core.db

GROUP_URN_PATTERN = (
    r"^urn:geant:(([a-zA-Z0-9\._-]+:)+)group:(([a-zA-Z0-9\._-]+:)*)([a-zA-Z0-9\._-]+)$"
)


class ActorIdentity(BaseModel):
    """
    An ActorIdentity is a data structure representing the authorization context of an entity when performing actions.
    It is used for providing data to authorization checks such as matching resource request Policies.

    For now it only represents users, in the future this may also include e.g. services and functional accounts.
    """

    vo_person_id: UUID4 | None = Field(..., serialization_alias="voPersonID")

    # See: https://wiki.refeds.org/display/STAN/eduPerson
    edu_person_entitlement: list[str] | None = Field(
        ..., serialization_alias="eduPersonEntitlement"
    )
    edu_person_assurance: list[str] | None = Field(
        ..., serialization_alias="eduPersonAssurance"
    )
    edu_person_scoped_affiliation: list[str] | None = Field(
        ..., serialization_alias="eduPersonScopedAffiliation"
    )

    def get_group_memberships(
        self: "ActorIdentity", *, authoritative_directory: str
    ) -> list[str]:
        """Get group URNs of groups asserted by directory (e.g. login.helmholtz.de)."""

        if self.edu_person_entitlement is None:
            return []

        return [
            urn[: -(len(authoritative_directory) + 1)]
            for urn in self.edu_person_entitlement
            if urn.endswith(f"#{authoritative_directory}")
        ]


async def get_group_urns() -> list[str]:
    """Return all group URNs."""

    async with core.db.DB.get_connection_with_transaction() as connection:
        dictionary_rows = await connection.cursor(
            row_factory=psycopg.rows.dict_row
        ).execute("SELECT urn FROM identity_group")

        result_rows = await dictionary_rows.fetchall()

    return [urn_record["urn"] for urn_record in result_rows]


async def import_group_urns(urns: list[str]) -> None:
    """Replace all group URNs in the database with the list"""

    # Deduplicate list
    deduplicated_urns = list(set(urns))
    deduplicated_urns.sort()

    # Validate URNs
    for urn in deduplicated_urns:
        if urn is None or not re.match(GROUP_URN_PATTERN, urn):
            message = "Stopping import! Invalid urn."
            raise RuntimeError(message)

    # Overwrite DB table
    async with core.db.DB.get_connection_with_transaction() as connection:
        if len(deduplicated_urns) > 0:
            # First import, then delete those no longer present
            await connection.cursor().executemany(
                "INSERT INTO identity_group (urn) VALUES (%(urn)s) ON CONFLICT (urn) DO NOTHING;",
                [{"urn": urn} for urn in urns],
            )

            await connection.execute(
                "DELETE FROM identity_group WHERE NOT urn = ANY(%(deduplicated_urns)s);",
                {"deduplicated_urns": deduplicated_urns},
            )
        else:
            # If there are no URNs, delete everything
            await connection.execute("DELETE FROM identity_group;")
