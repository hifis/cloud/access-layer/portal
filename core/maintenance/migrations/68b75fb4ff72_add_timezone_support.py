"""
add_timezone_support
"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "68b75fb4ff72"
down_revision = "2f6711ae4e5a"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column(
        "maintenance_message",
        "show_until",
        existing_type=sa.DateTime(timezone=False),
        type_=sa.DateTime(timezone=True),
    )


def downgrade() -> None:
    op.alter_column(
        "maintenance_message",
        "show_until",
        existing_type=sa.DateTime(timezone=True),
        type_=sa.DateTime(timezone=False),
    )
