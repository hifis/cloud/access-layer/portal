import { type Page, expect, test } from "@playwright/test";
import config from "../playwright.config";

// Base testing profile, so random profiles do not accumulate at HIDaH
export const TEST_PROFILE = {
    sub: "28a0c7b1-74fe-4158-a7a0-a3bef18571ee",
    name: "Bobby Tables",

    // https://github.com/biomejs/biome/discussions/187
    // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
    given_name: "Bobby",

    // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
    family_name: "Tables",

    // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
    preferred_username: "bobby.tables",
    email: "bobby.tables@localhost",

    // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
    email_verified: "bobby.tables@localhost",

    // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
    eduperson_entitlement: ["urn:geant:localhost:group:hidah:falcon#hidah"],

    // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
    eduperson_assurance: ["https://localhost/assurance/lychee"],

    // biome-ignore lint/style/useNamingConvention: Attribute names are defined by the standard
    eduperson_scoped_affiliation: ["staff@hidah"],
};

// login procedure fixture
export const loggedInTest = test.extend<{ page: Page }>({
    page: async ({ page }, use) => {
        // Go to landing page
        await page.goto("/");

        // Find the login button
        await page.locator("nav").getByText("Sign in").click();

        // Sign into HIDaH
        await page.getByTestId("generated-identity-claims").fill(JSON.stringify(TEST_PROFILE, undefined, 2));
        await page.getByTestId("generated-identity-sign-in-button").click();

        // Check that we are back in the CP with the logout button visible
        await page.waitForURL(`${config.use?.baseURL}/services/`);
        await expect(page.locator("i.fa-caret-down")).toBeVisible();
        await use(page);
    },
});
