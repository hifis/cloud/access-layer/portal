"""Module for downloading KPIs."""

from uuid import UUID

import httpx
from pydantic import BaseModel, TypeAdapter


class KPIs(BaseModel):
    user_count: int


KPIMap = dict[UUID, KPIs]
KPIMapAdapter = TypeAdapter(KPIMap)


KPI_JSON_URL = "https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/user_count.json?job=plot_general"


class KPIDownloadError(Exception):
    pass


class Client:
    """An async API client for downloading KPIs"""

    def __init__(
        self: "Client",
    ) -> None:
        self.client = httpx.AsyncClient(
            follow_redirects=True,
            headers={
                "user-agent": "Cloud Portal (helmholtz.cloud)",
                "accept": "application/json",
            },
        )

    async def aclose(self: "Client") -> None:
        """Use this method to close the client once you're done."""
        await self.client.aclose()

    async def get_kpis(self: "Client") -> KPIMap:
        """Get latest KPIs."""

        kpi_response = await self.client.get(KPI_JSON_URL)

        if kpi_response.status_code != httpx.codes.OK:
            msg = f"KPI download returned staus code {kpi_response.status_code}."
            raise KPIDownloadError(msg)

        kpi_map: KPIMap = KPIMapAdapter.validate_json(kpi_response.text)

        return kpi_map
