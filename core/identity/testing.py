from uuid import uuid4

import core.identity


def make_actor_identity() -> core.identity.ActorIdentity:
    """Generates an actor identity for unit/integration testing purposes."""

    return core.identity.ActorIdentity(
        vo_person_id=uuid4(),
        edu_person_entitlement=[],
        edu_person_assurance=[],
        edu_person_scoped_affiliation=[],
    )
