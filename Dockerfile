FROM registry.hub.docker.com/denoland/deno:alpine-2.1.7@sha256:5bffd788568586570c570cf3dc20d2eb3ca187c2b16c8debcbdad82ab7d47806 as deno

RUN apk -U --no-cache upgrade
RUN mkdir /portal
WORKDIR /portal

COPY web /portal/web
COPY package.json deno.lock tailwind.config.js tsconfig.json vite.config.js /portal/

RUN deno install
RUN deno run build

FROM registry.hub.docker.com/library/python:3.13.2-slim-bookworm@sha256:f3614d98f38b0525d670f287b0474385952e28eb43016655dd003d0e28cf8652 as uv

# Update
RUN apt-get update && apt-get -y dist-upgrade

# Install uv for Python
RUN pip install uv

RUN mkdir /portal
WORKDIR /portal

COPY uv.lock pyproject.toml /portal/

RUN uv sync --no-dev --locked

FROM registry.hub.docker.com/library/python:3.13.2-slim-bookworm@sha256:f3614d98f38b0525d670f287b0474385952e28eb43016655dd003d0e28cf8652

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -y dist-upgrade

RUN mkdir /portal
WORKDIR /portal
COPY core /portal/core
COPY testing /portal/testing
COPY mock_data /portal/mock_data
COPY --from=deno /portal/web /portal/web
COPY --from=uv /portal/.venv /portal/.venv
COPY manage.py portalctl.py alembic.ini /portal/

RUN useradd -r portal -u 1001
USER 1001

ENV PATH "/portal/.venv/bin:$PATH"
