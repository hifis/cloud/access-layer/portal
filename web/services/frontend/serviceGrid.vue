<script lang="ts">
import Modal from "../../base/frontend/modal.vue";
import Multiselect from "./multiselect.vue";

import { Switch } from "@headlessui/vue";
import MiniSearch from "minisearch";

// Data structures received from the view
interface CatalogData {
    services: Record<string, ServiceData>;
    providers: Record<string, Provider>;
    categories: string[];
    software: string[];
}

interface ServiceData {
    profile: Service;
}

interface Service {
    id: string;
    title: string;
    category: string;
    isAvailable: boolean;
    generalShortTextCloudPortal: string;
    generalSoftwareName: string;
    catalogProviderID: string;
    generalLinkServiceUsage: string | null;
    userCount: number;
    searchKeywords: string;
    searchText: string;
}

interface Provider {
    id: string;
    title: string;
    abbreviation: string;
}

const DEFAULT_TITLE = "Services - Helmholtz Cloud";
const DEFAULT_DESCRIPTION =
    "Cloud for science: collaborate on documents, train AI models, organize your group's work, publish your data. Helmholtz Cloud services are available to everyone at Helmholtz and their collaboration partners.";

// biome-ignore lint/style/noDefaultExport: Vue Components use default exports
export default {
    head() {
        return {
            title: this.metaTitleOverride ? this.metaTitleOverride : DEFAULT_TITLE,
            meta: [
                {
                    name: "description",
                    content: this.metaDescriptionOverride ? this.metaDescriptionOverride : DEFAULT_DESCRIPTION,
                },
            ],
        };
    },
    data() {
        // Load service information from data attribute
        const gridComponent = document.getElementById("service-grid");
        if (gridComponent === null) {
            throw new Error("Could not find service-grid element in DOM! Ensure it is present before trying to mount the component.");
        }

        const serviceJSON = gridComponent.dataset.services;
        if (serviceJSON === undefined) {
            throw new Error("Service grid data attribute is missing!");
        }

        const currentPath = new URL(window.location.href);

        const catalogData = JSON.parse(serviceJSON) as CatalogData;

        // Build search index
        const searchIndex = new MiniSearch({
            fields: ["title", "category", "generalShortTextCloudPortal", "generalSoftwareName", "searchKeywords", "searchProvider", "searchText"],

            // Return ID only
            storeFields: [],

            searchOptions: {
                // Boost important fields
                boost: {
                    title: 5,
                    generalSoftwareName: 5,
                    category: 3,
                    searchKeywords: 3,
                    searchProvider: 3,
                    generalShortTextCloudPortal: 2,
                },

                // Fuzzy (allow typos) and prefix (while typing)
                fuzzy: 0.15,
                prefix: true,
            },
        });

        searchIndex.addAll(
            Object.keys(catalogData.services).map((id, _index) => {
                return catalogData.services[id].profile;
            }),
        );

        // Sorted items for select boxes
        const catalogSortedCategories = [...catalogData.categories].sort((a, b) => a.localeCompare(b));
        const catalogSortedSoftware = [...catalogData.software].sort((a, b) => a.localeCompare(b));

        const catalogSortedProviders = Object.values(catalogData.providers);
        catalogSortedProviders.sort((a, b) => a.abbreviation.localeCompare(b.abbreviation));

        // Get filter data from URL and validate
        let filterCategory = decodeURIComponent(currentPath.searchParams.get("filterCategory") || "");

        // Empty filter if that category or software does not exist in the list (anymore, e.g. outdated link)
        filterCategory = catalogSortedCategories.includes(filterCategory) ? filterCategory : "";

        let filterProviderID = decodeURIComponent(currentPath.searchParams.get("filterProviderID") || "");

        // Reject invalid provider IDs
        filterProviderID = Object.keys(catalogData.providers).includes(filterProviderID) ? filterProviderID : "";

        // Get sorting setting from URL and validate
        let sortByAttribute = decodeURIComponent(currentPath.searchParams.get("sortByAttribute") || "");

        // Empty setting if invalid
        sortByAttribute = ["title", "userCount"].includes(sortByAttribute) ? sortByAttribute : "userCount";

        return {
            // Base data from data attribute
            catalogData,
            catalogSortedCategories,
            catalogSortedSoftware,
            catalogSortedProviders,

            // Fulltext search index
            searchIndex,

            // Filter state
            // Empty string / list means no filter applied
            filterSearchInput: decodeURIComponent(currentPath.searchParams.get("filterSearchInput") || ""),
            filterCategory,
            filterProviderID,
            sortByAttribute,

            // Service profile modal state
            serviceModalSelectedServiceID: currentPath.searchParams.get("serviceID"),
            serviceModalVisible: false,
            serviceModalLoading: false,
            serviceModalContent: "",

            // Page metadata, set to non-null value if you need to override these (e.g. for SEO when opening a modal)
            metaTitleOverride: null as string | null,
            metaDescriptionOverride: null as string | null,
        };
    },
    computed: {
        // biome-ignore lint/complexity/noExcessiveCognitiveComplexity: breaking this out into multiple functions would not make it simple
        filteredAndSortedServices() {
            // Filter and sort search services
            let services: ServiceData[] = [];

            for (const [_serviceID, service] of Object.entries(this.catalogData.services)) {
                if (this.filterProviderID !== "" && service.profile.catalogProviderID !== this.filterProviderID) {
                    continue;
                }

                if (this.filterCategory !== "" && service.profile.category !== this.filterCategory) {
                    continue;
                }

                services.push(service);
            }

            if (this.filterSearchInput !== "") {
                // Sort by search rank
                const searchResult = this.searchIndex.search(this.filterSearchInput);

                const sortedServices: ServiceData[] = [];

                for (const result of searchResult) {
                    const service = services.find((service) => service.profile.id === result.id);

                    if (service !== undefined) {
                        sortedServices.push(service);
                    }
                }

                services = sortedServices;
            } else {
                // Sort by software sortByAttribute
                services.sort((a, b) => a.profile.generalSoftwareName.localeCompare(b.profile.generalSoftwareName));

                if (this.sortByAttribute === "userCount") {
                    services.sort((a, b) => b.profile.userCount - a.profile.userCount);
                }
            }

            return services;
        },
        anyFiltersPresent() {
            return this.filterCategory !== "" || this.filterProviderID !== "" || this.filterSearchInput.length > 0;
        },
    },
    watch: {
        serviceModalSelectedServiceID(_newID, _oldID) {
            // Watch for changes as this parameter can be loaded from URL
            this.syncStateToURL();
        },
        serviceModalVisible(newState, _oldState) {
            // Update local state in case it was updated by the modal
            if (newState === false) {
                this.closeServiceModal();
            }
        },
        filterProviderID(_newID, _oldID) {
            // Watch for changes as this parameter can be loaded from URL
            this.syncStateToURL();
        },
        filterCategory(_newCategory, _oldCategory) {
            // Watch for changes as this parameter can be loaded from URL
            this.syncStateToURL();
        },
        filterSearchInput(_newInput, _oldInput) {
            // Watch for changes as this parameter can be loaded from URL
            this.syncStateToURL();
        },
        sortByAttribute(_newAttribute, _oldAttribute) {
            // Watch for changes as this parameter can be loaded from URL
            this.syncStateToURL();
        },
        sortByUserCount(newEnabledStatus, _oldEnabledStatus) {
            // Watch for changes as this parameter can be loaded from URL

            if (newEnabledStatus) {
                this.sortByAttribute = "";
            } else {
                this.sortByAttribute = "title";
            }
        },
    },
    async mounted() {
        // Load state from query parameters on load
        await this.openServiceModalIfServiceInURL();

        // Sync the state to URL in case there was invalid data to clear the broken IDs
        this.syncStateToURL();
    },
    methods: {
        async openServiceModal(serviceID: string) {
            // Update URL
            this.serviceModalSelectedServiceID = serviceID;

            // Show modal visually
            this.serviceModalVisible = true;

            // Set metadata
            const service = this.catalogData.services[serviceID];

            if (service.profile.title !== service.profile.generalSoftwareName) {
                this.metaTitleOverride = `${service.profile.generalSoftwareName} (${service.profile.title}) - ${DEFAULT_TITLE}`;
            } else {
                this.metaTitleOverride = `${service.profile.generalSoftwareName} - ${DEFAULT_TITLE}`;
            }

            this.metaDescriptionOverride = service.profile.generalShortTextCloudPortal;

            // Download partial, show loader while downloading
            this.serviceModalLoading = true;
            this.serviceModalContent = await (await fetch(`/services/${serviceID}/_profile/`)).text();
            this.serviceModalLoading = false;
        },
        closeServiceModal() {
            // Update URL
            this.serviceModalSelectedServiceID = null;

            // Hide modal visually
            this.serviceModalVisible = false;

            // Reset content
            this.serviceModalContent = "";

            // Reset meta overrides
            this.metaTitleOverride = null;
            this.metaDescriptionOverride = null;
        },
        async openServiceModalIfServiceInURL() {
            // Open modal if ID in URL, must be done on mounted as `this`
            // is not available yet when data() is run.

            if (this.serviceModalSelectedServiceID !== null) {
                await this.openServiceModal(this.serviceModalSelectedServiceID);
            }
        },
        syncStateToURL() {
            // Sync state into query parameters
            const updatedURL = new URL(window.location.href);

            // Clear all search params
            updatedURL.search = "";

            if (this.serviceModalSelectedServiceID !== null) {
                updatedURL.searchParams.set("serviceID", this.serviceModalSelectedServiceID);
            }

            if (this.filterProviderID !== "") {
                updatedURL.searchParams.set("filterProviderID", encodeURIComponent(this.filterProviderID));
            }

            if (this.filterCategory !== "") {
                updatedURL.searchParams.set("filterCategory", encodeURIComponent(this.filterCategory));
            }

            if (this.filterSearchInput !== "") {
                updatedURL.searchParams.set("filterSearchInput", encodeURIComponent(this.filterSearchInput));
            }

            if (this.sortByAttribute !== "") {
                updatedURL.searchParams.set("sortByAttribute", encodeURIComponent(this.sortByAttribute));
            }

            updatedURL.searchParams.sort();

            window.history.replaceState({}, "", updatedURL);
        },
        toggleFilterProviderID(providerID: string) {
            if (this.filterProviderID === providerID) {
                this.filterProviderID = "";
            } else {
                this.filterProviderID = providerID;
            }
        },
        toggleFilterCategory(category: string) {
            if (this.filterCategory === category) {
                this.filterCategory = "";
            } else {
                this.filterCategory = category;
            }
        },
        resetAllFilters() {
            this.filterProviderID = "";
            this.filterSearchInput = "";
            this.filterCategory = "";
        },
    },
    components: { Multiselect, Modal, Switch },
};
</script>

<template>
    <section class="container mx-auto flex flex-col xl:flex-row items-start content-start">
        <section role="search" class="flex flex-col flex-grow-0 xl:w-1/5 p-4 pt-6 xl:pt-12 xl:mr-4">
            <div class="flex flex-col">
                <span class="mb-2 font-medium">
                    <i title="Search Icon" class="fa-solid text-sm text-slate-500 fa-magnifying-glass"></i>
                    Text
                </span>
                <div class="flex w-full">
                    <input type="text" v-model="filterSearchInput" role="searchbox" placeholder="Search services..."
                        class="mb-2 p-3" />
                </div>
                <span class="mt-4 mb-2 font-medium">
                    <i title="Category Icon" class="fa-solid text-sm text-slate-500 fa-tag"></i>
                    Category
                </span>
                <div class="flex flex-row flex-wrap w-full">
                    <span v-for="category in catalogSortedCategories"
                        class="cursor-pointer select-none inline-block whitespace-nowrap rounded-sm px-2 py-[2px] my-1 mr-1 max-w-full"
                        tabindex="0" @click.stop.prevent="toggleFilterCategory(category)"
                        v-on:keydown.enter.stop.prevent="toggleFilterCategory(category)" :class="{
                            'bg-slate-300': filterCategory === category,
                            'bg-slate-50 hover:bg-slate-300': filterCategory !== category
                        }">
                        {{ category }}
                    </span>
                </div>
                <span class="mt-4 xl:mt-6 mb-2 font-medium">
                    <i title="Category Icon" class="fa-solid text-sm text-slate-500 fa-building"></i>
                    Provider
                </span>
                <div class="flex flex-row flex-wrap w-full">
                    <span v-for="provider in catalogSortedProviders"
                        class="cursor-pointer select-none inline-block whitespace-nowrap rounded-sm px-2 py-[2px] my-1 mr-1 max-w-full"
                        tabindex="0" @click.stop.prevent="toggleFilterProviderID(provider.id)"
                        v-on:keydown.enter.stop.prevent="toggleFilterProviderID(provider.id)" :class="{
                            'bg-slate-300': filterProviderID === provider.id,
                            'bg-slate-50 hover:bg-slate-300': filterProviderID !== provider.id
                        }" :title="provider.title">
                        {{ provider.abbreviation }}
                    </span>
                </div>
            </div>
            <div class="flex flex-row pt-8 max-xl:pt-10 relative" v-if="anyFiltersPresent">
                <button class="w-full whitespace-nowrap" @click="resetAllFilters()" @keypress.enter="resetAllFilters()">
                    <hifis-button variant="outline">Reset Filters</hifis-button>
                </button>
            </div>
        </section>
        <section class="flex flex-col w-full xl:w-4/5">
            <div class="flex flex-row align-items-baseline justify-end mb-4 max-xl:py-4">
                <div class="flex flex-row lg:ml-auto" v-if="filterSearchInput === ''">
                    <div class="flex flex-row w-full relative top-bar-dropdown">
                        <select
                            class="border-0 m-0 p-2 pr-8 mt-[-0.3rem] max-xl:mt-1 text-ellipsis bg-transparent shadow-none"
                            v-model="sortByAttribute" aria-label="Service Sorting">
                            <option value="userCount">Sort by Popularity</option>
                            <option value="title">Sort by Name</option>
                        </select>
                    </div>
                </div>
                <div class="flex flex-row lg:ml-auto" v-if="filterSearchInput !== ''">
                    <div class="pl-2 flex flex-row w-full relative text-slate-500">
                        <select
                            class="border-0 m-0 p-0 mt-[-0.3rem] max-xl:mt-1 text-ellipsis bg-transparent shadow-none"
                            aria-label="Service Sorting"
                            title="When using text search results are ordered by relevance." disabled>
                            <option value="software">Sort by Relevance</option>
                        </select>
                    </div>
                </div>
            </div>
            <div v-if="filteredAndSortedServices.length !== 0"
                class="grid 2xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-4">
                <a v-for="(service, _serviceID) in filteredAndSortedServices " :key="service.profile.id"
                    :href="`/services/?serviceID=${encodeURIComponent(service.profile.id)}`"
                    class="flex flex-col justify-between bg-white shadow border rounded-md motion-safe:transition-all cursor-pointer hover:shadow-xl focus:shadow-xl no-underline font-normal"
                    :aria-labelledby="`service-title-${service.profile.id}`" tabindex="0"
                    @click.prevent="openServiceModal(service.profile.id)"
                    v-on:keydown.enter.prevent="openServiceModal(service.profile.id)">
                    <div class="flex flex-col flex-grow text-center justify-between">
                        <div class="relative pt-2 px-6 pb-10">
                            <div v-if="!service.profile.isAvailable" class="absolute top-4 right-4 text-brightorange">
                                <i title="Unable to reach the service. It may be offline."
                                    class="text-xl fa-solid fa-triangle-exclamation"></i>
                            </div>
                            <div class="h-[100px] flex flex-col items-center justify-center">
                                <img class="max-w-[60px] max-h-[40px] inline-block m-0"
                                    :alt="`Logo ${service.profile.title}`"
                                    :src="`/services/${service.profile.id}/logo/`" />
                            </div>
                            <div class="text-primary min-h-[75px] pb-2">
                                <span class="cursor-pointer line-clamp-2 text-xl font-bold"
                                    :id="`service-title-${service.profile.id}`">{{ service.profile.generalSoftwareName
								}}<br></span>
                                <span v-if="service.profile.title !== service.profile.generalSoftwareName"
                                    class="cursor-pointer line-clamp-2 font-light">{{ service.profile.title }}<br></span>
                            </div>
                            <p class="text-sm my-0 line-clamp-3">
                                {{ service.profile.generalShortTextCloudPortal }}
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div v-if="anyFiltersPresent && filteredAndSortedServices.length === 0"
                class="flex flex-col place-self-stretch items-center justify-center">
                <span class="text-lg xl:text-2xl max-xl:pt-6">There are no services matching these filters.</span>
            </div>
        </section>
        <Modal v-model:visible="serviceModalVisible" :content="serviceModalContent" :loading="serviceModalLoading" />
    </section>
</template>

<style scoped>
.top-bar-dropdown::after {
    position: absolute;
    top: 6px;
    right: 12px;
    font: var(--fa-font-solid);
    content: "\f0d7";
}
</style>
