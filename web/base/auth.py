import datetime
import json
from collections.abc import Callable
from typing import Any
from uuid import UUID, uuid4

import django.contrib.auth
from cryptography.fernet import Fernet, InvalidToken
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import SuspiciousOperation
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from mozilla_django_oidc.views import OIDCAuthenticationCallbackView
from pydantic import ValidationError
from sentry_sdk import capture_exception

from .models import AAIProfile, PortalToken, User

OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY = "portal_oidc_workaround_redirects"

Claims = dict[str, Any]


class AAIError(Exception):
    pass


# Ignore type error (OIDCAuthenticationCallbackView is not typed)
class PortalErrorRedirectView(OIDCAuthenticationCallbackView):  # type:ignore[misc]
    # Custom view to detect login failures and redirect users once on failure in case of state mismatch
    # https://github.com/mozilla/mozilla-django-oidc/issues/435

    def get(self: "PortalErrorRedirectView", request: HttpRequest) -> HttpResponse:
        # Check if session cookie is there
        if request.COOKIES.get(settings.SESSION_COOKIE_NAME, None) is None:
            return render(
                request,
                "base/error.html",
                {
                    "code": 400,
                    "message": "Login failed. Your Browser did not send the session cookie.",
                    "explanation": "Please check that your browser allows cookies on helmholtz.cloud. Cookies are required for the login process. You can read more about our use of cookies in our privacy policy. If the problem persists, please contact support@hifis.net",
                },
                status=400,
            )

        # Try the regular login
        try:
            response = super().get(request)

            # Everything went fine: remove counter if it exists
            request.session.pop(OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY, None)

        except SuspiciousOperation as error:
            # In normal operation this should not happen at all.
            # However, when users start multiple requests in parallel, the state parameter is not set correctly as it
            # is being reset by the new flow. The user may complete the flow in a now outdated tab. They'd return with
            # an invalid state parameter, then.
            #
            # In that case, redirect the user back to the authentication flow. As they're still signed into their OP,
            # they'll be directly signed in.

            if error.args == (
                "OIDC callback state not found in session `oidc_states`!",
            ):
                # Check how many times we redirected the user already so this does not turn into a loop
                previous_redirects = request.session.get(
                    OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY, 0
                )

                if previous_redirects > 0:
                    # There has been at least one redirect in the past, so raise to prevent login loop
                    raise

                request.session[OIDC_WORKAROUND_REDIRECT_COUNTER_SESSION_KEY] = (
                    previous_redirects + 1
                )

                # Capture exception anyway so we can keep an eye on it
                capture_exception(error)

                return HttpResponseRedirect(reverse("oidc_authentication_init"))
            else:
                raise

        return response


# This is an external base class for which we do not have annotations at the moment.
class PortalOIDCBackend(OIDCAuthenticationBackend):  # type: ignore[misc]
    """
    Custom authentication backend to integrate AAI.
    Also see https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html
    for details on the methods / base class.

    In the future create_user/update_user could also initate some user management
    processes in the core application so the Django part only ever focuses on
    authentication.
    """

    REQUIRED_CLAIMS = (
        "email",
        "name",
        "sub",
        "eduperson_entitlement",
        "eduperson_assurance",
    )

    def create_user(self: "PortalOIDCBackend", claims: Claims) -> User:
        """
        Create a user with a random ID and encrypted AAI profile information.
        The encryption key is handed to the user by the middleware.
        """

        if self.request.user.is_authenticated:
            error_message = (
                "Can't create user: there's already a user for this request."
            )
            raise AAIError(error_message)

        if hasattr(self.request, "aai_middleware_new_user_secret"):
            error_message = (
                "Only one user can be created per request. This should never happen!"
            )
            raise AAIError(error_message)

        # Create user with random ID
        # At this point the new logged-in session ID is not known, however it works when doing this in the middleware.
        # After this function is called users are logged in and they receive a new session.
        user = User(
            id=uuid4(),
            # Fallback expire at regular session length
            fallback_expire_at=(
                timezone.now() + datetime.timedelta(seconds=settings.SESSION_COOKIE_AGE)
            ),
            # The function calling this function may reset the session ID. The final ID is assigned by middleware_after_view.
            session_id=None,
        )

        # Encrypt AAI profile and store the encryption secret in request so middleware can pass it to the user as a cookie.
        aai_profile = self._claims_to_aai_profile(claims)

        self.request.aai_middleware_new_user_secret = user.set_encrypted_aai_profile(
            aai_profile
        )
        user.save()

        return user

    def update_user(self: "PortalOIDCBackend", user: User, claims: Claims) -> User:
        """
        Update the user profile by setting a new encrypted profile and instructing the middleware
        to pass the updated decryption key into the cookie.

        This method gets called when filter_users_by_claims returns a user. It only returns a user
        when the same user re-authenticates. This update function only handles profile updates
        for re-authentication.
        """

        if claims["sub"] != str(self.request.aai_profile.vo_person_id):
            error_message = "Attempted to update user using another user's OIDC claims. This is unexpected."
            raise (AAIError(error_message))

        aai_profile = self._claims_to_aai_profile(claims)

        self.request.aai_profile = aai_profile

        # Set marker so the user gets an updated cookie as the encryption key changes
        # when the profile gets updated
        self.request.aai_middleware_updated_user = True
        self.request.aai_middleware_new_user_secret = user.set_encrypted_aai_profile(
            aai_profile
        )
        user.save()

        return user

    def get_username(self: "PortalOIDCBackend", claims: Claims) -> Any:  # noqa: ANN401
        """Return sub(ject) as the username. This is the voPersonID UUID."""
        return claims["sub"]

    def filter_users_by_claims(self: "PortalOIDCBackend", claims: Claims) -> list[User]:
        """
        Each login creates a new user, there's no way to retrieve them by claims unless the same user re-authenticates.

        CAUTION: When changing this method, remember to cross-check the expected behaviour with other methods in this
        class (e.g. update_user), and the middleware functions.
        """

        if (
            self.request.user.is_authenticated
            and claims is not None
            and "sub" in claims
            and claims["sub"] is not None
            and claims["sub"] == str(self.request.aai_profile.vo_person_id)
        ):
            return [self.request.user]

        return []

    def verify_claims(self: "PortalOIDCBackend", claims: Claims) -> bool:
        """Validate that the required claims are present."""

        if claims is None:
            return False

        profile_claims = set(claims.keys())
        required_claims = set(self.REQUIRED_CLAIMS)

        return profile_claims.issuperset(required_claims)

    @staticmethod
    def _claims_to_aai_profile(claims: Claims) -> AAIProfile:
        """Turn claims into an AAI profile."""

        # Even though it is a multi field in the standard, AAI returns a string instead of a list if there' only a single affiliation.
        # Workaround until
        # https://codebase.helmholtz.cloud/hifis/cloud/access-layer/portal/-/issues/205
        # is fixed in AAI.
        if isinstance(claims["eduperson_scoped_affiliation"], str):
            claims["eduperson_scoped_affiliation"] = [
                claims["eduperson_scoped_affiliation"]
            ]

        if isinstance(claims["eduperson_entitlement"], str):
            claims["eduperson_entitlement"] = [claims["eduperson_entitlement"]]

        if isinstance(claims["eduperson_assurance"], str):
            claims["eduperson_assurance"] = [claims["eduperson_assurance"]]

        return AAIProfile(
            vo_person_id=UUID(claims["sub"]),
            name=claims["name"],
            email=claims["email"],
            eduperson_entitlements=claims["eduperson_entitlement"],
            eduperson_assurances=claims["eduperson_assurance"],
            eduperson_scoped_affiliation=claims["eduperson_scoped_affiliation"],
        )


def aai_session_encryption_middleware(
    get_response: Callable[[HttpRequest], HttpResponse],
) -> Callable[[HttpRequest], HttpResponse]:
    """
    This middleware performs session integrity checks, manages the portal token and assigns users to sessions.
    """

    def run_middleware(request: HttpRequest) -> HttpResponse:
        response: HttpResponse | None = None

        try:
            request = middleware_before_view(request)
            response = get_response(request)

            return middleware_after_view(request, response)
        except AAIError:
            # Something went wrong, logout and delete token cookie
            django.contrib.auth.logout(request)

            # Delete token cookie
            if response is not None:
                response.delete_cookie(settings.PORTAL_TOKEN_COOKIE_NAME)

            messages.add_message(
                request=request,
                message="You were signed out due to an error. Please try signing in again. If the problem persists, please contact support.",
                level=messages.ERROR,
            )

            # Re-raise so it gets caught by some error page
            raise

    return run_middleware


def middleware_before_view(
    request: HttpRequest,
) -> HttpRequest:
    """
    Check the session is OK and decrypt user profile for views etc.
    """

    # Check that the user Id stored in the session and the user ID on the request match
    # Technically this should not ever happen unless someone fiddles with the request. But better be on the safe side here.
    if (
        request.user.is_authenticated
        and request.session.has_key(django.contrib.auth.SESSION_KEY)
        and request.session[django.contrib.auth.SESSION_KEY] != str(request.user.id)
    ):
        django.contrib.auth.logout(request)
        error_message = (
            "Session user and authenticated user mismatch! This should never happen."
        )
        raise AAIError(error_message)

    # When a user is deleted before the session expired, the user ID is still kept around in the session.
    # Technically, this should not be a problem because on the next call to auth.login() django will prevent re-using
    # the session for a different user by flushing the session. Nevertheless, it's a bit weird that it's only cleared
    # when one tried to log in. This middleware flushes the session in case there is no authenticated user and an old entry
    # in the session data.
    if (
        not request.user.is_authenticated
        and request.session.get(django.contrib.auth.SESSION_KEY, None) is not None
    ):
        # There's residual auth information in the session even though the user is logged-out. Clear session.
        django.contrib.auth.logout(request)

    # Load profile and add to request if authenticated
    if request.user.is_authenticated:
        if request.session.session_key is None:
            error_message = "Can't decrypt profile without a session."
            raise AAIError(error_message)

        if request.COOKIES.get(settings.PORTAL_TOKEN_COOKIE_NAME) is None:
            error_message = "Can't decrypt profile without a portal token."
            raise AAIError(error_message)

        # Decrypt portal token
        try:
            portal_token_json = Fernet(settings.PORTAL_TOKEN_ENCRYPTION_SECRET).decrypt(
                request.COOKIES[settings.PORTAL_TOKEN_COOKIE_NAME]
            )
        except InvalidToken as e:
            error_message = "Could not decrypt token cookie"
            raise AAIError(error_message) from e

        portal_token = PortalToken(**json.loads(portal_token_json))

        if portal_token.session_key != request.session.session_key:
            error_message = (
                "Portal token's session ID and request session ID do not match!"
            )
            raise AAIError(error_message)

        if portal_token.session_key != request.user.session_id:
            error_message = "Portal token does not match session linked to the user!"
            raise AAIError(error_message)

        try:
            request.aai_profile = request.user.decrypt_aai_profile(
                portal_token.user_secret
            )
        except InvalidToken as e:
            error_message = "Invalid fernet secret"
            raise AAIError(error_message) from e
        except ValidationError as e:
            error_message = "Invalid encrypted data"
            raise AAIError(error_message) from e
        except RuntimeError as e:
            error_message = "Runtime error"
            raise AAIError(error_message) from e
    else:
        request.aai_profile = None

    return request


def middleware_after_view(request: HttpRequest, response: HttpResponse) -> HttpResponse:
    """Once the view is done, set or clear the portal token cookie, if needed."""

    if (
        not request.user.is_authenticated
        and request.COOKIES.get(settings.PORTAL_TOKEN_COOKIE_NAME) is not None
    ):
        # User is logged out but they still have the portal token
        # Delete portal token
        response.delete_cookie(settings.PORTAL_TOKEN_COOKIE_NAME)

    # True if user was updated (e.g. during AAI OIDC token renewal)
    user_was_updated_as_part_of_request = (
        hasattr(request, "aai_middleware_updated_user")
        and request.aai_middleware_updated_user is True
    )

    if (
        hasattr(request, "aai_middleware_new_user_secret")
        and request.user.is_authenticated
        and (
            request.COOKIES.get(settings.PORTAL_TOKEN_COOKIE_NAME) is None
            or user_was_updated_as_part_of_request
        )
    ):
        # User is authenticated and a new user was created or the profile was updated, hand them the token
        # The final session ID is only known at this point.

        if request.session.session_key is None:
            # This needs to be turned into an exception, otherwise pydantic can't parse this
            # Normally this happens when there's a mismatch between users on logging in
            # This should be prevented by middleware_before_view
            error_message = "Session was flushed upstream."
            raise AAIError(error_message)

        if not user_was_updated_as_part_of_request:
            # Only assign a session ID if there isn't a session already
            number_updated = User.objects.filter(
                id=request.user.id, session_id=None
            ).update(session_id=request.session.session_key)

            # Validate there is no session assigned
            if number_updated != 1:
                error_message = f"Error assigning session ID to user. Number of IDs assigned: {number_updated} User already may already have a session assigned! This should never happen."
                raise AAIError(error_message)

        token = PortalToken(
            session_key=request.session.session_key,
            user_secret=request.aai_middleware_new_user_secret,
        )

        token_json = token.model_dump_json()

        # Combine secret into a tamper-proof portal token tied to the current session
        portal_token_json_bytes = token_json.encode("utf-8")

        encrypted_portal_token = (
            Fernet(settings.PORTAL_TOKEN_ENCRYPTION_SECRET)
            .encrypt(portal_token_json_bytes)
            .decode("utf-8")
        )

        # Pass token to user in a cookie
        response.set_cookie(
            settings.PORTAL_TOKEN_COOKIE_NAME,
            encrypted_portal_token,
            max_age=request.session.get_expiry_age(),
            secure=settings.SESSION_COOKIE_SECURE,
            samesite=settings.SESSION_COOKIE_SAMESITE,
            httponly=settings.SESSION_COOKIE_HTTPONLY,
        )

    return response
