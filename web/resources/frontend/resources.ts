import { createApp } from "vue";

import RequestForm from "./requestForm.vue";
import ResourcePage from "./resourcePage.vue";

if (document.getElementById("resource-page")) {
    createApp(ResourcePage).mount("#resource-page");
}

if (document.getElementById("request-form")) {
    createApp(RequestForm).mount("#request-form");
}
