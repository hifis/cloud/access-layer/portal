/*!
 * The Helmholtz Cloud Portal is open source software. It also uses several open source components.
 *
 * You can find this software's source code and information on its dependencies here:
 * https://gitlab.hzdr.de/hifis/cloud/access-layer/portal
 *
 */

// External dependencies
import "@fortawesome/fontawesome-free/css/fontawesome.css";
import "@fortawesome/fontawesome-free/css/solid.css";

// Import app modules for apps which use TS/JS
import "../frontend/base";
import "../../resources/frontend/resources";
import "../../services/frontend/services";
