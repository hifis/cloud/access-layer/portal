from uuid import uuid4

import pytest

import core.app
import core.catalog
import core.plony
from core.catalog import testing


@pytest.mark.usefixtures("_context_database")
class TestKeyword:
    @staticmethod
    @pytest.mark.asyncio
    async def test_create_or_update_and_delete_keyword() -> None:
        # At the beginning there are no keywords in the database
        keywords = await core.catalog.get_keywords()

        assert len(keywords) == 0

        new_keyword = testing.make_keyword()

        # Create

        await testing.create_or_update_keyword(keyword=new_keyword)

        keywords = await core.catalog.get_keywords()

        assert len(keywords) == 1
        assert keywords[0] == new_keyword

        # Update

        new_keyword.title = "Updated Keyword"

        await testing.create_or_update_keyword(keyword=new_keyword)

        keywords = await core.catalog.get_keywords()

        assert len(keywords) == 1
        assert keywords[0] == new_keyword

        # Delete

        await testing.delete_keyword(keyword_id=new_keyword.id)
        keywords = await core.catalog.get_keywords()

        assert len(keywords) == 0

        with pytest.raises(RuntimeError, match="Keyword could not be deleted!"):
            await testing.delete_keyword(keyword_id=uuid4())


@pytest.mark.usefixtures("_context_database")
class TestProvider:
    @staticmethod
    @pytest.mark.asyncio
    async def test_create_or_update_and_delete_provider() -> None:
        # At the beginning there are no providers in the database
        providers = await core.catalog.get_providers()

        assert len(providers) == 0

        new_provider = testing.make_provider()

        # Create

        await testing.create_or_update_provider(provider=new_provider)

        providers = await core.catalog.get_providers()

        assert len(providers) == 1
        assert providers[0] == new_provider

        # Update

        new_provider.title = "Updated provider"

        await testing.create_or_update_provider(provider=new_provider)

        providers = await core.catalog.get_providers()

        assert len(providers) == 1
        assert providers[0] == new_provider

        # Delete

        await testing.delete_provider(provider_id=new_provider.id)
        providers = await core.catalog.get_providers()

        assert len(providers) == 0

        with pytest.raises(RuntimeError, match="Provider could not be deleted!"):
            await testing.delete_provider(provider_id=uuid4())


@pytest.mark.usefixtures("_context_database")
class TestResourceType:
    @staticmethod
    @pytest.mark.asyncio
    async def test_create_or_update_and_delete_resource_type() -> None:
        # At the beginning there are no resource_types in the database
        resource_types = await core.catalog.get_resource_types()

        assert len(resource_types) == 0

        new_resource_type = testing.make_resource_type()

        # Create

        await testing.create_or_update_resource_type(resource_type=new_resource_type)

        resource_types = await core.catalog.get_resource_types()

        assert len(resource_types) == 1
        assert resource_types[0] == new_resource_type

        # Update

        new_resource_type.name = "Updated resource_type"

        await testing.create_or_update_resource_type(resource_type=new_resource_type)

        resource_types = await core.catalog.get_resource_types()

        assert len(resource_types) == 1
        assert resource_types[0] == new_resource_type

        # Delete

        await testing.delete_resource_type(resource_type_id=new_resource_type.id)
        resource_types = await core.catalog.get_resource_types()

        assert len(resource_types) == 0

        with pytest.raises(RuntimeError, match="ResourceType could not be deleted!"):
            await testing.delete_resource_type(resource_type_id=uuid4())


@pytest.mark.usefixtures("_context_database")
class TestQuota:
    @staticmethod
    @pytest.mark.asyncio
    async def test_create_or_update_quota() -> None:
        # At the beginning there are no quotas in the database
        quotas = await core.catalog.get_quotas()

        assert len(quotas) == 0

        provider = testing.make_provider()
        await testing.create_or_update_provider(provider=provider)

        service = testing.make_service(provider_id=provider.id)
        await testing.create_or_update_service(service=service)

        resource_type = testing.make_resource_type()
        await testing.create_or_update_resource_type(resource_type=resource_type)

        # Create

        new_quota = testing.make_quota(
            service_id=service.id, resource_type_id=resource_type.id
        )
        await testing.create_or_update_quota(quota=new_quota)

        quotas = await core.catalog.get_quotas()

        assert len(quotas) == 1
        assert quotas[0] == new_quota

        # Update

        new_quota.name = "Updated quota"

        await testing.create_or_update_quota(quota=new_quota)

        quotas = await core.catalog.get_quotas()

        assert len(quotas) == 1
        assert quotas[0] == new_quota


@pytest.mark.usefixtures("_context_database")
class TestPolicy:
    @staticmethod
    @pytest.mark.asyncio
    async def test_create_or_update_and_delete_policy() -> None:
        # At the beginning there are no policies in the database
        policies = await core.catalog.get_policies()

        assert len(policies) == 0

        provider = testing.make_provider()
        await testing.create_or_update_provider(provider=provider)

        service = testing.make_service(provider_id=provider.id)
        await testing.create_or_update_service(service=service)

        resource_type = testing.make_resource_type()
        await testing.create_or_update_resource_type(resource_type=resource_type)

        quota = testing.make_quota(
            service_id=service.id, resource_type_id=resource_type.id
        )
        await testing.create_or_update_quota(quota=quota)

        # Create

        new_policy = testing.make_policy(quota_id=quota.id)
        await testing.create_or_update_policy(policy=new_policy)

        policies = await core.catalog.get_policies()

        assert len(policies) == 1
        assert policies[0] == new_policy

        # Update

        new_policy.name = "Updated policy"

        await testing.create_or_update_policy(policy=new_policy)

        policies = await core.catalog.get_policies()

        assert len(policies) == 1
        assert policies[0] == new_policy

        # Delete

        await testing.delete_policy(policy_id=new_policy.id)
        policies = await core.catalog.get_policies()

        assert len(policies) == 0

        with pytest.raises(RuntimeError, match="Policy could not be deleted!"):
            await testing.delete_policy(policy_id=uuid4())


@pytest.mark.usefixtures("_context_database")
class TestService:
    @staticmethod
    @pytest.mark.asyncio
    async def test_create_or_update_and_delete_service() -> None:
        # At the beginning there are no services in the database
        services = await core.catalog.get_services()

        assert len(services) == 0

        provider = testing.make_provider()
        await testing.create_or_update_provider(provider=provider)

        # Create

        new_service = testing.make_service(provider_id=provider.id)
        await testing.create_or_update_service(service=new_service)

        services = await core.catalog.get_services()

        assert len(services) == 1
        assert services[0] == new_service

        # Update

        new_service.title = "Updated service"

        await testing.create_or_update_service(service=new_service)

        services = await core.catalog.get_services()

        assert len(services) == 1
        assert services[0] == new_service

        # Delete

        await testing.delete_service(service_id=new_service.id)
        services = await core.catalog.get_services()

        assert len(services) == 0

        with pytest.raises(RuntimeError, match="Service could not be deleted!"):
            await testing.delete_service(service_id=uuid4())
