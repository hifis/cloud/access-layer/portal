import http
import json
from uuid import UUID

import httpx
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.safestring import mark_safe
from httpx import HTTPError
from pydantic import BaseModel
from sentry_sdk import capture_exception

from core.catalog import get_provider, get_service


class HelpdeskArticle(BaseModel):
    subject: str
    body: str
    type: str
    internal: bool


class HelpdeskTicket(BaseModel):
    title: str
    group: str
    customer: str
    article: HelpdeskArticle


async def open_support_ticket(request: HttpRequest) -> HttpResponse:
    if request.method == "GET":
        return render(
            request,
            "helpdesk/helpdesk.html",
        )
    elif request.method == "POST":
        request_data = request.POST
        message = request_data["message"]
        email = (
            "contact@hifis.net"
            if request_data["email"] == ""
            else request_data["email"]
        )

        article = HelpdeskArticle(
            subject="Portal Feedback",
            body=message,
            type=settings.HELPDESK_TYPE,
            internal=False,
        )

        ticket = HelpdeskTicket(
            title="Portal Feedback",
            group=settings.HELPDESK_FEEDBACK_GROUP,
            customer=email,
            article=article,
        )

        endpoint = settings.HELPDESK_ENDPOINT
        path = settings.HELPDESK_PATH

        try:
            async with httpx.AsyncClient() as async_client:
                await async_client.post(
                    endpoint + path,
                    json=ticket.model_dump(),
                    headers={
                        "Content-type": "application/json",
                        "Authorization": settings.HELPDESK_TOKEN,
                    },
                )
        except HTTPError as e:
            capture_exception(e)

            error_message = 'There was a problem submitting your feedback. Please contact&nbsp;<a class="text-white" href="mailto:support@hifis.net?subject=Error%20sending%20feedback%20from%20cloud%20portal">HIFIS support</a>.'
            messages.add_message(
                request,
                messages.ERROR,
                mark_safe(error_message),  # nosec B308, B703
            )
            return HttpResponseRedirect(reverse("services"))

        messages.add_message(request, messages.SUCCESS, "Thank you for your feedback.")
        return HttpResponseRedirect(reverse("services"))
    else:
        response = HttpResponse("Not Implemented")
        response.status_code = 405
        return response


@login_required  # type: ignore[misc]
async def confirm(request: HttpRequest, service_id: UUID) -> HttpResponse:
    service = await get_service(service_id)
    if service is None:
        error_message = "Service not found."
        raise Http404(error_message)

    provider = await get_provider(service.catalog_provider_id)
    if provider is None:
        error_message = "Provider not found."
        raise Http404(error_message)

    if request.method == "GET":
        return render(
            request,
            "deprovisioning/confirm.html",
            {
                "service": service,
                "provider": provider.title,
            },
        )
    elif request.method == "POST":
        body = f"{settings.HELPDESK_DEPROV_MESSAGE}\nName: {request.aai_profile.name}\nSub: {request.aai_profile.vo_person_id}\nEmail: {request.aai_profile.email}\nService: {service.title}\nProvider: {provider.title}"

        article = HelpdeskArticle(
            subject="User Deprovisioning",
            body=body,
            type=settings.HELPDESK_TYPE,
            internal=False,
        )

        ticket = HelpdeskTicket(
            title=f"{settings.HELPDESK_DEPROV_TITLE} {request.aai_profile.name}",
            group=settings.HELPDESK_DEPROV_GROUP,
            customer=request.aai_profile.email,
            article=article,
        )

        endpoint = settings.HELPDESK_ENDPOINT
        path = settings.HELPDESK_PATH

        try:
            async with httpx.AsyncClient() as async_client:
                r = await async_client.post(
                    endpoint + path,
                    json=ticket.model_dump(),
                    headers={
                        "Content-type": "application/json",
                        "Authorization": settings.HELPDESK_TOKEN,
                    },
                )
                if r.status_code != http.HTTPStatus.CREATED:
                    error_message = 'There was a problem submitting your request. Please contact&nbsp;<a class="text-white" href="mailto:support@hifis.net?subject=Error%20sending%20deprovisioning%20request%20from%20cloud%20portal">HIFIS support</a>.'
                    messages.add_message(
                        request,
                        messages.ERROR,
                        mark_safe(error_message),  # nosec B308, B703
                    )
                else:
                    response = json.loads(r.text)
                    support_link = (
                        '<a class="text-white" href="https://support.hifis.net/#ticket/zoom/'
                        + str(int(response["id"]))
                        + '">here</a>.'
                    )
                    success_message = (
                        "Your request has been submitted. You can check it&nbsp;"
                        + support_link
                    )
                    messages.add_message(
                        request,
                        messages.SUCCESS,
                        mark_safe(success_message),  # nosec B308, B703
                    )
        except HTTPError as e:
            capture_exception(e)

            error_message = 'There was a problem submitting your request. Please contact&nbsp;<a class="text-white" href="mailto:support@hifis.net?subject=Error%20sending%20deprovisioning%20request%20from%20cloud%20portal">HIFIS support</a>.'

            messages.add_message(
                request,
                messages.ERROR,
                mark_safe(error_message),  # nosec B308, B703
            )
            return HttpResponseRedirect(reverse("services"))

        return HttpResponseRedirect(reverse("services"))
    else:
        response = HttpResponse("Not Implemented")
        response.status_code = 405
        return response
